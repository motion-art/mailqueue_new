#!/bin/bash

DIRNAME=$( dirname "$0" )
LOG=$1

shift

FILES=$*

rhino "$DIRNAME/csslint-rhino.js --format=checkstyle-xml $FILES" > $LOG

exit $?
