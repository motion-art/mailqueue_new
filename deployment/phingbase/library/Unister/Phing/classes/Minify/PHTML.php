<?php
/**
 * File for Minify_PHTML
 *
 * @category   Phing
 * @package    Phing_Classes
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id:$
 */

require_once 'Minify/HTML.php';

/**
 * Minify PHTML - per Tokenizer
 *
 * @category   Phing
 * @package    Phing_Classes
 * @copyright  Copyright (c) 2010, Unister GmbH
 */
class Minify_PHTML
{
    /**
     * Minify
     *
     * @param string $content
     * @return string
     */
    public static function minify($content)
    {
        $minify = new self();
        return $minify->minifyPhtml($content);
    }

    /**
     * Minifiziert die übergebene Zeichenkette
     *
     * @param string $phtml
     * @return string
     */
    public function minifyPhtml($phtml)
    {
        $id = 0;
        $result = '';
        $extractedPHP = array();
        $isCode = false;
        $tokens = token_get_all($phtml);

        foreach ($tokens as $token) {
            if (is_array($token) && ($token[0] == T_OPEN_TAG || $token[0] == T_OPEN_TAG_WITH_ECHO)) {
                $id = uniqid();
                $extractedPHP[$id] = $token[1];
                $isCode = true;
            } elseif (is_array($token) && $token[0] == T_CLOSE_TAG) {
                $extractedPHP[$id] .= $token[1];
                $result .= $id;
                $isCode = false;
            } elseif ($isCode) {
                $extractedPHP[$id] .= $this->_getTextByToken($token);
            } else {
                $result .= $this->_getTextByToken($token);
            }
        }
        if ($isCode) {
            $result .= $id;
        }

        $result = Minify_HTML::minify($result);
        if (count($extractedPHP) > 0) {
            $result = str_replace(array_keys($extractedPHP), $extractedPHP, $result);
        }

        return $result;
    }

    /**
     * Gibt den Text des Tokens zurück
     *
     * @param int $token
     * @return string
     */
    protected function _getTextByToken($token)
    {
        if (is_array($token)) {
            return $token[1];
        }
        return (string) $token;
    }
}
