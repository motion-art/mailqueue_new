<?php
/**
 * File for Repository_Subversion
 *
 * @category   Phing
 * @package    Phing_Classes
 * @author     Hans-Peter Buniat <hpbuniat@googlemail.com>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id$
 */

/**
 * Check a svn-repository for outdated externals
 *
 * @category   Phing
 * @package    Phing_Classes
 */
class Repository_Subversion
{
    /**
    * Command to get the externals of a repo
    *
    * @var string
    */
    const COMMAND_PROPGET = 'svn propget svn:externals -R --xml %s';

    /**
     * Command to get info about working copy
     *
     * @var string
     */
    const COMMAND_INFO = 'svn info --xml %s';

    /**
    * Command to get the tag-list
    *
    * @var string
    */
    const COMMAND_LIST = 'svn list %s --xml';

    public function getInfo($directory)
    {
        return $this->_execute(sprintf(self::COMMAND_INFO, $directory));
    }

    public function getProperties($directory)
    {
        return $this->_execute(sprintf(self::COMMAND_PROPGET, $directory));
    }

    public function getList($url)
    {
        return $this->_execute(sprintf(self::COMMAND_LIST, $url));
    }

    /**
    * Execute a command
    *
    * @param string $sCommand
    * @return SimpleXMLElement
    * @throws Exception
    */
    protected function _execute($sCommand)
    {
        $rCommand = popen($sCommand, 'r');
        $sReturn = '';
        while (feof($rCommand) !== true) {
            $sReturn .= fread($rCommand, 4096);
        }

        $iStatus = pclose($rCommand);
        if ($iStatus !== 0) {
            throw new Exception();
        }

        return simplexml_load_string($sReturn);
    }
}
