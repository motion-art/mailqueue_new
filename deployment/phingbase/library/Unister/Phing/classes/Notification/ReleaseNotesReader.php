<?php
/**
 * @category    Notification
 * @package     Notification_ReleaseNotesReader
 * @copyright   Copyright (c) 2013 Unister GmbH
 * @author      Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author      Tommy Helm <tommy.helm@unister-gmbh.de>
 * @author      Sven Weingartner <sven.weingartner@unister.de>
 * @version     $Id$
 */

/**
 * PVG-artiges Auslesen einer Release-Notes-Datei. Beispiel zum Verständnis des Formats:

29.01.2013 (3.31.0)
===================

<strong>Feature: Google AdSense - bitte in IBE schritt 2-4 deaktivieren</strong>
Die Google-AFS wurden für Schritt 2-4 via Config deaktiviert

<strong>Feature: Fehlender Autocomplete beim Eingabefeld "Suche nach Hotelnamen"</strong>
Bei der "Suche nach Hotelnamen" wurde ein AutoCompleter verbaut.

<strong>Bugfix: QALog: 'Object of class Zend_View_Helper_Translate could not be converted to string'</strong>
Wurden auf der Landingpage Eingabedaten gelöscht (zB Hotelname), konnte es zu Fehlern kommen. Diese Fehler wurden nun
entfernt.

<strong>Feature: Veranstalteragbs: TUI-Gruppe, Alltours usw. sowie Sonnengarantie entfernen</strong>
Veranstalter-Agbs von ehem. Partnern wurden aus dem Servicebereich entfernt. Die Agb der Sonnengarantie ist im
Servicebereich nicht mehr erreichbar.

24.01.2013 (3.30.1)
===================

<strong>Bugfix: JS Fehler auf zahllungsdatenseite</strong>
JS-Fehler, der sich aus dem Conversiontracking (doubleclick) im Schritt 5.2 ergab, wurde behoben.</strong>


<strong>Bugfix: SEO: Description auf Reisen Startseite ändern</strong>
Meta-Description der Startseite wurde SEM-optimiert


<strong>Bugfix: QALog: 'Object of class Zend_View_Helper_Translate could not be converted to string'</strong>
Ein SysLog Feheler wurde versucht zu eliminieren. Resultat im erst im Live-SysLog erkennbar
(Fehler taucht nicht mehr auf)


<strong>Externals</strong>
-Anhebung der TT-Ibe auf Version 1.20.00
-Umstellung der restlichen Externals auf interne Pfade

 *
 * Diese Klasse ist eine angepasste Version der Klasse Core_Class_Process_Helper_Releasenotes aus dem PVG-Unister-Core.
 *
 * @category    Notification
 * @package     Notification_ReleaseNotesReader
 * @copyright   Copyright (c) 2013 Unister GmbH
 */
class Notification_ReleaseNotesReader
{
    /**
     * sprintf Format für die Kopfzeile eines Eintrages
     *
     * @var string
     */
    protected $_headerFormat = '%s %s';

    /**
     * Position des Datums im headerFormat
     *
     * @var int
     */
    protected $_headerDatePosition = 0;

    /**
     * Position der Version im headerFormat
     *
     * @var int
     */
    protected $_headerVersionPosition = 1;

    /**
     * Zeile, welche unter dem Header steht beginnt mit folgenden Zeichen
     *
     * @var string
     */
    protected $_headerDelimiter = '===';

    /**
     * Lädt die Release-Notes für eine bestimmte Version
     *
     * @param string $version
     * @return string
     * @throws BuildException Falls die angegebene Version nicht in der angebenen Datei zu finden ist oder wenn es
     *      die angebene Datei nicht gibt
     */
    public function loadReleaseNotesForVersion($absoluteReleaseNotesPath, $version)
    {
        $releaseNotes = $this->_loadReleaseNotes($absoluteReleaseNotesPath);
        if (isset($releaseNotes[$version])) {
            return trim(implode("\n", $releaseNotes[$version]));
        }
        throw new BuildException('The version ' . $version . ' cannot be found in ' . $absoluteReleaseNotesPath);
    }

    /**
     * Lädt alle Releasenotes
     *
     * @return array
     */
    protected function _loadReleaseNotes($absoluteReleaseNotesPath)
    {
        if (!file_exists($absoluteReleaseNotesPath)) {
            throw new BuildException('Given path to release notes does not exist: ' . $absoluteReleaseNotesPath);
        }

        // aus der Datei laden
        $lineBuffer = array();
        $releaseNotes = array();
        $handle = fopen($absoluteReleaseNotesPath, 'r');

        $currentVersion = null;
        while (!feof($handle) && $handle !== false) {
            $line = fgets($handle, 8192);
            $lineBuffer[] = trim($line);

            if (strpos($line, $this->_headerDelimiter) === 0) {
                $header = $lineBuffer[(count($lineBuffer) - 2)];
                if ($currentVersion !== null) {
                    $entry = array_splice($lineBuffer, 2, (count($lineBuffer) - 4));
                    $releaseNotes[$currentVersion] = $entry;
                }

                $currentVersion = $this->_getHeaderKeyFromEntry($header, $this->_headerVersionPosition);
                $lineBuffer = array($header);
            }
        }
        fclose($handle);

        $entry = array_splice($lineBuffer, 2, (count($lineBuffer) - 2));
        $releaseNotes[$currentVersion] = $entry;

        return $releaseNotes;
    }

    /**
     * Holt die Kopfzeile aus dem Eintrag
     *
     * @param string $entry
     * @param int    $position
     *
     * @return string
     */
    protected function _getHeaderKeyFromEntry($entry, $position)
    {
        $header = trim(str_replace(array('(', ')'), array('', ''), $entry));
        $headerData = sscanf($header, $this->_headerFormat);
        return isset($headerData[$position]) ? $headerData[$position] : '';
    }
}