<?php
/**
 * @category    Notification
 * @package     Notification_ChangeLogReader
 * @copyright   Copyright (c) 2013 Unister GmbH
 * @author      Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author      Tommy Helm <tommy.helm@unister-gmbh.de>
 * @author      Sven Weingartner <sven.weingartner@unister.de>
 * @version     $Id$
 */

/**
 * PVG-artiges Auslesen einer Change-Log-Datei. Beispiel zum Verständnis des Formats:
    29.01.2013 (3.31.0)
    ===================

    Bugfix:     Bug 160556 - QALog: 'Object of class Zend_View_Helper_Translate could not be converted to string'
    Feature:    Bug 145980 - Fehlender Autocomplete beim Eingabefeld "Suche nach Hotelnamen" (Nils Mammen)
    Feature:    Bug 160703 - Veranstalteragbs: TUI-Gruppe, Alltours usw. sowie Sonnengarantie entfernen
    Feature:    Bug 160865 - Google AdSense - bitte in IBE schritt 2-4 deaktivieren (Nils Mammen)

    24.01.2013 (3.30.1)
    ===================

    Bugfix:     Bug 159847 - JS Fehler auf zahllungsdatenseite (Darja Kroeplin)
    Bugfix:     Bug 160263 - QALog: 'Object of class Zend_View_Helper_Translate could not be converted to string'
    Bugfix:     Bug 160269 - SEO: Description auf Reisen Startseite ändern (Jennifer Eder)

 *
 * Diese Klasse ist eine angepasste Version der Klasse Core_Class_Process_Helper_Changelog aus dem PVG-Unister-Core.
 *
 * @category    Notification
 * @package     Notification_ChangeLogReader
 * @copyright   Copyright (c) 2013 Unister GmbH
 */
class Notification_ChangeLogReader
{
    /**
     * Das Format der Changelog-Dateien entspricht (solange die Funktionalität dieser Klasse sich nicht ändert)
     * der der Release-Notes-Dateien. Deswegen erledigt ein Notification_ReleaseNotesReader die eigentliche Arbeit
     * dieser Klasse.
     *
     * @var Notification_ReleaseNotesReader
     */
    protected $_releaseNotesReader;

    /**
     * Konstruktor
     */
    public function __construct()
    {
        $this->_releaseNotesReader = new Notification_ReleaseNotesReader();
    }

    /**
     * @param string $absoluteReleaseNotesPath
     * @param string $version
     */
    public function loadChangeLogForVersion($absoluteReleaseNotesPath, $version)
    {
        return $this->_releaseNotesReader->loadReleaseNotesForVersion($absoluteReleaseNotesPath, $version);
    }
}