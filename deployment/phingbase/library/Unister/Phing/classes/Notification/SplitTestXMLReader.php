<?php
/**
 * @category    Notification
 * @package     Notification_SplitXMLReader
 * @copyright   Copyright (c) 2013 Unister GmbH
 * @author      Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author      Tommy Helm <tommy.helm@unister-gmbh.de>
 * @author      Tobias Reinwarth <tobias.reinwarth@unister-gmbh.de>
 * @version     $Id$
 */

/**
 * PVG-artiges Auslesen einer Split-XML-Datei wie sie mit dem Unister-Core V1 verwendet werden können. Nähere
 * Informationen findet man in der Unister-Core-V1-Dokumentation im Kapitel "Splittest".
 *
 * Diese Klasse ist eine angepasste Version der Klasse Core_Class_Controller_Action_Helper_Splits aus dem
 * PVG-Unister-Core.
 *
 * @see https://subversion.unister-gmbh.de/zf-bootstrap/trunk/docs/unister-core.pdf
 *
 * @category    Notification
 * @package     Notification_SplitXMLReader
 * @copyright   Copyright (c) 2013 Unister GmbH
 */
class Notification_SplitTestXMLReader
{
    /**
     * @param string $absoluteSplitXmlPath
     * @return array jeweils stdClass mit info, splitId und rate
     * @throws BuildExcpetion falls das XML nicht valide ist oder aus anderen Gründen nicht gelesen werden kann
     */
    public function calculateSplittingRates($absoluteSplitXmlPath)
    {
        if (!file_exists($absoluteSplitXmlPath)) {
            throw new BuildException('Given path to split test XML does not exist: ' . $absoluteSplitXmlPath);
        }
        $xml = simplexml_load_file($absoluteSplitXmlPath);
        if (false === $xml) {
            throw new BuildException('Could not parse split xml file: ' . $this->_absoluteSplitXmlPath);
        }

        $splittingRates = array();

        $splitInfo = new stdClass();
        $splitInfo->info = 'Defaultsplit';
        $splitInfo->splitId = 'default';
        $splitInfo->rate = 100;
        $splittingRates[$splitInfo->splitId] = $splitInfo;

        foreach ($xml as $splitTest) {
            $enabled = (int) $splitTest->setup->enabled;
            if ($enabled) { //Wenn Splittest aktiviert
                $splittingRates['default']->rate -= (int) $splitTest->setup->splitting_rate;
                $splitVariants = $splitTest->xpath('//variant[@enabled = 1]');
                $chanceCount = 0;
                foreach ($splitVariants as $splitVariant) {
                    $chanceCount += (int) $splitVariant->chance;
                }
                $rate = (int) ($splitTest->setup->splitting_rate) / $chanceCount;
                foreach ($splitVariants as $splitVariant) {
                    $splitInfo = new stdClass();

                    $splitInfo->info = (string) $splitVariant->info;
                    $splitInfo->splitId = (int) $splitVariant->attributes()->id;
                    $splitInfo->rate = ((int) $splitVariant->chance) * $rate;
                    $splittingRates[$splitInfo->splitId] = $splitInfo;
                }
            }
        }
        ksort($splittingRates);

        return $splittingRates;
    }
}