<?php
/**
 * File for the class PrettyPrinter.php
 *
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     c.klaeren
 * @version    $Id$
 */

/**
 * used to pretty print things while using phing
 * @copyright  Copyright (c) 2012 Unister GmbH
 */

class PrettyPrinter
{

    const RED = "\033[31m";
    const GREEN = "\033[32m";
    const YELLOW = "\033[33m";
    const BLUE = "\033[34m";
    const MAGENTA = "\033[35m";
    const CYAN = "\033[36m";
    const RESET = "\033[0m";
    const BOLD = "\033[1m";

    public static function printAnsi($argument)
    {
        if (is_array($argument)) {
            self::printArray($argument);
        }
        if ($argument instanceof AbstractContainer) {
            self::printContainer($argument);
        }
    }



    public static function printArray($data)
    {
        if (! isset($data) || empty($data)) {
            echo  self::MAGENTA . self::BOLD . 'No Data found !' . self::RESET . PHP_EOL;
            return;
        }


        foreach ($data as $key=> $value) {
            if ($value instanceof AbstractContainer) {
                self::printContainer($value);
                continue;
            }
            echo ' ' .  self::RED . self::BOLD . $key . ' : ' . self::RESET . $value . PHP_EOL;
        }
        echo PHP_EOL;
    }

    /**
     *
     * Enter description here ...
     * @param AbstractContainer $data
     */
    public static function printContainer($data)
    {
        $keys = $data->getFieldList();
        foreach ($keys as $key) {
            $method = 'get' . ucfirst($key);
            $value = $data->$method();
            if (is_array($value)) {
                $value= '[' . implode(', ', $value) . ']';
            }
            echo ' ' .  self::RED . self::BOLD . $key . ' : ' . self::RESET . $value . PHP_EOL;
        }
        echo PHP_EOL;
    }
}