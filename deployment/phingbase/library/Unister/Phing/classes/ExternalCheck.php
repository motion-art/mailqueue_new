<?php
/**
 * File for ExternalCheck
 *
 * @category   Phing
 * @package    Phing_Classes
 * @author     Hans-Peter Buniat <hpbuniat@googlemail.com>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id$
 */
require_once 'classes/Repository/Subversion.php';

/**
 * Check a svn-repository for outdated externals
 *
 * @category   Phing
 * @package    Phing_Classes
 * @link       https://github.com/hpbuniat/external-check/blob/master/ExternalCheck.php
 */

class ExternalCheck
{
    /**
    * directories to check
    *
    * @var array
    */
    protected $_directories = array();

    /**
     * @param string $dir
     */
    public function setDirs(array $dirs)
    {
        $this->_directories = $dirs;
    }

    /**
    * The parsing results
    *
    * @var array
    */
    protected $_result = array();

    /**
    * Regular-expression, to match releases
    *
    * @var array
    */
    protected $_releaseRegex = array(
        '!tags/(release-)!',
        '!tags/(.*?)/(.*?)/!',
        '!(release-\d)!',
        '!(Smarty_\d)!'
    );

    /**
    * Process ..
    *
    * @return array
    */
    public function get()
    {
        if (empty($this->_directories) === true) {
            throw new BuildException('Directories not set');
        }

        $client = new Repository_Subversion();
        $outdated = array();

        $xml = $client->getInfo($this->_directory);
        $this->_parseBaseUri($xml);

        foreach ($this->_directories as $directory) {
            $xml = $client->getProperties($directory);
            $this->_parseExternals($xml);
        }

        foreach ($this->_result as $path) {
            foreach ($path['externals'] as $name => $external) {

                try {
                    $link = $this->_getExternalBasePath($external['link']);
                    $xml = $client->getList($link);
                    $latest = $this->_getLatest($xml);

                    if (strpos($external['link'], $latest) === false) {
                        $outdated[$path['path']][$name] = array(
                            'link' => $external['link'],
                            'update' => $latest
                        );

                    }
                } catch (InvalidArgumentException $e) {
                }
            }
        }

        return $outdated;
    }

    /**
    * Get the latest tag
    *
    * @param SimpleXMLElement $oXml
    * @throws InvalidArgumentException
    */
    protected function _getLatest(SimpleXMLElement $oXml)
    {
        $sLatest = '';
        $iTime = 0;
        foreach ($oXml->list->children() as $oEntry) {
            if ((string) $oEntry['kind'] === 'dir') {
                $iCompareTime = strtotime((string) $oEntry->commit->date);
                if ($iCompareTime > $iTime) {
                    $iTime = $iCompareTime;
                    $sLatest = (string) $oEntry->name;
                }
            }
        }

        if ($iTime === 0) {
            throw new InvalidArgumentException();
        }

        return $sLatest;
    }

    /**
    * Get the base-path for tags
    *
    * @param string $sLink
    * @throws InvalidArgumentException
    */
    protected function _getExternalBasePath($sLink)
    {
        $aMatches = array();
        foreach ($this->_releaseRegex as $sRegex) {

            if (preg_match($sRegex, $sLink, $aMatches) !== 0) {
                $sLink = substr($sLink, 0, strpos($sLink, $aMatches[1]) - 1);
                if (count($aMatches) === 3) {
                    $sLink .= '/' . $aMatches[1];
                }

                return $sLink;
            }
        }

        throw new InvalidArgumentException();
    }

    /**
    * Get the base uri
    *
    * @param SimpleXMLElement $xml
    * @throws InvalidArgumentException
    */
    protected function _parseBaseUri(SimpleXMLElement $xml)
    {
        if (!isset($xml->entry)) {
            throw new InvalidArgumentException();
        }

        $url = parse_url($xml->entry->url);
        if (!$url) {
            throw new InvalidArgumentException();
        }
        $this->_base = $url['scheme'] . '://' . $url['host'];

        return $this;
    }

    /**
    * Parse the externals
    *
    * @param SimpleXMLElement $oXml
    * @return ExternalCheck
    */
    protected function _parseExternals(SimpleXMLElement $oXml)
    {
        foreach ($oXml->children() as $oTarget) {
            $aExternal = array(
                'path' => (string) $oTarget['path'],
                'externals' => array()
            );
            $aExternals = explode(PHP_EOL, (string) $oTarget->property);
            foreach ($aExternals as $sExternal) {
                $aSplit = explode(' ', $sExternal);
                if (count($aSplit) === 2) {

                    $link = $aSplit[1];
                    $target = $aSplit[0];
                    if (preg_match('~^(http|/)~', $aSplit[0])) {
                        $link = $aSplit[0];
                        $target = $aSplit[1];
                    }

                    if (strpos($link, '/') === 0) {
                        $link = $this->_base . $link;
                    }

                    $aExternal['externals'][$target] = array(
                        'link' => $link
                    );
                }
            }

            $this->_result[] = $aExternal;
        }

        return $this;
    }
}
