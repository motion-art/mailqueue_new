<?php

class Package_Aliases
{
    protected $index = array();

    public function load($filename)
    {
        $config = json_decode(file_get_contents($filename), true);
        foreach ($config as $name => $aliases) {
            foreach ($aliases as $alias) {
                $this->add($alias, $name);
            }
        }
    }

    public function add($alias, $name)
    {
        $this->index[$alias] = $name;
    }

    public function get($alias)
    {
        if (!isset($this->index[$alias])) {
            return $alias;
        }
        return $this->index[$alias];
    }

    public function has($alias)
    {
        return isset($this->index[$alias]);
    }

    public function mapExternals($externals)
    {
        $requirements = array();
        foreach ($externals as $external) {
            $reqVersion = '*';
            $foundVersion = '';
            $found = null;
            $match = array();
            if (preg_match('~([0-9]+[\._][0-9]+[\._][0-9]+(PL[0-9]+)?)~', $external, $match)) {
                $found = $match[1];
                $foundVersion = str_replace('_', '.', $found);
                $reqVersion = $foundVersion;
            }
            if (empty($foundVersion) && preg_match('~([0-9]+\.[0-9]+(PL[0-9]+)?)~', $external, $match)) {
                $found = $match[1];
                $reqVersion = $match[1];
                $foundVersion = $match[1];
            }
            if (strpos($external, 'trunk') || strpos($external, 'branches')) {
                $reqVersion = '@dev';
            }

            $match = array();
            if (!preg_match('~(https://subversion.unister-gmbh.de)?/([^\s]+)~', $external, $match)) {
                continue;
            }

            $name = rtrim($match[2], '/');
            if ($found !== null) {
                $name = str_replace($found, '', $name);
            }

            if ($this->has($name)) {
                $requirements[$this->get($name)] = $reqVersion;
            }
        }

        return $requirements;
    }
}