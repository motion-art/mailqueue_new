<?php
/**
 * File for SecurityPromptTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id:$
 */

require_once 'phing/Task.php';

/**
 * Prompt-Task, Eingabe wird nicht gezeigt
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2010, Unister GmbH
 */
class SecurityPromptTask extends Task
{
    /**
     * The property name to set with the output.
     * @var string
     */
    protected $_propertyName;

    /**
     * The entered value.
     * @var string
     */
    private $_proposedValue;

    /**
     * The text to use for the prompt.
     * @var string
     */
    protected $_promptText;

    /**
     * The character to put after the text.
     * @var string
     */
    protected $_promptCharacter = '?';

    public function main()
    {
        $currentValue = $this->project->getProperty($this->_propertyName);

        if ($currentValue === null) {

            print "\n" . $this->_promptText . ' ' . $this->_promptCharacter . ' ';
            $value = rtrim($this->_performSecurePromptCommand(), "\r\n");
            print "\n\n";

            $this->project->setProperty($this->_propertyName, $value);
        }
    }

    /**
     * Performs the command `stty -echo; head -n1 ; stty echo`
     *
     * @codeCoverageIgnore
     *
     * @return string
     */
    protected function _performSecurePromptCommand()
    {
        return `stty -echo; head -n1 ; stty echo`;
    }

    /**
     * Sets the terminating character used to
     * punctuate the prompt text (default is "?").
     * @param string $newPromptcharacter
     */
    public function setPromptCharacter($char)
    {
        $this->_promptCharacter = $char;
    }

    /**
     * Sets text of the prompt.
     * @param string $newPrompttext
     */
    public function setPromptText($text)
    {
        $this->_promptText = $text;
    }

    /**
     * Specifies the Phing Project Property
     * being set by this task.
     * @param newPropertyname java.lang.String
     */
    public function setPropertyName($name)
    {
        $this->_propertyName = $name;
    }
}
