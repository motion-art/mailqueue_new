<?php
/**
 * File for SvnExternalTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id$
 */

require_once 'phing/Task.php';
require_once 'classes/ExternalCheck.php';

/**
 * Check a svn-repository for outdated externals
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2011, Unister GmbH
 */
class SvnExternalTask extends Task
{
    /**
    * Working Copy to check
    *
    * @var string
    */
    protected $_directory = null;

    /**
     * @param string $dir
     */
    public function setDir($dir)
    {
        $this->_directory = $dir;
    }

    public function main()
    {
        if (empty($this->_directory) === true) {
            throw new BuildException('Directory not set');
        }

        $check = $this->_getExternalCheck();
        $check->setDirs(array($this->_directory));
        $outdated = $check->get();

        foreach ($outdated as $path) {
            foreach ($path as $name => $external) {
                $this->log('new version found: ' . $name . ' (' . $external['update'] . ')', Project::MSG_WARN);
            }
        }
    }

    /**
     * Creates an ExternalCheck instance
     *
     * @return ExternalCheck
     */
    protected function _getExternalCheck()
    {
        return new ExternalCheck();
    }
}