<?php
/**
 * File for ZFConfigurationTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2010 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id:$
 */

require_once 'phing/Task.php';

/**
 * ZFConfigurationTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2010, Unister GmbH
 */
class ZFConfigurationTask extends Task
{
    /**
     * Pfad zu ZF
     *
     * @var string
     */
    protected $_libraryPath;

    /**
     * Pfad zur Konfiguration
     *
     * @var string
     */
    protected $_configuration;

    /**
     * Umgebungsdefinition
     *
     * @var string
     */
    protected $_environment;

    /**
     * @param string $path
     */
    public function setLibraryPath($path)
    {
        $this->_libraryPath = $path;
    }

    /**
     * @param string $config
     */
    public function setConfiguration($config)
    {
        $this->_configuration = $config;
    }

    /**
     * @param string $environment
     */
    public function setEnvironment($environment)
    {
        $this->_environment = $environment;
    }

    /**
     * Konvertiert Zend_Config_Ini => PHP-Array
     */
    public function main()
    {
        if ($this->_libraryPath === null) {
            throw new BuildException('LibraryPath expected');
        }
        if ($this->_configuration === null) {
            throw new BuildException('ConfigurationFile expected');
        }
        if ($this->_environment === null) {
            throw new BuildException('Environment expected');
        }
        if (!file_exists($this->_libraryPath . '/Zend/Config.php')) {
            throw new BuildException('Could not find ZF: ' . $this->_libraryPath);
        }

        $this->_requireZendClasses();

        try {
            $config = new Zend_Config_Ini($this->_configuration, $this->_environment);
        } catch (Exception $e) {
            throw new BuildException($e->getMessage());
        }

        $result = var_export($config->toArray(), true);
        file_put_contents($this->_configuration . '.php', "<?php\nreturn " . $result . ';');
        $this->log('Converted: ' . $this->_configuration . '.php', Project::MSG_INFO);
    }

    /**
     * Setzt den Include_Path und bindet die benötigten Zend Klassen ein
     *
     * @codeCoverageIgnore
     *
     * @return void
     */
    protected function _requireZendClasses()
    {
        set_include_path(get_include_path() . ':' . $this->_libraryPath);

        require_once 'Zend/Config.php';
        require_once 'Zend/Config/Ini.php';
    }
}