<?php
/**
 * File for ListenerTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id$
 */

require_once 'phing/Task.php';
include_once 'phing/types/Parameter.php';

/**
 * Loggt Build-Result mittels Listener
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2011, Unister GmbH
 */
class ListenerTask extends Task
{
    /**
     * Listener Name
     * @var string
     */
    protected $_name = null;

    /**
     * Optionen
     * @var array
     */
    protected $_options = array();

    /**
     * Setzt den Listener Namen
     * @param string $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * Erstellt eine Syslog Option
     * @return Parameter
     */
    public function createOption()
    {
        $option = new Parameter();
        $this->_options[] = $option;
        return $option;
    }

    /**
     * Erstellt Listener bei Bedarf
     * @return BuildListener
     */
    protected function _findOrInitListener($listenerName)
    {
        if ($listenerName == null) {
            throw new BuildException('Listener not defined');
        }

        $className = $listenerName . 'Listener';
        $listeners = $this->project->getBuildListeners();

        foreach ($listeners as $listener) {
            if (get_class($listener) == $className) {
                return $listener;
            }
        }

        $filename = 'listener/' . $className . '.php';

        $this->_requireListenerClassFile($filename);

        if (!class_exists($className)) {
            throw new BuildException('Listener not found');
        }
        $listener = new $className;
        $this->project->addBuildListener($listener);

        return $listener;
    }

    /**
     * Includes via require_once() the file containing a certain Listener class
     *
     * @codeCoverageIgnore
     *
     * @param string $filename
     */
    protected function _requireListenerClassFile($filename)
    {
        require_once $filename;
    }

    /**
     * The main entry point method.
     */
    public function main()
    {
        $listener = $this->_findOrInitListener($this->_name);

        if (count($this->_options) > 0) {
            foreach ($this->_options as $option) {
                $listener->setOption($option->getName(), $option->getValue());
            }
        }
    }
}