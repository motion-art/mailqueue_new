<?php
/**
 * File for BulkSshExecTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Mike Rötgers <mike.roetgers@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id$
 */

require_once 'classes/PrettyPrinter.php';
require_once 'tasks/Abstract/SshBulkTask.php';

/**
 * Führt Remote Befehle via SSH aus, allerdings nicht via PHP SSH2-Extension sondern über exec()
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2010, Unister GmbH
 */
class ListInstalledTask extends Abstract_SshBulkTask
{
    /**
     * Baut den Kommandostring zusammen und führt den SSH-Befehl aus
     */
    public function main()
    {
        $hosts = $this->getHosts();

        $result = array();

        foreach ($hosts as $host) {
            $exitCode = null;
            $this->_remoteHost = $host['host'];
            $this->_remoteUser = $host['user'];
            $output = $this->execCommand('cat ' . $host['vhost'] . '/current-release', $exitCode);

            $result[$host['host']] = current($output);
        }

        echo PrettyPrinter::YELLOW
            . PrettyPrinter::BOLD
            . '             '
            . '============= Slaves ============= '
            . PrettyPrinter::RESET
            . PHP_EOL
            . PHP_EOL;

        PrettyPrinter::printAnsi($result);
    }
}