<?php
/**
 * File for MemcacheTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2010 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Mike Rötgers <mike.roetgers@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id:$
 */

require_once 'phing/Task.php';

/**
 * Memcache Task, um einzelne Einträge zu setzen, zu löschen oder den gesamten Memcache zu flushen
 *
 * Mögliche Aktionen sind "set", "delete" und "flush"
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2010, Unister GmbH
 */
class MemcacheTask extends Task
{
    /**
     * Verbindung zum Memcache-Server
     *
     * @var Memcache
     */
    protected $_memcache;

    /**
     * Hostname oder IP
     *
     * @var string
     */
    protected $_server;

    /**
     * Portnummer
     *
     * @var int
     */
    protected $_port;

    /**
     * Aktion, die der Task ausführen soll
     *
     * @var string
     */
    protected $_action;

    /**
     * Key um einen Eintrag zu identifizieren
     *
     * @var string
     */
    protected $_key;

    /**
     * Wert, der in den Memcache geschrieben werden soll
     *
     * @var mixed
     */
    protected $_value;

    /**
     * Verarbeitung bei Fehler beenden?
     *
     * @var boolean
     */
    protected $_haltOnFailure = false;

    /**
     * @param string $host
     */
    public function setServer($host)
    {
        $this->_server = $host;
    }

    /**
     * @param int $port
     */
    public function setPort($port)
    {
        if (1 == preg_match('/^\d+$/', $port)) {
            $this->_port = (int)$port;
        } else {
            throw new BuildException('"' . $port . '" is not a valid Port');
        }
    }

    /**
     * @param string $action
     */
    public function setAction($action)
    {
        $this->_action = $action;
    }

    /**
     * @param string $key
     */
    public function setKey($key)
    {
        $this->_key = $key;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->_value = $value;
    }

    /**
     * @param boolean $flag
     */
    public function setHaltOnFailure($flag)
    {
        $this->_haltOnFailure = (bool)$flag;
    }

    /**
     * Entscheidet anhand der action, was für eine Aktion durchgeführt werden soll
     */
    public function main()
    {
        switch ((string)$this->_action) {
            case 'set':
                $this->log('Creating record "' . $this->_key . '" in memcache');
                if (!$this->_set()) {
                    $this->log('Could not create record "' . $this->_key . '" in memcache');
                    if ($this->_haltOnFailure) {
                        throw new BuildException('Memcache error');
                    }
                }
                break;
            case 'delete':
                $this->log('Deleting record "' . $this->_key . '" in memcache');
                if (!$this->_delete()) {
                    $this->log('Could not delete record "' . $this->_key . '" in memcache');
                    if ($this->_haltOnFailure) {
                        throw new BuildException('Memcache error');
                    }
                }
                break;
            case 'flush':
                $this->log('Flushing memcache');
                if (!$this->_flush()) {
                    $this->log('Could not flush memcache');
                    if ($this->_haltOnFailure) {
                        throw new BuildException('Memcache error');
                    }
                }
                break;
            default:
                throw new BuildException('Memcache action "' . $this->_action . '" is unknown');
                break;
        }
    }

    /**
     * Verbindet zum Memcache-Server
     *
     * @return Memcache
     */
    protected function _connect()
    {
        if ($this->_memcache instanceof Memcache) {
            return $this->_memcache;
        }

        $this->_memcache = $this->_getMemcache();
        if (!$this->_memcache->connect($this->_server, $this->_port)) {
            throw new BuildException('Could not connect to memcache server');
        }
        return $this->_memcache;
    }

    /**
     * Instanziert Memcache
     *
     * @return Memcache
     */
    protected function _getMemcache()
    {
        return new Memcache;
    }

    /**
     * Setzt ein Key/Value Paar im Memcache
     *
     * @return boolean
     */
    protected function _set()
    {
        $memcache = $this->_connect();

        if (empty($this->_key)) {
            throw new BuildException('Key required for setting data to memcache');
        }

        if (empty($this->_value)) {
            throw new BuildException('Value required for setting data to memcache');
        }

        return $memcache->add($this->_key, $this->_value);
    }

    /**
     * Löscht Key vom Memcache
     *
     * @return boolean
     */
    protected function _delete()
    {
        $memcache = $this->_connect();

        if (empty($this->_key)) {
            throw new BuildException('Key required for setting data to memcache');
        }

        return $memcache->delete($this->_key);
    }

    /**
     * Flushed den Memcache
     *
     * @return boolean
     */
    protected function _flush()
    {
        $memcache = $this->_connect();

        return $memcache->flush();
    }
}