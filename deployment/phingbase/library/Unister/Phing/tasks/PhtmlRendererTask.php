<?php
/**
 * @category    Phing
 * @package     Phing_Task
 * @copyright   Copyright (c) 2013 Unister GmbH
 * @author      Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author      Tommy Helm <tommy.helm@unister-gmbh.de>
 * @version     $Id$
 */

/**
 * Task zum Rendern von PHTML-Dateien.
 *
 * @category    Phing
 * @package     Phing_Task
 * @copyright   Copyright (c) 2013 Unister GmbH
 */

class PhtmlRendererTask extends Task
{
    /**
     * Absoluter Pfad zum zu rendernden Template.
     *
     * @var string
     */
    protected $_template;

    /**
     * Name des Property, dass das gerenderte Template nach Ausführen des Tasks enthalten soll.
     *
     * @var string
     */
    protected $_property;

    /**
     * Ob das gerenderte Template an das Property angehangen werden soll, oder evtl. schon vorhandenen
     * Inhalt überschreiben soll. Default ist false.
     *
     * @var boolean
     */
    protected $_append = false;

    /**
     * @var array
     */
    protected $_assignments = array();

    /**
     * (non-PHPdoc)
     * @see Task::main()
     */
    public function main()
    {
        $this->_assertRequiredParameters();
        $result = $this->_renderTemplate($this->_template, $this->_assignments);
        if ($this->_append) {
            $currentContent = $this->project->getProperty($this->_property);
            if (null !== $currentContent) {
                $result = $currentContent . $result;
            }
        }
        $this->project->setProperty($this->_property, $result);
    }

    /**
     * @param string $template
     * @return void
     */
    public function setTemplate($template)
    {
        $this->_template = $template;
    }

    /**
     * @param string $property
     * @return void
     */
    public function setProperty($property)
    {
        $this->_property = $property;
    }

    /**
     * @param boolean $append
     * @return void
     */
    public function setAppend($append)
    {
        $this->_append = $append;
    }

    /**
     * @param PhtmlRendererTask_AssignTask $assign
     * @return void
     */
    public function addAssign(PhtmlRendererTask_AssignTask $assign)
    {
        $this->_assignments[] = $assign;
    }

    /**
     * @return string
     */
    public function getTemplate()
    {
        return $this->_template;
    }

    /**
     * @return string
     */
    public function getProperty()
    {
        return $this->_property;
    }

    /**
     * @return boolean
     */
    public function isAppend()
    {
        return $this->_append;
    }

    /**
     * @return array
     */
    public function getAssignments()
    {
        return $this->_assignments;
    }

    /**
     * @throws BuildException
     * @return void
     */
    protected function _assertRequiredParameters()
    {
        if (empty($this->_property)) {
            throw new BuildException('The property property is required', $this->location);
        }
        if (empty($this->_template)) {
            throw new BuildException('The property template is required', $this->location);
        }
    }

    /**
     * @param string $template
     * @param array $parameters
     * @return string
     */
    protected function _renderTemplate($template, array $assignments)
    {
        $view = new Zend_View();
        $view->assign($this->_makeParametersByAssignments($assignments));
        $templateDirectory = dirname($template);
        $templateName = basename($template);
        $view->setScriptPath($templateDirectory);
        return $view->render($templateName);
    }

    /**
     * @param array $assignments
     * @return array
     */
    protected function _makeParametersByAssignments(array $assignments)
    {
        $params = array();
        foreach ($assignments as $assign) {
            if ($assign->isJsonEncoded()) {
                $value = json_decode($assign->getValue());
            } else {
                $value = $assign->getValue();
            }
            $params[$assign->getName()] = $value;
        }
        return $params;
    }
}