CREATE TABLE `_DbVersion` (
  `releaseNumber` mediumint NOT NULL,
  `applied` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  `appliedBy` varchar(55) NOT NULL,
  `description` varchar(200) NOT NULL,
  PRIMARY KEY  (`releaseNumber`)
) ENGINE=InnoDb DEFAULT CHARSET=utf8 COMMENT 'Holds the Changes and Release Version for DB Structure';
