<?php
/**
 * File for CheckRequirementsTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id$
 */

require_once 'classes/PrettyPrinter.php';
require_once 'tasks/Abstract/SshBulkTask.php';

/**
 * Überprüft Abhängigkeiten der Software gegen die angegebenen Hosts der Liste
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2010, Unister GmbH
 */
class CheckRequirementsTask extends Abstract_SshBulkTask
{
    protected $_filename = null;

    public function setFilename($filename)
    {
        $this->_filename = $filename;
    }

    /**
     * (non-PHPdoc)
     * @see SshExecTask::execCommand()
     */
    public function execCommand($commandString, &$exitCode)
    {
        if (!empty($this->_dir)) {
            $commandString = 'cd ' . $this->_dir . '\; ' . $commandString;
        }

        $commandString = $this->_remoteHost . ' ' . escapeshellarg($commandString);

        if (!empty($this->_remoteUser)) {
            $commandString = $this->_remoteUser . '@' . $commandString;
        }

        $cmd = $this->_cmd . implode(' ', $this->_cmdArgs) . ' ' . $commandString;
        $this->_performExecCommand($cmd, $output, $exitCode);

        if ($this->_returnProperty) {
            $this->project->setProperty($this->_returnProperty, $exitCode);
        }

        if ($this->_outputProperty) {
            $this->project->setProperty($this->_outputProperty, implode("\n", $output));
        }

        return $output;
    }

    /**
     * Performs a call to exec()
     *
     * @param  string  $cmd
     * @param  string  &$output
     * @param  integer &$exitCode
     * @return void
     */
    protected function _performExecCommand($cmd, &$output, &$exitCode)
    {
        exec($cmd, $output, $exitCode);
    }

    /**
     * Überprüft Requirement
     *
     * @param string $command
     * @param string $parameter
     * @param string $version
     * @param string $pattern
     *
     * @return array
     */
    protected function _checkRequirement($command, $parameter, $version, $pattern)
    {
        $exitCode = 0;
        $matches = $this->execCommand(sprintf($command, $parameter), $exitCode);

        $matchInfo = array();
        if ($matches && preg_match($pattern, current($matches), $matchInfo)) {
            $installedVersion = $matchInfo[1];
            return array(
                'required' => $version,
                'installed' => $installedVersion,
                'failure' => !version_compare($installedVersion, $version, '>=')
            );
        }

        return array(
            'required' => $version,
            'installed' => 0,
        	'failure' => true
        );
    }

    /**
     * Baut den Kommandostring zusammen und führt den SSH-Befehl aus
     */
    public function main()
    {
        if (!file_exists($this->_filename)) {
            throw new BuildException('Requirement-definition not found: ' . $this->_filename);
        }
        $hosts = $this->getHosts();
        $requirements = json_decode(file_get_contents($this->_filename));

        foreach ($hosts as $host) {
            $exitCode = null;
            $this->_remoteHost = $host['host'];
            $this->_remoteUser = $host['user'];

            if (isset($requirements->require->packages)) {
                foreach ($requirements->require->packages as $package) {
                    $result['package'][$package->name] = $this->_checkRequirement('dpkg --list | grep %s',
                        $package->name, $package->version, '~ii\s+[^\s]+\s+([^\s]+)~');
                }
            }

            if (isset($requirements->require->pear)) {
                foreach ($requirements->require->pear as $pearPackage) {
                    $result['pear'][$pearPackage->name] = $this->_checkRequirement('pear list -a | grep %s',
                        $pearPackage->name, $pearPackage->version, '~[^\s]+\s+([^\s]+)~');
                }
            }

            if (isset($requirements->require->php)) {
                if (isset($requirements->require->php->version)) {
                    $exitCode = 0;
                    $installedPHP = $this->execCommand('php -v', $exitCode);

                    $result['php']['version'] = array(
                        'required' => $requirements->require->php->version,
                        'installed' => 0,
                        'failure' => false
                    );
                    if (preg_match('~PHP\s+([^\s]+)~', current($installedPHP), $match)) {
                        $compareResult = true;
                        if (isset($requirements->require->php->maxVersion)) {
                            $compareResult = version_compare($match[1], $requirements->require->php->maxVersion, '<');
                        }
                        $result['php']['version'] = array(
                            'required' => $requirements->require->php->version,
                            'installed' => $match[1],
                            'failure' => !($compareResult && version_compare($match[1], $requirements->require->php->version, '>='))
                        );
                    }
                }

                if (isset($requirements->require->php->modules)) {
                    foreach ($requirements->require->php->modules as $module) {
                        $exitCode = 0;
                        $valid = (boolean) $this->execCommand('php -m | grep "' . $module->name . '"', $exitCode);
                        $result['php-modules'][$module->name] = array(
                            'required' => 'enabled',
                            'installed' => 1,
                            'failure' => !$valid
                        );

                        if (isset($module->options)) {
                            foreach ($module->options as $option => $value) {
                                $exitCode = 0;
                                $valid = $this->execCommand('php -r \'phpinfo();\' | grep "' . $option . '"', $exitCode);
                                $valid = current($valid);

                                if (preg_match('~([^\s]+) => ([^\s]+)~', $valid, $match)) {
                                    $result['php-modules'][$module->name . '::' . $option] = array(
                                        'required' => $value,
                                        'installed' => $match[2],
                                        'failure' => ($match[2] != $value)
                                    );
                                }

                            }
                        }
                    }
                }
            }

            $results[$host['host']] = $result;
        }

        $this->_printResults($results);

        if ($this->_hasFailure($results)) {
            throw new BuildException('Requirements not met');
        }

    }

    /**
     * Gibt es einen Fehler in der Resultliste
     *
     * @param array $results
     * @return boolean
     */
    protected function _hasFailure($results)
    {
        foreach ($results as $host => $sections) {
            foreach ($sections as $section => $result) {
                foreach ($result as $package => $info) {
                    if ($info['failure']) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Stellt Ergebnis übersichtlich dar (CLI)
     *
     * @param array $results
     */
    protected function _printResults($results)
    {
        echo PrettyPrinter::YELLOW
            . PrettyPrinter::BOLD
            . '    '
            . '============= Requirements ============= '
            . PrettyPrinter::RESET
            . PHP_EOL
            . PHP_EOL;

        foreach ($results as $host => $sections) {
            echo ' ' .  PrettyPrinter::YELLOW . PrettyPrinter::BOLD . $host . ' : ' . PHP_EOL;

            foreach ($sections as $section => $result) {

                echo '   ' .  PrettyPrinter::YELLOW . PrettyPrinter::BOLD . $section . ' : ' . PHP_EOL;
                foreach ($result as $package => $info) {
                    $color = PrettyPrinter::GREEN;
                    $flag = 'I ';
                    $message = $info['required'] . ' == ' . $info['installed'];

                    if ($info['failure']) {
                        $color = PrettyPrinter::RED;
                        $flag = 'F ';
                        $message = $info['required'] . ' != ' . $info['installed'];

                        if ($info['installed'] === 0) {
                            $flag = '- ';
                            $message = 'not installed';
                        }
                    }
                    echo '     ' .  $color . PrettyPrinter::BOLD . $flag . ' ' . $package . ' : ' . $message . PrettyPrinter::RESET . PHP_EOL;
                }
            }
        }
    }
}