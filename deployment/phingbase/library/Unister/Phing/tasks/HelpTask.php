<?php
/**
 * File for the class HelpTask.php
 *
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     c.klaeren
 * @version    $Id$
 */

require_once 'classes/PrettyPrinter.php';

/**
 * displays a help screen for the User
 * @copyright  Copyright (c) 2012 Unister GmbH
 */
class HelpTask extends Task
{
    const INDENT='           ';

    /**
     * @see Task::main()
     */
    public function main()
    {
        $targetList = array(
            'install' => 'Initialisieren der Arbeitskopie',
            'package' => 'Bauen eines Paketes',
            'deploy' => 'Ausliefern des Paketes',
            'info' => 'Überblick über Targets und Umgebungsvariablen'
        );
        self::_printHead();
        self::_printIntro();
        PrettyPrinter::printAnsi($targetList);
    }
    protected static function _printHead()
    {
        echo PrettyPrinter::YELLOW
            . PrettyPrinter::BOLD
            . self::INDENT
            . ' *#######################* '
            . PHP_EOL
            . self::INDENT
            . '###########################'
            . PHP_EOL
            . self::INDENT
            . '####        Hilfe      ####'
            . PHP_EOL
            . self::INDENT
            . '###########################'
            . PHP_EOL
            . self::INDENT
            . ' *#######################* '
            . PrettyPrinter::RESET
            . PHP_EOL
            . PHP_EOL
            . PHP_EOL;


    }

    protected static function _printIntro()
    {
        echo  self::INDENT
            . '    '
            . PrettyPrinter::MAGENTA
            . PrettyPrinter::BOLD
            . ' wichtige Targets: '
            . PrettyPrinter::RESET
            . PHP_EOL
            . PHP_EOL;
    }
}