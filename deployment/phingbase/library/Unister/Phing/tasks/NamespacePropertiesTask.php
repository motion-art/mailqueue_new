<?php
/**
 * File for NamespacePropertiesTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id:$
 */

require_once 'phing/Task.php';

/**
 * Exportiert Liste von Properties in anderen Namespace
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2011, Unister GmbH
 */
class NamespacePropertiesTask extends Task
{
    /**
     * Schlüssel des Listenelementes, welches verwendet werden soll
     *
     * @var string
     */
    protected $_key;

    /**
     * Vordefiniert mit "list"
     *
     * Keinen abschließenden Separator setzen
     *
     * @var string
     */
    protected static $_prefix = 'list';

    /**
     * Separator für Verkettung
     *
     * @var string
     */
    protected static $_separator = '.';

    /**
     * @param string $name
     */
    public function setKey($key)
    {
        $this->_key = $key;
    }

    /**
     * Vordefinierter Prefix ist änderbar (NUR machen, wenn du weißt, was du tust!)
     *
     * @param string $prefix
     */
    public function setPrefix($prefix)
    {
        self::$_prefix = $prefix;
    }

    /**
     * Setzt neue Properties im Projekt
     *
     * @return array
     */
    public function main()
    {
        $substring = self::$_prefix . self::$_separator . $this->_key;

        foreach ($this->project->getProperties() as $key => $value) {
            if (strpos($key, $substring) === 0) {
                $key = str_replace($substring, self::$_prefix, $key);
                $this->project->setProperty($key, $value);
            }
        }
    }


    /**
     * Filtert alle Properties, die nicht dem Substring entsprechen
     *
     * @codeCoverageIgnore
     * @todo die Funktion wird vermutlich nicht mehr verwendet -> löschen?
     *
     * @param string $substring
     * @return array
     */
    public function filterArrayForValuesWithSubstring($substring)
    {
        $result = array();

        foreach ($this->project->getProperties() as $key => $value) {
            if (strpos($key, $substring) !== false) {
                $result[$key] = $value;
            }
        }
        return $result;
    }
}