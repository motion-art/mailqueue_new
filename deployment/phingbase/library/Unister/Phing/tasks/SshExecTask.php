<?php
/**
 * File for SshExecTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Mike Rötgers <mike.roetgers@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id:$
 */

require_once 'phing/Task.php';

/**
 * Führt Remote Befehle via SSH aus, allerdings nicht via PHP SSH2-Extension sondern über exec()
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2010, Unister GmbH
 */
class SshExecTask extends Task
{
    /**
     * SSH-Kommando
     *
     * @var string
     */
    protected $_cmd = 'ssh ';

    /**
     * Array mit Argumenten
     *
     * @var array
     */
    protected $_cmdArgs = array();

    /**
     * @var string
     */
    protected $_remoteUser;

    /**
     * @var string
     */
    protected $_remotePassword;

    /**
     * @var string
     */
    protected $_remoteHost;

    /**
     * @var string
     */
    protected $_command;

    /**
     * @var string
     */
    protected $_dir;

    /**
     * @var boolean
     */
    protected $_haltOnFailure = false;

    /**
     * Property name to set with return value from exec call.
     *
     * @var string
     */
    protected $_returnProperty;

    /**
     * Property name to set with output value from exec call.
     *
     * @var string
     */
    protected $_outputProperty;


    /**
     * Fügt Argument an den Befehl an
     *
     * @param string $argument
     */
    public function appendCmdArg($argument)
    {
        $this->_cmdArgs[] = $argument;
    }

    /**
     * Setzt SSH Benutzer für das Remotesystem
     *
     * @param string $username
     */
    public function setRemoteUser($username)
    {
        $this->_remoteUser = $username;
    }

    /**
     * Setzt Hostname für das Remotesystem
     *
     * @param string $hostname
     */
    public function setRemoteHost($host)
    {
        $this->_remoteHost = $host;
    }

    /**
     * Befehl, der Remote ausgeführt werden soll
     *
     * @param string $command
     */
    public function setCommand($command)
    {
        $this->_command = $command;
    }

    /**
     * Frei zu definierende SSH-Argumente
     *
     * @param string $arguments
     */
    public function setArgs($arguments)
    {
        $this->appendCmdArg($arguments);
    }

    /**
     * Soll Ausführung bei Fehler beendet werden?
     *
     * @param boolean $flag
     */
    public function setHaltOnFailure($flag)
    {
        $this->_haltOnFailure = (bool)$flag;
    }

    /**
     * Verzeichnis, in das gewechselt wird vor Befehlsausführung
     *
     * @param string $directory
     */
    public function setDir($directory)
    {
        $this->_dir = $directory;
    }

    /**
     * The name of property to set to return value from exec() call.
     *
     * @param string $prop Property name
     *
     * @return void
     */
    public function setReturnProperty($prop)
    {
        $this->_returnProperty = $prop;
    }

    /**
     * The name of property to set to output value from exec() call.
     *
     * @param string $prop Property name
     *
     * @return void
     */
    public function setOutputProperty($prop)
    {
        $this->_outputProperty = $prop;
    }

    public function execCommand($commandString, &$exitCode)
    {
        if (!empty($this->_dir)) {
            $commandString = 'cd ' . $this->_dir . ' && ' . $commandString;
        }

        $commandString = $this->_remoteHost . ' ' . escapeshellarg($commandString);

        if (!empty($this->_remoteUser)) {
            $commandString = $this->_remoteUser . '@' . $commandString;
        }

        $this->log('Executing remote command "' . $commandString . '" on host "' . $this->_remoteHost . '"');

        $cmd = $this->_cmd . implode(' ', $this->_cmdArgs) . ' ' . $commandString;
        $this->_performExecCommand($cmd, $output, $exitCode);

        if ($this->_returnProperty) {
            $this->project->setProperty($this->_returnProperty, $exitCode);
        }

        if ($this->_outputProperty) {
            $this->project->setProperty($this->_outputProperty, implode("\n", $output));
        }

        return $output;
    }

    /**
     * Performs a call to exec()
     *
     * @param  string  $cmd
     * @param  string  &$output
     * @param  integer &$exitCode
     * @return void
     */
    protected function _performExecCommand($cmd, &$output, &$exitCode)
    {
        exec($cmd, $output, $exitCode);
    }

    /**
     * Baut den Kommandostring zusammen und führt den SSH-Befehl aus
     */
    public function main()
    {
        if (empty($this->_remoteHost)) {
            throw new BuildException('You must provide a remote host');
        }

        if (empty($this->_command)) {
            throw new BuildException('You must provide a remote command');
        }

        $exitCode = null;
        $output = $this->execCommand($this->_command, $exitCode);

        foreach ($output as $line) {
            $this->log($line, Project::MSG_VERBOSE);
        }

        // @codeCoverageIgnoreStart
        if ($exitCode != 0 && $this->_haltOnFailure) {
            throw new BuildException('Task exited with code ' . $exitCode);
        }
        // @codeCoverageIgnoreEnd
    }
}