<?php
/**
 * File for MinifyTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id:$
 */

/**
 * Minify Task, zum Optimieren von CSS, JS, HTML
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2010, Unister GmbH
 */
class MinifyTask extends Task
{
    /**
     * Dateityp CSS
     * @var string
     */
    const TYPE_CSS = 'css';

    /**
     * Dateityp JS
     * @var string
     */
    const TYPE_JS = 'js';

    /**
     * Dateityp HTML
     * @var string
     */
    const TYPE_PHTML = 'phtml';

    /**
     * the source files
     *
     * @var  FileSet
     */
    protected $_filesets = array();

    /**
     * Whether the build should fail, if
     * errors occured
     *
     * @var boolean
     */
    protected $_failonerror = false;
    /**
     * directory to put minified javascript files into
     *
     * @var  string
     */
    protected $_targetDir;

    /**
     * Dateityp
	 * @var string
     */
    protected $_type = null;

    /**
     * Nested creator, adds a set of files (nested fileset attribute).
     */
    public function createFileSet()
    {
        $num = array_push($this->_filesets, new FileSet());
        return $this->_filesets[$num - 1];
    }

    public function setType($type)
    {
        $this->_type = $type;
    }

    /**
     * Whether the build should fail, if an error occured.
     *
     * @param boolean $value
     */
    public function setFailonerror($value)
    {
        $this->_failonerror = $value;
    }

    /**
     * The init method: Do init steps.
     */
    public function init()
    {
        return true;
    }

    /**
     * The main entry point method.
     */
    public function main()
    {
        foreach ($this->_filesets as $fs) {
            try {
                $files = $fs->getDirectoryScanner($this->project)->getIncludedFiles();
                $fullPath = realpath($fs->getDir($this->project));
                foreach ($files as $file) {
                    $this->log('Minifying file ' . $file);
                    $filename = $fullPath . '/' . $file;

                    try {
                        $content = file_get_contents($filename);

                        switch ($this->_type) {
                            case self::TYPE_CSS:
                                $content = Minify_CSS::minify($content);
                                break;
                            case self::TYPE_JS:
                                $content = JSMin::minify($content);
                                break;
                            case self::TYPE_PHTML:
                                $content = Minify_PHTML::minify($content);
                                break;
                            default:
                                throw new BuildException('Type unknown');
                                break;
                        }

                        file_put_contents($filename, $content);
                    } catch (Exception $e) {
                        $this->log('Could not minify file ' . $file . ': ' . $e->getMessage(), Project::MSG_ERR);
                    }
                }
            } catch (BuildException $be) {
                // directory doesn't exist or is not readable
                if ($this->_failonerror) {
                    throw $be;
                } else {
                    $this->log($be->getMessage(), Project::MSG_WARN);
                }
            }
        }
    }
}
