<?php
/**
 * File for DatabaseDeployTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id:$
 */

require_once 'phing/Task.php';

/**
 * DbDeploy Task
 * http://dbdeploy.com/
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2011, Unister GmbH
 */
class DatabaseDeployTask extends Task
{
    /**
     * Versionierungstabelle
     * @var string
     */
    const TABLE_NAME = '_DbVersion';

    /**
     * Metatag Pattern
     * @var string
     */
    const META_TAG_PATTERN = '~//@([^\s]+) (.+)$~';

    /**
     * Undo Marker
     * @var string
     */
    const UNDO_PATTERN = '~//@UNDO~';

    /**
     * Verzeichnis der DbDeploy-Scripte
     * @var string
     */
    protected $_dir;

    /**
     * Database Url
     * @var string
     */
    protected $_url;

    /**
     * Database Username
     * @var string
     */
    protected $_username;

    /**
     * Database Password
     * @var string
     */
    protected $_password;

    /**
     * Ausgabedatei für Do-Tasks
     * @var string
     */
    protected $_outputFile = 'dbdeploy_deploy.sql';

    /**
     * Ausgabedatei für Undo-Tasks
     * @var string
     */
    protected $_undoOutputFile = 'dbdeploy_undo.sql';

    /**
     * Bereits durchgeführte Changes
     * @var string
     */
    protected $_appliedChangeNumbers = array();

    /**
     * Exception werfen bevor DB-Änderungen vorgenommen werden
     * @var bool
     */
    protected $_exceptionIfChangesExist = false;

    /**
     * Propertyname der Version
     * @var string
     */
    protected $_versionProperty;

    /**
     * Propertyname für Flag, ob Änderungen durchgeführt wurden
     * @var string
     */
    protected $_changedProperty;

    /**
     * (non-PHPdoc)
     * @see Task::main()
     */
    public function main()
    {
        try {
            // open file handles for output
            $outputFileHandle = fopen($this->_outputFile, 'w+');
            $undoOutputFileHandle = fopen($this->_undoOutputFile, 'w+');

            // figure out which revisions are in the db already
            $this->_appliedChangeNumbers = $this->getAppliedChangeNumbers();
            $this->log('Current db revision: ' . $this->getLastChangeAppliedInDb());

            // generate sql file needed to take db to "lastChangeToApply" version
            $doSql = $this->doDeploy();
            $undoSql = $this->undoDeploy();

            // write the do and undo SQL to their respective files
            fwrite($outputFileHandle, $doSql);
            fwrite($undoOutputFileHandle, $undoSql);

        } catch (Exception $e) {
            throw new BuildException($e);
        }
    }

    /**
     * Ermittelt die aktuelle Datenbankversion und gibt alle bereits
     * durchgeführten Änderungen zurück
     *
     * @return array
     */
    public function getAppliedChangeNumbers()
    {
        if (count($this->_appliedChangeNumbers) == 0) {
            $this->log('Getting applied changed numbers from DB: ' . $this->_url);

            $appliedChangeNumbers = array();

            $dbh = $this->_createPDO($this->_url, $this->_username, $this->_password);
            $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

            $tables = array();
            foreach ($dbh->query('SHOW TABLES LIKE \'' . self::TABLE_NAME .'\'') as $table) {
                $tables[] = $table[0];
            }
            if (count($tables) == 0) {
                $schema = file_get_contents(dirname(__FILE__) . '/DatabaseDeploy/schema.sql');
                $dbh->query($schema);
            } elseif (!in_array(self::TABLE_NAME, $tables) && count($tables) > 0) {
                throw new BuildException('Database not compatible');
            }

            $sql = 'SELECT * FROM ' . DatabaseDeployTask::TABLE_NAME
                . ' ORDER BY releaseNumber';

            foreach ($dbh->query($sql) as $change) {
                $appliedChangeNumbers[] = $change['releaseNumber'];
            }
            $this->_appliedChangeNumbers = $appliedChangeNumbers;
        }
        return $this->_appliedChangeNumbers;
    }

    /**
     * Erzeugt eine PDO Instanz
     *
     * @codeCoverageIgnore
     *
     * @param  string $url
     * @param  string $username
     * @param  string $password
     * @return PDO
     */
    protected function _createPDO($url, $username, $password)
    {
        return new PDO($url, $username, $password);
    }

    /**
     * Gibt die aktuelle Datenbankversion zurück
     *
     * @return int
     */
    public function getLastChangeAppliedInDb()
    {
        return (count($this->_appliedChangeNumbers) > 0) ? max($this->_appliedChangeNumbers) : 0;
    }

    /**
     * Gibt alle offenen Anweisungen aus
     *
     * @return string
     */
    public function doDeploy()
    {
        $sqlToPerformDeploy = '';
        $lastChangeAppliedInDb = $this->getLastChangeAppliedInDb();
        $files = $this->getDeltasFilesArray();
        ksort($files);

        $changesExist = false;

        foreach ($files as $fileChangeNumber => $fileName) {
            if ($fileChangeNumber > $lastChangeAppliedInDb) {
                if (!$changesExist) {
                    $changesExist = true;
                }
                $changeset = $this->_parse($this->_dir . DIRECTORY_SEPARATOR . $fileName);

                $sqlToPerformDeploy .= '-- Fragment begins: ' . $fileChangeNumber . ' --' . "\n";
                $sqlToPerformDeploy .= 'INSERT INTO ' . DatabaseDeployTask::TABLE_NAME
                    . ' (releaseNumber, appliedBy, description)'
                    . ' VALUES (' . $fileChangeNumber . ', '
                    . ' "' . $changeset['meta']['appliedBy'] . '", "'
                    . $changeset['meta']['description'] . '");' . "\n";

                $deploySQLFromFile = implode("\n", $changeset['do']);
                $sqlToPerformDeploy .= $deploySQLFromFile . "\n";
                $sqlToPerformDeploy .= 'INSERT INTO ' . DatabaseDeployTask::TABLE_NAME
                    . ' SET applied  = NOW(), '
                    . ' releaseNumber = ' . $fileChangeNumber . ', '
                    . ' appliedBy = "' . $changeset['meta']['appliedBy'] . '", '
                    . ' description = "' . $changeset['meta']['description'] . '" '
                    . ' ON DUPLICATE KEY UPDATE applied = NOW();' . "\n";
                $sqlToPerformDeploy .= '-- Fragment ends: ' . $fileChangeNumber . ' --' . "\n";
            }
        }

        $nextVersion = !empty($files) ? max(array_keys($files)) : 1;

        $this->getProject()->setProperty($this->_versionProperty, $nextVersion);
        $this->getProject()->setProperty($this->_changedProperty, ($sqlToPerformDeploy != ''));

        if ($changesExist && $this->_exceptionIfChangesExist) {
            throw new BuildException('DB changes exist');
        }

        return $sqlToPerformDeploy;
    }

    /**
     * Ermittelt Metainformationen
     *
     * @return array
     */
    protected function _parse($filename)
    {
        $result = array(
            'do' => array(),
            'undo' => array(),
            'meta' => array(
                'description' => '',
                'appliedBy' => ''
            )
        );
        $contents = file_get_contents($filename);
        $lines = explode("\n", $contents);
        $currentSection = 'do';

        foreach ($lines as $line) {
            $match = array();
            if (preg_match(self::META_TAG_PATTERN, $line, $match)) {
                $result['meta'][$match[1]] = $match[2];
            } elseif (preg_match(self::UNDO_PATTERN, $line, $match)) {
                $currentSection = 'undo';
            } else {
                $result[$currentSection][] = $line;
            }
        }

        return $result;
    }

    /**
     * Gibt alle offenen Anweisungen aus
     *
     * @return string
     */
    public function undoDeploy()
    {
        $sqlToPerformUndo = '';
        $lastChangeAppliedInDb = $this->getLastChangeAppliedInDb();
        $files = $this->getDeltasFilesArray();
        krsort($files);

        foreach ($files as $fileChangeNumber=>$fileName) {
            if ($fileChangeNumber > $lastChangeAppliedInDb) {

                $changeset = $this->_parse($this->_dir . DIRECTORY_SEPARATOR . $fileName);

                $sqlToPerformUndo .= '-- Fragment begins: ' . $fileChangeNumber . ' --' . "\n";
                $sqlToPerformUndo .= implode("\n", $changeset['undo']) . "\n";
                $sqlToPerformUndo .= 'DELETE FROM ' . DatabaseDeployTask::TABLE_NAME
                    . ' WHERE releaseNumber = ' . $fileChangeNumber . ';' . "\n";

                $sqlToPerformUndo .= '-- Fragment ends: ' . $fileChangeNumber . ' --' . "\n";
            }
        }

        return $sqlToPerformUndo;
    }

    /**
     * Gibt alle Deltafiles zurück
     *
     * @return array
     */
    public function getDeltasFilesArray()
    {
        $baseDir = realpath($this->_dir);
        $dh = opendir($baseDir);
        $fileChangeNumberPrefix = '';

        $files = array();

        while (($file = readdir($dh)) !== false) {
            if (preg_match('[\d+]', $file, $fileChangeNumberPrefix)) {
                $files[intval($fileChangeNumberPrefix[0])] = $file;
            }
        }
        return $files;
    }

    /**
     * Setzt Pfad
     *
     * @param string $dir
     */
    public function setDir($dir)
    {
        $this->_dir = $dir;
    }

    /**
     * Setzt Database-Uri
     *
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->_url = $url;
    }

    /**
     * Setzt Db-Username
     *
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->_username = $username;
    }

    /**
     * Setzt Db-Password
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->_password = $password;
    }

    /**
     * Setzt Ausgabedatei
     *
     * @param string $outputFile
     */
    public function setOutputFile($outputFile)
    {
        $this->_outputFile = $outputFile;
    }

    /**
     * Setzt Ausgabedatei (Undo)
     *
     * @param string $undoOutputFile
     */
    public function setUndoOutputFile($undoOutputFile)
    {
        $this->_undoOutputFile = $undoOutputFile;
    }

    /**
     * Setzt Version Propertyname
     *
     * @param string $name
     */
    public function setVersionProperty($name)
    {
        $this->_versionProperty = $name;
    }

    /**
     * Setzt Changed Propertyname
     *
     * @param string $name
     */
    public function setChangedProperty($name)
    {
        $this->_changedProperty = $name;
    }

    /**
     * Add a new fileset
     *
     * @return FileSet
     */
    public function createFileSet()
    {
        $this->fileset = new FileSet();
        $this->filesets[] = $this->fileset;

        return $this->fileset;
    }

    /**
     * @param boolean $exceptionIfChangesExist
     */
    public function setExceptionIfChangesExist($exceptionIfChangesExist)
    {
        $this->_exceptionIfChangesExist = (bool) $exceptionIfChangesExist;
    }
}