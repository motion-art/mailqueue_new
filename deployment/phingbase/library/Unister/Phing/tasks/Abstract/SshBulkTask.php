<?php
/**
 * File for CheckRequirementsTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id$
 */

require_once 'tasks/SshExecTask.php';

/**
 * Überprüft Abhängigkeiten der Software gegen die angegebenen Hosts der Liste
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2010, Unister GmbH
 */
abstract class Abstract_SshBulkTask extends SshExecTask
{
    /**
     * kommaseparierte Liste der Keys
     * @var string
     */
    protected $_list = null;

    /**
     * Listenprefix
     * @var string
     */
    protected $_prefix = 'list';

    /**
     * @param string $list
     */
    public function setList($list)
    {
        $this->_list = $list;
    }

    /**
     * Vordefinierter Prefix ist änderbar (NUR machen, wenn du weißt, was du tust!)
     *
     * @param string $prefix
     */
    public function setPrefix($prefix)
    {
        $this->_prefix = $prefix;
    }

    /**
     * Baut den Kommandostring zusammen und führt den SSH-Befehl aus
     */
    public function getHosts()
    {
        if (empty($this->_list)) {
            throw new BuildException('You must provide a remote host list');
        }

        $hosts = array();
        foreach (explode(',', $this->_list) as $key) {

            $user = $this->project->getProperty($this->_prefix . '.' . $key . '.user');
            if ($user == null) {
                $user = $this->project->getProperty($this->_prefix . '.user');
            }

            $hosts[] = array(
                'host' => $this->project->getProperty($this->_prefix . '.' . $key . '.host'),
                'user' => $user,
                'vhost' => $this->project->getProperty($this->_prefix . '.' . $key . '.vhost')
            );
        }

        return $hosts;
    }
}
