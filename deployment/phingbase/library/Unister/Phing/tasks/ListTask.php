<?php
/**
 * File for the class ListTask.php
 *
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     c.klaeren
 * @version    $Id$
 */

require_once 'classes/PrettyPrinter.php';

/**
 * list all available targets
 * (ignores tasks with preceding underscore, as by convention,
 * they're considered private
 *
 * @copyright  Copyright (c) 2012 Unister GmbH
 */
class ListTask extends Task
{
    /**
     * @see Task::main()
     */
    public function main()
    {
        echo PrettyPrinter::YELLOW
            . PrettyPrinter::BOLD
            . '             '
            . '============= Targets ============= '
            . PrettyPrinter::RESET
            . PHP_EOL
            . PHP_EOL;

        PrettyPrinter::printAnsi($this->_getPublicTargets());
    }

    protected function _getPublicTargets()
    {
        $targets = $this->project->getTargets();
        $publicTargets = array();

        foreach ($targets as $target) {
            /** Task $target */
             $name = $target->getName();
             if (strpos($name, '_') === 0) {
                 continue;
             }
             if (strpos($name, '.') !== false) {
                 continue;
             }
             $publicTargets[$name] = $target->getDescription();
        }
        ksort($publicTargets);

        return $publicTargets;
    }
}