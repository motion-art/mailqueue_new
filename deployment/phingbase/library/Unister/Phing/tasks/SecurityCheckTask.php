<?php
/**
 * File for SecurityCheckTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id$
 */
use SensioLabs\Security\SecurityChecker;

require_once 'phing/Task.php';

/**
 * Check a lockfile for security issues
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2011, Unister GmbH
 */
class SecurityCheckTask extends Task
{
    /**
    * Working Copy to check
    *
    * @var string
    */
    protected $_lock = null;

    protected $_strict = false;

    /**
     * @param string $dir
     */
    public function setLock($filename)
    {
        $this->_lock = $filename;
    }

    public function setStrict($value)
    {
        $this->_strict = $value;
    }

    public function main()
    {
        if (!file_exists($this->_lock) === true) {
            throw new BuildException('Lockfile "' . $this->_lock . '" not found');
        }

        $checker = new SecurityChecker();
        $alerts = json_decode($checker->check($this->_lock, 'json'));

        foreach ($alerts as $name => $alert) {
            $packageInfo = sprintf('Found security advisories for package: %s (%s)', $name, $alert->version);
            $this->log($packageInfo, Project::MSG_WARN);
            foreach ($alert->advisories as $advisory) {
                $this->log(sprintf('Issue: %s (%s)', $advisory->title, $advisory->link), Project::MSG_WARN);
            }
            if ($this->_strict) {
                throw new BuildException($packageInfo);
            }
        }
    }
}