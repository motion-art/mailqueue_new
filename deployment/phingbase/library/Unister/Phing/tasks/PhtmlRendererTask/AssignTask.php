<?php
/**
 * @category    Phing
 * @package     Phing_Task
 * @subpackage  PhtmlRendererTask
 * @copyright   Copyright (c) 2013 Unister GmbH
 * @author      Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author      Tommy Helm <tommy.helm@unister-gmbh.de>
 * @version     $Id$
 */

/**
 * "Creator-Klasse" für das phtmlRenderer-Task zum Umsetzen der eingeschlossenen assign-Tags.
 *
 * @category    Phing
 * @package     Phing_Task
 * @subpackage  PhtmlRendererTask
 * @copyright   Copyright (c) 2013 Unister GmbH
 */
class PhtmlRendererTask_AssignTask extends Task
{
    /**
     * @var string
     */
    protected $_name;

    /**
     * @var string
     */
    protected $_value;

    /**
     * Ob der übergebene Wert JSON-kodiert ist. So können komplexere Strukturen übergeben werden, die innerhalb
     * der Templates als stdClass-Objekte verwendet werden können. Der Default ist false.
     *
     * @var boolean
     */
    protected $_jsonEncoded = false;

    /**
     * (non-PHPdoc)
     * @see Task::main()
     */
    public function main()
    {
        if (empty($this->_name)) {
            throw new BuildException('The property name is required', $this->location);
        }
        if (empty($this->_value)) {
            throw new BuildException('The property value is required', $this->location);
        }
    }

    /**
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @param string $value
     * @return void
     */
    public function setValue($value)
    {
        $this->_value = $value;
    }

    /**
     * @param boolean $jsonEncoded
     * @return void
     */
    public function setJsonEncoded($jsonEncoded = false)
    {
        $this->_jsonEncoded = $jsonEncoded;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->_name;
    }

    /**
     * Falls der Wert JSON-kodiert übergeben wurde, wird er auch JSON-kodiert zurück gegeben.
     *
     * @return string
     */
    public function getValue()
    {
        return $this->_value;
    }

    /**
     * @return boolean
     */
    public function isJsonEncoded()
    {
        return $this->_jsonEncoded;
    }
}