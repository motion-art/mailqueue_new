<?php
/**
 * File for SvnExternalMigratorTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id$
 */

require_once 'phing/Task.php';
require_once 'classes/Repository/Subversion.php';

/**
 * Check a svn-repository for outdated externals
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2011, Unister GmbH
 */
class SvnExternalMigratorTask extends Task
{
    /**
    * Working Copy to check
    *
    * @var string
    */
    protected $_directory = null;

    /**
     * Propertyname für Flag, ob Änderungen durchgeführt wurden
     * @var string
     */
    protected $_outputProperty;

    protected $_aliasConfig;

    /**
     * Setzt Changed Propertyname
     *
     * @param string $name
     */
    public function setOutputProperty($name)
    {
        $this->_outputProperty = $name;
    }

    /**
     * @param string $dir
     */
    public function setDir($dir)
    {
        $this->_directory = $dir;
    }

    public function setAliasConfig($filename)
    {
        $this->_aliasConfig = $filename;
    }

    public function main()
    {
        if (empty($this->_directory) === true) {
            throw new BuildException('Directory not set');
        }
        if (!file_exists($this->_directory) === true) {
            throw new BuildException('Directory not found');
        }
        if (!file_exists($this->_aliasConfig) === true) {
            throw new BuildException('Alias configuration not found');
        }

        $lockedPackages = array(
            'packages' => array()
        );
        $subversion = new Repository_Subversion();
        $properties = $subversion->getProperties($this->_directory);

        foreach ($properties->target as $target) {

            $aliases = new Package_Aliases();
            $aliases->load($this->_aliasConfig);

            $externals = explode(PHP_EOL, (string) $target->property);
            $packages = $aliases->mapExternals($externals);

            foreach ($packages as $name => $version) {
                $lockedPackages['packages'][] = array(
                    'name' => $name,
                    'version' => $version
                );
            }
        }

        $this->getProject()->setProperty($this->_outputProperty, json_encode($lockedPackages));
    }
}