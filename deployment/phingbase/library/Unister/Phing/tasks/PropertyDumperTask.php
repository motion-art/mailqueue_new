<?php
/**
 * File for the class PropertyDumper.php
 *
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     c.klaeren
 * @version    $Id$
 */

require_once 'classes/PrettyPrinter.php';

/**
 * yeah, it just dumps all build properties to the console
 *
 * @copyright  Copyright (c) 2012 Unister GmbH
 */
class PropertyDumperTask extends Task
{
    /**
     * @see Task::main()
     */
    public function main()
    {
        $properties = $this->project->getProperties();
        /**
         *  by default this also includes all the boring environment variables from the shell.
         *   those are totally not needed. as we totally could get them via calling 'env'
         *   inside a terminal.
         */
         echo PrettyPrinter::YELLOW
            . PrettyPrinter::BOLD
            . '             '
            . '============= Properties ============= '
            . PrettyPrinter::RESET
            . PHP_EOL
            . PHP_EOL;

        foreach (array_keys($properties) as $propertyName ) {
            if (strpos($propertyName, 'env') === 0) {
                unset ($properties[$propertyName]);
            }
        }
        ksort($properties);

        PrettyPrinter::printArray($properties);

        echo PHP_EOL
            . PrettyPrinter::YELLOW
            . PrettyPrinter::BOLD
            . ' ####### '
            . 'all system-wide Enviromentvariables are available via env.KEYNAME'
            . ' #######'
            . PrettyPrinter::RESET
            . PHP_EOL
            . PHP_EOL;
    }
}