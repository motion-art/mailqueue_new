<?php
/**
 * Datei für ErrorTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Gordon Schmidt <gordon.schmidt@unister.de>
 * @version    $Id$
 */

//require_once 'phing/Task.php';

/**
 * Beendet PHP
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 */
class ErrorTask extends Task
{

    /**
     * Soll Fatal Error erzeugt werden?
     *
     * @var boolean
     */
    protected $_fatal = false;

    /**
     * Setze Fatal Error
     *
     * @param boolean $fatal
     */
    public function setFatal($fatal)
    {
        $this->_fatal = $fatal;
    }

    /**
     * Beende PHP
     */
    public function main()
    {
        if ($this->_fatal) {
            $this->callNoneExistingMethod();
        }
        exit();
    }
}
