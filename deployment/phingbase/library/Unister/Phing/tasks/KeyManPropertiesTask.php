<?php
/**
 * File for KeyManPropertiesTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Gennadiy Litvinyuk <gennadiy.litvinyuk@unister-gmbh.de>
 * @version    $Id$
 */

require_once 'phing/Task.php';

/**
 * KeyMan properties task.
 *
 * This task reads a list of properties and urls and makes requests to KeyMan server.
 * If requests are successfull, the result are saved in properties.
 *
 * Usage example:
 * in build.xml:
 * <target>
 *     <keyman />
 * </target>
 *
 * in build.xml:
 *
 * keyman.properties = pass,user
 * keyman.pass.name = db.password
 * keyman.pass.url  = http://keyman/pass/
 * keyman.user.name = db.username
 * keyman.user.url  = http://keyman/username/
 *
 * Or with customised keys:
 * in build.xml:
 * <target>
 *     <keyman key="keymanlive" />
 * </target>
 *
 * in build.xml:
 *
 * keymanlive.properties = pass,user
 * keymanlive.pass.name = db.password
 * keymanlive.pass.url  = http://keyman/pass/
 * keymanlive.user.name = db.username
 * keymanlive.user.url  = http://keyman/username/
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Gennadiy Litvinyuk <gennadiy.litvinyuk@unister-gmbh.de>
 *
 */
class KeyManPropertiesTask extends Task
{

    /**
     * Properties prefix.
     *
     * @var string
     */
    protected $_key = 'keyman';

    /**
     * @inheritdoc
     */
    public function main()
    {
        $propertiesList = $this->_getPropertiesList();

        if (empty($propertiesList)) {
            throw new BuildException('No keyman properties are found');
        }

        $propertiesValues = $this->_getPropertiesValues($propertiesList);

        foreach ($propertiesValues as $name => $value) {
            $this->project->setProperty($name, $value);
        }
    }

    /**
     * Sets properties prefix.
     *
     * @param string $key
     */
    public function setKey($key)
    {
        $this->_key = $key;
    }

    /**
     * Returns properties prefix.
     *
     * @return string
     */
    public function getKey()
    {
        return $this->_key;
    }

    /**
     * Returns an array of properties to be read.
     *
     * @return string[]
     */
    protected function _getPropertiesList()
    {
        $list = $this->project->getProperty($this->_key . '.' . 'properties');
        return split(',', $list);
    }

    protected function _getPropertiesValues($list)
    {
        $result = array();
        foreach ($list as $propertyName) {
            $name  = $this->project->getProperty($this->_getNameKey($propertyName));
            $value = $this->_getContent($this->project->getProperty($this->_getUrlKey($propertyName)));
            $result[$name] = $value;
        }
        return $result;
    }

    /**
     * Returns name key
     *
     * @param string $name
     * @return string
     */
    protected function _getNameKey($name)
    {
        return  $this->_key . '.' . $name . '.name';
    }

    /**
     * Returns URL key
     *
     * @param string $name
     * @return string
     */
    protected function _getUrlKey($name)
    {
        return $this->_key . '.' . $name . '.url';
    }

    protected function _getContent($url)
    {
        $request = $this->_getRequest($url);

        try {
            $response = $request->send();
            if (200 == $response->getStatus()) {
                return $response->getBody();
            } else {
                throw new BuildException(
                    'Unexpected HTTP status: ' . $response->getStatus() . ' ' . $response->getReasonPhrase()
                );
            }
        } catch (HTTP_Request2_Exception $e) {
            throw new BuildException('Error', $e);
        }
    }

    /**
     * Creates an HTTP_Request2 instance
     *
     * @codeCoverageIgnore
     *
     * @param string $url
     */
    protected function _getRequest($url)
    {
        return new HTTP_Request2($url, HTTP_Request2::METHOD_GET);
    }
}
