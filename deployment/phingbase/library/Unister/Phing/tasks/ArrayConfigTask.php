<?php
/**
 * Erstellung von PHP-Array-Konfigurationen aus anderen Konfigurationsformaten (xml, json, yaml, yml, ini, inc)
 *
 * @category Phing
 * @package Phing_Task
 * @copyright Copyright (c) 2013 Unister GmbH
 * @author Unister GmbH <entwicklung@unister.de>
 * @author Erik Witthauer <erik.witthauer@unister.de>
 * @author Gordon Schmidt <gordon.schmidt@unister.de>
 * @version $Id$
 */
function psr0Autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    $lastNsPos = strrpos($className, '\\');
    if (false !== $lastNsPos) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    // prüfe, ob Datei im include_path gefunden werden kann
    if (false !== stream_resolve_include_path($fileName)) {
        require $fileName;
    }
}
spl_autoload_register('psr0Autoload');

/**
 * Erzeugt PHP-Array-Konfiguration
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2013 Unister GmbH
 */
class ArrayConfigTask extends Task
{
    /**
     * Quelldatei der Konfiguration
     * @var string
     */
    protected $_source;

    /**
     * Zieldatei der Konfiguration
     * @var string
     */
    protected $_destination;

    /**
     * Applikationsumgebung
     * @var string
     */
    protected $_applicationEnv;

    /**
     * Quelldatei der Konfiguration
     * @param string $source
     */
    public function setSource($source)
    {
        $this->_source = (string)$source;
    }

    /**
     * Zieldatei der Konfiguration
     * @param string $destination
     */
    public function setDestination($destination)
    {
        $this->_destination = (string)$destination;
    }

    /**
     * Setze Applikationsumgebung
     * @param string $constants
     */
    public function setApplicationEnv($applicationEnv)
    {
        $this->_applicationEnv = (string)$applicationEnv;
    }

    /**
     * Erzeugt PHP-Array-Konfiguration
     * @throws BuildException
     */
    public function main()
    {
        if ($this->_source === null) {
            throw new BuildException("source attribute is required", $this->location);
        }
        if ($this->_destination === null) {
            throw new BuildException("destination attribute is required", $this->location);
        }
        if ($this->_applicationEnv === null) {
            throw new BuildException("applicationEnv attribute is required", $this->location);
        }
        $config = $this->_loadConfig($this->_source);
        $writer = new Zend_Config_Writer_Array();
        $writer->write($this->_destination, $config, true);
    }

    /**
     * Lade Konfiguration aus einer Datei
     * @param string $fileName
     * @return Zend_Config Description
     * @throws BuildException
     */
    protected function _loadConfig($fileName)
    {
        $fileExt = pathinfo($fileName, PATHINFO_EXTENSION);
        $knownExtensions = array('xml', 'json', 'yml', 'ini', 'yaml');
        if (in_array($fileExt, $knownExtensions)) {
            $fileExt = ($fileExt === 'yml') ? 'yaml': $fileExt;
            $class = 'Zend_Config_' . ucfirst($fileExt);
            return new $class($fileName, $this->_applicationEnv);
        } elseif ($fileExt === 'inc') {
            $configRead = include $fileName;
            if (!is_array($configRead)) {
                throw new BuildException(
                    'Invalid configuration file provided; PHP file does not return array value! (' . $fileName . ')'
                );
            }
            $config = isset($configRead[$this->_applicationEnv]) ? $configRead[$this->_applicationEnv] : array();
            return new Zend_Config($config);
        }
        throw new BuildException('Invalid configuration file provided; unknown config type! (' . $fileName . ')');
    }
}
