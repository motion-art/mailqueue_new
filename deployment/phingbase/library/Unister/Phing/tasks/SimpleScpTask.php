<?php
/**
 * File for SimpleScpTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id$
 */

require_once 'phing/Task.php';

/**
 * Kopiert Dateien per SCP
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2010, Unister GmbH
 */
class SimpleScpTask extends Task
{
    /**
     * SCP-Kommando
     *
     * @var string
     */
    protected $_cmd = 'scp ';

    /**
     * Array mit Argumenten
     *
     * @var array
     */
    protected $_cmdArgs = array();

    /**
     * @var string
     */
    protected $_remoteUser;

    /**
     * @var string
     */
    protected $_remotePassword;

    /**
     * @var string
     */
    protected $_remoteHost;

    /**
     * @var string
     */
    protected $_source;

    /**
     * @var string
     */
    protected $_target;

    /**
     * @var boolean
     */
    protected $_haltOnFailure = false;

    /**
     * Property name to set with return value from exec call.
     *
     * @var string
     */
    protected $_returnProperty;

    /**
     * Property name to set with output value from exec call.
     *
     * @var string
     */
    protected $_outputProperty;


    /**
     * Fügt Argument an den Befehl an
     *
     * @param string $argument
     */
    public function appendCmdArg($argument)
    {
        $this->_cmdArgs[] = $argument;
    }

    /**
     * Setzt SSH Benutzer für das Remotesystem
     *
     * @param string $username
     */
    public function setRemoteUser($username)
    {
        $this->_remoteUser = $username;
    }

    /**
     * Setzt Hostname für das Remotesystem
     *
     * @param string $hostname
     */
    public function setRemoteHost($host)
    {
        $this->_remoteHost = $host;
    }

    /**
     * Befehl, der Remote ausgeführt werden soll
     *
     * @param string $command
     */
    public function setSource($source)
    {
        $this->_source = $source;
    }

    /**
     * Frei zu definierende SSH-Argumente
     *
     * @param string $arguments
     */
    public function setArgs($arguments)
    {
        $this->appendCmdArg($arguments);
    }

    /**
     * Soll Ausführung bei Fehler beendet werden?
     *
     * @param boolean $flag
     */
    public function setHaltOnFailure($flag)
    {
        $this->_haltOnFailure = (bool)$flag;
    }

    /**
     * Verzeichnis, in das gewechselt wird vor Befehlsausführung
     *
     * @param string $directory
     */
    public function setTarget($directory)
    {
        $this->_target = $directory;
    }

    /**
     * The name of property to set to return value from exec() call.
     *
     * @param string $prop Property name
     *
     * @return void
     */
    public function setReturnProperty($prop)
    {
        $this->_returnProperty = $prop;
    }

    /**
     * The name of property to set to output value from exec() call.
     *
     * @param string $prop Property name
     *
     * @return void
     */
    public function setOutputProperty($prop)
    {
        $this->_outputProperty = $prop;
    }


    /**
     * Baut den Kommandostring zusammen und führt den SSH-Befehl aus
     */
    public function main()
    {
        if (empty($this->_remoteHost)) {
            throw new BuildException('You must provide a remote host');
        }

        if (empty($this->_source)) {
            throw new BuildException('You must provide a source path');
        }

        if (empty($this->_target)) {
            throw new BuildException('You must provide a target path');
        }

        $commandString = $this->_remoteHost . ':' . $this->_target;

        if (!empty($this->_remoteUser)) {
            $commandString = $this->_remoteUser . '@' . $commandString;
        }

        $commandString = $this->_source . ' ' . $commandString;

        $this->log('Copy "' . $this->_source . '" to host "' . $this->_remoteHost . '"');

        $cmd = $this->_cmd . implode(' ', $this->_cmdArgs) . ' ' . $commandString;
        $output = array();
        $this->_performExecCommand($cmd, $output, $exitCode);

        if ($this->_returnProperty) {
            $this->project->setProperty($this->_returnProperty, $exitCode);
        }

        if ($this->_outputProperty) {
            $this->project->setProperty($this->_outputProperty, implode("\n", $output));
        }

        foreach ($output as $line) {
            $this->log($line, Project::MSG_VERBOSE);
        }

        if ($exitCode != 0 && $this->_haltOnFailure) {
            throw new BuildException('Task exited with code ' . $exitCode);
        }
    }

    /**
     * Performs a call to exec()
     *
     * @param  string  $cmd
     * @param  string  &$output
     * @param  integer &$exitCode
     * @return void
     */
    protected function _performExecCommand($cmd, &$output, &$exitCode)
    {
        exec($cmd, $output, $exitCode);
    }
}