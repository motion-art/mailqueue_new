<?php
/**
 * Validierung einer gegebenen Versionsnummer nach Semantic Versioning 2.0.0-rc.1 (http://semver.org).
 *
 * @category Phing
 * @package Phing_Task
 * @copyright Copyright (c) 2013 Unister GmbH
 * @author Unister GmbH <entwicklung@unister.de>
 * @author Erik Witthauer <erik.witthauer@unister.de>
 * @author Gordon Schmidt <gordon.schmidt@unister.de>
 * @version $Id$
 */

/**
 * Überprüft die Versionsnummer
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2013 Unister GmbH
 */
class ValidVersionTask extends Task implements Condition
{
    /**
     * Name der Property, die gesetzt wird
     * @var string
     */
    protected $_property;

    /**
     * Versionsnummer
     * @var string
     */
    protected $_version;

    /**
     * Setze den Namen der Property
     *
     * @param string $property
     */
    public function setProperty($property)
    {
        $this->_property = (string)$property;
    }

    /**
     * Setze die Versionsnummer
     *
     * @param string $version
     */
    public function setVersion($version)
    {
        $this->_version = (string)$version;
    }

    /**
     * Setze die Property auf den Prüfwert der Versionsnummer
     *
     * @throws BuildException
     * @return boolean true, wenn valide - sonst false
     */
    public function main()
    {
        if ($this->_property === null) {
            throw new BuildException("property attribute is required", $this->location);
        }
        //$value = $this->evaluate() ? 'true': 'false';
        $this->project->setProperty($this->_property, $this->evaluate());
    }

    /**
     * Überprüfe die Versionsnummer
     *
     * @throws BuildException
     * @return boolean
     */
    public function evaluate()
    {
        if ($this->_version === null) {
            throw new BuildException("version attribute is required", $this->location);
        }
        $pattern = "/^(\d+)\.(\d+)\.(\d+)(?:-([\da-z\-]+(?:\.[\da-z\-]+)*))?(?:\+([\da-z\-]+(?:\.[\da-z\-]+)*))?$/i";

        if (preg_match($pattern, $this->_version)) {
            return true;
        }
        return false;
    }
}
