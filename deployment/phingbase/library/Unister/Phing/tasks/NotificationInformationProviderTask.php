<?php
/**
 * @category    Phing
 * @package     Phing_Task
 * @copyright   Copyright (c) 2013 Unister GmbH
 * @author      Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author      Tommy Helm <tommy.helm@unister-gmbh.de>
 * @version     $Id$
 */

/**
 * Task zum Beschaffen von Informationen, die benötigt werden, um Release-Mails zu erstellen. Allgemein lassen sich
 * diese Informationen für alle Targets verwenden, die für automatisierte Änderungsinformationen zuständig sind.
 *
 * Dieses Task macht selber kaum was, sondern delegiert die Arbeit an andere Klassen und setzt entsprechend ein
 * angegebenes Property. Alle Arten von Informationen werden zur Zeit anhand von Dateien beschafft, dessen Pfad dem
 * Task übergeben werden muss.
 *
 * @category    Phing
 * @package     Phing_Task
 * @copyright   Copyright (c) 2013 Unister GmbH
 */

class NotificationInformationProviderTask extends Task
{
    /**
     * Möglicher Werte des Attributs type. Wenn gesetzt, wird eine Change-Log-Datei im angebenen Pfad
     * (Attribut: path) ausgelesen.
     *
     * @see Notification_ChangeLogReader
     * @var string
     */
    const TYPE_CHANGE_LOG = 'changeLog';

    /**
     * Möglicher Werte des Attributs type. Wenn gesetzt, wird eine Release-Notes-Datei im angebenen Pfad
     * (Attribut: path) ausgelesen.
     *
     * @see Notification_ReleaseNotesReader
     * @var string
     */
    const TYPE_RELEASE_NOTES = 'releaseNotes';

    /**
     * Möglicher Werte des Attributs type. Wenn gesetzt, wird eine Split-Test-XML-Datei im angebenen Pfad
     * (Attribut: path) ausgelesen. Das Ergebnis ist ein JSON-kodiertes Array mit stdClass-Objekten.
     *
     * @see Notification_SplitTestXMLReader
     * @var string
     */
    const TYPE_SPLITS = 'splits';

    /**
     * Alle unterstützten Typen in einem Array gesammelt, gemappt zu einem Funktionsnamen dieser Klasse, die die
     * entsprechende Information beschafft.
     *
     * @var array
     */
    protected $_availableTypes = array(
        self::TYPE_CHANGE_LOG    => '_provideChangeLog',
        self::TYPE_RELEASE_NOTES => '_provideReleaseNotes',
        self::TYPE_SPLITS        => '_provideSplits'
    );

    /**
     * Die hier angebenen Typen benötigen das Property version, damit sie richtig funktionieren können. Wird für
     * diese Typen keine Version angegeben schlägt die Parameter-Validation fehl.
     *
     * @var array
     */
    protected $_typesWhichRequireVersionProperty = array(self::TYPE_CHANGE_LOG, self::TYPE_RELEASE_NOTES);

    /**
     * Name des Properties, dass immer das Ergebnis einer Anfrage nach der erfolgreichen Ausführung des Tasks enthält.
     *
     * @var string
     */
    protected $_property;

    /**
     * Art der Information, die beschafft werden soll. Für mögliche Typen siehe die TYPE-Konstanten dieser Klasse.
     *
     * @var string
     */
    protected $_type;

    /**
     * Absoluter Pfad zur Datei, die entsprechend des gesetzten Typs ausgelesen werden soll.
     *
     * @var string
     */
    protected $_path;

    /**
     * Für die Typen releaseNotes und changeLog muss die Version angegeben werden, für die diese Informationen
     * ausgelesen werden sollen.
     *
     * @var string
     */
    protected $_version;

    /**
     * @var Notification_ChangeLogReader
     */
    private $_changeLogReader;

    /**
     * @var Notification_ReleaseNotesReader
     */
    private $_releaseNotesReader;

    /**
     * @var Notification_SplitTestXMLReader
     */
    private $_splitTestXMLReader;

    /**
     * @param string $property
     * @return void
     */
    public function setProperty($property)
    {
        $this->_property = $property;
    }

    /**
     * @param string $type
     * @return void
     */
    public function setType($type)
    {
        $this->_type = $type;
    }

    /**
     * @param string $path
     * @return void
     */
    public function setPath($path)
    {
        $this->_path = $path;
    }

    /**
     * @param string $version
     * @return void
     */
    public function setVersion($version)
    {
        $this->_version = $version;
    }

    /**
     * (non-PHPdoc)
     * @see Task::main()
     */
    public function main()
    {
        $this->_validateParameters();

        // Information beschaffen
        $dataProvider = $this->_availableTypes[$this->_type];
        $information = $this->$dataProvider();

        // und setzen
        $this->project->setProperty($this->_property, $information);
    }

    /**
     * Gibt changeLogReader zurück.
     *
     * @return Notification_ChangeLogReader
     */
    public function getChangeLogReader()
    {
        if (!$this->_changeLogReader) {
            $this->_changeLogReader = new Notification_ChangeLogReader();
        }
        return $this->_changeLogReader;
    }

    /**
     * @param Notification_ChangeLogReader $changeLogReader
     * @return void
     */
    public function setChangeLogReader(Notification_ChangeLogReader $changeLogReader)
    {
        $this->_changeLogReader = $changeLogReader;
    }

    /**
     * Gibt releaseNotesReader zurück.
     *
     * @return Notification_ReleaseNotesReader
     */
    public function getReleaseNotesReader()
    {
        if (!$this->_releaseNotesReader) {
            $this->_releaseNotesReader = new Notification_ReleaseNotesReader();
        }
        return $this->_releaseNotesReader;
    }

    /**
     * @param Notification_ReleaseNotesReader $releaseNotesReader
     * @return void
     */
    public function setReleaseNotesReader(Notification_ReleaseNotesReader $releaseNotesReader)
    {
        $this->_releaseNotesReader = $releaseNotesReader;
    }

    /**
     * Gibt splitTestXMLReader zurück.
     *
     * @return Notification_SplitTestXMLReader
     */
    public function getSplitTestXMLReader()
    {
        if (!$this->_splitTestXMLReader) {
            $this->_splitTestXMLReader = new Notification_SplitTestXMLReader();
        }
        return $this->_splitTestXMLReader;
    }

    /**
     * @param Notification_SplitTestXMLReader $splitTestXMLReader
     */
    public function setSplitTestXMLReader(Notification_SplitTestXMLReader $splitTestXMLReader)
    {
        $this->_splitTestXMLReader = $splitTestXMLReader;
    }

    /**
     * @throws BuildException falls Parameter falsch gesetzt wurden bzw. vergessen wurden.
     * @return void
     */
    protected function _validateParameters()
    {
        if (empty($this->_property)) {
            throw new BuildException('The property property is required', $this->location);
        }
        if (empty($this->_path)) {
            throw new BuildException('The property path is required', $this->location);
        }
        if (empty($this->_type)) {
            throw new BuildException('The property type is required', $this->location);
        }
        if (!isset($this->_availableTypes[$this->_type])) {
            throw new BuildException('The given type is not supported: ' . $this->_type, $this->location);
        }
        if (in_array($this->_type, $this->_typesWhichRequireVersionProperty) && empty($this->_version)) {
            throw new BuildException('The given type \'' . $this->_type
                . '\' requires the property version to be set.', $this->location);
        }
    }

    /**
     * @return string
     */
    protected function _provideChangeLog()
    {
        $reader = $this->getChangeLogReader();
        return $reader->loadChangeLogForVersion($this->_path, $this->_version);
    }

    /**
     * @return string
     */
    protected function _provideReleaseNotes()
    {
        $reader = $this->getReleaseNotesReader();
        return $reader->loadReleaseNotesForVersion($this->_path, $this->_version);
    }

    /**
     * @return string
     */
    protected function _provideSplits()
    {
        $reader = $this->getSplitTestXMLReader();
        return json_encode($reader->calculateSplittingRates($this->_path));
    }
}