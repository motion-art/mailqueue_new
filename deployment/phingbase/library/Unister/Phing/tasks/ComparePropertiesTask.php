<?php
/**
 * File for ComparePropertiesTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2010 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id:$
 */

require_once 'phing/Task.php';

/**
 * ComparePropertiesTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2010, Unister GmbH
 */
class ComparePropertiesTask extends Task
{
    /**
    * Dist configuration
    *
    * @var string
    */
    protected $_dist = null;

    /**
     * is strict mode active
     * @var boolean
     */
    protected $_strict = false;


    /**
     * @return the $_strict
     */
    public function getStrict()
    {
        return $this->_strict;
    }

	/**
     *
     * @param boolean $_strict
     * @throws BuildException
     */
    public function setStrict($_strict)
    {
        $this->_strict = (boolean)$_strict;
    }

    /**
     * @param string $filename
     */
    public function setDist($filename)
    {
        $this->_dist = $filename;
    }

    /**
     * (non-PHPdoc)
     * @see Task::main()
     */
    public function main()
    {
        $current = $this->getProject()->getProperty('config.file');
        $dist = $this->_dist;

        $this->log(sprintf('Compare "%s" <-> "%s"', $current, basename($dist)), Project::MSG_INFO);

        try {

            $current = $this->_loadConfiguration($current);
            $dist = $this->_loadConfiguration($dist);

            $diff = array_diff_key($dist->getProperties(), $current->getProperties());
            foreach ($diff as $name => $value) {
                $this->log(sprintf('Property "%s" (%s) not set', $name, $value), Project::MSG_WARN);
            }

            if ($this->_strict === true && count($diff) > 0) {
                $msg = PHP_EOL . 'Strict mode used.' .PHP_EOL . 'There were ' . count($diff) . ' not set properties.';
                throw new BuildException(strtoupper($msg));
            }

        } catch (IOException $exception) {
            throw new BuildException($exception->getMessage());
        }
    }

    /**
     *
     * @param string $filename
     * @throws BuildException
     * @return Properties
     */
    protected function _loadConfiguration($filename)
    {
        $props = $this->_getProperties();
        $in    = $this->_getPhingFile($filename);
        if ($in === null) {
            throw new BuildException('Can\'t load: ' . $filename);
        }
        $props->load($in);

        return $props;
    }

    /**
     * Creates a Properties instance
     *
     * @return Properties
     */
    protected function _getProperties()
    {
        return new Properties();
    }

    /**
     * Creates a PhingFile instance
     *
     * @codeCoverageIgnore
     *
     * @param  string $filename
     * @return Properties
     */
    protected function _getPhingFile($filename)
    {
        return new PhingFile($filename);
    }
}
