<?php
/**
 * File for ParallelTargetTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id$
 */

require_once 'DocBlox/Parallel/Manager.php';
require_once 'DocBlox/Parallel/Worker.php';
require_once 'DocBlox/Parallel/WorkerPipe.php';

/**
 * Führt ein Target mit unterschiedlichen Parametern parallel aus
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class ParallelTargetTask extends Task
{
    /**
    * The concurrency manager
    * @var DocBlox_Parallel_Manager
    */
    protected $_manager = null;
    protected $_list = null;
    protected $_target = null;
    protected $_delimiter = ',';

    /**
     * Set list (comma separated)
     * @param string $list
     */
    public function setList($list)
    {
        $this->_list = $list;
    }

    /**
     * Set delimiter
     * @param string $delimiter
     */
    public function setDelimiter($delimiter)
    {
        $this->_delimiter = $delimiter;
    }

    /**
     * Set target
     * @param string $target
     */
    public function setTarget($target)
    {
        $this->_target = $target;
    }

   /**
	* Sets the maximum number of threads / processes to use
	* @param int $threadCount
	*/
    public function setThreadCount($threadCount)
    {
        $this->_manager->setProcessLimit($threadCount);
    }

    /**
     * load manager
     *
     * @codeCoverageIgnore
     */
    public function init()
    {
        $this->_manager = new DocBlox_Parallel_Manager();
    }

    /**
     * main task
     */
    public function main()
    {
        $keys = explode($this->_delimiter, $this->_list);

        foreach ($keys as $key) {

            $task = $this->_getTask();
            $task->setTarget($this->_target);
            $task->setProject($this->getProject());
            $task->setOwningTarget($this->getOwningTarget());
            $task->setLocation($this->getLocation());
            $property = $task->createProperty();
            $property->setName('key');
            $property->setValue($key);

            $worker = $this->_getWorker(array($task, 'perform'));

            $this->_manager->addWorker($worker);
        }

        $this->_manager->execute();

        $failed = 0;
        foreach ($this->_manager as $worker) {
            if ($worker->getError()) {
                $failed++;
            }
        }

        if ($failed > 0) {
            throw new BuildException($failed . '/' . count($this->_manager) . ' tasks failed');
        }
    }

    /**
     * Instantiates a PhingCallTask
     *
     * @return PhingCallTask
     */
    protected function _getTask()
    {
        return new PhingCallTask();
    }

    /**
     * Instantiates a DocBlox_Parallel_Worker with a given task
     *
     * @codeCoverageIgnore
     *
     * @param  callback|array $task
     * @return DocBlox_Parallel_Worker
     */
    protected function _getWorker($task)
    {
        return new DocBlox_Parallel_Worker($task);
    }
}
