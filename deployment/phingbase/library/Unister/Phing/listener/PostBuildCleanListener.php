<?php
/**
 * File for PostBuildCleanListener
 *
 * @category   Phing
 * @package    Phing_Listener
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id$
 */

require_once 'phing/BuildListener.php';

/**
 * PostBuildCleanListener
 *
 * @category   Phing
 * @package    Phing_Listener
 * @copyright  Copyright (c) 2011, Unister GmbH
 */
class PostBuildCleanListener implements BuildListener
{
    /**
     * Build Status Finished
     * @var string
     */
    const BUILD_FINISHED = 'BUILD FINISHED';

    /**
     * Build Status Failed
     * @var string
     */
    const BUILD_FAILED = 'BUILD FAILED';

    /**
     * Optionen
     * @var array
     */
    protected $_options = array();

    /**
     * Target Exception
     * @var Exception
     */
    protected $_targetException = null;

    /*
     * Registriere Shutdown-Funktion zum Aufräumen auch nach Fatalen Fehlern
     */
    public function __construct()
    {
        register_shutdown_function(array($this, '__destruct'));
    }

    /*
     * Aufräumen im Destruktor
     */
    public function __destruct()
    {
        $this->_clean();
    }
    
    /**
     * Entferne alle registrierten Dateien
     */
    protected function _clean()
    {
        foreach ($this->_options as $key => $option) {
            unlink($option);
            unset($this->_options[$key]);
        }
    }

    /**
     *  Sets the start-time when the build started.
     *
     * @param  BuildEvent  The BuildEvent
     */
    public function buildStarted(BuildEvent $event)
    {
    }

    /**
     *  Logs whether the build succeeded or failed, and any errors that
     *  occured during the build. Also outputs the total build-time.
     *
     * @param  BuildEvent  The BuildEvent
     * @see    BuildEvent::getException()
     */
    public function buildFinished(BuildEvent $event)
    {
        $this->_clean();
    }

    /**
     * Logs the current target name
     *
     * @param  BuildEvent  The BuildEvent
     * @see    BuildEvent::getTarget()
     */
    public function targetStarted(BuildEvent $event)
    {
    }

    /**
     *  Fired when a target has finished. We don't need specific action on this
     *  event. So the methods are empty.
     *
     *  @param  BuildEvent  The BuildEvent
     *  @access public
     *  @see    BuildEvent::getException()
     */
    public function targetFinished(BuildEvent $event)
    {
        if ($this->_targetException == null) {
            $this->_targetException = $event->getException();
        }
    }

    /**
     *  Fired when a task is started. We don't need specific action on this
     *  event. So the methods are empty.
     *
     *  @param  BuildEvent  The BuildEvent
     *  @access public
     *  @see    BuildEvent::getTask()
     */
    public function taskStarted(BuildEvent $event)
    {
    }

    /**
     *  Fired when a task has finished. We don't need specific action on this
     *  event. So the methods are empty.
     *
     * @param  BuildEvent  The BuildEvent
     * @see    BuildEvent::getException()
     */
    public function taskFinished(BuildEvent $event)
    {
    }

    /**
     *  Logs a message to the configured PEAR logger.
     *
     * @param  BuildEvent  The BuildEvent
     * @see    BuildEvent::getMessage()
     */
    public function messageLogged(BuildEvent $event)
    {
    }

    /**
     * Setze eine Option
     *
     * @param string $name
     * @param string $value
     */
    public function setOption($name, $value)
    {
        $this->_options[$name] = $value;
    }
}
