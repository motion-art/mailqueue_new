<?php
/**
 * File for SyslogListener
 *
 * @category   Phing
 * @package    Phing_Listener
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id:$
 */

require_once 'phing/BuildListener.php';

/**
 * SyslogListener
 *
 * @category   Phing
 * @package    Phing_Listener
 * @copyright  Copyright (c) 2011, Unister GmbH
 */
class SyslogListener implements BuildListener
{
    /**
     * Build Status Finished
     * @var string
     */
    const BUILD_FINISHED = 'BUILD FINISHED';

    /**
     * Build Status Failed
     * @var string
     */
    const BUILD_FAILED = 'BUILD FAILED';

    /**
     * Target Exception
     * @var Exception
     */
    protected $_targetException = null;

    /**
     * Optionen
     * @var array
     */
    protected $_options = array(
        'release' => '',
        'username' => '',
        'notice' => '',
        'databaseVersion' => 0,
        'databaseChanged' => 0,
        'environment' => ''
    );

    /**
     *  Sets the start-time when the build started.
     *
     * @param  BuildEvent  The BuildEvent
     */
    public function buildStarted(BuildEvent $event)
    {
    }

    /**
     *  Logs whether the build succeeded or failed, and any errors that
     *  occured during the build. Also outputs the total build-time.
     *
     * @param  BuildEvent  The BuildEvent
     * @see    BuildEvent::getException()
     */
    public function buildFinished(BuildEvent $event)
    {
        if ($this->_options['release'] == '') {
            return;
        }

        $exception = $event->getException();

        $message = new stdClass();
        $message->timestamp = date('Y-m-d H:i:s');
        $message->release = $this->_options['release'];
        $message->deployer = $this->_options['username'];
        $message->notice = $this->_options['notice'];
        if (isset($this->_options['databaseVersion'])) {
            $message->databaseVersion = $this->_options['databaseVersion'];
        }
        if (isset($this->_options['databaseChanged'])) {
            $message->databaseChange = $this->_options['databaseChanged'];
        }
        $message->environment = $this->_options['environment'];

        if ($exception == null) {
            $message->status = self::BUILD_FINISHED;
        } else {
            $message->status = self::BUILD_FAILED;
        }

        if ($this->_targetException !=  null) {
            $message->errorMessage = $this->_targetException->getMessage();
        }

        $this->_log($event->getProject()->getName(), $message);
    }

    /**
     * Log zu Syslog
     *
     * @param string $program
     * @param string $message
     */
    protected function _log($program, $message)
    {
        openlog($program, LOG_PID, LOG_LOCAL1);
        syslog(LOG_INFO, json_encode($message));
        closelog();
    }

    /**
     * Logs the current target name
     *
     * @param  BuildEvent  The BuildEvent
     * @see    BuildEvent::getTarget()
     */
    public function targetStarted(BuildEvent $event)
    {
    }

    /**
     *  Fired when a target has finished. We don't need specific action on this
     *  event. So the methods are empty.
     *
     *  @param  BuildEvent  The BuildEvent
     *  @access public
     *  @see    BuildEvent::getException()
     */
    public function targetFinished(BuildEvent $event)
    {
        if ($this->_targetException == null) {
            $this->_targetException = $event->getException();
        }
    }

    /**
     *  Fired when a task is started. We don't need specific action on this
     *  event. So the methods are empty.
     *
     *  @param  BuildEvent  The BuildEvent
     *  @access public
     *  @see    BuildEvent::getTask()
     */
    public function taskStarted(BuildEvent $event)
    {
    }

    /**
     *  Fired when a task has finished. We don't need specific action on this
     *  event. So the methods are empty.
     *
     * @param  BuildEvent  The BuildEvent
     * @see    BuildEvent::getException()
     */
    public function taskFinished(BuildEvent $event)
    {
    }

    /**
     *  Logs a message to the configured PEAR logger.
     *
     * @param  BuildEvent  The BuildEvent
     * @see    BuildEvent::getMessage()
     */
    public function messageLogged(BuildEvent $event)
    {
    }

    /**
     * Setze eine Option
     *
     * @param string $name
     * @param string $value
     */
    public function setOption($name, $value)
    {
        $this->_options[$name] = $value;
    }
}
