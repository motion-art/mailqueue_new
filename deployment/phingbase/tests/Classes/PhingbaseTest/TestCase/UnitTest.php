<?php
/**
 * File for the class PhingbaseTest_TestCase_UnitTest
 *
 * @category   PhingbaseTest
 * @package    PhingbaseTest_TestCase
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Abstract base class for all unit test classes within the phingbase project
 *
 * @category   PhingbaseTest
 * @package    PhingbaseTest_TestCase
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
abstract class PhingbaseTest_TestCase_UnitTest extends UTF_TestCase_UnitTest
{
    /**
     * names of test methods that need filesystem access
     * @var array
     */
    protected static $_methodsWithFileSystemAccess = array();

    /**
     * the temporary directory for the current test method
     * @var string|null
     */
    protected $_dir;

    /**
     * output buffering counter per test method
     * @var array
     */
    protected $_resetOBCounter = array();


    /**
     * Sets up the fixture.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        parent::setUp();

        $this->_setUpOutputBuffering();

        if (in_array($this->getName(false), static::$_methodsWithFileSystemAccess)) {
            if (!$this->_createTempDir()) {
                $this->fail('error creating temporary directory "' . $this->_dir . '"');
            }
        }
    }

    /**
     * Tears down the fixture.
     * This method is called after a test is executed.
     */
    public function tearDown()
    {
        if (in_array($this->getName(false), static::$_methodsWithFileSystemAccess)) {
            $this->_removeTempDir();
        }

        $this->_tearDownOutputBuffering();

        parent::tearDown();
    }


    /**
     * gets the SHA1 hash of the current test method's name
     *
     * @param  boolean $withDataSet use current data set as part of the name
     * @return string
     */
    public function getNameHash($withDataSet = true)
    {
        return sha1($this->getName($withDataSet));
    }

    /**
     * @return string
     */
    public function getTestDataDirectory()
    {
        return APPLICATION_PATH . '/../tests/data';
    }

    /**
     * prepares usage of output buffering
     *
     * @return void
     */
    protected function _setUpOutputBuffering()
    {
        $this->_resetOBCounter[$this->getNameHash()] = 0;
    }

    /**
     * cleans up after a possible use of output buffering
     *
     * @return void
     */
    protected function _tearDownOutputBuffering()
    {
        for ($i = $this->_resetOBCounter[$this->getNameHash()]; $i > 0; $i--) {
            ob_end_clean();
        }
    }

    /**
     * invokes a method or function on a test object using safe output buffering
     *
     * @param  string &$output        the buffered output will be put into this variable
     * @param  object $out            the object under test
     * @param  string $method         the method or function to invoke on the object under test
     * @param  array  $args = array() arguments for the method or function call
     * @return mixed the function result
     */
    public function executeTestWithOutputBuffering(&$output, $out, $method, $args = array())
    {
        $this->_obStart();
        $result = $this->invoke($out, $method, $args);
        $output = $this->_obGetClean();
        return $result;
    }

    /**
     * safely starts an output buffer
     *
     * @return void
     */
    private function _obStart()
    {
        if (ob_start()) {
            $this->_resetOBCounter[$this->getNameHash()]++;
        }
    }

    /**
     * safely ends and returns an output buffer
     *
     * @return string
     */
    private function _obGetClean()
    {
        $output = ob_get_clean();
        if ($output !== false) {
            $this->_resetOBCounter[$this->getNameHash()]--;
        }
        return $output;
    }


    ###################################################################################################################


    /**
     * creates a temporary directory within the operating system's temporary directory
     * named after the current test method
     *
     * @return boolean
     */
    protected function _createTempDir()
    {
        $this->_dir = sys_get_temp_dir() . '/' . get_class($this) . '_' . $this->getName(false);

        if (file_exists($this->_dir)) {
            if (!$this->_removeTempDir()) {
                return false;
            }
        }

        return mkdir($this->_dir);
    }

    /**
     * removes the current temporary directory prepared by {@link _createTempDir()}
     *
     * @return boolean
     */
    protected function _removeTempDir()
    {
        if (!empty($this->_dir) && file_exists($this->_dir)) {

            if (($dir = opendir($this->_dir)) === false) {
                return false;
            }
            while (($fileName = readdir($dir)) !== false) {
                $filePath = $this->_dir . '/' . $fileName;
                if (is_file($filePath)) {
                    if (!unlink($filePath)) {
                        return false;
                    }
                }
            }
            closedir($dir);

            return rmdir($this->_dir);
        }

        return true;
    }

    /**
     * creates a temporary file within the current temporary directory prepared by {@link _createTempDir()}
     * and optionally writes some content to it
     *
     * @param  string      $fileName
     * @param  string|null $fileContent = null
     * @return string|false the absolute path to the temporary file or false on failure
     */
    protected function _createTempFile($fileName, $fileContent = null)
    {
        $filePath = $this->_dir . '/' . $fileName;

        if (($file = fopen($filePath, 'w+')) === false) {
            return false;
        }

        if ($fileContent) {
            if (fwrite($file, (string) $fileContent) === false) {
                return false;
            }
        }

        return fclose($file) ? $filePath : false;
    }
}