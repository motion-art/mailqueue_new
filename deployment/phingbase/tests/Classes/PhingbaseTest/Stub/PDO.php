<?php
/**
 * File for the class PhingbaseTest_Stub_PDO
 *
 * @category   PhingbaseTest
 * @package    PhingbaseTest_Stub
 * @copyright  Copyright (c) 2012, Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Stub class for PDO objects
 *
 * @category   PhingbaseTest
 * @package    PhingbaseTest_Stub
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class PhingbaseTest_Stub_PDO extends PDO
{
    /**
     * override the original constructor to prevent the instance from connecting to the database
     */
    public function __construct()
    {}

    /**
     * this function also must be overriden if the original constructor is never called
     *
     * (non-PHPdoc)
     * @see PDO::setAttribute()
     */
    public function setAttribute()
    {
        return true;
    }
}