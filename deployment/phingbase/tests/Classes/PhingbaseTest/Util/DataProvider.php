<?php
/**
 * File for the class PhingbaseTest_Util_DataProvider
 *
 * @category   PhingbaseTest
 * @package    PhingbaseTest_Util
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Utility class providing commonly used data sets
 *
 * @category   PhingbaseTest
 * @package    PhingbaseTest_Util
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
abstract class PhingbaseTest_Util_DataProvider
{
    /**
     * all boolean values
     * @var array
     */
    protected static $_booleanValues = array(true, false);

    /**
     * all values considered empty by PHP
     * @var array
     */
    protected static $_emptyValues = array(
        null,
        '',
        false,
        0,
        '0',
        array()
    );

    /**
     * senseful values considered not null by PHP - except for new stdClass(), which is appended by getNotNullValues()
     * @var array
     */
    protected static $_notNullValues = array(
        true,
        false,
        0,
        1,
        '',
        '0',
        '1',
        array()
    );


    /**
     * returns all boolean values
     *
     * @return array
     */
    public static function getBooleanValues()
    {
        return self::$_booleanValues;
    }

    /**
     * returns all values considered empty by PHP
     *
     * @return array
     */
    public static function getEmptyValues()
    {
        return self::$_emptyValues;
    }

    /**
     * returns senseful values considered not null by PHP
     *
     * @return array
     */
    public static function getNotNullValues()
    {
        return array_merge(self::$_notNullValues, array(new stdClass()));
    }


    /**
     * returns all boolean values as a ready-to-use PHPUnit data provider array
     *
     * @param  string|null $keyName = null the key to use for every data set of the data provider array;
     *                                     if anything else than a non-empty string is given, 0 will be used as the key
     * @return array
     */
    public static function getDataProviderBooleanValues($keyName = null)
    {
        return self::_getDataProvider(self::getBooleanValues(), $keyName);
    }

    /**
     * returns all values considered empty by PHP as a ready-to-use PHPUnit data provider array
     *
     * @param  string|null $keyName = null the key to use for every data set of the data provider array;
     *                                     if anything else than a non-empty string is given, 0 will be used as the key
     * @return array
     */
    public static function getDataProviderEmptyValues($keyName = null)
    {
        return self::_getDataProvider(self::getEmptyValues(), $keyName);
    }

    /**
     * returns senseful values considered not null by PHP as a ready-to-use PHPUnit data provider array
     *
     * @param  string|null $keyName = null the key to use for every data set of the data provider array;
     *                                     if anything else than a non-empty string is given, 0 will be used as the key
     * @return array
     */
    public static function getDataProviderNotNullValues($keyName = null)
    {
        return self::_getDataProvider(self::getNotNullValues(), $keyName);
    }

    /**
     * returns a given set of values as a ready-to-use PHPUnit data provider array
     *
     * @param  array       $sourceValues
     * @param  string|null $keyName = null the key to use for every data set of the data provider array;
     *                                     if anything else than a non-empty string is given, 0 will be used as the key
     * @return array
     */
    protected static function _getDataProvider(array $sourceValues, $keyName = null)
    {
        if (is_string($keyName) && !empty($keyName)) {
            return array_map(
                function ($value, $key) {
                    return array($key => $value);
                },
                $sourceValues,
                array_fill(0, count($sourceValues), $keyName)
            );
        } else {
            return array_map(
                function ($value) {
                    return array($value);
                },
                $sourceValues
            );
        }
    }
}