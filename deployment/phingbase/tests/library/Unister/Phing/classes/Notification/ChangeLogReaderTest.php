<?php
/**
 * @category    Notification
 * @package     Notification_ChangeLogReader
 * @copyright   Copyright (c) 2013 Unister GmbH
 * @author      Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author      Tommy Helm <tommy.helm@unister-gmbh.de>
 * @version     $Id$
 */

/**
 * @category    Notification
 * @package     Notification_ChangeLogReader
 * @copyright   Copyright (c) 2013 Unister GmbH
 */
class Notification_ChangeLogReaderTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * @var string
     */
    protected static $_cut = 'Notification_ChangeLogReader';

    /**
     * @var Notification_ChangeLogReader
     */
    protected $_reader;

    /**
     * @var string
     */
    protected $_changeLogAbsoluteFilePath;

    /**
     * (non-PHPdoc)
     * @see PhingbaseTest_TestCase_UnitTest::setUp()
     */
    public function setUp()
    {
        parent::setUp();
        $this->_changeLogAbsoluteFilePath = $this->getTestDataDirectory() . '/changeLogReaderTest/changeLog.txt';
        $this->_reader = new self::$_cut();
    }

    /**
     * @expectedException BuildException
     * @return void
     */
    public function testLoadChangeLogForVersionThrowsExceptionOnUnknownFile()
    {
        $this->_reader->loadChangeLogForVersion($this->_changeLogAbsoluteFilePath . '2', '1.2.3');
    }

    /**
     * @dataProvider provideDataForTestLoadChangeLogForVersion
     * @param string $version
     * @return void
     */
    public function testLoadChangeLogForVersion($version, $expectedResult)
    {
        $result = $this->_reader->loadChangeLogForVersion($this->_changeLogAbsoluteFilePath, $version);
        $this->assertEquals($expectedResult, $result, 'release notes could not be loaded');
    }

    /**
     * @param string $version
     * @return void
     * @expectedException BuildException
     */
    public function testLoadChangeLogForVersionWhichIsUnknownThrowsException()
    {
        $unknownVersion = '3.3.3';
        $this->_reader->loadChangeLogForVersion($this->_changeLogAbsoluteFilePath, $unknownVersion);
    }

    /**
     * @return array
     */
    public function provideDataForTestLoadChangeLogForVersion()
    {
        return array(
            array('2.79.0', $this->_loadExpectedChangeLog('2.79.0')),
            array('2.79.1', $this->_loadExpectedChangeLog('2.79.1')),
            array('2.79.2', $this->_loadExpectedChangeLog('2.79.2'))
        );
    }

    /**
     * @param string $name
     * @return string
     */
    protected function _loadExpectedChangeLog($version)
    {
        $fileName = 'expected_' . str_replace('.', '-', $version) . '.txt';
        $absoluteFilePath = $this->getTestDataDirectory() . '/changeLogReaderTest/' . $fileName;
        return file_get_contents($absoluteFilePath);
    }
}