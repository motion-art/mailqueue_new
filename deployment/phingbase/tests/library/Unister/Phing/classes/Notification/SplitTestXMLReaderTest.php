<?php
/**
 * @category    Notification
 * @package     Notification_SplitXMLReader
 * @copyright   Copyright (c) 2013 Unister GmbH
 * @author      Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author      Tommy Helm <tommy.helm@unister-gmbh.de>
 * @version     $Id$
 */

/**
 * @category    Notification
 * @package     Notification_SplitXMLReader
 * @copyright   Copyright (c) 2013 Unister GmbH
 */
class Notification_SplitTestXMLReaderTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * @var string
     */
    protected static $_cut = 'Notification_SplitTestXMLReader';

    /**
     * @var Notification_SplitTestXMLReader
     */
    protected $_reader;

    /**
     * @var string
     */
    protected $_absoluteSplitTestXMLPath;

    /**
     * (non-PHPdoc)
     * @see PhingbaseTest_TestCase_UnitTest::setUp()
     */
    public function setUp()
    {
        parent::setUp();
        $this->_absoluteSplitTestXMLPath = $this->getTestDataDirectory() . '/splitTestXMLReaderTest/Splittest.xml';
        $this->_reader = new self::$_cut();
    }

    /**
     * @expectedException BuildException
     * @return void
     */
    public function testCalculateSplittingRatesThrowsExceptionOnUnknownFile()
    {
        $this->_reader->calculateSplittingRates($this->_absoluteSplitTestXMLPath . '2');
    }

    /**
     * @return void
     */
    public function testCalculateSplittingRates()
    {
        $splittingRates = $this->_reader->calculateSplittingRates($this->_absoluteSplitTestXMLPath);

        // gesamte Anzahl
        $this->assertEquals(4, count($splittingRates), 'wrong split count');

        // einzelne Einträge vorhanden?
        $this->assertArrayHasKey('default', $splittingRates);
        $this->assertArrayHasKey(51, $splittingRates);
        $this->assertArrayHasKey(52, $splittingRates);
        $this->assertArrayHasKey(53, $splittingRates);

        // prüfe, ob Werte enthalten sind
        foreach ($splittingRates as $splitId => $splittingRate) {
            $this->assertTrue(!empty($splittingRate->info));
            $this->assertEquals($splitId, $splittingRate->splitId, 'array key must be the split id');
        }

        // stimmen die rates?
        $this->assertEquals(1, $splittingRates['default']->rate);
        $this->assertEquals(20, round($splittingRates[51]->rate));
        $this->assertEquals(20, round($splittingRates[52]->rate));
        $this->assertEquals(59, round($splittingRates[53]->rate));
    }
}