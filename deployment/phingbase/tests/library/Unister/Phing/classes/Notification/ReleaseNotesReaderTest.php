<?php
/**
 * @category    Notification
 * @package     Notification_ReleaseNotesReader
 * @copyright   Copyright (c) 2013 Unister GmbH
 * @author      Unister GmbH <teamleitung-dev@unister.de>
 * @author      Tommy Helm <tommy.helm@unister.de>
 * @version     $Id:$
 */

/**
 * @category    Notification
 * @package     Notification_ReleaseNotesReader
 * @copyright   Copyright (c) 2013 Unister GmbH
 */
class Notification_ReleaseNotesReaderTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * @var string
     */
    protected static $_cut = 'Notification_ReleaseNotesReader';

    /**
     * @var Notification_ReleaseNotesReader
     */
    protected $_reader;

    /**
     * @var string
     */
    protected $_releaseNotesAbsoluteFilePath;

    public function setUp()
    {
        parent::setUp();
        $this->_releaseNotesAbsoluteFilePath = $this->getTestDataDirectory()
            . '/releaseNotesReaderTest/releaseNotes.txt';
        $this->_reader = new self::$_cut();
    }

    /**
     * @expectedException BuildException
     * @return void
     */
    public function testLoadReleaseNotesForVersionThrowsExceptionOnUnknownFile()
    {
        $this->_reader->loadReleaseNotesForVersion($this->_releaseNotesAbsoluteFilePath . '2', '1.2.3');
    }

    /**
     * @dataProvider provideDataForTestLoadReleaseNotesForVersion
     * @param string $version
     * @return void
     */
    public function testLoadReleaseNotesForVersion($version, $expectedResult)
    {
        $result = $this->_reader->loadReleaseNotesForVersion($this->_releaseNotesAbsoluteFilePath, $version);
        $this->assertEquals($expectedResult, $result, 'release notes could not be loaded');
    }

    /**
     * @param string $version
     * @return void
     * @expectedException BuildException
     */
    public function testLoadReleaseNotesForVersionWhichIsUnknownThrowsException()
    {
        $unknownVersion = '3.3.3';
        $this->_reader->loadReleaseNotesForVersion($this->_releaseNotesAbsoluteFilePath, $unknownVersion);
    }

    /**
     * @return array
     */
    public function provideDataForTestLoadReleaseNotesForVersion()
    {
        return array(
            array('2.83.0', $this->_loadExpectedReleaseNote('2.83.0')),
            array('2.83.1', $this->_loadExpectedReleaseNote('2.83.1')),
            array('2.83.2', $this->_loadExpectedReleaseNote('2.83.2'))
        );
    }

    /**
     *
     * @param string $version
     * @return string
     */
    protected function _loadExpectedReleaseNote($version)
    {
        $fileName = 'expected_' . str_replace('.', '-', $version) . '.txt';
        $absoluteFilePath = $this->getTestDataDirectory() . '/releaseNotesReaderTest/' . $fileName;
        return file_get_contents($absoluteFilePath);
    }
}
