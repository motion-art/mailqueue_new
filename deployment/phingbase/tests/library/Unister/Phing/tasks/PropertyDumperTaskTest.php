<?php
/**
 * File for the class PropertyDumperTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class PropertyDumperTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class PropertyDumperTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (PropertyDumperTask)
     * @var string
     */
    protected static $_cut = 'PropertyDumperTask';


    /**
     * @covers PropertyDumperTask::main
     *
     * @dataProvider dataProviderForTestMain
     *
     * @param array $properties
     * @param array $expectedIntermediateProperties
     */
    public function testMain($properties, $expectedIntermediateProperties)
    {
        $project = new Project();

        foreach ($properties as $propertyName => $propertyValue) {
            $project->setProperty($propertyName, $propertyValue);
        }

        ob_start();
        PrettyPrinter::printArray($expectedIntermediateProperties);
        $expectedPrintedArray = ob_get_clean();

        $expectedOutput = PrettyPrinter::YELLOW
            . PrettyPrinter::BOLD
            . '             '
            . '============= Properties ============= '
            . PrettyPrinter::RESET
            . PHP_EOL
            . PHP_EOL
            . $expectedPrintedArray
            . PHP_EOL
            . PrettyPrinter::YELLOW
            . PrettyPrinter::BOLD
            . ' ####### '
            . 'all system-wide Enviromentvariables are available via env.KEYNAME'
            . ' #######'
            . PrettyPrinter::RESET
            . PHP_EOL
            . PHP_EOL;


        $out = new self::$_cut();

        $this->setProperty($out, 'project', $project);


        $output = null;
        $this->executeTestWithOutputBuffering($output, $out, 'main');


        $this->assertSame($expectedOutput, $output);
    }

    /**
     * data provider for the test method testMain()
     * @return array
     */
    public function dataProviderForTestMain()
    {
        return array(
            /* data set #0 */
            array(
                'properties'                     => array(
                    'prop1' => '1',
                    'prop2' => '2'
                ),
                'expectedIntermediateProperties' => array(
                    'prop1' => '1',
                    'prop2' => '2'
                ),
            ),
            /* data set #1 */
            array(
                'properties'                     => array(
                    'prop2' => '2',
                    'prop1' => '1'
                ),
                'expectedIntermediateProperties' => array(
                    'prop1' => '1',
                    'prop2' => '2'
                )
            ),
            /* data set #2 */
            array(
                'properties'                     => array(
                    'env1' => '1',
                    'env2' => '2'
                ),
                'expectedIntermediateProperties' => array()
            ),
            /* data set #3 */
            array(
                'properties'                     => array(
                    'env2' => '2',
                    'env1' => '1'
                ),
                'expectedIntermediateProperties' => array()
            ),
            /* data set #4 */
            array(
                'properties'                     => array(
                    'prop1' => '1',
                    'prop2' => '2',
                    'env1' => '1',
                    'env2' => '2'
                ),
                'expectedIntermediateProperties' => array(
                    'prop1' => '1',
                    'prop2' => '2'
                )
            ),
            /* data set #5 */
            array(
                'properties'                     => array(
                    'prop2' => '2',
                    'prop1' => '1',
                    'env2' => '2',
                    'env1' => '1'
                ),
                'expectedIntermediateProperties' => array(
                    'prop1' => '1',
                    'prop2' => '2'
                )
            ),
            /* data set #6 */
            array(
                'properties'                     => array(),
                'expectedIntermediateProperties' => array()
            )
        );
    }
}