<?php
/**
 * @category    Phing
 * @package     Phing_Task
 * @subpackage  PhtmlRendererTask
 * @copyright   Copyright (c) 2013 Unister GmbH
 * @author      Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author      Tommy Helm <tommy.helm@unister-gmbh.de>
 * @version     $Id:$
 */

/**
 * @category    Phing
 * @package     Phing_Task
 * @subpackage  PhtmlRenderer
 * @copyright   Copyright (c) 2013 Unister GmbH
 */
class PhtmlRendererTask_AssignTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * @var string
     */
    protected static $_cut = 'PhtmlRendererTask_AssignTask';


    /**
     * @dataProvider provideDataForTestMainShouldThrowException
     * @expectedException BuildException
     */
    public function testMainShouldThrowException(array $params)
    {
        $assign = new self::$_cut();
        $this->_assignParameters($assign, $params);
        $assign->main();
    }

    /**
     * @dataProvider provideDataForTestMainShouldThrowNoException
     * @return void
     */
    public function testMainShouldThrowNoException(array $params)
    {
        $assign = new self::$_cut();
        $this->_assignParameters($assign, $params);
        $assign->main();

        // Die gesetzten Werte sollen eins zu eins, wie sie gesetzt wurden, zurück gegeben werden
        foreach ($params as $key => $value) {
            $verb = ($key == 'jsonEncoded') ? 'is' : 'get';
            $getter = $verb . ucfirst($key);
            $this->assertEquals($assign->$getter(), $value);
        }
    }

    /**
     * @return void
     */
    public function testIsJsonEncodedDefaultValue()
    {
        $assign = new self::$_cut();
        $this->assertEquals($assign->isJsonEncoded(), false, 'Default value of peroperty jsonEncoded should be false');
    }

    /**
     * @return array
     */
    public function provideDataForTestMainShouldThrowException()
    {
        return array(
            array(
                array('name'  => 'test')
            ),
            array(
                array('value' => 'test')
            )
        );
    }

    /**
     * @return array
     */
    public function provideDataForTestMainShouldThrowNoException()
    {
        return array(
            array(
                array(
                    'name'        => 'test',
                    'value'       => 'value',
                    'jsonEncoded' => 'true'
                )
            ),
            array(
                array(
                    'name'        => 'anotherTest',
                    'value'       => 'value2'
                )
            ),
        );
    }

    /**
     * @param PhtmlRendererTask_AssignTask $assign
     * @param array $parameters
     * @return void
     */
    protected function _assignParameters(PhtmlRendererTask_AssignTask $assign, array $params)
    {
        foreach ($params as $key => $value) {
            $setter = 'set' . ucfirst($key);
            $assign->$setter($value);
        }
    }
}