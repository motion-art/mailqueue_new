<?php
/**
 * File for the class ZFConfigurationTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class ZFConfigurationTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class ZFConfigurationTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (ZFConfigurationTask)
     * @var string
     */
    protected static $_cut = 'ZFConfigurationTask';

    /**
     * names of test methods that need filesystem access
     * @var array
     */
    protected static $_methodsWithFileSystemAccess = array('testMainWhenMandatoryAttributesAreNotNull');

    /**
     * path to the Zend Framework library; determined by the method setUp()
     * @var string
     */
    protected static $_zfLibraryPath = '';


    /**
     * Sets up the fixture.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        parent::setUp();

        $zfLibraryPath = APPLICATION_PATH . '/../../vendor/zend/zf1/library';

        self::$_zfLibraryPath = realpath($zfLibraryPath);

        if (empty(self::$_zfLibraryPath)) {
            $this->fail('The expected path to the Zend Framework library (' . $zfLibraryPath . ') does not exist.'
                . ' Please check your installation!');
        }
    }


    /**
     * @covers ZFConfigurationTask::setLibraryPath
     */
    public function testSetLibraryPath()
    {
        $libraryPath = 'test';

        $out = new self::$_cut();

        $out->setLibraryPath($libraryPath);

        $this->assertAttributeSame($libraryPath, '_libraryPath', $out);
    }

    /**
     * @covers ZFConfigurationTask::setConfiguration
     */
    public function testSetConfiguration()
    {
        $configuration = 'test';

        $out = new self::$_cut();

        $out->setConfiguration($configuration);

        $this->assertAttributeSame($configuration, '_configuration', $out);
    }

    /**
     * @covers ZFConfigurationTask::setEnvironment
     */
    public function testSetEnvironment()
    {
        $environment = 'test';

        $out = new self::$_cut();

        $out->setEnvironment($environment);

        $this->assertAttributeSame($environment, '_environment', $out);
    }

    /**
     * @covers ZFConfigurationTask::main
     *
     * @dataProvider dataProviderForTestMainWhenMandatoryAttributeIsNull
     *
     * @param string|null $libraryPath
     * @param string|null $configuration
     * @param string|null $environment
     * @param string      $expectedExceptionMessage
     */
    public function testMainWhenMandatoryAttributeIsNull($libraryPath, $configuration, $environment,
        $expectedExceptionMessage)
    {
        $out = $this->getMock(self::$_cut, array('log', '_requireZendClasses'));

        $out->expects($this->never())
            ->method('_requireZendClasses');

        $out->expects($this->never())
            ->method('log');

        $this->setProperty($out, '_libraryPath', $libraryPath);
        $this->setProperty($out, '_configuration', $configuration);
        $this->setProperty($out, '_environment', $environment);


        $this->setExpectedException('BuildException', $expectedExceptionMessage);


        $out->main();
    }

    /**
     * data provider for the test method testMainWhenMandatoryAttributeIsNull()
     * @return array
     */
    public function dataProviderForTestMainWhenMandatoryAttributeIsNull()
    {
        return array(
            /* data set #0 */
            array(
                'libraryPath'              => null,
                'configuration'            => 'some value',
                'environment'              => 'some value',
                'expectedExceptionMessage' => 'LibraryPath expected'
            ),
            /* data set #1 */
            array(
                'libraryPath'              => 'some value',
                'configuration'            => null,
                'environment'              => 'some value',
                'expectedExceptionMessage' => 'ConfigurationFile expected'
            ),
            /* data set #2 */
            array(
                'libraryPath'              => 'some value',
                'configuration'            => 'some value',
                'environment'              => null,
                'expectedExceptionMessage' => 'Environment expected'
            )
        );
    }

    /**
     * @covers ZFConfigurationTask::main
     */
    public function testMainWhenMandatoryAttributesAreNotNullButZendLibaryIsInvalid()
    {
        $libraryPath   = '/doesnotexist';
        $configuration = 'some value';
        $environment   = 'some value';

        $expectedExceptionMessage = 'Could not find ZF: ' . $libraryPath;


        $out = $this->getMock(self::$_cut, array('log', '_requireZendClasses'));

        $out->expects($this->never())
            ->method('_requireZendClasses');

        $out->expects($this->never())
            ->method('log');

        $this->setProperty($out, '_libraryPath', $libraryPath);
        $this->setProperty($out, '_configuration', $configuration);
        $this->setProperty($out, '_environment', $environment);


        $this->setExpectedException('BuildException', $expectedExceptionMessage);


        $out->main();
    }

    /**
     * @covers ZFConfigurationTask::main
     */
    public function testMainWhenMandatoryAttributesAreNotNullButZendConfigIniThrowsAnException()
    {
        $libraryPath   = self::$_zfLibraryPath;
        $configuration = 'doesnotexist';
        $environment   = 'testing';

        $expectedExceptionMessage = 'parse_ini_file(' . $configuration
            . '): failed to open stream';


        $out = $this->getMock(self::$_cut, array('log', '_requireZendClasses'));

        $out->expects($this->once())
            ->method('_requireZendClasses')
            ->will($this->returnValue(null));

        $out->expects($this->never())
            ->method('log');

        $this->setProperty($out, '_libraryPath', $libraryPath);
        $this->setProperty($out, '_configuration', $configuration);
        $this->setProperty($out, '_environment', $environment);


        $this->setExpectedException('BuildException', $expectedExceptionMessage);


        $out->main();
    }

    /**
     * @covers ZFConfigurationTask::main
     *
     * @dataProvider dataProviderForTestMainWhenMandatoryAttributesAreNotNull
     *
     * @param string $originalFileContent
     * @param string $environment
     * @param array  $expectedParsedFileContentAsArray
     */
    public function testMainWhenMandatoryAttributesAreNotNull($originalFileContent, $environment,
        $expectedParsedFileContentAsArray)
    {
        $fileName = __CLASS__ . '_' . $this->getName(false) . '.ini';

        if (($filePath = $this->_createTempFile($fileName, $originalFileContent)) === false) {
            $this->fail('error creating temporary file "' . $fileName
                . '" in temporary directory "' . $this->_dir . '"');
        }


        $libraryPath   = self::$_zfLibraryPath;
        $configuration = $filePath;

        $expectedLogMessage = 'Converted: ' . $configuration . '.php';

        $expectedProcessedFilePath = $configuration . '.php';
        $expectedProcessedFileContent = "<?php\nreturn " . var_export($expectedParsedFileContentAsArray, true) . ';';


        $out = $this->getMock(self::$_cut, array('log', '_requireZendClasses'));

        $out->expects($this->once())
            ->method('_requireZendClasses')
            ->will($this->returnValue(null));

        $out->expects($this->once())
            ->method('log')
            ->with($this->equalTo($expectedLogMessage), $this->equalTo(Project::MSG_INFO));

        $this->setProperty($out, '_libraryPath', $libraryPath);
        $this->setProperty($out, '_configuration', $configuration);
        $this->setProperty($out, '_environment', $environment);


        $out->main();


        $this->assertFileExists($expectedProcessedFilePath);

        $this->assertSame($expectedProcessedFileContent, file_get_contents($expectedProcessedFilePath));
    }

    /**
     * data provider for the test method testMainWhenMandatoryAttributesAreNotNull()
     * @return array
     */
    public function dataProviderForTestMainWhenMandatoryAttributesAreNotNull()
    {
        return array(
            /* data set #0 */
            array(
                'originalFileContent'              => '[testing]',
                'environment'                      => 'testing',
                'expectedParsedFileContentAsArray' => array()
            ),
            /* data set #1 */
            array(
                'originalFileContent'              => '[testing]\n;a comment',
                'environment'                      => 'testing',
                'expectedParsedFileContentAsArray' => array()
            ),
            /* data set #2 */
            array(
                'originalFileContent'              => "[testing]\nproperty = value",
                'environment'                      => 'testing',
                'expectedParsedFileContentAsArray' => array('property' => 'value')
            ),
            /* data set #3 */
            array(
                'originalFileContent'              => "[testing]\nproperty = value\nproperties.property = value",
                'environment'                      => 'testing',
                'expectedParsedFileContentAsArray' => array(
                    'property'   => 'value',
                    'properties' => array('property' => 'value')
                )
            )
        );
    }
}