<?php
/**
 * File for the class ListInstalledTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class ListInstalledTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class ListInstalledTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (ListInstalledTask)
     * @var string
     */
    protected static $_cut = 'ListInstalledTask';


    /**
     * @covers ListInstalledTask::main
     *
     * @dataProvider dataProviderForTestMain
     *
     * @param array      $hosts
     * @param array|null $expectedCommandOutput
     * @param string     $expectedPrintedIntermediateResult
     */
    public function testMain($hosts, $expectedCommandOutputs, $expectedPrintedIntermediateResult)
    {
        $expectedOutput = PrettyPrinter::YELLOW
            . PrettyPrinter::BOLD
            . '             '
            . '============= Slaves ============= '
            . PrettyPrinter::RESET
            . PHP_EOL
            . PHP_EOL
            . $expectedPrintedIntermediateResult;


        $out = $this->getMock(self::$_cut, array('getHosts', 'execCommand'));

        $out->expects($this->once())
            ->method('getHosts')
            ->will($this->returnValue($hosts));

        if (!empty($hosts)) {
            $i = 0;
            foreach ($hosts as $host) {
                $out->expects($this->at(1 + $i))
                    ->method('execCommand')
                    ->with($this->equalTo('cat ' . $host['vhost'] . '/current-release'), $this->isNull())
                    ->will($this->returnValue($expectedCommandOutputs[$i++]));
            }
        } else {
            $out->expects($this->never())
                ->method('execCommand');
        }


        $output = null;
        $this->executeTestWithOutputBuffering($output, $out, 'main');


        $this->assertSame($expectedOutput, $output);
    }

    /**
     * data provider for the test method testMain()
     * @return array
     */
    public function dataProviderForTestMain()
    {
        /* preparations for data set #0 */

        $emptyIntermediateCommandResult = array();
        ob_start();
        PrettyPrinter::printAnsi($emptyIntermediateCommandResult);
        $expectedPrintedEmptyCommandResult = ob_get_clean();

        /* preparations for data sets containing test host A */

        $testHostA = 'testHostA';
        $commandOutputA = array('line1');
        $intermediateCommandResultA = array($testHostA => current($commandOutputA));

        ob_start();
        PrettyPrinter::printAnsi($intermediateCommandResultA);
        $expectedPrintedCommandResultA = ob_get_clean();

        /* preparations for data sets containing test host B */

        $testHostB = 'testHostB';
        $commandOutputB = array('line1');
        $intermediateCommandResultB = array($testHostB => current($commandOutputB));

        ob_start();
        PrettyPrinter::printAnsi($intermediateCommandResultB);
        $expectedPrintedCommandResultB = ob_get_clean();

        /* preparations for data sets containing test hosts A and B */

        ob_start();
        PrettyPrinter::printAnsi(array_merge($intermediateCommandResultA, $intermediateCommandResultB));
        $expectedPrintedCommandResultAB = ob_get_clean();

        /* return data sets */

        return array(
            /* data set #0 */
            array(
                'hosts'                        => array(),
                'expectedCommandOutputs'       => null,
                'expectedPrintedCommandResult' => $expectedPrintedEmptyCommandResult
            ),
            /* data set #1 */
            array(
                'hosts'                        => array(
                    array(
                        'host'  => $testHostA,
                        'user'  => 'testUserA',
                        'vhost' => 'testVHostA'
                    )
                ),
                'expectedCommandOutputs'       => array($commandOutputA),
                'expectedPrintedCommandResult' => $expectedPrintedCommandResultA
            ),
            /* data set #2 */
            array(
                'hosts'                        => array(
                    array(
                        'host'  => $testHostB,
                        'user'  => 'testUserB',
                        'vhost' => 'testVHostB'
                    )
                ),
                'expectedCommandOutputs'       => array($commandOutputB),
                'expectedPrintedCommandResult' => $expectedPrintedCommandResultB
            ),
            /* data set #3 */
            array(
                'hosts'                        => array(
                    array(
                        'host'  => $testHostA,
                        'user'  => 'testUserA',
                        'vhost' => 'testVHostA'
                    ),
                    array(
                        'host'  => $testHostB,
                        'user'  => 'testUserB',
                        'vhost' => 'testVHostB'
                    )
                ),
                'expectedCommandOutputs'       => array($commandOutputA, $commandOutputB),
                'expectedPrintedCommandResult' => $expectedPrintedCommandResultAB
            ),
            /* data set #4 */
            array(
                'hosts'                        => array(
                    array(
                        'host'  => $testHostA,
                        'user'  => 'testUserA',
                        'vhost' => 'testVHostA'
                    )
                ),
                'expectedCommandOutputs'       => array(array_merge($commandOutputA, array('line2'))),
                'expectedPrintedCommandResult' => $expectedPrintedCommandResultA
            ),
            /* data set #5 */
            array(
                'hosts'                        => array(
                    array(
                        'host'  => $testHostB,
                        'user'  => 'testUserB',
                        'vhost' => 'testVHostB'
                    )
                ),
                'expectedCommandOutputs'       => array(array_merge($commandOutputB, array('line2'))),
                'expectedPrintedCommandResult' => $expectedPrintedCommandResultB
            ),
            /* data set #6 */
            array(
                'hosts'                        => array(
                    array(
                        'host'  => $testHostA,
                        'user'  => 'testUserA',
                        'vhost' => 'testVHostA'
                    ),
                    array(
                        'host'  => $testHostB,
                        'user'  => 'testUserB',
                        'vhost' => 'testVHostB'
                    )
                ),
                'expectedCommandOutputs'       => array(
                     array_merge($commandOutputA, array('line2')),
                     array_merge($commandOutputB, array('line2'))
                ),
                'expectedPrintedCommandResult' => $expectedPrintedCommandResultAB
            )
        );
    }
}