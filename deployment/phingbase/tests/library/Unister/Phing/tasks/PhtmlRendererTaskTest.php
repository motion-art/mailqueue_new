<?php
/**
 * @category    Phing
 * @package     Phing_Task
 * @copyright   Copyright (c) 2013 Unister GmbH
 * @author      Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author      Tommy Helm <tommy.helm@unister-gmbh.de>
 * @version     $Id:$
 */

/**
 * @category    Phing
 * @package     Phing_Task
 * @copyright   Copyright (c) 2013 Unister GmbH
 */
class PhtmlRendererTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test
     * @var string
     */
    protected static $_cut = 'PhtmlRendererTask';

    /**
     * @var string
     */
    protected $_testDataDirectory;

    /**
     * (non-PHPdoc)
     * @see PhingbaseTest_TestCase_UnitTest::setUp()
     */
    public function setUp()
    {
       parent::setUp();
       $this->_testDataDirectory = $this->getTestDataDirectory() . '/phtmlRendererTaskTest';
    }

    /**
     * @return void
     */
    public function testMainRendering()
    {
        /* @var $task PhtmlRendererTask */
        $task = new self::$_cut();

        $project = new Project();
        $task->setProject($project);

        // property: property
        $propertyName = 'outputProperty';
        $task->setProperty($propertyName);

        $this->_render($task);

        $actualResult = $project->getProperty($propertyName);
        $expectedResult = file_get_contents($this->_testDataDirectory . '/expectedResult.txt');

        $this->assertEquals($expectedResult, $actualResult);
    }

    /**
     * test des Properties append
     *
     * @return void
     */
    public function testMainAppendingRendering()
    {
        /* @var $task PhtmlRendererTask */
        $task = new self::$_cut();

        $project = new Project();
        $task->setProject($project);

        // property: property
        $propertyName = 'outputProperty';
        $task->setProperty($propertyName);

        // initialisiere existierenden Inhalt des properties
        $existingPropertyValue = 'please do not overwrite me';
        $project->setProperty($propertyName, $existingPropertyValue);

        // property: append
        $task->setAppend(true);

        $this->_render($task);

        $actualResult = $project->getProperty($propertyName);
        $expectedResult = $existingPropertyValue . file_get_contents($this->_testDataDirectory . '/expectedResult.txt');

        $this->assertEquals($expectedResult, $actualResult);
    }

    /**
     * @dataProvider provideDataForTestMainThrowsExceptionBecauseOfMissingProperty
     * @expectedException BuildException
     */
    public function testMainThrowsExceptionBecauseOfMissingProperty(array $params)
    {
        $task = new self::$_cut();
        foreach ($params as $key => $value) {
            $setter = 'set' . ucfirst($key);
            $task->$setter($value);
        }
        $task->main();
    }

    /**
     * @return void
     */
    public function testGetAppendDefaultValue()
    {
        /* @var $task PhtmlRendererTask */
        $task = new self::$_cut();
        $this->assertEquals($task->isAppend(), false, 'default value of property append should be false');
    }

    /**
     * @return array
     */
    public function provideDataForTestMainThrowsExceptionBecauseOfMissingProperty()
    {
        return array(
            array(
                array('property' => 'propertyName')
            ),
            array(
                array('template' => 'templatePath')
            )
        );
    }

    /**
     * Verwendet feste Testdaten.
     *
     * @param PhtmlRendererTask $task
     * @return void
     */
    protected function _render(PhtmlRendererTask $task)
    {

        $albums = array(
            array(
                'name'        => 'Blue Period',
                'releaseYear' => 1951
            ),
            array(
                'name'        => 'Dig',
                'releaseYear' => 1951
            ),
            array(
                'name'         => 'Walkin\'',
                'releaseYear'  => 1954,
                'tracks' => array(
                    'Walkin’',
                    'Blue ’n Boogie',
                    'Solar',
                    'You Don’t Know What Love Is',
                    'Love Me or Leave Me'
                )
            )
        );

        // property: template
        $templatePath = $this->_testDataDirectory . '/template.phtml';
        $task->setTemplate($templatePath);

        // Variablen zuweisen
        $task->addAssign($this->_createAssignmentMock('artistName', 'Dr. Miles Davis'));
        $task->addAssign($this->_createAssignmentMock('artistBirthYear', 1926));
        $task->addAssign($this->_createAssignmentMock('albums', json_encode($albums), true));

        $task->main();
    }

    /**
     * @param string $name
     * @param string $value
     * @param boolean $jsonEncoded default ist false
     * @return PhtmlRendererTask_AssignTask
     */
    protected function _createAssignmentMock($name, $value, $jsonEncoded = false)
    {
        $assignMock = $this->_getMock('PhtmlRendererTask_AssignTask', array('getName', 'getValue', 'isJsonEncoded'));
        $assignMock->expects($this->any())->method('getName')->will($this->returnValue($name));
        $assignMock->expects($this->any())->method('getValue')->will($this->returnValue($value));
        $assignMock->expects($this->any())->method('isJsonEncoded')->will($this->returnValue($jsonEncoded));
        return $assignMock;
    }
}