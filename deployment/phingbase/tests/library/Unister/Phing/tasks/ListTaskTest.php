<?php
/**
 * File for the class ListTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class ListTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class ListTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (ListTask)
     * @var string
     */
    protected static $_cut = 'ListTask';


    /**
     * @covers ListTask::main
     */
    public function testMain()
    {
        $expectedPublicTargets = array('test' => 'test');

        ob_start();
        PrettyPrinter::printAnsi($expectedPublicTargets);
        $expectedPrintedPublicTargets = ob_get_clean();

        $expectedOutput = PrettyPrinter::YELLOW
            . PrettyPrinter::BOLD
            . '             '
            . '============= Targets ============= '
            . PrettyPrinter::RESET
            . PHP_EOL
            . PHP_EOL
            . $expectedPrintedPublicTargets;


        $out = $this->getMock(self::$_cut, array('_getPublicTargets'), array(), '', false, false, true);

        $out->expects($this->once())
            ->method('_getPublicTargets')
            ->will($this->returnValue($expectedPublicTargets));


        $output = null;
        $this->executeTestWithOutputBuffering($output, $out, 'main');


        $this->assertEquals($expectedOutput, $output);
    }

    /**
     * @covers ListTask::_getPublicTargets
     *
     * @dataProvider dataProviderForTestGetPublicTargets
     *
     * @param $projectTargets
     * @param $expectedPublicTargets
     */
    public function testGetPublicTargets($projectTargets, $expectedPublicTargets)
    {
        $project = new Project();

        $this->setProperty($project, 'targets', $projectTargets);


        $out = new self::$_cut();

        $this->setProperty($out, 'project', $project);


        $result = $this->invoke($out, '_getPublicTargets');


        $this->assertSame($expectedPublicTargets, $result);
    }

    /**
     * data provider for the test method testGetPublicTargets()
     * @return array
     */
    public function dataProviderForTestGetPublicTargets()
    {
        $publicTarget1 = new Target();
        $publicTarget1->setName('target1');
        $publicTarget1->setDescription('public target 1');

        $publicTarget2 = new Target();
        $publicTarget2->setName('target2');
        $publicTarget2->setDescription('public target 2');

        $nonPublicTarget1 = new Target();
        $nonPublicTarget1->setName('test.target1');
        $nonPublicTarget1->setDescription('non-public target 1');

        $nonPublicTarget2 = new Target();
        $nonPublicTarget2->setName('_test.target2');
        $nonPublicTarget2->setDescription('non-public target 2');

        $nonPublicTarget3 = new Target();
        $nonPublicTarget3->setName('_target3');
        $nonPublicTarget3->setDescription('non-public target 3');

        return array(
            /* data set #0 */
            array(
                'projectTargets'        => array(),
                'expectedPublicTargets' => array()
            ),
            /* data set #1 */
            array(
                'projectTargets'        => array(
                    $publicTarget1,
                    $publicTarget2,
                    $nonPublicTarget1,
                    $nonPublicTarget2,
                    $nonPublicTarget3
                ),
                'expectedPublicTargets' => array(
                    $publicTarget1->getName() => $publicTarget1->getDescription(),
                    $publicTarget2->getName() => $publicTarget2->getDescription()
                )
            ),
            /* data set #2 */
            array(
                'projectTargets'        => array(
                    $publicTarget2,
                    $publicTarget1,
                    $nonPublicTarget1,
                    $nonPublicTarget2,
                    $nonPublicTarget3
                ),
                'expectedPublicTargets' => array(
                    $publicTarget1->getName() => $publicTarget1->getDescription(),
                    $publicTarget2->getName() => $publicTarget2->getDescription()
                )
            )
        );
    }
}