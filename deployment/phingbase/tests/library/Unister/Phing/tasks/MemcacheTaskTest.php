<?php
/**
 * File for the class MemcacheTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class MemcacheTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class MemcacheTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (MemcacheTask)
     * @var string
     */
    protected static $_cut = 'MemcacheTask';


    /**
     * @covers MemcacheTask::setServer
     */
    public function testSetServer()
    {
        $server = 'test';

        $out = new self::$_cut();

        $out->setServer($server);

        $this->assertAttributeSame($server, '_server', $out);
    }

    /**
     * @covers MemcacheTask::setPort
     *
     * @dataProvider dataProviderForTestSetPortWithValidArgument
     *
     * @param string|integer $port
     */
    public function testSetPortWithValidArgument($port)
    {
        $expectedAttributeValue = (integer) $port;

        $out = new self::$_cut();

        $out->setPort($port);

        $this->assertAttributeSame($expectedAttributeValue, '_port', $out);
    }

    /**
     * data provider for the test method testSetPortWithValidArgument()
     * @return array
     */
    public function dataProviderForTestSetPortWithValidArgument()
    {
        return array(
            /* data set #0 */
            array('port' => '200'),
            /* data set #1 */
            array('port' => 200)
        );
    }

    /**
     * @covers MemcacheTask::setPort
     *
     * @dataProvider dataProviderForTestSetPortWithInvalidArgument
     *
     * @param mixed $port
     */
    public function testSetPortWithInvalidArgument($port)
    {
        $out = new self::$_cut();

        $this->setExpectedException('BuildException', '"' . $port . '" is not a valid Port');

        $out->setPort($port);
    }

    /**
     * data provider for the test method testSetPortWithInvalidArgument()
     * @return array
     */
    public function dataProviderForTestSetPortWithInvalidArgument()
    {
        return array(
            /* data set #0 */
            array('port' => 'boo'),
            /* data set #1 */
            array('port' => 'zero'),
            /* data set #2 */
            array('port' => 'one'),
            /* data set #3 */
            array('port' => ''),
            /* data set #4 */
            array('port' => 'null'),
            /* data set #5 */
            array('port' => null)
        );
    }

    /**
     * @covers MemcacheTask::setAction
     */
    public function testSetAction()
    {
        $action = 'test';

        $out = new self::$_cut();

        $out->setAction($action);

        $this->assertAttributeSame($action, '_action', $out);
    }

    /**
     * @covers MemcacheTask::setKey
     */
    public function testSetKey()
    {
        $key = 'test';

        $out = new self::$_cut();

        $out->setKey($key);

        $this->assertAttributeSame($key, '_key', $out);
    }

    /**
     * @covers MemcacheTask::setValue
     */
    public function testSetValue()
    {
        $value = 'test';

        $out = new self::$_cut();

        $out->setValue($value);

        $this->assertAttributeSame($value, '_value', $out);
    }

    /**
     * @covers MemcacheTask::setHaltOnFailure
     *
     * @dataProvider dataProviderForTestSetHaltOnFailure
     *
     * @param mixed   $flag
     * @param boolean $expectedPropertyValue
     */
    public function testSetHaltOnFailure($flag, $expectedPropertyValue)
    {
        $out = new self::$_cut();

        $out->setHaltOnFailure($flag);

        $this->assertAttributeSame($expectedPropertyValue, '_haltOnFailure', $out);
    }

    /**
     * data provider for the test method testSetHaltOnFailure()
     * @return array
     */
    public function dataProviderForTestSetHaltOnFailure()
    {
        return array(
            /* data set #0 */
            array(
                'flag'                  => true,
                'expectedPropertyValue' => true
            ),
            /* data set #1 */
            array(
                'flag'                  => false,
                'expectedPropertyValue' => false
            ),
            /* data set #2 */
            array(
                'flag'                  => 1,
                'expectedPropertyValue' => true
            ),
            /* data set #3 */
            array(
                'flag'                  => 0,
                'expectedPropertyValue' => false
            ),
            /* data set #4 */
            array(
                'flag'                  => '1',
                'expectedPropertyValue' => true
            ),
            /* data set #5 */
            array(
                'flag'                  => '0',
                'expectedPropertyValue' => false
            ),
            /* data set #6 */
            array(
                'flag'                  => 'yes',
                'expectedPropertyValue' => true
            ),
            /* data set #7 */
            array(
                'flag'                  => 'no',
                'expectedPropertyValue' => true
            )
        );
    }

    /**
     * @covers MemcacheTask::main
     *
     * @dataProvider dataProviderForTestMainWithInvalidAction
     *
     * @param mixed $invalidAction
     */
    public function testMainWithInvalidAction($invalidAction)
    {
        $validActions = array('_set', '_delete', '_flush');

        $out = $this->getMock(self::$_cut, array_merge($validActions, array('log', '_connect')));

        foreach ($validActions as $validAction) {
            $out->expects($this->never())
                ->method($validAction);
        }

        $out->expects($this->never())
            ->method('log');

        $this->setProperty($out, '_action', $invalidAction);


        $this->setExpectedException('BuildException', 'Memcache action "' . $invalidAction . '" is unknown');


        $out->main();
    }

    /**
     * data provider for the test method testMainWithInvalidAction()
     * @return array
     */
    public function dataProviderForTestMainWithInvalidAction()
    {
        return array(
            /* data set #0 */
            array('invalidAction' => 'connect'),
            /* data set #1 */
            array('invalidAction' => 'disconnect'),
            /* data set #2 */
            array('invalidAction' => null),
            /* data set #3 */
            array('invalidAction' => false),
            /* data set #4 */
            array('invalidAction' => true),
            /* data set #5 */
            array('invalidAction' => 0),
            /* data set #6 */
            array('invalidAction' => 1),
            /* data set #7 */
            array('invalidAction' => ''),
            /* data set #8 */
            array('invalidAction' => array())
        );
    }

    /**
     * @covers MemcacheTask::main
     *
     * @dataProvider dataProviderForTestMainWithValidAction
     *
     * @param string $key
     * @param mixed  $action
     * @param string $expectedLogMessage1
     */
    public function testMainWithValidActionSucceeding($key, $action, $expectedLogMessage1)
    {
        $validActions = array('_set', '_delete', '_flush');

        $actionMethod = '_' . $action;

        $out = $this->getMock(self::$_cut, array_merge($validActions, array('log', '_connect')));

        $out->expects($this->once())
            ->method('log')
            ->with($this->equalTo($expectedLogMessage1))
            ->will($this->returnValue(null));

        foreach ($validActions as $validAction) {
            if ($actionMethod === $validAction) {
                $out->expects($this->once())
                    ->method($validAction)
                    ->will($this->returnValue(true));
            } else {
                $out->expects($this->never())
                    ->method($validAction);
            }
        }

        $this->setProperty($out, '_key', $key);
        $this->setProperty($out, '_action', $action);


        $out->main();
    }

    /**
     * @covers MemcacheTask::main
     *
     * @dataProvider dataProviderForTestMainWithValidAction
     *
     * @param string $key
     * @param mixed  $action
     * @param string $expectedLogMessage1
     * @param string $expectedLogMessage2
     */
    public function testMainWithValidActionFailingWithoutHaltOnFailure($key, $action, $expectedLogMessage1,
        $expectedLogMessage2)
    {
        $haltOnFailure = false;


        $validActions = array('_set', '_delete', '_flush');

        $actionMethod = '_' . $action;

        $out = $this->getMock(self::$_cut, array_merge($validActions, array('log', '_connect')));

        $out->expects($this->at(0))
            ->method('log')
            ->with($this->equalTo($expectedLogMessage1))
            ->will($this->returnValue(null));

        foreach ($validActions as $validAction) {
            if ($actionMethod === $validAction) {
                $out->expects($this->at(1))
                    ->method($validAction)
                    ->will($this->returnValue(false));
            } else {
                $out->expects($this->never())
                    ->method($validAction);
            }
        }

        $out->expects($this->at(2))
            ->method('log')
            ->with($this->equalTo($expectedLogMessage2))
            ->will($this->returnValue(null));

        $this->setProperty($out, '_key', $key);
        $this->setProperty($out, '_action', $action);
        $this->setProperty($out, '_haltOnFailure', $haltOnFailure);


        $out->main();
    }

    /**
     * @covers MemcacheTask::main
     *
     * @dataProvider dataProviderForTestMainWithValidAction
     *
     * @param string $key
     * @param mixed  $action
     * @param string $expectedLogMessage1
     * @param string $expectedLogMessage2
     */
    public function testMainWithValidActionFailingWithHaltOnFailure($key, $action, $expectedLogMessage1,
        $expectedLogMessage2)
    {
        $haltOnFailure = true;


        $validActions = array('_set', '_delete', '_flush');

        $actionMethod = '_' . $action;

        $out = $this->getMock(self::$_cut, array_merge($validActions, array('log', '_connect')));

        $out->expects($this->at(0))
            ->method('log')
            ->with($this->equalTo($expectedLogMessage1))
            ->will($this->returnValue(null));

        foreach ($validActions as $validAction) {
            if ($actionMethod === $validAction) {
                $out->expects($this->at(1))
                    ->method($validAction)
                    ->will($this->returnValue(false));
            } else {
                $out->expects($this->never())
                    ->method($validAction);
            }
        }

        $out->expects($this->at(2))
            ->method('log')
            ->with($this->equalTo($expectedLogMessage2))
            ->will($this->returnValue(null));

        $this->setProperty($out, '_key', $key);
        $this->setProperty($out, '_action', $action);
        $this->setProperty($out, '_haltOnFailure', $haltOnFailure);


        $this->setExpectedException('BuildException', 'Memcache error');


        $out->main();
    }

    /**
     * data provider for the test methods testMainWithValidAction*()
     * @return array
     */
    public function dataProviderForTestMainWithValidAction()
    {
        $key = 'test';

        return array(
            /* data set #0 */
            array(
                'key'                 => $key,
                'action'              => 'set',
                'expectedLogMessage1' => 'Creating record "' . $key . '" in memcache',
                'expectedLogMessage2' => 'Could not create record "' . $key . '" in memcache'
            ),
            /* data set #1 */
            array(
                'key'                 => $key,
                'action'              => 'delete',
                'expectedLogMessage1' => 'Deleting record "' . $key . '" in memcache',
                'expectedLogMessage2' => 'Could not delete record "' . $key . '" in memcache'
            ),
            /* data set #2 */
            array(
                'key'                 => $key,
                'action'              => 'flush',
                'expectedLogMessage1' => 'Flushing memcache',
                'expectedLogMessage2' => 'Could not flush memcache'
            )
        );
    }

    /**
     * @covers MemcacheTask::_connect
     */
    public function testConnectWhenMemcacheAlreadyExists()
    {
        $memcacheMock = $this->getMock('Memcache', array('connect'));
        $memcacheMock->expects($this->never())
            ->method('connect');


        $out = $this->getMock(self::$_cut, array('_getMemcache'));

        $out->expects($this->never())
            ->method('_getMemcache');

        $this->setProperty($out, '_memcache', $memcacheMock);


        $result = $this->invoke($out, '_connect');


        $this->assertSame($memcacheMock, $result);
    }

    /**
     * @covers MemcacheTask::_connect
     */
    public function testConnectWhenMemcacheConnectionFails()
    {
        $server = 'server';
        $port   = 'port';


        $memcacheMock = $this->getMock('Memcache', array('connect'));
        $memcacheMock->expects($this->once())
            ->method('connect')
            ->with($this->equalTo($server), $this->equalTo($port))
            ->will($this->returnValue(false));


        $out = $this->getMock(self::$_cut, array('_getMemcache'));

        $out->expects($this->once())
            ->method('_getMemcache')
            ->will($this->returnValue($memcacheMock));

        $this->setProperty($out, '_server', $server);
        $this->setProperty($out, '_port', $port);


        $this->setExpectedException('BuildException', 'Could not connect to memcache server');


        $this->invoke($out, '_connect');
    }

    /**
     * @covers MemcacheTask::_connect
     */
    public function testConnect()
    {
        $server = 'server';
        $port   = 'port';


        $memcacheMock = $this->getMock('Memcache', array('connect'));
        $memcacheMock->expects($this->once())
            ->method('connect')
            ->with($this->equalTo($server), $this->equalTo($port))
            ->will($this->returnValue(true));


        $out = $this->getMock(self::$_cut, array('_getMemcache'));

        $out->expects($this->once())
            ->method('_getMemcache')
            ->will($this->returnValue($memcacheMock));

        $this->setProperty($out, '_server', $server);
        $this->setProperty($out, '_port', $port);


        $result = $this->invoke($out, '_connect');


        $this->assertSame($memcacheMock, $result);
    }

    /**
     * @covers MemcacheTask::_getMemcache
     */
    public function testGetMemcache()
    {
        $out = new self::$_cut();

        $result = $this->invoke($out, '_getMemcache');

        $this->assertInstanceOf('Memcache', $result);
    }

    /**
     * @covers MemcacheTask::_set
     *
     * @dataProvider dataProviderForTestSetWhenKeyIsEmpty
     *
     * @param mixed $emptyKey
     */
    public function testSetWhenKeyIsEmpty($emptyKey)
    {
        $value = 'value';


        $memcacheMock = $this->getMock('Memcache', array('add'));
        $memcacheMock->expects($this->never())
            ->method('add');


        $out = $this->getMock(self::$_cut, array('_connect'));

        $out->expects($this->once())
            ->method('_connect')
            ->will($this->returnValue($memcacheMock));

        $this->setProperty($out, '_key', $emptyKey);
        $this->setProperty($out, '_value', $value);


        $this->setExpectedException('BuildException', 'Key required for setting data to memcache');


        $this->invoke($out, '_set');
    }

    /**
     * data provider for the test method testSetWhenKeyIsEmpty()
     * @return array
     */
    public function dataProviderForTestSetWhenKeyIsEmpty()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderEmptyValues('emptyKey');
    }

    /**
     * @covers MemcacheTask::_set
     *
     * @dataProvider dataProviderForTestSetWhenValueIsEmpty
     *
     * @param mixed $emptyValue
     */
    public function testSetWhenValueIsEmpty($emptyValue)
    {
        $key = 'key';


        $memcacheMock = $this->getMock('Memcache', array('add'));
        $memcacheMock->expects($this->never())
            ->method('add');


        $out = $this->getMock(self::$_cut, array('_connect'));

        $out->expects($this->once())
            ->method('_connect')
            ->will($this->returnValue($memcacheMock));

        $this->setProperty($out, '_key', $key);
        $this->setProperty($out, '_value', $emptyValue);


        $this->setExpectedException('BuildException', 'Value required for setting data to memcache');


        $this->invoke($out, '_set');
    }

    /**
     * data provider for the test method testSetWhenValueIsEmpty()
     * @return array
     */
    public function dataProviderForTestSetWhenValueIsEmpty()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderEmptyValues('emptyValue');
    }

    /**
     * @covers MemcacheTask::_set
     *
     * @dataProvider dataProviderForTestSet
     *
     * @param boolean $setResult
     */
    public function testSet($setResult)
    {
        $key = 'key';
        $value = 'value';


        $memcacheMock = $this->getMock('Memcache', array('add'));
        $memcacheMock->expects($this->once())
            ->method('add')
            ->with($this->equalTo($key))
            ->will($this->returnValue($setResult));


        $out = $this->getMock(self::$_cut, array('_connect'));

        $out->expects($this->once())
            ->method('_connect')
            ->will($this->returnValue($memcacheMock));

        $this->setProperty($out, '_key', $key);
        $this->setProperty($out, '_value', $value);


        $result = $this->invoke($out, '_set');


        $this->assertSame($setResult, $result);
    }

    /**
     * data provider for the test method testSet()
     * @return array
     */
    public function dataProviderForTestSet()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderBooleanValues('setResult');
    }

    /**
     * @covers MemcacheTask::_delete
     *
     * @dataProvider dataProviderForTestDeleteWhenKeyIsEmpty
     *
     * @param mixed $emptyKey
     */
    public function testDeleteWhenKeyIsEmpty($emptyKey)
    {
        $memcacheMock = $this->getMock('Memcache', array('delete'));
        $memcacheMock->expects($this->never())
            ->method('delete');


        $out = $this->getMock(self::$_cut, array('_connect'));

        $out->expects($this->once())
            ->method('_connect')
            ->will($this->returnValue($memcacheMock));

        $this->setProperty($out, '_key', $emptyKey);


        $this->setExpectedException('BuildException', 'Key required for setting data to memcache');


        $this->invoke($out, '_delete');
    }

    /**
     * data provider for the test method testDeleteWhenKeyIsEmpty()
     * @return array
     */
    public function dataProviderForTestDeleteWhenKeyIsEmpty()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderEmptyValues('emptyKey');
    }

    /**
     * @covers MemcacheTask::_delete
     *
     * @dataProvider dataProviderForTestDelete
     *
     * @param boolean $deleteResult
     */
    public function testDelete($deleteResult)
    {
        $key = 'test';


        $memcacheMock = $this->getMock('Memcache', array('delete'));
        $memcacheMock->expects($this->once())
            ->method('delete')
            ->with($this->equalTo($key))
            ->will($this->returnValue($deleteResult));


        $out = $this->getMock(self::$_cut, array('_connect'));

        $out->expects($this->once())
            ->method('_connect')
            ->will($this->returnValue($memcacheMock));

        $this->setProperty($out, '_key', $key);


        $result = $this->invoke($out, '_delete');


        $this->assertSame($deleteResult, $result);
    }

    /**
     * data provider for the test method testDelete()
     * @return array
     */
    public function dataProviderForTestDelete()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderBooleanValues('deleteResult');
    }

    /**
     * @covers MemcacheTask::_flush
     *
     * @dataProvider dataProviderForTestFlush
     *
     * @param boolean $flushResult
     */
    public function testFlush($flushResult)
    {
        $memcacheMock = $this->getMock('Memcache', array('flush'));
        $memcacheMock->expects($this->once())
            ->method('flush')
            ->will($this->returnValue($flushResult));


        $out = $this->getMock(self::$_cut, array('_connect'));

        $out->expects($this->once())
            ->method('_connect')
            ->will($this->returnValue($memcacheMock));


        $result = $this->invoke($out, '_flush');


        $this->assertSame($flushResult, $result);
    }

    /**
     * data provider for the test method testFlush()
     * @return array
     */
    public function dataProviderForTestFlush()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderBooleanValues('flushResult');
    }
}