<?php
/**
 * File for the class SimpleScpTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class SimpleScpTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class SimpleScpTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (SimpleScpTask)
     * @var string
     */
    protected static $_cut = 'SimpleScpTask';


    /**
     * @covers SimpleScpTask::appendCmdArg
     */
    public function testAppendCmdArg()
    {
        $argument = 'test';

        $expectedAttributeValue = array($argument);

        $out = new self::$_cut();

        $out->appendCmdArg($argument);

        $this->assertAttributeSame($expectedAttributeValue, '_cmdArgs', $out);
    }

    /**
     * @covers SimpleScpTask::setRemoteUser
     */
    public function testSetRemoteUser()
    {
        $remoteUser = 'test';

        $out = new self::$_cut();

        $out->setRemoteUser($remoteUser);

        $this->assertAttributeSame($remoteUser, '_remoteUser', $out);
    }

    /**
     * @covers SimpleScpTask::setRemoteHost
     */
    public function testSetRemoteHost()
    {
        $remoteHost = 'test';

        $out = new self::$_cut();

        $out->setRemoteHost($remoteHost);

        $this->assertAttributeSame($remoteHost, '_remoteHost', $out);
    }

    /**
     * @covers SimpleScpTask::setSource
     */
    public function testSetSource()
    {
        $source = 'test';

        $out = new self::$_cut();

        $out->setSource($source);

        $this->assertAttributeSame($source, '_source', $out);
    }

    /**
     * @covers SimpleScpTask::setArgs
     *
     * @dataProvider dataProviderForTestSetArgs
     *
     * @param mixed $arguments
     */
    public function testSetArgs($arguments)
    {
        $out = $this->getMock(self::$_cut, array('appendCmdArg'));

        $out->expects($this->once())
            ->method('appendCmdArg')
            ->with($this->equalTo($arguments))
            ->will($this->returnValue(null));


        $out->setArgs($arguments);
    }

    /**
     * data provider for the test method testSetArgs()
     * @return array
     */
    public function dataProviderForTestSetArgs()
    {
        return array(
            /* data set #0 */
            array('arguments' => null),
            /* data set #1 */
            array('arguments' => true),
            /* data set #2 */
            array('arguments' => false),
            /* data set #3 */
            array('arguments' => 0),
            /* data set #4 */
            array('arguments' => 1),
            /* data set #5 */
            array('arguments' => ''),
            /* data set #6 */
            array('arguments' => '0'),
            /* data set #7 */
            array('arguments' => '1'),
            /* data set #8 */
            array('arguments' => array()),
            /* data set #9 */
            array('arguments' => array('0')),
            /* data set #10 */
            array('arguments' => array('0', '1')),
            /* data set #11 */
            array('arguments' => new stdClass())
        );
    }

    /**
     * @covers SimpleScpTask::setHaltOnFailure
     *
     * @dataProvider dataProviderForTestSetHaltOnFailure
     *
     * @param mixed   $flag
     * @param boolean $expectedPropertyValue
     */
    public function testSetHaltOnFailure($flag, $expectedPropertyValue)
    {
        $out = new self::$_cut();

        $out->setHaltOnFailure($flag);

        $this->assertAttributeSame($expectedPropertyValue, '_haltOnFailure', $out);
    }

    /**
     * data provider for the test method testSetHaltOnFailure()
     * @return array
     */
    public function dataProviderForTestSetHaltOnFailure()
    {
        return array(
            /* data set #0 */
            array(
                'flag'                  => true,
                'expectedPropertyValue' => true
            ),
            /* data set #1 */
            array(
                'flag'                  => false,
                'expectedPropertyValue' => false
            ),
            /* data set #2 */
            array(
                'flag'                  => 1,
                'expectedPropertyValue' => true
            ),
            /* data set #3 */
            array(
                'flag'                  => 0,
                'expectedPropertyValue' => false
            ),
            /* data set #4 */
            array(
                'flag'                  => '1',
                'expectedPropertyValue' => true
            ),
            /* data set #5 */
            array(
                'flag'                  => '0',
                'expectedPropertyValue' => false
            ),
            /* data set #6 */
            array(
                'flag'                  => 'yes',
                'expectedPropertyValue' => true
            ),
            /* data set #7 */
            array(
                'flag'                  => 'no',
                'expectedPropertyValue' => true
            )
        );
    }

    /**
     * @covers SimpleScpTask::setTarget
     */
    public function testSetTarget()
    {
        $target = 'test';

        $out = new self::$_cut();

        $out->setTarget($target);

        $this->assertAttributeSame($target, '_target', $out);
    }

    /**
     * @covers SimpleScpTask::setReturnProperty
     */
    public function testSetReturnProperty()
    {
        $returnProperty = 'test';

        $out = new self::$_cut();

        $out->setReturnProperty($returnProperty);

        $this->assertAttributeSame($returnProperty, '_returnProperty', $out);
    }

    /**
     * @covers SimpleScpTask::setOutputProperty
     */
    public function testSetOutputProperty()
    {
        $outputProperty = 'test';

        $out = new self::$_cut();

        $out->setOutputProperty($outputProperty);

        $this->assertAttributeSame($outputProperty, '_outputProperty', $out);
    }

    /**
     * @covers SimpleScpTask::main
     *
     * @dataProvider dataProviderForTestMainWithEmptyMandatoryAttribute
     *
     * @param mixed  $remoteHost
     * @param mixed  $source
     * @param mixed  $target
     * @param string $expectedExceptionMessage
     */
    public function testMainWithEmptyMandatoryAttribute($remoteHost, $source, $target, $expectedExceptionMessage)
    {
        $projectMock = $this->getMock('Project', array('setProperty'));
        $projectMock->expects($this->never())
            ->method('setProperty');


        $out = $this->getMock(self::$_cut, array('log', '_performExecCommand'));

        $out->expects($this->never())
            ->method('log');

        $out->expects($this->never())
            ->method('_performExecCommand');

        $this->setProperty($out, '_remoteHost', $remoteHost);
        $this->setProperty($out, '_target', $target);
        $this->setProperty($out, '_source', $source);

        $this->setProperty($out, 'project', $projectMock);


        $this->setExpectedException('BuildException', $expectedExceptionMessage);


        $out->main();
    }

    /**
     * data provider for the test method testMainWithEmptyMandatoryAttribute()
     * @return array
     */
    public function dataProviderForTestMainWithEmptyMandatoryAttribute()
    {
        $emptyValues = PhingbaseTest_Util_DataProvider::getEmptyValues();

        $validRemoteHostArray = array_fill(0, count($emptyValues), 'remoteHost');
        $validSourceArray     = array_fill(0, count($emptyValues), 'source');
        $validTargetArray     = array_fill(0, count($emptyValues), 'target');

        $exceptionMessageForRemoteHostArray = array_fill(0, count($emptyValues), 'You must provide a remote host');
        $exceptionMessageForSourceArray     = array_fill(0, count($emptyValues), 'You must provide a source path');
        $exceptionMessageForTargetArray     = array_fill(0, count($emptyValues), 'You must provide a target path');

        $dataSet0 = array_map(null, $emptyValues, $validSourceArray, $validTargetArray,
           $exceptionMessageForRemoteHostArray);

        $dataSet1 = array_map(null, $validRemoteHostArray, $emptyValues, $validTargetArray,
            $exceptionMessageForSourceArray);

        $dataSet2 = array_map(null, $validRemoteHostArray, $validSourceArray, $emptyValues,
            $exceptionMessageForTargetArray);

        return array_merge($dataSet0, $dataSet1, $dataSet2);
    }

    /**
     * @depends testMainWithEmptyMandatoryAttribute
     *
     * @covers SimpleScpTask::main
     *
     * @dataProvider dataProviderForTestMainWithEmptyRemoteUser
     *
     * @param mixed $emptyRemoteUser
     */
    public function testMainWithEmptyRemoteUser($emptyRemoteUser)
    {
        $cmd = 'phpunit ';
        $cmdArgs = array('--version', '--x');
        $remoteHost = 'nowhere';
        $source = 'someLocalResource';
        $target = 'someRemoteTarget';

        // == "someLocalResource nowhere:someRemoteTarget"
        $expectedPartiallyAssembledCommand =  $source . ' ' . $remoteHost . ':' . $target;

        $expectedLogMessage = 'Copy "' . $source . '" to host "' . $remoteHost . '"';

        // == "phpunit --version --x someLocalResource nowhere:someRemoteTarget"
        $expectedAssembledCommand = $cmd . implode(' ', $cmdArgs) . ' ' . $expectedPartiallyAssembledCommand;


        $projectMock = $this->getMock('Project', array('setProperty'));
        $projectMock->expects($this->never())
            ->method('setProperty');


        $out = $this->getMock(self::$_cut, array('log', '_performExecCommand'));

        $out->expects($this->once())
            ->method('log')
            ->with($this->equalTo($expectedLogMessage))
            ->will($this->returnValue(null));

        $out->expects($this->once())
            ->method('_performExecCommand')
            ->with($this->equalTo($expectedAssembledCommand), $this->isType('array'), $this->isEmpty())
            ->will($this->returnValue(null));

        $this->setProperty($out, '_cmd', $cmd);
        $this->setProperty($out, '_cmdArgs', $cmdArgs);
        $this->setProperty($out, '_remoteUser', $emptyRemoteUser);
        $this->setProperty($out, '_remoteHost', $remoteHost);
        $this->setProperty($out, '_target', $target);
        $this->setProperty($out, '_source', $source);

        $this->setProperty($out, 'project', $projectMock);


        $out->main();
    }

    /**
     * data provider for the test method testMainWithEmptyRemoteUser()
     * @return array
     */
    public function dataProviderForTestMainWithEmptyRemoteUser()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderEmptyValues('emptyRemoteUser');
    }

    /**
     * @depends testMainWithEmptyRemoteUser
     *
     * @covers SimpleScpTask::main
     */
    public function testMain()
    {
        $cmd = 'phpunit ';
        $cmdArgs = array('--version', '--x');
        $remoteUser = 'nobody';
        $remoteHost = 'nowhere';
        $source = 'someLocalResource';
        $target = 'someRemoteTarget';

        // == "someLocalResource nobody@nowhere:someRemoteTarget"
        $expectedPartiallyAssembledCommand =  $source . ' ' . $remoteUser . '@' . $remoteHost . ':' . $target;

        $expectedLogMessage = 'Copy "' . $source . '" to host "' . $remoteHost . '"';

        // == "phpunit --version --x someLocalResource nowhere:someRemoteTarget"
        $expectedAssembledCommand = $cmd . implode(' ', $cmdArgs) . ' ' . $expectedPartiallyAssembledCommand;


        $projectMock = $this->getMock('Project', array('setProperty'));
        $projectMock->expects($this->never())
            ->method('setProperty');


        $out = $this->getMock(self::$_cut, array('log', '_performExecCommand'));

        $out->expects($this->once())
            ->method('log')
            ->with($this->equalTo($expectedLogMessage))
            ->will($this->returnValue(null));

        $out->expects($this->once())
            ->method('_performExecCommand')
            ->with($this->equalTo($expectedAssembledCommand), $this->isType('array'), $this->isEmpty())
            ->will($this->returnValue(null));

        $this->setProperty($out, '_cmd', $cmd);
        $this->setProperty($out, '_cmdArgs', $cmdArgs);
        $this->setProperty($out, '_remoteUser', $remoteUser);
        $this->setProperty($out, '_remoteHost', $remoteHost);
        $this->setProperty($out, '_target', $target);
        $this->setProperty($out, '_source', $source);

        $this->setProperty($out, 'project', $projectMock);


        $out->main();
    }

    /**
     * @depends testMain
     *
     * @covers SimpleScpTask::main
     * @covers SimpleScpTask::_performExecCommand
     *
     * @dataProvider dataProviderForTestMainWithResultEvaluationForSucceedingCommand
     *
     * @param string|null $returnProperty
     * @param string|null $outputProperty
     */
    public function testMainWithResultEvaluationForSucceedingCommand($returnProperty, $outputProperty)
    {
        $cmd = 'phpunit ';
        $cmdArgs = array('--version');
        $remoteUser = 'nobody';
        $remoteHost = 'nowhere';
        $source = 'someLocalResource';
        $target = 'someRemoteTarget';

        // == "someLocalResource nobody@nowhere:someRemoteTarget"
        $expectedPartiallyAssembledCommand =  $source . ' ' . $remoteUser . '@' . $remoteHost . ':' . $target;

        $expectedLogMessage1 = 'Copy "' . $source . '" to host "' . $remoteHost . '"';

        // == "phpunit --version someLocalResource nowhere:someRemoteTarget"
        $expectedAssembledCommand = $cmd . implode(' ', $cmdArgs) . ' ' . $expectedPartiallyAssembledCommand;

        $output   = array();
        $exitCode = null;
        exec($expectedAssembledCommand, $output, $exitCode);

        // == array('PHPUnit *.*.* by Sebastian Bergmann.', '');
        $expectedOutput = $output;
        // == 0
        $expectedExitCode = $exitCode;

        $expectedLogMessages2 = $expectedOutput;


        $projectMock = $this->getMock('Project', array('setProperty'));

        if ($returnProperty) {
            $projectMock->expects($this->at(0))
                ->method('setProperty')
                ->with($this->equalTo($returnProperty), $this->identicalTo($expectedExitCode))
                ->will($this->returnValue(null));
        }
        if ($outputProperty) {
            $projectMock->expects($this->at((integer) !empty($returnProperty)))
                ->method('setProperty')
                ->with($this->equalTo($outputProperty), $this->identicalTo(implode("\n", $expectedOutput)))
                ->will($this->returnValue(null));
        }
        if (!$returnProperty && !$outputProperty) {
            $projectMock->expects($this->never())
                ->method('setProperty');
        }


        $out = $this->getMock(self::$_cut, array('log'));

        $out->expects($this->at(0))
            ->method('log')
            ->with($this->equalTo($expectedLogMessage1))
            ->will($this->returnValue(null));

        $i = 0;
        foreach ($expectedLogMessages2 as $expectedLogMessage2) {
            $out->expects($this->at(1 + $i++))
                ->method('log')
                ->with($this->equalTo($expectedLogMessage2), $this->equalTo(Project::MSG_VERBOSE))
                ->will($this->returnValue(null));
        }

        $this->setProperty($out, '_cmd', $cmd);
        $this->setProperty($out, '_cmdArgs', $cmdArgs);
        $this->setProperty($out, '_remoteUser', $remoteUser);
        $this->setProperty($out, '_remoteHost', $remoteHost);
        $this->setProperty($out, '_target', $target);
        $this->setProperty($out, '_source', $source);

        $this->setProperty($out, '_returnProperty', $returnProperty);
        $this->setProperty($out, '_outputProperty', $outputProperty);

        $this->setProperty($out, 'project', $projectMock);


        $out->main();
    }

    /**
     * data provider for the test method testMainWithResultEvaluationForSucceedingCommand()
     * @return array
     */
    public function dataProviderForTestMainWithResultEvaluationForSucceedingCommand()
    {
        return array(
            /* data set #0 */
            array(
                'returnProperty' => null,
                'outputProperty' => null
            ),
            /* data set #1 */
            array(
                'returnProperty' => 'exitCode',
                'outputProperty' => null
            ),
            /* data set #2 */
            array(
                'returnProperty' => null,
                'outputProperty' => 'output'
            ),
            /* data set #3 */
            array(
                'returnProperty' => 'exitCode',
                'outputProperty' => 'output'
            )
        );
    }

    /**
     * @depends testMain
     *
     * @covers SimpleScpTask::main
     * @covers SimpleScpTask::_performExecCommand
     *
     * @dataProvider dataProviderForTestMainWithResultEvaluationForFailingCommand
     *
     * @param boolean $haltOnFailure
     */
    public function testMainWithResultEvaluationForFailingCommand($haltOnFailure)
    {
        $cmd = 'phpunit ';
        $cmdArgs = array('--version', '--x');
        $remoteUser = 'nobody';
        $remoteHost = 'nowhere';
        $source = 'someLocalResource';
        $target = 'someRemoteTarget';

        $returnProperty = 'exitCode';
        $outputProperty = 'output';

        // == "someLocalResource nobody@nowhere:someRemoteTarget"
        $expectedPartiallyAssembledCommand =  $source . ' ' . $remoteUser . '@' . $remoteHost . ':' . $target;

        $expectedLogMessage1 = 'Copy "' . $source . '" to host "' . $remoteHost . '"';

        // == "phpunit --version --x someLocalResource nowhere:someRemoteTarget"
        $expectedAssembledCommand = $cmd . implode(' ', $cmdArgs) . ' ' . $expectedPartiallyAssembledCommand;

        $output   = array();
        $exitCode = null;
        exec($expectedAssembledCommand, $output, $exitCode);

        // == array('PHPUnit *.*.* by Sebastian Bergmann.', '', 'unrecognized option --x');
        $expectedOutput = $output;
        // == 1
        $expectedExitCode = $exitCode;

        $expectedLogMessages2 = $expectedOutput;


        $projectMock = $this->getMock('Project', array('setProperty'));
        $projectMock->expects($this->at(0))
            ->method('setProperty')
            ->with($this->equalTo($returnProperty), $this->identicalTo($expectedExitCode))
            ->will($this->returnValue(null));
        $projectMock->expects($this->at(1))
            ->method('setProperty')
            ->with($this->equalTo($outputProperty), $this->identicalTo(implode("\n", $expectedOutput)))
            ->will($this->returnValue(null));


        $out = $this->getMock(self::$_cut, array('log'));

        $out->expects($this->at(0))
            ->method('log')
            ->with($this->equalTo($expectedLogMessage1))
            ->will($this->returnValue(null));

        $i = 0;
        foreach ($expectedLogMessages2 as $expectedLogMessage2) {
            $out->expects($this->at(1 + $i++))
                ->method('log')
                ->with($this->equalTo($expectedLogMessage2), $this->equalTo(Project::MSG_VERBOSE))
                ->will($this->returnValue(null));
        }

        $this->setProperty($out, '_cmd', $cmd);
        $this->setProperty($out, '_cmdArgs', $cmdArgs);
        $this->setProperty($out, '_remoteUser', $remoteUser);
        $this->setProperty($out, '_remoteHost', $remoteHost);
        $this->setProperty($out, '_target', $target);
        $this->setProperty($out, '_source', $source);

        $this->setProperty($out, '_returnProperty', $returnProperty);
        $this->setProperty($out, '_outputProperty', $outputProperty);

        $this->setProperty($out, '_haltOnFailure', $haltOnFailure);

        $this->setProperty($out, 'project', $projectMock);


        if ($haltOnFailure) {
            $this->setExpectedException('BuildException', 'Task exited with code ' . $expectedExitCode);
        }


        $out->main();
    }

    /**
     * data provider for the test method testMainWithResultEvaluationForFailingCommand()
     * @return array
     */
    public function dataProviderForTestMainWithResultEvaluationForFailingCommand()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderBooleanValues('haltOnFailure');
    }
}