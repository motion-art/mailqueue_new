<?php
/**
 * File for the class SshBulkTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class SshBulkTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class SshBulkTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (SshBulkTask)
     * @var string
     */
    protected static $_cut = 'Abstract_SshBulkTask';


    /**
     * @covers Abstract_SshBulkTask::setList
     */
    public function testSetList()
    {
        $list = 'test';

        $out = $this->getMockForAbstractClass(self::$_cut);

        $out->setList($list);

        $this->assertAttributeSame($list, '_list', $out);
    }

    /**
     * @covers Abstract_SshBulkTask::setPrefix
     */
    public function testSetPrefix()
    {
        $prefix = 'test';

        $out = $this->getMockForAbstractClass(self::$_cut);

        $out->setPrefix($prefix);

        $this->assertAttributeSame($prefix, '_prefix', $out);
    }

    /**
     * @covers Abstract_SshBulkTask::getHosts
     *
     * @dataProvider dataProviderForTestGetHostsWhenListIsEmpty
     *
     * @param mixed $emptyList
     */
    public function testGetHostsWhenListIsEmpty($emptyList)
    {
        $out = $this->getMockForAbstractClass(self::$_cut);

        $this->setProperty($out, '_list', $emptyList);


        $this->setExpectedException('BuildException', 'You must provide a remote host list');


        $out->getHosts();
    }

    /**
     * data provider for the test method testGetHostsWhenListIsEmpty()
     * @return array
     */
    public function dataProviderForTestGetHostsWhenListIsEmpty()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderEmptyValues('emptyList');
    }

    /**
     * @covers Abstract_SshBulkTask::getHosts
     *
     * @dataProvider dataProviderForTestGetHosts
     *
     * @param string $list
     * @param string $prefix
     * @param array  $projectProperties
     * @param array  $expectedHosts
     */
    public function testGetHosts($list, $prefix, $projectProperties, $expectedHosts)
    {
        $project = new Project();

        foreach ($projectProperties as $propertyName => $propertyValue) {
            $project->setProperty($propertyName, $propertyValue);
        }


        $out = $this->getMockForAbstractClass(self::$_cut);

        $this->setProperty($out, '_list', $list);
        $this->setProperty($out, '_prefix', $prefix);

        $this->setProperty($out, 'project', $project);


        $result = $out->getHosts();


        $this->assertSame($expectedHosts, $result);
    }

    /**
     * data provider for the test method testGetHosts()
     * @return array
     */
    public function dataProviderForTestGetHosts()
    {
        $key1 = 'key1';
        $key2 = 'key2';

        $prefix = 'prefix';

        $user  = 'user';

        $host1  = 'host1';
        $vhost1 = 'vhost1';

        $host2  = 'host2';
        $vhost2 = 'vhost2';

        return array(
            /* data set #0 */
            array(
                'list'              => $key1,
                'prefix'            => $prefix,
                'projectProperties' => array(
                    $prefix . '.' . $key1 . '.user'  => $user,
                    $prefix . '.' . $key1 . '.host'  => $host1,
                    $prefix . '.' . $key1 . '.vhost' => $vhost1
                ),
                'expectedHosts'     => array(
                    array(
                        'host'  => $host1,
                        'user'  => $user,
                        'vhost' => $vhost1
                    )
                )
            ),
            /* data set #1 */
            array(
                'list'              => $key1,
                'prefix'            => $prefix,
                'projectProperties' => array(
                    $prefix . '.user'                => $user,
                    $prefix . '.' . $key1 . '.host'  => $host1,
                    $prefix . '.' . $key1 . '.vhost' => $vhost1
                ),
                'expectedHosts'     => array(
                    array(
                        'host'  => $host1,
                        'user'  => $user,
                        'vhost' => $vhost1
                    )
                )
            ),
            /* data set #2 */
            array(
                'list'              => $key1 . ',' . $key2,
                'prefix'            => $prefix,
                'projectProperties' => array(
                    $prefix . '.' . $key1 . '.user'  => $user,
                    $prefix . '.' . $key1 . '.host'  => $host1,
                    $prefix . '.' . $key1 . '.vhost' => $vhost1,
                    $prefix . '.' . $key2 . '.user'  => $user,
                    $prefix . '.' . $key2 . '.host'  => $host2,
                    $prefix . '.' . $key2 . '.vhost' => $vhost2
                ),
                'expectedHosts'     => array(
                    array(
                        'host'  => $host1,
                        'user'  => $user,
                        'vhost' => $vhost1
                    ),
                    array(
                        'host'  => $host2,
                        'user'  => $user,
                        'vhost' => $vhost2
                    )
                )
            ),
            /* data set #3 */
            array(
                'list'              => $key1 . ',' . $key2,
                'prefix'            => $prefix,
                'projectProperties' => array(
                    $prefix . '.user'                => $user,
                    $prefix . '.' . $key1 . '.host'  => $host1,
                    $prefix . '.' . $key1 . '.vhost' => $vhost1,
                    $prefix . '.' . $key2 . '.host'  => $host2,
                    $prefix . '.' . $key2 . '.vhost' => $vhost2
                ),
                'expectedHosts'     => array(
                    array(
                        'host'  => $host1,
                        'user'  => $user,
                        'vhost' => $vhost1
                    ),
                    array(
                        'host'  => $host2,
                        'user'  => $user,
                        'vhost' => $vhost2
                    )
                )
            )
        );
    }
}