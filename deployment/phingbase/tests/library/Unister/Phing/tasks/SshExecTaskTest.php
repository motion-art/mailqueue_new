<?php
/**
 * File for the class SshExecTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class SshExecTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class SshExecTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (SshExecTask)
     * @var string
     */
    protected static $_cut = 'SshExecTask';


    /**
     * @covers SshExecTask::appendCmdArg
     */
    public function testAppendCmdArg()
    {
        $argument = 'test';

        $expectedAttributeValue = array($argument);

        $out = new self::$_cut();

        $out->appendCmdArg($argument);

        $this->assertAttributeSame($expectedAttributeValue, '_cmdArgs', $out);
    }

    /**
     * @covers SshExecTask::setRemoteUser
     */
    public function testSetRemoteUser()
    {
        $remoteUser = 'test';

        $out = new self::$_cut();

        $out->setRemoteUser($remoteUser);

        $this->assertAttributeSame($remoteUser, '_remoteUser', $out);
    }

    /**
     * @covers SshExecTask::setRemoteHost
     */
    public function testSetRemoteHost()
    {
        $remoteHost = 'test';

        $out = new self::$_cut();

        $out->setRemoteHost($remoteHost);

        $this->assertAttributeSame($remoteHost, '_remoteHost', $out);
    }

    /**
     * @covers SshExecTask::setCommand
     */
    public function testSetCommand()
    {
        $command = 'test';

        $out = new self::$_cut();

        $out->setCommand($command);

        $this->assertAttributeSame($command, '_command', $out);
    }

    /**
     * @covers SshExecTask::setArgs
     *
     * @dataProvider dataProviderForTestSetArgs
     *
     * @param mixed $arguments
     */
    public function testSetArgs($arguments)
    {
        $out = $this->getMock(self::$_cut, array('appendCmdArg'));

        $out->expects($this->once())
            ->method('appendCmdArg')
            ->with($this->equalTo($arguments))
            ->will($this->returnValue(null));


        $out->setArgs($arguments);
    }

    /**
     * data provider for the test method testSetArgs()
     * @return array
     */
    public function dataProviderForTestSetArgs()
    {
        return array(
            /* data set #0 */
            array('arguments' => null),
            /* data set #1 */
            array('arguments' => true),
            /* data set #2 */
            array('arguments' => false),
            /* data set #3 */
            array('arguments' => 0),
            /* data set #4 */
            array('arguments' => 1),
            /* data set #5 */
            array('arguments' => ''),
            /* data set #6 */
            array('arguments' => '0'),
            /* data set #7 */
            array('arguments' => '1'),
            /* data set #8 */
            array('arguments' => array()),
            /* data set #9 */
            array('arguments' => array('0')),
            /* data set #10 */
            array('arguments' => array('0', '1')),
            /* data set #11 */
            array('arguments' => new stdClass())
        );
    }

    /**
     * @covers SshExecTask::setHaltOnFailure
     *
     * @dataProvider dataProviderForTestSetHaltOnFailure
     *
     * @param mixed   $flag
     * @param boolean $expectedPropertyValue
     */
    public function testSetHaltOnFailure($flag, $expectedPropertyValue)
    {
        $out = new self::$_cut();

        $out->setHaltOnFailure($flag);

        $this->assertAttributeSame($expectedPropertyValue, '_haltOnFailure', $out);
    }

    /**
     * data provider for the test method testSetHaltOnFailure()
     * @return array
     */
    public function dataProviderForTestSetHaltOnFailure()
    {
        return array(
            /* data set #0 */
            array(
                'flag'                  => true,
                'expectedPropertyValue' => true
            ),
            /* data set #1 */
            array(
                'flag'                  => false,
                'expectedPropertyValue' => false
            ),
            /* data set #2 */
            array(
                'flag'                  => 1,
                'expectedPropertyValue' => true
            ),
            /* data set #3 */
            array(
                'flag'                  => 0,
                'expectedPropertyValue' => false
            ),
            /* data set #4 */
            array(
                'flag'                  => '1',
                'expectedPropertyValue' => true
            ),
            /* data set #5 */
            array(
                'flag'                  => '0',
                'expectedPropertyValue' => false
            ),
            /* data set #6 */
            array(
                'flag'                  => 'yes',
                'expectedPropertyValue' => true
            ),
            /* data set #7 */
            array(
                'flag'                  => 'no',
                'expectedPropertyValue' => true
            )
        );
    }

    /**
     * @covers SshExecTask::setDir
     */
    public function testSetDir()
    {
        $dir = 'test';

        $out = new self::$_cut();

        $out->setDir($dir);

        $this->assertAttributeSame($dir, '_dir', $out);
    }

    /**
     * @covers SshExecTask::setReturnProperty
     */
    public function testSetReturnProperty()
    {
        $returnProperty = 'test';

        $out = new self::$_cut();

        $out->setReturnProperty($returnProperty);

        $this->assertAttributeSame($returnProperty, '_returnProperty', $out);
    }

    /**
     * @covers SshExecTask::setOutputProperty
     */
    public function testSetOutputProperty()
    {
        $outputProperty = 'test';

        $out = new self::$_cut();

        $out->setOutputProperty($outputProperty);

        $this->assertAttributeSame($outputProperty, '_outputProperty', $out);
    }

    /**
     * @covers SshExecTask::execCommand
     *
     * @dataProvider dataProviderForTestExecCommandWithEmptyDir
     *
     * @param mixed $emptyDir
     */
    public function testExecCommandWithEmptyDir($emptyDir)
    {
        $cmd = 'phpunit ';
        $cmdArgs = array('--version', '--x');
        $remoteUser = 'nobody';
        $remoteHost = 'nowhere';

        $remoteCommandString = 'some remote command';

        // == "nobody@nowhere 'some remote command'"
        $expectedPartiallyAssembledCommand = $remoteUser . '@' . $remoteHost . ' '
            . escapeshellarg($remoteCommandString);

        $expectedLogMessage = 'Executing remote command "'
            . $expectedPartiallyAssembledCommand
            . '" on host "' . $remoteHost . '"';

        // == "phpunit --version --x nobody@nowhere 'some remote command'"
        $expectedAssembledCommand = $cmd . implode(' ', $cmdArgs) . ' ' . $expectedPartiallyAssembledCommand;


        $projectMock = $this->getMock('Project', array('setProperty'));
        $projectMock->expects($this->never())
            ->method('setProperty');


        $out = $this->getMock(self::$_cut, array('log', '_performExecCommand'));

        $out->expects($this->once())
            ->method('log')
            ->with($this->equalTo($expectedLogMessage))
            ->will($this->returnValue(null));

        $out->expects($this->once())
            ->method('_performExecCommand')
            ->with($this->equalTo($expectedAssembledCommand), $this->isEmpty(), $this->isEmpty())
            ->will($this->returnValue(null));

        $this->setProperty($out, '_cmd', $cmd);
        $this->setProperty($out, '_cmdArgs', $cmdArgs);
        $this->setProperty($out, '_remoteUser', $remoteUser);
        $this->setProperty($out, '_remoteHost', $remoteHost);
        $this->setProperty($out, '_dir', $emptyDir);

        $this->setProperty($out, 'project', $projectMock);


        $exitCode = null;
        $out->execCommand($remoteCommandString, $exitCode);
    }

    /**
     * data provider for the test method testExecCommandWithEmptyDir()
     * @return array
     */
    public function dataProviderForTestExecCommandWithEmptyDir()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderEmptyValues('emptyDir');
    }

    /**
     * @depends testExecCommandWithEmptyDir
     *
     * @covers SshExecTask::execCommand
     *
     * @dataProvider dataProviderForTestExecCommandWithEmptyRemoteUser
     *
     * @param mixed $emptyRemoteUser
     */
    public function testExecCommandWithEmptyRemoteUser($emptyRemoteUser)
    {
        $cmd = 'phpunit ';
        $cmdArgs = array('--version', '--x');
        $remoteHost = 'nowhere';
        $dir = '..';

        $remoteCommandString = 'some remote command';

        // == "nowhere 'cd ..\; some remote command'"
        $expectedPartiallyAssembledCommand = $remoteHost . ' '
            . escapeshellarg('cd ' . $dir . ' && ' . $remoteCommandString);

        $expectedLogMessage = 'Executing remote command "'
            . $expectedPartiallyAssembledCommand
            . '" on host "' . $remoteHost . '"';

        // == "phpunit --version --x nowhere 'cd ..\; some remote command'"
        $expectedAssembledCommand = $cmd . implode(' ', $cmdArgs) . ' ' . $expectedPartiallyAssembledCommand;


        $projectMock = $this->getMock('Project', array('setProperty'));
        $projectMock->expects($this->never())
            ->method('setProperty');


        $out = $this->getMock(self::$_cut, array('log', '_performExecCommand'));

        $out->expects($this->once())
            ->method('log')
            ->with($this->equalTo($expectedLogMessage))
            ->will($this->returnValue(null));

        $out->expects($this->once())
            ->method('_performExecCommand')
            ->with($this->equalTo($expectedAssembledCommand), $this->isEmpty(), $this->isEmpty())
            ->will($this->returnValue(null));

        $this->setProperty($out, '_cmd', $cmd);
        $this->setProperty($out, '_cmdArgs', $cmdArgs);
        $this->setProperty($out, '_remoteUser', $emptyRemoteUser);
        $this->setProperty($out, '_remoteHost', $remoteHost);
        $this->setProperty($out, '_dir', $dir);

        $this->setProperty($out, 'project', $projectMock);


        $exitCode = null;
        $out->execCommand($remoteCommandString, $exitCode);
    }

    /**
     * data provider for the test method testExecCommandWithEmptyRemoteUser()
     * @return array
     */
    public function dataProviderForTestExecCommandWithEmptyRemoteUser()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderEmptyValues('emptyRemoteUser');
    }

    /**
     * @depends testExecCommandWithEmptyRemoteUser
     *
     * @covers SshExecTask::execCommand
     */
    public function testExecCommand()
    {
        $cmd = 'phpunit ';
        $cmdArgs = array('--version', '--x');
        $remoteUser = 'nobody';
        $remoteHost = 'nowhere';
        $dir = '..';

        $remoteCommandString = 'some remote command';

        // == "nobody@nowhere 'cd ..\; some remote command'"
        $expectedPartiallyAssembledCommand = $remoteUser . '@' . $remoteHost . ' '
            . escapeshellarg('cd ' . $dir . ' && ' . $remoteCommandString);

        $expectedLogMessage = 'Executing remote command "'
            . $expectedPartiallyAssembledCommand
            . '" on host "' . $remoteHost . '"';

        // == "phpunit --version --x nobody@nowhere 'cd ..\; some remote command'"
        $expectedAssembledCommand = $cmd . implode(' ', $cmdArgs) . ' ' . $expectedPartiallyAssembledCommand;


        $projectMock = $this->getMock('Project', array('setProperty'));
        $projectMock->expects($this->never())
            ->method('setProperty');


        $out = $this->getMock(self::$_cut, array('log', '_performExecCommand'));

        $out->expects($this->once())
            ->method('log')
            ->with($this->equalTo($expectedLogMessage))
            ->will($this->returnValue(null));

        $out->expects($this->once())
            ->method('_performExecCommand')
            ->with($this->equalTo($expectedAssembledCommand), $this->isEmpty(), $this->isEmpty())
            ->will($this->returnValue(null));

        $this->setProperty($out, '_cmd', $cmd);
        $this->setProperty($out, '_cmdArgs', $cmdArgs);
        $this->setProperty($out, '_remoteUser', $remoteUser);
        $this->setProperty($out, '_remoteHost', $remoteHost);
        $this->setProperty($out, '_dir', $dir);

        $this->setProperty($out, 'project', $projectMock);


        $exitCode = null;
        $out->execCommand($remoteCommandString, $exitCode);
    }

    /**
     * @depends testExecCommand
     *
     * @covers SshExecTask::execCommand
     * @covers SshExecTask::_performExecCommand
     *
     * @dataProvider dataProviderForTestExecCommandWithResultEvaluation
     *
     * @param string|null $returnProperty
     * @param string|null $outputProperty
     */
    public function testExecCommandWithResultEvaluation($returnProperty, $outputProperty)
    {
        $cmd = 'phpunit ';
        $cmdArgs = array('--version', '--x');
        $remoteUser = 'nobody';
        $remoteHost = 'nowhere';
        $dir = '..';

        $remoteCommandString = 'some remote command';

        // == "nobody@nowhere 'cd ..\; some remote command'"
        $expectedPartiallyAssembledCommand = $remoteUser . '@' . $remoteHost . ' '
            . escapeshellarg('cd ' . $dir . ' && ' . $remoteCommandString);

        $expectedLogMessage = 'Executing remote command "'
            . $expectedPartiallyAssembledCommand
            . '" on host "' . $remoteHost . '"';

        // == "phpunit --version --x nobody@nowhere 'cd ..\; some remote command'"
        $expectedAssembledCommand = $cmd . implode(' ', $cmdArgs) . ' ' . $expectedPartiallyAssembledCommand;

        $output   = null;
        $exitCode = null;
        exec($expectedAssembledCommand, $output, $exitCode);

        // == array('PHPUnit *.*.* by Sebastian Bergmann.', '', 'unrecognized option --x');
        $expectedOutput = $output;
        // == 1
        $expectedExitCode = $exitCode;


        $projectMock = $this->getMock('Project', array('setProperty'));

        if ($returnProperty) {
            $projectMock->expects($this->at(0))
                ->method('setProperty')
                ->with($this->equalTo($returnProperty), $this->identicalTo($expectedExitCode))
                ->will($this->returnValue(null));
        }
        if ($outputProperty) {
            $projectMock->expects($this->at((integer) !empty($returnProperty)))
                ->method('setProperty')
                ->with($this->equalTo($outputProperty), $this->identicalTo(implode("\n", $expectedOutput)))
                ->will($this->returnValue(null));
        }
        if (!$returnProperty && !$outputProperty) {
            $projectMock->expects($this->never())
                ->method('setProperty');
        }


        $out = $this->getMock(self::$_cut, array('log'));

        $out->expects($this->once())
            ->method('log')
            ->with($this->equalTo($expectedLogMessage))
            ->will($this->returnValue(null));

        $this->setProperty($out, '_cmd', $cmd);
        $this->setProperty($out, '_cmdArgs', $cmdArgs);
        $this->setProperty($out, '_remoteUser', $remoteUser);
        $this->setProperty($out, '_remoteHost', $remoteHost);
        $this->setProperty($out, '_dir', $dir);

        $this->setProperty($out, '_returnProperty', $returnProperty);
        $this->setProperty($out, '_outputProperty', $outputProperty);

        $this->setProperty($out, 'project', $projectMock);


        $exitCode = null;
        $result = $out->execCommand($remoteCommandString, $exitCode);


        $this->assertSame($expectedOutput, $result);
        $this->assertSame($expectedExitCode, $exitCode);
    }

    /**
     * data provider for the test method testExecCommandWithResultEvaluation()
     * @return array
     */
    public function dataProviderForTestExecCommandWithResultEvaluation()
    {
        return array(
            /* data set #0 */
            array(
                'returnProperty' => null,
                'outputProperty' => null
            ),
            /* data set #1 */
            array(
                'returnProperty' => 'exitCode',
                'outputProperty' => null
            ),
            /* data set #2 */
            array(
                'returnProperty' => null,
                'outputProperty' => 'output'
            ),
            /* data set #3 */
            array(
                'returnProperty' => 'exitCode',
                'outputProperty' => 'output'
            )
        );
    }

    /**
     * @covers SshExecTask::main
     *
     * @dataProvider dataProviderForTestMainWhenRemoteHostIsEmpty
     *
     * @param mixed $emptyRemoteHost
     */
    public function testMainWhenRemoteHostIsEmpty($emptyRemoteHost)
    {
        $command = 'command';


        $out = $this->getMock(self::$_cut, array('execCommand'));

        $out->expects($this->never())
            ->method('execCommand');

        $this->setProperty($out, '_remoteHost', $emptyRemoteHost);
        $this->setProperty($out, '_command', $command);


        $this->setExpectedException('BuildException', 'You must provide a remote host');


        $out->main();
    }

    /**
     * data provider for the test method testMainWhenRemoteHostIsEmpty()
     * @return array
     */
    public function dataProviderForTestMainWhenRemoteHostIsEmpty()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderEmptyValues('emptyRemoteHost');
    }

    /**
     * @covers SshExecTask::main
     *
     * @dataProvider dataProviderForTestMainWhenCommandIsEmpty
     *
     * @param mixed $emptyCommand
     */
    public function testMainWhenCommandIsEmpty($emptyCommand)
    {
        $remoteHost = 'remoteHost';


        $out = $this->getMock(self::$_cut, array('execCommand'));

        $out->expects($this->never())
            ->method('execCommand');

        $this->setProperty($out, '_remoteHost', $remoteHost);
        $this->setProperty($out, '_command', $emptyCommand);


        $this->setExpectedException('BuildException', 'You must provide a remote command');


        $out->main();
    }

    /**
     * data provider for the test method testMainWhenCommandIsEmpty()
     * @return array
     */
    public function dataProviderForTestMainWhenCommandIsEmpty()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderEmptyValues('emptyCommand');
    }

    /**
     * @covers SshExecTask::main
     *
     * @dataProvider dataProviderForTestMainWithNecessaryAttributeValues
     *
     * @param array $output
     */
    public function testMainWithNecessaryAttributeValuesSucceeding($output)
    {
        $remoteHost = 'remoteHost';
        $command    = 'command';


        $out = $this->getMock(self::$_cut, array('log', 'execCommand'));

        $out->expects($this->at(0))
            ->method('execCommand')
            ->with($this->equalTo($command), $this->isNull())
            ->will($this->returnValue($output));

        if (!empty($output)) {
            $i = 0;
            foreach ($output as $line) {
                $out->expects($this->at(1 + $i++))
                    ->method('log')
                    ->with($this->equalTo($line), $this->equalTo(Project::MSG_VERBOSE))
                    ->will($this->returnValue(null));
            }
        } else {
            $out->expects($this->never())
                ->method('log');
        }

        $this->setProperty($out, '_remoteHost', $remoteHost);
        $this->setProperty($out, '_command', $command);


        $out->main();
    }

    /**
     * @covers SshExecTask::main
     *
     * @dataProvider dataProviderForTestMainWithNecessaryAttributeValues
     *
     * @param array $output
     */
    public function testMainWithNecessaryAttributeValuesFailingWithoutHaltOnFailure($output)
    {
        $this->markTestIncomplete(
            'probably not testable in isolation'
            . ' - setting the value of a by-reference parameter by a mocked method is not possible (yet)');

        /* test code must be mostly the same, but with
         * - the command $this->setProperty($out, '_haltOnFailure', false);
         * prior to $out->main(); */
    }

    /**
     * @covers SshExecTask::main
     *
     * @dataProvider dataProviderForTestMainWithNecessaryAttributeValues
     *
     * @param array $output
     */
    public function testMainWithNecessaryAttributeValuesFailingWithHaltOnFailure($output)
    {
        $this->markTestIncomplete(
            'probably not testable in isolation'
            . ' - setting the value of a by-reference parameter by a mocked method is not possible (yet)');

        /* test code must be mostly the same, but with
         * - the command $this->setProperty($out, '_haltOnFailure', true);
         * - the command $this->setExpectedException('BuildException', 'Task exited with code ' . $exitCode);
         * prior to $out->main(); */
    }

    /**
     * data provider for the test methods testMainWithNecessaryAttributeValues*()
     * @return array
     */
    public function dataProviderForTestMainWithNecessaryAttributeValues()
    {
        return array(
            /* data set #0 */
            array('output' => array()),
            /* data set #1 */
            array('output' => array('line1')),
            /* data set #2 */
            array('output' => array('line1', 'line2')),
        );
    }
}