<?php
/**
 * File for the class SecurityPromptTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class SecurityPromptTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class SecurityPromptTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (SecurityPromptTask)
     * @var string
     */
    protected static $_cut = 'SecurityPromptTask';


    /**
     * @covers SecurityPromptTask::main
     *
     * @dataProvider dataProviderForTestMainWhenCurrentValueIsNotNull
     *
     * @param mixed $notNullValue
     */
    public function testMainWhenCurrentValueIsNotNull($notNullValue)
    {
        $propertyName = 'test';

        $expectedOutput = '';


        $projectMock = $this->getMock('Project', array('getProperty', 'setProperty'));
        $projectMock->expects($this->once())
            ->method('getProperty')
            ->with($this->equalTo($propertyName))
            ->will($this->returnValue($notNullValue));
        $projectMock->expects($this->never())
            ->method('setProperty');


        $out = $this->getMock(self::$_cut, array('_performSecurePromptCommand'));

        $out->expects($this->never())
            ->method('_performSecurePromptCommand');

        $this->setProperty($out, '_propertyName', $propertyName);

        $this->setProperty($out, 'project', $projectMock);


        $output = null;
        $this->executeTestWithOutputBuffering($output, $out, 'main');


        $this->assertSame($expectedOutput, $output);
    }

    /**
     * data provider for the test method testMainWhenCurrentValueIsNotNull()
     * @return array
     */
    public function dataProviderForTestMainWhenCurrentValueIsNotNull()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderNotNullValues('notNullValue');
    }

    /**
     * @covers SecurityPromptTask::main
     *
     * @dataProvider dataProviderForTestMain
     *
     * @param string $securePromptCommandResult
     * @param string $expectedPropertyValue
     */
    public function testMain($securePromptCommandResult, $expectedPropertyValue)
    {
        $propertyName = 'test';

        $promptText = 'Dein Passwort';
        $promptCharacter = '?';

        $expectedOutput = "\n" . $promptText . ' ' . $promptCharacter . ' ' . "\n\n";


        $projectMock = $this->getMock('Project', array('getProperty', 'setProperty'));
        $projectMock->expects($this->once())
            ->method('getProperty')
            ->with($this->equalTo($propertyName))
            ->will($this->returnValue(null));
        $projectMock->expects($this->once())
            ->method('setProperty')
            ->with($this->equalTo($propertyName), $this->equalTo($expectedPropertyValue))
            ->will($this->returnValue(null));


        $out = $this->getMock(self::$_cut, array('_performSecurePromptCommand'));

        $out->expects($this->once())
            ->method('_performSecurePromptCommand')
            ->will($this->returnValue($securePromptCommandResult));

        $this->setProperty($out, '_propertyName', $propertyName);
        $this->setProperty($out, '_promptText', $promptText);
        $this->setProperty($out, '_promptCharacter', $promptCharacter);

        $this->setProperty($out, 'project', $projectMock);


        $output = null;
        $this->executeTestWithOutputBuffering($output, $out, 'main');


        $this->assertSame($expectedOutput, $output);
    }

    /**
     * data provider for the test method testMain()
     * @return array
     */
    public function dataProviderForTestMain()
    {
        return array(
            /* data set #0 */
            array(
                'securePromptCommandResult' => "password\r\n",
                'expectedPropertyValue'     => 'password'
            ),
            /* data set #1 */
            array(
                'securePromptCommandResult' => "password\n",
                'expectedPropertyValue'     => 'password'
            ),
            /* data set #2 */
            array(
                'securePromptCommandResult' => "password\r",
                'expectedPropertyValue'     => 'password'
            ),
            /* data set #3 */
            array(
                'securePromptCommandResult' => 'password',
                'expectedPropertyValue'     => 'password'
            )
        );
    }

    /**
     * @covers SecurityPromptTask::setPromptCharacter
     */
    public function testSetPromptCharacter()
    {
        $promptCharacter = 'test';

        $out = new self::$_cut();

        $out->setPromptCharacter($promptCharacter);

        $this->assertAttributeSame($promptCharacter, '_promptCharacter', $out);
    }

    /**
     * @covers SecurityPromptTask::setPromptText
     */
    public function testSetPromptText()
    {
        $promptText = 'test';

        $out = new self::$_cut();

        $out->setPromptText($promptText);

        $this->assertAttributeSame($promptText, '_promptText', $out);
    }

    /**
     * @covers SecurityPromptTask::setPropertyName
     */
    public function testSetPropertyName()
    {
        $propertyName = 'test';

        $out = new self::$_cut();

        $out->setPropertyName($propertyName);

        $this->assertAttributeSame($propertyName, '_propertyName', $out);
    }
}