<?php
/**
 * File for the class SvnExternalTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class SvnExternalTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class SvnExternalTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (SvnExternalTask)
     * @var string
     */
    protected static $_cut = 'SvnExternalTask';


    /**
     * @covers SvnExternalTask::setDir
     */
    public function testSetDir()
    {
        $dir = 'test';

        $out = new self::$_cut();

        $out->setDir($dir);

        $this->assertAttributeSame($dir, '_directory', $out);
    }

    /**
     * @covers SvnExternalTask::main
     *
     * @dataProvider dataProviderForTestMainWhenDirectoryIsEmpty
     *
     * @param mixed $emptyDirectory
     */
    public function testMainWhenDirectoryIsEmpty($emptyDirectory)
    {
        $out = $this->getMock(self::$_cut, array('_getExternalCheck'));

        $out->expects($this->never())
            ->method('_getExternalCheck');

        $this->setProperty($out, '_directory', $emptyDirectory);

        $this->setExpectedException('BuildException', 'Directory not set');

        $out->main();
    }

    /**
     * data provider for the test method testMainWhenDirectoryIsEmpty()
     * @return array
     */
    public function dataProviderForTestMainWhenDirectoryIsEmpty()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderEmptyValues('emptyDirectory');
    }

    /**
     * @covers SvnExternalTask::main
     *
     * @dataProvider dataProviderForTestMainWithNecessaryAttributeValue
     *
     * @param array $outdated
     * @param array $expectedLogMessages
     */
    public function testMainWithNecessaryAttributeValue($outdated, $expectedLogMessages)
    {
        $directory = 'directory';


        $externalCheckMock = $this->getMock('ExternalCheck', array('setDirs', 'get'));
        $externalCheckMock->expects($this->once())
            ->method('setDirs')
            ->with($this->equalTo(array($directory)))
            ->will($this->returnValue(null));
        $externalCheckMock->expects($this->once())
            ->method('get')
            ->will($this->returnValue($outdated));


        $out = $this->getMock(self::$_cut, array('log', '_getExternalCheck'));

        $out->expects($this->once())
            ->method('_getExternalCheck')
            ->will($this->returnValue($externalCheckMock));

        if (!empty($expectedLogMessages)) {
            $i = 0;
            foreach ($expectedLogMessages as $expectedLogMessage) {
                $out->expects($this->at(1 + $i++))
                    ->method('log')
                    ->with($this->equalTo($expectedLogMessage), $this->equalTo(Project::MSG_WARN))
                    ->will($this->returnValue(null));
            }
        } else {
            $out->expects($this->never())
                ->method('log');
        }

        $this->setProperty($out, '_directory', $directory);


        $out->main();
    }

    /**
     * data provider for the test method testMainWithNecessaryAttributeValue()
     * @return array
     */
    public function dataProviderForTestMainWithNecessaryAttributeValue()
    {
        $name1       = 'name1';
        $external1   = array('update' => 'update1');
        $logMessage1 = 'new version found: ' . $name1 . ' (' . $external1['update'] . ')';

        $name2       = 'name2';
        $external2   = array('update' => 'update2');
        $logMessage2 = 'new version found: ' . $name2 . ' (' . $external2['update'] . ')';

        return array(
            /* data set #0 */
            array(
                'outdated'            => array(),
                'expectedLogMessages' => array()
            ),
            /* data set #1 */
            array(
                'outdated'            => array(array()),
                'expectedLogMessages' => array()
            ),
            /* data set #2 */
            array(
                'outdated'            => array(
                    array(
                        $name1 => $external1
                    )
                ),
                'expectedLogMessages' => array($logMessage1)
            ),
            /* data set #3 */
            array(
                'outdated'            => array(
                    array(
                        $name1 => $external1,
                        $name2 => $external2
                    )
                ),
                'expectedLogMessages' => array(
                    $logMessage1,
                    $logMessage2
                )
            ),
            /* data set #4 */
            array(
                'outdated'            => array(
                    array($name1 => $external1),
                    array($name2 => $external2)
                ),
                'expectedLogMessages' => array(
                    $logMessage1,
                    $logMessage2
                )
            )
        );
    }

    /**
     * @covers SvnExternalTask::_getExternalCheck
     */
    public function testGetExternalCheck()
    {
        $out = new self::$_cut();

        $result = $this->invoke($out, '_getExternalCheck');

        $this->assertInstanceOf('ExternalCheck', $result);
    }
}