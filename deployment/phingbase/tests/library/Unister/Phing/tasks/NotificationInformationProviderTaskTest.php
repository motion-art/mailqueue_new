<?php
/**
 * @category    Phing
 * @package     Phing_Task
 * @copyright   Copyright (c) 2013 Unister GmbH
 * @author      Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author      Tommy Helm <tommy.helm@unister-gmbh.de>
 * @version     $Id:$
 */

/**
 * @category    Phing
 * @package     Phing_Task
 * @copyright   Copyright (c) 2013 Unister GmbH
 */
class NotificationInformationProviderTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test
     * @var string
     */
    protected static $_cut = 'NotificationInformationProviderTask';

    /**
     * @dataProvider provideDataForTestValidateParameters
     */
    public function testValidateParameters(array $params, $shouldThrowException)
    {
        $task = new self::$_cut();
        foreach ($params as $key => $value) {
            $setter = 'set' . ucfirst($key);
            $task->$setter($value);
        }
        $exception = false;
        try {
            $this->invoke($task, '_validateParameters');
        } catch (BuildException $e) {
            $exception = $e;
        }
        if ($shouldThrowException) {
            $errorMessage = 'Property validation should have failed, but did not.';
        } else {
            $errorMessage = 'Property validation should not have failed, but did: ';
            if ($exception) {
                $errorMessage .= $exception->getMessage();
            }
        }
        $this->assertEquals($shouldThrowException, $exception !== false, $errorMessage);
    }

    /**
     * Test der Beschaffung von Change-Logs
     *
     * @return void
     */
    public function testMainChangeLog()
    {
        $version = '1.2.4';
        $path = 'this/is/the/path';
        $changeLog = 'theChangeLog';
        $propertyName = 'propertyName';
        $type = 'changeLog';
        $project = new Project();

        // Atrappe für den Change-Log-Reader
        $changeLogReaderMock = $this->_getMock('Notification_ChangeLogReader', array('loadChangeLogForVersion'));
        $changeLogReaderMock->expects($this->once())->method('loadChangeLogForVersion')->with($this->equalTo($path), $this->equalTo($version))
            ->will($this->returnValue($changeLog));

        $task = new self::$_cut();

        $task->setProject($project);
        $task->setPath($path);
        $task->setVersion($version);
        $task->setProperty($propertyName);
        $task->setChangeLogReader($changeLogReaderMock);
        $task->setType($type);

        $task->main();

        // wurde das Property wie erwartet mit dem Change-Log gesetzt?
        $this->assertEquals($project->getProperty($propertyName), $changeLog);
    }

    /**
     * Test der Beschaffung von Release-Logs
     *
     * @return void
     */
    public function testMainReleaseNotes()
    {
        $version = '1.3.5';
        $path = 'this/is/the/path';
        $releaseNotes = 'theReleaseNotes';
        $propertyName = 'propertyName';
        $type = 'releaseNotes';
        $project = new Project();

        // Atrappe für den Release-Notes-Reader
        $releaseNotesReaderMock = $this->_getMock('Notification_ReleaseNotesReader', array('loadReleaseNotesForVersion'));
        $releaseNotesReaderMock->expects($this->once())->method('loadReleaseNotesForVersion')->with($this->equalTo($path), $this->equalTo($version))
            ->will($this->returnValue($releaseNotes));

        $task = new self::$_cut();

        $task->setProject($project);
        $task->setPath($path);
        $task->setVersion($version);
        $task->setProperty($propertyName);
        $task->setReleaseNotesReader($releaseNotesReaderMock);
        $task->setType($type);

        $task->main();

        // wurde das Property wie erwartet mit den Release-Notes gesetzt?
        $this->assertEquals($project->getProperty($propertyName), $releaseNotes);
    }

    /**
     * Test der Beschaffung von Split-Informationen
     *
     * @return void
     */
    public function testMainSplits()
    {
        $path = 'this/is/the/path';
        $returnedSplits = $this->_getSplitsTestData();
        $propertyName = 'propertyName';
        $type = 'splits';
        $project = new Project();

        // Atrappe für den Split-Test-XML-Reader
        $splitTestXMLReaderMock = $this->_getMock('Notification_SplitTestXMLReader');
        $splitTestXMLReaderMock->expects($this->once())->method('calculateSplittingRates')->with($this->equalTo($path))
            ->will($this->returnValue($returnedSplits));

        $task = new self::$_cut();

        $task->setProject($project);
        $task->setPath($path);
        $task->setProperty($propertyName);
        $task->setSplitTestXMLReader($splitTestXMLReaderMock);
        $task->setType($type);

        $task->main();

        // wurde das Property wie erwartet mit den Release-Notes gesetzt und ist es JSON-kodiert?
        $this->assertEquals($project->getProperty($propertyName), json_encode($returnedSplits));
    }

    /**
     * @return array
     */
    public function provideDataForTestValidateParameters()
    {
        $valid = false;
        $invalid = true;

        // Existenz des Pfads wird vom Task selbst nicht überprüft
        $anyPath = '/path/to/file';

        return array(
            array(
                array(
                    'type'     => 'changeLog',
                    'property' => 'propertyName1',
                    'version'  => '1.1.0',
                    'path'     => $anyPath
                ),
                $valid
            ),
            array(
                array(
                    'type'     => 'releaseNotes',
                    'property' => 'propertyName2',
                    'version'  => '1.1.0',
                    'path'     => $anyPath
                ),
                $valid
            ),
            array(
                array(
                    'type'     => 'splits',
                    'property' => 'propertyName3',
                    'path'     => $anyPath
                ),
                $valid
            ),
            array(
                array(
                    // version fehlt
                    'type'     => 'changeLog',
                    'property' => 'propertyName4',
                    'path'     => $anyPath
                ),
                $invalid
            ),
            array(
                array(
                    // path fehlt
                    'type'     => 'releaseNotes',
                    'property' => 'propertyName5',
                    'version'  => '1.2.3'
                ),
                $invalid
            ),
            array(
                array(
                    // property fehlt
                    'type'    => 'releaseNotes',
                    'version' => '1.2.5',
                    'path'    => $anyPath
                ),
                $invalid
            ),
            array(
                // vieles fehlt
                array('type' => 'splits'),
                $invalid
            ),
            array(
                // type fehlt
                array(
                    'property' => 'propertyName6',
                    'version'  => '1.1.0',
                    'path'     => $anyPath
                ),
                $invalid
            ),
            array(
                array(
                    // nicht existierender Typ
                    'type'     => 'unknownType',
                    'property' => 'propertyName7',
                    'version'  => '1.2.3',
                    'path'     => $anyPath
                ),
                $invalid
            ),
            array(
                array(
                    // Version ist leer
                    'type'     => 'changeLog',
                    'property' => 'propertyName',
                    'path'     => $anyPath,
                    'version'  => ''
                ),
                $invalid
            )
        );
    }

    /**
     * Testdaten, so wie sie auch von einem Notification_SplitTestXMLReader kommen könnten.
     *
     * @return string
     */
    protected function _getSplitsTestData()
    {
        $splits = array();

        $split1 = new stdClass();
        $split1->info = 'red background';
        $split1->splitId = 'split23';
        $split1->rate = 12;
        $splits[] = $split1;

        $split2 = new stdClass();
        $split2->info = 'white background, orange links';
        $split2->splitId = 'split42';
        $split2->rate = 45;
        $splits[] = $split2;

        return $splits;
    }
}