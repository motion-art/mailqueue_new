<?php
/**
 * File for the class CheckRequirementsTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class CheckRequirementsTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class CheckRequirementsTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (CheckRequirementsTask)
     * @var string
     */
    protected static $_cut = 'CheckRequirementsTask';


    /**
     * @covers CheckRequirementsTask::setFilename
     */
    public function testSetFilename()
    {
        $filename = 'test';

        $out = new self::$_cut();

        $out->setFilename($filename);

        $this->assertAttributeSame($filename, '_filename', $out);
    }

    /**
     * @covers CheckRequirementsTask::execCommand
     *
     * @dataProvider dataProviderForTestExecCommandWithEmptyDir
     *
     * @param mixed $emptyDir
     */
    public function testExecCommandWithEmptyDir($emptyDir)
    {
        $cmd = 'phpunit ';
        $cmdArgs = array('--version', '--x');
        $remoteUser = 'nobody';
        $remoteHost = 'nowhere';

        $remoteCommandString = 'some remote command';

        // == "phpunit --version --x nobody@nowhere 'some remote command'"
        $expectedAssembledCommand = $cmd . implode(' ', $cmdArgs) . ' '
            . $remoteUser . '@' . $remoteHost . ' '
            . escapeshellarg($remoteCommandString);


        $projectMock = $this->getMock('Project', array('setProperty'));
        $projectMock->expects($this->never())
            ->method('setProperty');


        $out = $this->getMock(self::$_cut, array('_performExecCommand'));

        $out->expects($this->once())
            ->method('_performExecCommand')
            ->with($this->equalTo($expectedAssembledCommand), $this->isEmpty(), $this->isEmpty())
            ->will($this->returnValue(null));

        $this->setProperty($out, '_cmd', $cmd);
        $this->setProperty($out, '_cmdArgs', $cmdArgs);
        $this->setProperty($out, '_remoteUser', $remoteUser);
        $this->setProperty($out, '_remoteHost', $remoteHost);
        $this->setProperty($out, '_dir', $emptyDir);

        $this->setProperty($out, 'project', $projectMock);


        $exitCode = null;
        $out->execCommand($remoteCommandString, $exitCode);
    }

    /**
     * data provider for the test method testExecCommandWithEmptyDir()
     * @return array
     */
    public function dataProviderForTestExecCommandWithEmptyDir()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderEmptyValues('emptyDir');
    }

    /**
     * @depends testExecCommandWithEmptyDir
     *
     * @covers CheckRequirementsTask::execCommand
     *
     * @dataProvider dataProviderForTestExecCommandWithEmptyRemoteUser
     *
     * @param mixed $emptyRemoteUser
     */
    public function testExecCommandWithEmptyRemoteUser($emptyRemoteUser)
    {
        $cmd = 'phpunit ';
        $cmdArgs = array('--version', '--x');
        $remoteHost = 'nowhere';
        $dir = '..';

        $remoteCommandString = 'some remote command';

        // == "phpunit --version --x nowhere 'cd ..\; some remote command'"
        $expectedAssembledCommand = $cmd . implode(' ', $cmdArgs) . ' '
            . $remoteHost . ' '
            . escapeshellarg('cd ' . $dir . '\; ' . $remoteCommandString);


        $projectMock = $this->getMock('Project', array('setProperty'));
        $projectMock->expects($this->never())
            ->method('setProperty');


        $out = $this->getMock(self::$_cut, array('_performExecCommand'));

        $out->expects($this->once())
            ->method('_performExecCommand')
            ->with($this->equalTo($expectedAssembledCommand), $this->isEmpty(), $this->isEmpty())
            ->will($this->returnValue(null));

        $this->setProperty($out, '_cmd', $cmd);
        $this->setProperty($out, '_cmdArgs', $cmdArgs);
        $this->setProperty($out, '_remoteUser', $emptyRemoteUser);
        $this->setProperty($out, '_remoteHost', $remoteHost);
        $this->setProperty($out, '_dir', $dir);

        $this->setProperty($out, 'project', $projectMock);


        $exitCode = null;
        $out->execCommand($remoteCommandString, $exitCode);
    }

    /**
     * data provider for the test method testExecCommandWithEmptyRemoteUser()
     * @return array
     */
    public function dataProviderForTestExecCommandWithEmptyRemoteUser()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderEmptyValues('emptyRemoteUser');
    }

    /**
     * @depends testExecCommandWithEmptyRemoteUser
     *
     * @covers CheckRequirementsTask::execCommand
     */
    public function testExecCommand()
    {
        $cmd = 'phpunit ';
        $cmdArgs = array('--version', '--x');
        $remoteUser = 'nobody';
        $remoteHost = 'nowhere';
        $dir = '..';

        $remoteCommandString = 'some remote command';

        // == "phpunit --version --x nobody@nowhere 'cd ..\; some remote command'"
        $expectedAssembledCommand = $cmd . implode(' ', $cmdArgs) . ' '
            . $remoteUser . '@' . $remoteHost . ' '
            . escapeshellarg('cd ' . $dir . '\; ' . $remoteCommandString);


        $projectMock = $this->getMock('Project', array('setProperty'));
        $projectMock->expects($this->never())
            ->method('setProperty');


        $out = $this->getMock(self::$_cut, array('_performExecCommand'));

        $out->expects($this->once())
            ->method('_performExecCommand')
            ->with($this->equalTo($expectedAssembledCommand), $this->isEmpty(), $this->isEmpty())
            ->will($this->returnValue(null));

        $this->setProperty($out, '_cmd', $cmd);
        $this->setProperty($out, '_cmdArgs', $cmdArgs);
        $this->setProperty($out, '_remoteUser', $remoteUser);
        $this->setProperty($out, '_remoteHost', $remoteHost);
        $this->setProperty($out, '_dir', $dir);

        $this->setProperty($out, 'project', $projectMock);


        $exitCode = null;
        $out->execCommand($remoteCommandString, $exitCode);
    }

    /**
     * @depends testExecCommand
     *
     * @covers CheckRequirementsTask::execCommand
     * @covers CheckRequirementsTask::_performExecCommand
     *
     * @dataProvider dataProviderForTestExecCommandWithResultEvaluation
     *
     * @param string|null $returnProperty
     * @param string|null $outputProperty
     */
    public function testExecCommandWithResultEvaluation($returnProperty, $outputProperty)
    {
        $cmd = 'phpunit ';
        $cmdArgs = array('--version', '--x');
        $remoteUser = 'nobody';
        $remoteHost = 'nowhere';
        $dir = '..';

        $remoteCommandString = 'some remote command';

        // == "phpunit --version --x nobody@nowhere 'cd ..\; some remote command'"
        $expectedAssembledCommand = $cmd . implode(' ', $cmdArgs) . ' '
            . $remoteUser . '@' . $remoteHost . ' '
            . escapeshellarg('cd ' . $dir . '\; ' . $remoteCommandString);

        $output   = null;
        $exitCode = null;
        exec($expectedAssembledCommand, $output, $exitCode);

        // == array('PHPUnit *.*.* by Sebastian Bergmann.', '', 'unrecognized option --x');
        $expectedOutput = $output;
        // == 1
        $expectedExitCode = $exitCode;


        $projectMock = $this->getMock('Project', array('setProperty'));

        if ($returnProperty) {
            $projectMock->expects($this->at(0))
                ->method('setProperty')
                ->with($this->equalTo($returnProperty), $this->identicalTo($expectedExitCode))
                ->will($this->returnValue(null));
        }
        if ($outputProperty) {
            $projectMock->expects($this->at((integer) !empty($returnProperty)))
                ->method('setProperty')
                ->with($this->equalTo($outputProperty), $this->identicalTo(implode("\n", $expectedOutput)))
                ->will($this->returnValue(null));
        }
        if (!$returnProperty && !$outputProperty) {
            $projectMock->expects($this->never())
                ->method('setProperty');
        }


        $out = new self::$_cut();

        $this->setProperty($out, '_cmd', $cmd);
        $this->setProperty($out, '_cmdArgs', $cmdArgs);
        $this->setProperty($out, '_remoteUser', $remoteUser);
        $this->setProperty($out, '_remoteHost', $remoteHost);
        $this->setProperty($out, '_dir', $dir);

        $this->setProperty($out, '_returnProperty', $returnProperty);
        $this->setProperty($out, '_outputProperty', $outputProperty);

        $this->setProperty($out, 'project', $projectMock);


        $exitCode = null;
        $result = $out->execCommand($remoteCommandString, $exitCode);


        $this->assertSame($expectedOutput, $result);
        $this->assertSame($expectedExitCode, $exitCode);
    }

    /**
     * data provider for the test method testExecCommandWithResultEvaluation()
     * @return array
     */
    public function dataProviderForTestExecCommandWithResultEvaluation()
    {
        return array(
            /* data set #0 */
            array(
                'returnProperty' => null,
                'outputProperty' => null
            ),
            /* data set #1 */
            array(
                'returnProperty' => 'exitCode',
                'outputProperty' => null
            ),
            /* data set #2 */
            array(
                'returnProperty' => null,
                'outputProperty' => 'output'
            ),
            /* data set #3 */
            array(
                'returnProperty' => 'exitCode',
                'outputProperty' => 'output'
            )
        );
    }

    /**
     * @covers CheckRequirementsTask::_checkRequirement
     *
     * @dataProvider dataProviderForTestMethodTestCheckRequirement
     *
     * @param string $command
     * @param string $parameter
     * @param string $version
     * @param string $pattern
     * @param array  $expectedMatches
     * @param array  $expectedResult
     */
    public function testCheckRequirement($command, $parameter, $version, $pattern, $expectedMatches, $expectedResult)
    {
        $expectedCommandLine = sprintf($command, $parameter);


        $out = $this->getMock(self::$_cut, array('execCommand'));

        $out->expects($this->once())
            ->method('execCommand')
            ->with($this->equalTo($expectedCommandLine), $this->equalTo(0))
            ->will($this->returnValue($expectedMatches));


        $result = $this->invoke($out, '_checkRequirement', array($command, $parameter, $version, $pattern));


        $this->assertSame($expectedResult, $result);
    }

    /**
     * data provider for the test method testCheckRequirement()
     * @return array
     */
    public function dataProviderForTestMethodTestCheckRequirement()
    {
        return array(
            /* data set #0 */
            array(
                'command'         => 'command %s',
                'parameter'       => 'param',
                'version'         => '1.0',
                'pattern'         => '/^doesNotMatterHereBecauseThereAreNoMatches$/',
                'expectedMatches' => array(),
                'expectedResult'  => array(
                    'required'  => '1.0',
                    'installed' => 0,
                    'failure'   => true
                )
            ),
            /* data set #1 */
            array(
                'command'         => 'command %s',
                'parameter'       => 'param',
                'version'         => '1.0',
                'pattern'         => '/^v(\d.\d)$/',
                'expectedMatches' => array('testversion'),
                'expectedResult'  => array(
                    'required'  => '1.0',
                    'installed' => 0,
                    'failure'   => true
                )
            ),
            /* data set #2 */
            array(
                'command'         => 'command %s',
                'parameter'       => 'param',
                'version'         => '1.0',
                'pattern'         => '/^v(\d.\d)$/',
                'expectedMatches' => array('v1.0'),
                'expectedResult'  => array(
                    'required'  => '1.0',
                    'installed' => '1.0',
                    'failure'   => false
                )
            ),
            /* data set #3 */
            array(
                'command'         => 'command %s',
                'parameter'       => 'param',
                'version'         => '1.0',
                'pattern'         => '/^v(\d.\d)$/',
                'expectedMatches' => array('v1.1'),
                'expectedResult'  => array(
                    'required'  => '1.0',
                    'installed' => '1.1',
                    'failure'   => false
                )
            ),
            /* data set #4 */
            array(
                'command'         => 'command %s',
                'parameter'       => 'param',
                'version'         => '1.0',
                'pattern'         => '/^v(\d.\d)$/',
                'expectedMatches' => array('v0.9'),
                'expectedResult'  => array(
                    'required'  => '1.0',
                    'installed' => '0.9',
                    'failure'   => true
                )
            )
        );
    }

    /**
     * @covers CheckRequirementsTask::main
     */
    public function testMain()
    {
        $method = new ReflectionMethod(
            'CheckRequirementsTask', 'main'
        );
        $this->assertTrue($method->isPublic(), 'must be public');
    }

    /**
     * @covers CheckRequirementsTask::_hasFailure
     *
     * @dataProvider dataProviderForTestHasFailure
     *
     * @param array   $results
     * @param boolean $expectedResult
     */
    public function testHasFailure($results, $expectedResult)
    {
        $out = new self::$_cut();

        $result = $this->invoke($out, '_hasFailure', array($results));

        $this->assertSame($expectedResult, $result);
    }

    /**
     * data provider for the test method testHasFailure()
     *
     * @todo in den Tests für Funktionen, die ein 'result' zurückgeben,
     *       absichern, dass 'result' immer das Feld 'failure' besitzt
     *       -> sonst sind hier weitere Testdatensätze notwendig
     *
     * @todo falls 'failure' immer boolean ist, entfallen die Testdatensätze 5-10
     *
     * @return array
     */
    public function dataProviderForTestHasFailure()
    {
        return array(
            /* data set #0 */
            array(
                'results'        => array(),
                'expectedResult' => false
            ),
            /* data set #1 */
            array(
                'results'        => array(
                    'host' => array()
                ),
                'expectedResult' => false
            ),
            /* data set #2 */
            array(
                'results'        => array(
                    'host' => array(
                        'section' => array()
                    )
                ),
                'expectedResult' => false
            ),
            /* data set #3 */
            array(
                'results'        => array(
                    'host' => array(
                        'section' => array(
                            'result' => array(
                                'failure' => false
                            )
                        )
                    )
                ),
                'expectedResult' => false
            ),
            /* data set #4 */
            array(
                'results'        => array(
                    'host' => array(
                        'section' => array(
                            'result' => array(
                                'failure' => true
                            )
                        )
                    )
                ),
                'expectedResult' => true
            ),
            /* data set #5 */
            array(
                'results'        => array(
                    'host' => array(
                        'section' => array(
                            'result' => array(
                                'failure' => 0
                            )
                        )
                    )
                ),
                'expectedResult' => false
            ),
            /* data set #6 */
            array(
                'results'        => array(
                    'host' => array(
                        'section' => array(
                            'result' => array(
                                'failure' => 1
                            )
                        )
                    )
                ),
                'expectedResult' => true
            ),
            /* data set #7 */
            array(
                'results'        => array(
                    'host' => array(
                        'section' => array(
                            'result' => array(
                                'failure' => '0'
                            )
                        )
                    )
                ),
                'expectedResult' => false
            ),
            /* data set #8 */
            array(
                'results'        => array(
                    'host' => array(
                        'section' => array(
                            'result' => array(
                                'failure' => '1'
                            )
                        )
                    )
                ),
                'expectedResult' => true
            ),
            /* data set #9 */
            array(
                'results'        => array(
                    'host' => array(
                        'section' => array(
                            'result' => array(
                                'failure' => 'no'
                            )
                        )
                    )
                ),
                'expectedResult' => true
            ),
            /* data set #10 */
            array(
                'results'        => array(
                    'host' => array(
                        'section' => array(
                            'result' => array(
                                'failure' => 'yes'
                            )
                        )
                    )
                ),
                'expectedResult' => true
            )
        );
    }

    /**
     * @covers CheckRequirementsTask::_printResults
     *
     * @dataProvider dataProviderForTestPrintResults
     *
     * @param array  $results
     * @param string $expectedPrintedResult
     */
    public function testPrintResults($results, $expectedPrintedResult)
    {
        $expectedOutput = PrettyPrinter::YELLOW
            . PrettyPrinter::BOLD
            . '    '
            . '============= Requirements ============= '
            . PrettyPrinter::RESET
            . PHP_EOL
            . PHP_EOL
            . $expectedPrintedResult;


        $out = new self::$_cut();


        $output = null;
        $this->executeTestWithOutputBuffering($output, $out, '_printResults', array($results));


        $this->assertSame($expectedOutput, $output);
    }

    /**
     * data provider for the test method testPrintResults()
     *
     * @todo in den Tests für Funktionen, die ein 'result' zurückgeben,
     *       absichern, dass 'result' immer die Felder 'failure' (boolean), 'required' und 'installed' besitzt
     *       -> sonst sind hier weitere Testdatensätze notwendig
     *
     * @return array
     */
    public function dataProviderForTestPrintResults()
    {
        return array(
            /* data set #0 */
            array(
                'results'               => array(),
                'expectedPrintedResult' => ''
            ),
            /* data set #1 */
            array(
                'results'               => array(
                    'host' => array()
                ),
                'expectedPrintedResult' => ' ' .  PrettyPrinter::YELLOW . PrettyPrinter::BOLD . 'host' . ' : ' . PHP_EOL
            ),
            /* data set #2 */
            array(
                'results'               => array(
                    'host' => array(
                        'section' => array()
                    )
                ),
                'expectedPrintedResult' => ' ' .  PrettyPrinter::YELLOW . PrettyPrinter::BOLD . 'host' . ' : ' . PHP_EOL
                    . '   ' .  PrettyPrinter::YELLOW . PrettyPrinter::BOLD . 'section' . ' : ' . PHP_EOL
            ),
            /* data set #3 */
            array(
                'results'               => array(
                    'host' => array(
                        'section' => array(
                            'result' => array(
                                'required'  => 'x',
                                'installed' => 'x',
                                'failure'   => false,
                            )
                        )
                    )
                ),
                'expectedPrintedResult' => ' ' .  PrettyPrinter::YELLOW . PrettyPrinter::BOLD . 'host' . ' : ' . PHP_EOL
                    . '   ' .  PrettyPrinter::YELLOW . PrettyPrinter::BOLD . 'section' . ' : ' . PHP_EOL
                    . '     ' .  PrettyPrinter::GREEN . PrettyPrinter::BOLD
                    . 'I ' . ' ' . 'result' . ' : ' . 'x == x' . PrettyPrinter::RESET . PHP_EOL
            ),
            /* data set #4 */
            array(
                'results'               => array(
                    'host' => array(
                        'section' => array(
                            'result' => array(
                                'required'  => 'x',
                                'installed' => 'y',
                                'failure'   => true,
                            )
                        )
                    )
                ),
                'expectedPrintedResult' => ' ' .  PrettyPrinter::YELLOW . PrettyPrinter::BOLD . 'host' . ' : ' . PHP_EOL
                    . '   ' .  PrettyPrinter::YELLOW . PrettyPrinter::BOLD . 'section' . ' : ' . PHP_EOL
                    . '     ' .  PrettyPrinter::RED . PrettyPrinter::BOLD
                    . 'F ' . ' ' . 'result' . ' : ' . 'x != y' . PrettyPrinter::RESET . PHP_EOL
            ),
            /* data set #5 */
            array(
                'results'               => array(
                    'host' => array(
                        'section' => array(
                            'result' => array(
                                'required'  => 'x',
                                'installed' => 0,
                                'failure'   => true,
                            )
                        )
                    )
                ),
                'expectedPrintedResult' => ' ' .  PrettyPrinter::YELLOW . PrettyPrinter::BOLD . 'host' . ' : ' . PHP_EOL
                    . '   ' .  PrettyPrinter::YELLOW . PrettyPrinter::BOLD . 'section' . ' : ' . PHP_EOL
                    . '     ' .  PrettyPrinter::RED . PrettyPrinter::BOLD
                    . '- ' . ' ' . 'result' . ' : ' . 'not installed' . PrettyPrinter::RESET . PHP_EOL
            ),
        );
    }
}