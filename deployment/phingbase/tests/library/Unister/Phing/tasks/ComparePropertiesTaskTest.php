<?php
/**
 * File for the class ComparePropertiesTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class ComparePropertiesTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class ComparePropertiesTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (ComparePropertiesTask)
     * @var string
     */
    protected static $_cut = 'ComparePropertiesTask';

    /**
     * @return array
     */
    public function dataProvider()
    {
        return array(
            array('true', true),
            array('1', true),
            array(1, true),
            array(true, true),
            /* 'false' > '' w/ help of phing magic  */
            array('false', true),
            array('', false),
            array('0', false),
            array(0, false),
            array(false, false),
        );
    }

    /**
     * ComparePropertiesTask::setStrict
     * @dataProvider dataProvider
     */
    public function testSetStrict($value, $expected)
    {
        $object = new ComparePropertiesTask;

        $object->setStrict($value);
        $this->assertEquals($expected, $object->getStrict());
    }


    /**
     * @covers ComparePropertiesTask::setDist
     */
    public function testSetDist()
    {
        $dist = 'test';

        $out = new self::$_cut();

        $out->setDist($dist);

        $this->assertAttributeSame($dist, '_dist', $out);
    }

    /**
     * @covers ComparePropertiesTask::main
     */
    public function testMainWithException()
    {
        $propertyNameForCurrent = 'config.file';

        $current = 'anotherfile';
        $dist    = 'folder/file';

        $expectedLogMessage = sprintf('Compare "%s" <-> "%s"', $current, basename($dist));

        $exceptionMessage = 'test';
        $exception = new IOException($exceptionMessage);


        $projectMock = $this->getMock('Project', array('getProperty'));
        $projectMock->expects($this->once())
            ->method('getProperty')
            ->with($this->equalTo($propertyNameForCurrent))
            ->will($this->returnValue($current));


        $out = $this->getMock(self::$_cut, array('log', '_loadConfiguration'));

        $out->expects($this->once())
            ->method('log')
            ->with($this->equalTo($expectedLogMessage), $this->equalTo(Project::MSG_INFO))
            ->will($this->returnValue(null));
        $out->expects($this->once())
            ->method('_loadConfiguration')
            ->with($this->equalTo($current))
            ->will($this->throwException($exception));

        $this->setProperty($out, '_dist', $dist);

        $this->setProperty($out, 'project', $projectMock);


        $this->setExpectedException('BuildException', $exceptionMessage);


        $out->main();
    }

    /**
     * @covers ComparePropertiesTask::main
     *
     * @dataProvider dataProviderForTestMainWithoutException
     *
     * @param array $currentPropertiesArray
     * @param array $distPropertiesArray
     * @param array $expectedLogMessages2
     */
    public function testMainWithoutException($currentPropertiesArray, $distPropertiesArray, $expectedLogMessages2)
    {
        $propertyNameForCurrent = 'config.file';

        $current = 'anotherfile';
        $dist    = 'folder/file';

        $expectedLogMessage1 = sprintf('Compare "%s" <-> "%s"', $current, basename($dist));


        $projectMock = $this->getMock('Project', array('getProperty'));
        $projectMock->expects($this->once())
            ->method('getProperty')
            ->with($this->equalTo($propertyNameForCurrent))
            ->will($this->returnValue($current));

        $currentPropertiesObjectMock = $this->getMock('Properties', array('getProperties'));
        $currentPropertiesObjectMock->expects($this->once())
            ->method('getProperties')
            ->will($this->returnValue($currentPropertiesArray));

        $distPropertiesObjectMock = $this->getMock('Properties', array('getProperties'));
        $distPropertiesObjectMock->expects($this->once())
            ->method('getProperties')
            ->will($this->returnValue($distPropertiesArray));


        $out = $this->getMock(self::$_cut, array('log', '_loadConfiguration'));

        $out->expects($this->at(0))
            ->method('log')
            ->with($this->equalTo($expectedLogMessage1), $this->equalTo(Project::MSG_INFO))
            ->will($this->returnValue(null));

        $out->expects($this->at(1))
            ->method('_loadConfiguration')
            ->with($this->equalTo($current))
            ->will($this->returnValue($currentPropertiesObjectMock));
        $out->expects($this->at(2))
            ->method('_loadConfiguration')
            ->with($this->equalTo($dist))
            ->will($this->returnValue($distPropertiesObjectMock));

        $i = 0;
        foreach ($expectedLogMessages2 as $expectedLogMessage2) {
            $out->expects($this->at(3 + $i++))
                ->method('log')
                ->with($this->equalTo($expectedLogMessage2), $this->equalTo(Project::MSG_WARN))
                ->will($this->returnValue(null));
        }

        $this->setProperty($out, '_dist', $dist);

        $this->setProperty($out, 'project', $projectMock);


        $out->main();
    }


    /**
     * data provider for the test method testMainWithoutException()
     * @return array
     */
    public function dataProviderForTestMainWithoutException()
    {
        return array(
            /* data set #0 */
            array(
                'currentPropertiesArray' => array(),
                'distPropertiesArray'    => array(),
                'expectedLogMessages2'   => array()
            ),
            /* data set #1 */
            array(
                'currentPropertiesArray' => array('key1' => 'value1'),
                'distPropertiesArray'    => array('key1' => 'value2'),
                'expectedLogMessages2'   => array()
            ),
            /* data set #2 */
            array(
                'currentPropertiesArray' => array(),
                'distPropertiesArray'    => array('key1' => 'value1'),
                'expectedLogMessages2'   => array('Property "key1" (value1) not set')
            ),
            /* data set #3 */
            array(
                'currentPropertiesArray' => array('key1' => 'value1'),
                'distPropertiesArray'    => array(
                    'key1' => 'value1',
                    'key2' => 'value2'
                ),
                'expectedLogMessages2'   => array('Property "key2" (value2) not set')
            )
        );
    }

    /**
     * @covers ComparePropertiesTask::_loadConfiguration
     */
    public function testLoadConfigurationWhenFileDoesNotExist()
    {
        $filename = 'test';


        $propertiesMock = $this->getMock('Properties', array('load'));
        $propertiesMock->expects($this->never())
            ->method('load');


        $out = $this->getMock(self::$_cut, array('_getProperties', '_getPhingFile'));

        $out->expects($this->once())
            ->method('_getProperties')
            ->will($this->returnValue($propertiesMock));
        $out->expects($this->once())
            ->method('_getPhingFile')
            ->with($this->equalTo($filename))
            ->will($this->returnValue(null));


        $this->setExpectedException('BuildException', 'Can\'t load: ' . $filename);


        $this->invoke($out, '_loadConfiguration', array($filename));
    }

    /**
     * @covers ComparePropertiesTask::_loadConfiguration
     */
    public function testLoadConfiguration()
    {
        $filename = 'test';


        /* create the stub object bypassing execution of the constructor of the original class */
        $phingFileStub = $this->_getMock('PhingFile');

        $propertiesMock = $this->getMock('Properties', array('load'));
        $propertiesMock->expects($this->once())
            ->method('load')
            ->with($this->equalTo($phingFileStub))
            ->will($this->returnValue(null));


        $out = $this->getMock(self::$_cut, array('_getProperties', '_getPhingFile'));

        $out->expects($this->once())
            ->method('_getProperties')
            ->will($this->returnValue($propertiesMock));
        $out->expects($this->once())
            ->method('_getPhingFile')
            ->with($this->equalTo($filename))
            ->will($this->returnValue($phingFileStub));


        $result = $this->invoke($out, '_loadConfiguration', array($filename));


        $this->assertSame($propertiesMock, $result);
    }

    /**
     * @covers ComparePropertiesTask::_getProperties
     */
    public function testGetProperties()
    {
        $out = new self::$_cut();

        $result = $this->invoke($out, '_getProperties');

        $this->assertInstanceOf('Properties', $result);
    }
}