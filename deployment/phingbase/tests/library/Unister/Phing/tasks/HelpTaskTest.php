<?php
/**
 * File for the class HelpTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class HelpTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class HelpTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (HelpTask)
     * @var string
     */
    protected static $_cut = 'HelpTask';


    /**
     * @covers HelpTask::main
     * @covers HelpTask::_printHead
     * @covers HelpTask::_printIntro
     */
    public function testMain()
    {
        $expectedPrintedHead = PrettyPrinter::YELLOW
            . PrettyPrinter::BOLD
            . HelpTask::INDENT
            . ' *#######################* '
            . PHP_EOL
            . HelpTask::INDENT
            . '###########################'
            . PHP_EOL
            . HelpTask::INDENT
            . '####        Hilfe      ####'
            . PHP_EOL
            . HelpTask::INDENT
            . '###########################'
            . PHP_EOL
            . HelpTask::INDENT
            . ' *#######################* '
            . PrettyPrinter::RESET
            . PHP_EOL
            . PHP_EOL
            . PHP_EOL;

        $expectedPrintedIntro = HelpTask::INDENT
            . '    ' . PrettyPrinter::MAGENTA
            . PrettyPrinter::BOLD
            . ' wichtige Targets: '
            . PrettyPrinter::RESET
            . PHP_EOL
            . PHP_EOL;

        $expectedTargetList = array(
            'install' => 'Initialisieren der Arbeitskopie',
            'package' => 'Bauen eines Paketes',
            'deploy'  => 'Ausliefern des Paketes',
            'info'    => 'Überblick über Targets und Umgebungsvariablen'
        );

        ob_start();
        PrettyPrinter::printAnsi($expectedTargetList);
        $expectedPrintedTargetList = ob_get_clean();

        $expectedOutputString = $expectedPrintedHead . $expectedPrintedIntro . $expectedPrintedTargetList;


        $out = new self::$_cut();


        $output = null;
        $this->executeTestWithOutputBuffering($output, $out, 'main');


        $this->assertEquals($expectedOutputString, $output);
    }
}