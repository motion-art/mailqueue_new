<?php
/**
 * File for the class DatabaseDeployTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class DatabaseDeployTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class DatabaseDeployTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (DatabaseDeployTask)
     * @var string
     */
    protected static $_cut = 'DatabaseDeployTask';

    /**
     * names of test methods that need filesystem access
     * @var array
     */
    protected static $_methodsWithFileSystemAccess = array(
        'testMain',
        'testMainWithException',
        'testParse',
        'testGetDeltasFilesArray'
    );


    /**
     * @covers DatabaseDeployTask::main
     */
    public function testMain()
    {
        $appliedChangeNumbers = array(1);
        $lastChangeAppliedInDb = 1;

        $expectedLogMessage = 'Current db revision: ' . $lastChangeAppliedInDb;

        $doSQLScript   = '/* do script */';
        $undoSQLScript = '/* undo script */';


        $out = $this->getMock(self::$_cut,
            array('getAppliedChangeNumbers', 'getLastChangeAppliedInDb', 'log', 'doDeploy', 'undoDeploy'));

        $out->expects($this->once())
            ->method('getAppliedChangeNumbers')
            ->will($this->returnValue($appliedChangeNumbers));
        $out->expects($this->once())
            ->method('getLastChangeAppliedInDb')
            ->will($this->returnValue($lastChangeAppliedInDb));
        $out->expects($this->once())
            ->method('log')
            ->with($this->equalTo($expectedLogMessage))
            ->will($this->returnValue(null));
        $out->expects($this->once())
            ->method('doDeploy')
            ->will($this->returnValue($doSQLScript));
        $out->expects($this->once())
            ->method('undoDeploy')
            ->will($this->returnValue($undoSQLScript));


        $expectedDoOutputFileName   = $this->_dir . '/' . $this->getProperty($out, '_outputFile');
        $expectedUndoOutputFileName = $this->_dir . '/' . $this->getProperty($out, '_undoOutputFile');

        $this->setProperty($out, '_outputFile', $expectedDoOutputFileName);
        $this->setProperty($out, '_undoOutputFile', $expectedUndoOutputFileName);


        $out->main();


        $this->assertAttributeSame($appliedChangeNumbers, '_appliedChangeNumbers', $out);

        $this->assertFileExists($expectedDoOutputFileName);
        $this->assertFileExists($expectedUndoOutputFileName);

        $this->assertSame($doSQLScript, file_get_contents($expectedDoOutputFileName));
        $this->assertSame($undoSQLScript, file_get_contents($expectedUndoOutputFileName));
    }

    /**
     * @covers DatabaseDeployTask::main
     */
    public function testMainWithException()
    {
        $expectedCaughtException = new Exception('Database not compatible');
        $expectedThrownException = new BuildException($expectedCaughtException);


        $out = $this->getMock(self::$_cut,
            array('getAppliedChangeNumbers', 'getLastChangeAppliedInDb', 'log', 'doDeploy', 'undoDeploy'));

        $out->expects($this->once())
            ->method('getAppliedChangeNumbers')
            ->will($this->throwException($expectedCaughtException));
        $out->expects($this->never())
            ->method('getLastChangeAppliedInDb');
        $out->expects($this->never())
            ->method('log');
        $out->expects($this->never())
            ->method('doDeploy');
        $out->expects($this->never())
            ->method('undoDeploy');


        $expectedDoOutputFileName   = $this->_dir . '/' . $this->getProperty($out, '_outputFile');
        $expectedUndoOutputFileName = $this->_dir . '/' . $this->getProperty($out, '_undoOutputFile');

        $this->setProperty($out, '_outputFile', $expectedDoOutputFileName);
        $this->setProperty($out, '_undoOutputFile', $expectedUndoOutputFileName);


        try {

            $out->main();

            $this->fail('expected BuildException was not thrown');

        } catch (BuildException $e) {

            $this->assertSame($expectedThrownException->getMessage(), $e->getMessage());

            $this->assertAttributeEmpty('_appliedChangeNumbers', $out);

            $this->assertFileExists($expectedDoOutputFileName);
            $this->assertFileExists($expectedUndoOutputFileName);

            $this->assertEmpty(file_get_contents($expectedDoOutputFileName));
            $this->assertEmpty(file_get_contents($expectedUndoOutputFileName));
        }
    }

    /**
     * @covers DatabaseDeployTask::getAppliedChangeNumbers
     */
    public function testGetAppliedChangeNumbersWhenNumbersHaveAlreadyBeenRetrieved()
    {
        $appliedChangeNumbers = array(1);


        $out = $this->getMock(self::$_cut, array('log', '_createPDO'));

        $out->expects($this->never())
            ->method('log');
        $out->expects($this->never())
            ->method('createPDO');

        $this->setProperty($out, '_appliedChangeNumbers', $appliedChangeNumbers);


        $result = $out->getAppliedChangeNumbers();


        $this->assertSame($appliedChangeNumbers, $result);
    }

    /**
     * @covers DatabaseDeployTask::getAppliedChangeNumbers
     */
    public function testGetAppliedChangeNumbersWhenDbIsIncompatible()
    {
        $appliedChangeNumbers = array();

        $url = 'url';
        $username = 'user';
        $password = 'password';

        $expectedLogMessage = 'Getting applied changed numbers from DB: ' . $url;


        $expectedQueryShowTables = 'SHOW TABLES LIKE \'' . DatabaseDeployTask::TABLE_NAME . '\'';

        $queryResultShowTables = array(array('Test'));

        $pdoMock = $this->getMock('PhingbaseTest_Stub_PDO', array('setAttribute', 'query'));
        $pdoMock->expects($this->once())
            ->method('setAttribute')
            ->with($this->equalTo(PDO::ATTR_ERRMODE), $this->equalTo(PDO::ERRMODE_EXCEPTION))
            ->will($this->returnValue(true));
        $pdoMock->expects($this->once())
            ->method('query')
            ->with($this->equalTo($expectedQueryShowTables))
            ->will($this->returnValue($queryResultShowTables));


        $out = $this->getMock(self::$_cut, array('log', '_createPDO'));

        $out->expects($this->once())
            ->method('log')
            ->with($this->equalTo($expectedLogMessage))
            ->will($this->returnValue(null));
        $out->expects($this->once())
            ->method('_createPDO')
            ->with($this->equalTo($url), $this->equalTo($username), $this->equalTo($password))
            ->will($this->returnValue($pdoMock));


        $this->setProperty($out, '_appliedChangeNumbers', $appliedChangeNumbers);
        $this->setProperty($out, '_url', $url);
        $this->setProperty($out, '_username', $username);
        $this->setProperty($out, '_password', $password);


        $this->setExpectedException('BuildException', 'Database not compatible');


        $out->getAppliedChangeNumbers();
    }

    /**
     * @covers DatabaseDeployTask::getAppliedChangeNumbers
     *
     * @dataProvider dataProviderForTestGetAppliedChangeNumbersWhenDbIsCompatible
     *
     * @param array $queryResultSelect
     * @param array $expectedAppliedChangeNumbers
     */
    public function testGetAppliedChangeNumbersWhenDbIsCompatibleAndVersionTableExists($queryResultSelect,
        $expectedAppliedChangeNumbers)
    {
        $appliedChangeNumbers = array();

        $url = 'url';
        $username = 'user';
        $password = 'password';

        $expectedLogMessage = 'Getting applied changed numbers from DB: ' . $url;


        $expectedQueryShowTables = 'SHOW TABLES LIKE \'' . DatabaseDeployTask::TABLE_NAME . '\'';
        $expectedQuerySelect     = 'SELECT * FROM ' . DatabaseDeployTask::TABLE_NAME . ' ORDER BY releaseNumber';

        $queryResultShowTables = array(array('Test'), array(DatabaseDeployTask::TABLE_NAME));

        $pdoMock = $this->getMock('PhingbaseTest_Stub_PDO', array('setAttribute', 'query'));
        $pdoMock->expects($this->at(0))
            ->method('setAttribute')
            ->with($this->equalTo(PDO::ATTR_ERRMODE), $this->equalTo(PDO::ERRMODE_EXCEPTION))
            ->will($this->returnValue(true));
        $pdoMock->expects($this->at(1))
            ->method('query')
            ->with($this->equalTo($expectedQueryShowTables))
            ->will($this->returnValue($queryResultShowTables));
        $pdoMock->expects($this->at(2))
            ->method('query')
            ->with($this->equalTo($expectedQuerySelect))
            ->will($this->returnValue($queryResultSelect));


        $out = $this->getMock(self::$_cut, array('log', '_createPDO'));

        $out->expects($this->once())
            ->method('log')
            ->with($this->equalTo($expectedLogMessage))
            ->will($this->returnValue(null));
        $out->expects($this->once())
            ->method('_createPDO')
            ->with($this->equalTo($url), $this->equalTo($username), $this->equalTo($password))
            ->will($this->returnValue($pdoMock));


        $this->setProperty($out, '_appliedChangeNumbers', $appliedChangeNumbers);
        $this->setProperty($out, '_url', $url);
        $this->setProperty($out, '_username', $username);
        $this->setProperty($out, '_password', $password);


        $result = $out->getAppliedChangeNumbers();

        $this->assertSame($expectedAppliedChangeNumbers, $result);

        $this->assertAttributeSame($expectedAppliedChangeNumbers, '_appliedChangeNumbers', $out);
    }

    /**
     * @covers DatabaseDeployTask::getAppliedChangeNumbers
     *
     * @dataProvider dataProviderForTestGetAppliedChangeNumbersWhenDbIsCompatible
     *
     * @param array $queryResultSelect
     * @param array $expectedAppliedChangeNumbers
     */
    public function testGetAppliedChangeNumbersWhenDbIsCompatibleAndVersionTableDoesNotExist($queryResultSelect,
        $expectedAppliedChangeNumbers)
    {
        $appliedChangeNumbers = array();

        $url = 'url';
        $username = 'user';
        $password = 'password';

        $expectedLogMessage = 'Getting applied changed numbers from DB: ' . $url;


        $expectedQueryShowTables = 'SHOW TABLES LIKE \'' . DatabaseDeployTask::TABLE_NAME . '\'';
        $expectedQuerySelect     = 'SELECT * FROM ' . DatabaseDeployTask::TABLE_NAME . ' ORDER BY releaseNumber';

        $queryResultShowTables = array();

        $expectedSchemaFileLocation = APPLICATION_PATH . '/Unister/Phing/tasks/DatabaseDeploy/schema.sql';
        if (!file_exists($expectedSchemaFileLocation)) {
            $this->fail('file "' . $expectedSchemaFileLocation . '" does not exist');
        }
        $expectedQuerySchema = file_get_contents($expectedSchemaFileLocation);

        $pdoMock = $this->getMock('PhingbaseTest_Stub_PDO', array('setAttribute', 'query'));
        $pdoMock->expects($this->at(0))
            ->method('setAttribute')
            ->with($this->equalTo(PDO::ATTR_ERRMODE), $this->equalTo(PDO::ERRMODE_EXCEPTION))
            ->will($this->returnValue(true));
        $pdoMock->expects($this->at(1))
            ->method('query')
            ->with($this->equalTo($expectedQueryShowTables))
            ->will($this->returnValue($queryResultShowTables));
        $pdoMock->expects($this->at(2))
            ->method('query')
            ->with($this->equalTo($expectedQuerySchema))
            ->will($this->returnValue(array()));
        $pdoMock->expects($this->at(3))
            ->method('query')
            ->with($this->equalTo($expectedQuerySelect))
            ->will($this->returnValue($queryResultSelect));


        $out = $this->getMock(self::$_cut, array('log', '_createPDO'));

        $out->expects($this->once())
            ->method('log')
            ->with($this->equalTo($expectedLogMessage))
            ->will($this->returnValue(null));
        $out->expects($this->once())
            ->method('_createPDO')
            ->with($this->equalTo($url), $this->equalTo($username), $this->equalTo($password))
            ->will($this->returnValue($pdoMock));


        $this->setProperty($out, '_appliedChangeNumbers', $appliedChangeNumbers);
        $this->setProperty($out, '_url', $url);
        $this->setProperty($out, '_username', $username);
        $this->setProperty($out, '_password', $password);


        $result = $out->getAppliedChangeNumbers();

        $this->assertSame($expectedAppliedChangeNumbers, $result);

        $this->assertAttributeSame($expectedAppliedChangeNumbers, '_appliedChangeNumbers', $out);
    }

    /**
     * data provider for the test methods testGetAppliedChangeNumbersWhenDbIsCompatible*()
     * @return array
     */
    public function dataProviderForTestGetAppliedChangeNumbersWhenDbIsCompatible()
    {
        return array(
            /* data set #0 */
            array(
                'queryResultSelect'            => array(),
                'expectedAppliedChangeNumbers' => array()
            ),
            /* data set #1 */
            array(
                'queryResultSelect'            => array(
                    array(
                        'releaseNumber' => 1,
                        'applied'       => '1970-01-01 00:00:00',
                        'appliedBy'     => '',
                        'description'   => ''
                    )
                ),
                'expectedAppliedChangeNumbers' => array(1)
            ),
            /* data set #2 */
            array(
                'queryResultSelect'            => array(
                    array(
                        'releaseNumber' => 1,
                        'applied'       => '1970-01-01 00:00:00',
                        'appliedBy'     => '',
                        'description'   => ''
                    ),
                    array(
                        'releaseNumber' => 2,
                        'applied'       => '1970-01-01 00:00:00',
                        'appliedBy'     => '',
                        'description'   => ''
                    )
                ),
                'expectedAppliedChangeNumbers' => array(1, 2)
            )
        );
    }

    /**
     * @covers DatabaseDeployTask::getLastChangeAppliedInDb
     *
     * @dataProvider dataProviderFortestGetLastChangeAppliedInDb
     *
     * @param array   $appliedChangeNumbers
     * @param integer $expectedLastAppliedChangeNumber
     */
    public function testGetLastChangeAppliedInDb($appliedChangeNumbers, $expectedLastAppliedChangeNumber)
    {
        $out = new self::$_cut();

        $this->setProperty($out, '_appliedChangeNumbers', $appliedChangeNumbers);

        $result = $out->getLastChangeAppliedInDb();

        $this->assertSame($expectedLastAppliedChangeNumber, $result);
    }

    /**
     * data provider for the test method testGetLastChangeAppliedInDb()
     * @return array
     */
    public function dataProviderFortestGetLastChangeAppliedInDb()
    {
        return array(
            /* data set #0 */
            array(
                'appliedChangeNumbers'            => array(),
                'expectedLastAppliedChangeNumber' => 0
            ),
            /* data set #1 */
            array(
                'appliedChangeNumbers'            => array(1),
                'expectedLastAppliedChangeNumber' => 1
            ),
            /* data set #2 */
            array(
                'appliedChangeNumbers'            => array(1, 2),
                'expectedLastAppliedChangeNumber' => 2
            ),
            /* data set #3 */
            array(
                'appliedChangeNumbers'            => array(2, 1),
                'expectedLastAppliedChangeNumber' => 2
            )
        );
    }

    /**
     * @covers DatabaseDeployTask::doDeploy
     *
     * @dataProvider dataProviderForTestDoDeploy
     *
     * @param integer $lastChangeAppliedInDb
     * @param array   $deltasFilesArray
     * @param array   $fileContents
     */
    public function testDoDeploy($lastChangeAppliedInDb, $deltasFilesArray, $fileContents)
    {
        $dir = 'test';

        $versionProperty = 'version';
        $changedProperty = 'changed';

        $expectedVersion = !empty($deltasFilesArray) ? max(array_keys($deltasFilesArray)) : 1;
        $expectedChangedFlag = !empty($deltasFilesArray) && $expectedVersion > $lastChangeAppliedInDb;


        $projectMock = $this->getMock('Project', array('setProperty'));
        $projectMock->expects($this->at(0))
            ->method('setProperty')
            ->with($this->equalTo($versionProperty), $this->equalTo($expectedVersion))
            ->will($this->returnValue(null));
        $projectMock->expects($this->at(1))
            ->method('setProperty')
            ->with($this->equalTo($changedProperty), $this->equalTo($expectedChangedFlag))
            ->will($this->returnValue(null));


        $out = $this->getMock(self::$_cut, array('getLastChangeAppliedInDb', 'getDeltasFilesArray', '_parse'));

        $out->expects($this->once())
            ->method('getLastChangeAppliedInDb')
            ->will($this->returnValue($lastChangeAppliedInDb));
        $out->expects($this->once())
            ->method('getDeltasFilesArray')
            ->will($this->returnValue($deltasFilesArray));

        $this->setProperty($out, '_dir', $dir);
        $this->setProperty($out, '_versionProperty', $versionProperty);
        $this->setProperty($out, '_changedProperty', $changedProperty);

        $this->setProperty($out, 'project', $projectMock);


        ksort($deltasFilesArray);

        $expectedFilePaths = array();
        $changeSets = array();
        $expectedResult = '';

        foreach ($deltasFilesArray as $fileChangeNumber => $fileName) {
            if ($fileChangeNumber > $lastChangeAppliedInDb) {

                $expectedFilePaths[$fileChangeNumber] = $dir . DIRECTORY_SEPARATOR . $fileName;

                $changeSets[$fileChangeNumber] = array(
                    'do'   => explode("\n", $fileContents[$fileName]),
                    'undo' => array(),
                    'meta' => array(
                        'description' => 'test',
                        'appliedBy'   => 'tester'
                    )
                );

                $expectedResult .= '-- Fragment begins: ' . $fileChangeNumber . ' --' . "\n";

                $expectedResult .= 'INSERT INTO ' . DatabaseDeployTask::TABLE_NAME
                    . ' (releaseNumber, appliedBy, description)'
                    . ' VALUES (' . $fileChangeNumber . ', '
                    . ' "' . $changeSets[$fileChangeNumber]['meta']['appliedBy'] . '", "'
                    . $changeSets[$fileChangeNumber]['meta']['description'] . '");' . "\n";

                $expectedResult .= $fileContents[$fileName] . "\n";

                $expectedResult .= 'INSERT INTO ' . DatabaseDeployTask::TABLE_NAME
                    . ' SET applied  = NOW(), '
                    . ' releaseNumber = ' . $fileChangeNumber . ', '
                    . ' appliedBy = "' . $changeSets[$fileChangeNumber]['meta']['appliedBy'] . '", '
                    . ' description = "' . $changeSets[$fileChangeNumber]['meta']['description'] . '" '
                    . ' ON DUPLICATE KEY UPDATE applied = NOW();' . "\n";

                $expectedResult .= '-- Fragment ends: ' . $fileChangeNumber . ' --' . "\n";
            }
        }

        $i = 0;
        foreach ($deltasFilesArray as $fileChangeNumber => $fileName) {
            if ($fileChangeNumber > $lastChangeAppliedInDb) {
                $out->expects($this->at(2 + $i))
                    ->method('_parse')
                    ->with($this->equalTo($expectedFilePaths[$fileChangeNumber]))
                    ->will($this->returnValue($changeSets[$fileChangeNumber]));
                $i++;
            }
        }


        $result = $out->doDeploy();

        $this->assertSame($expectedResult, $result);
    }

    /**
     * data provider for the test method testDoDeploy()
     * @return array
     */
    public function dataProviderForTestDoDeploy()
    {
        return array(
            /* data set #0 */
            array(
                'lastChangeAppliedInDb' => 0,
                'deltasFilesArray'      => array(),
                'fileContents'          => array()
            ),
            /* data set #1 */
            array(
                'lastChangeAppliedInDb' => 0,
                'deltasFilesArray'      => array(
                    1 => 'dofile1'
                ),
                'fileContents'          => array(
                    'dofile1' => '/* SQL do file 1 */'
                )
            ),
            /* data set #2 */
            array(
                'lastChangeAppliedInDb' => 1,
                'deltasFilesArray'      => array(
                    1 => 'dofile1'
                ),
                'fileContents'          => array()
            ),
            /* data set #3 */
            array(
                'lastChangeAppliedInDb' => 2,
                'deltasFilesArray'      => array(
                    1 => 'dofile1'
                ),
                'fileContents'          => array()
            ),
            /* data set #4 */
            array(
                'lastChangeAppliedInDb' => 0,
                'deltasFilesArray'      => array(
                    1 => 'dofile1',
                    2 => 'dofile2'
                ),
                'fileContents'          => array(
                    'dofile1' => '/* SQL do file 1 */',
                    'dofile2' => '/* SQL do file 2 */'
                )
            ),
            /* data set #5 */
            array(
                'lastChangeAppliedInDb' => 1,
                'deltasFilesArray'      => array(
                    1 => 'dofile1',
                    2 => 'dofile2'
                ),
                'fileContents'          => array(
                    'dofile2' => '/* SQL do file 2 */'
                )
            ),
            /* data set #6 */
            array(
            'lastChangeAppliedInDb' => 2,
                'deltasFilesArray'      => array(
                    1 => 'dofile1',
                    2 => 'dofile2'
                ),
                'fileContents'          => array()
            ),
            /* data set #7 */
            array(
                'lastChangeAppliedInDb' => 3,
                'deltasFilesArray'      => array(
                    1 => 'dofile1',
                    2 => 'dofile2'
                ),
                'fileContents'          => array()
            ),
            /* data set #8 */
            array(
                'lastChangeAppliedInDb' => 0,
                'deltasFilesArray'      => array(
                    2 => 'dofile2',
                    1 => 'dofile1'
                ),
                'fileContents'          => array(
                    'dofile1' => '/* SQL do file 1 */',
                    'dofile2' => '/* SQL do file 2 */'
            )
            ),
            /* data set #9 */
            array(
                'lastChangeAppliedInDb' => 1,
                'deltasFilesArray'      => array(
                    2 => 'dofile2',
                    1 => 'dofile1'
                ),
                'fileContents'          => array(
                    'dofile2' => '/* SQL do file 2 */'
                )
            ),
            /* data set #10 */
            array(
                'lastChangeAppliedInDb' => 2,
                'deltasFilesArray'      => array(
                    2 => 'dofile2',
                    1 => 'dofile1'
                ),
                'fileContents'          => array()
            ),
            /* data set #11 */
            array(
                'lastChangeAppliedInDb' => 3,
                'deltasFilesArray'      => array(
                    2 => 'dofile2',
                    1 => 'dofile1'
                ),
                'fileContents'          => array()
            )
        );
    }

    /**
     * @covers DatabaseDeployTask::_parse
     *
     * @dataProvider dataProviderForTestParse
     *
     * @param string $fileContent
     * @param array  $expectedResult
     */
    public function testParse($fileContent, $expectedResult)
    {
        $fileName = __CLASS__ . '_' . $this->getName(false);

        if (($filePath = $this->_createTempFile($fileName, $fileContent)) === false) {
            $this->fail('error creating temporary file "' . $fileName
                . '" in temporary directory "' . $this->_dir . '"');
        }


        $out = new self::$_cut();


        $result = $this->invoke($out, '_parse', array($filePath));


        $this->assertSame($expectedResult, $result);
    }

    /**
     * data provider for the test method testParse()
     * @return array
     */
    public function dataProviderForTestParse()
    {
        return array(
            /* data set #0 - an empty file */
            array(
                'fileContent'    => '',
                'expectedResult' => array(
                    'do'   => array(''),
                    'undo' => array(),
                    'meta' => array(
                        'description' => '',
                        'appliedBy'   => ''
                    )
                )
            ),
            /* data set #1 - a file with do commands */
            array(
                'fileContent'    => "/* do file\n * just for testing */",
                'expectedResult' => array(
                    'do'   => array('/* do file', ' * just for testing */'),
                    'undo' => array(),
                    'meta' => array(
                        'description' => '',
                        'appliedBy'   => ''
                    )
                )
            ),
            /* data set #2 - a file with undo commands */
            array(
                'fileContent'    => "--//@UNDO\n\n/* some undo commands */",
                'expectedResult' => array(
                    'do'   => array(),
                    'undo' => array('', '/* some undo commands */'),
                    'meta' => array(
                        'description' => '',
                        'appliedBy'   => ''
                    )
                )
            ),
            /* data set #3 - a file with senseful meta information */
            array(
                'fileContent'    => "--//@description test\n--//@appliedBy tester\n\n/* some commands */",
                'expectedResult' => array(
                    'do'   => array('', '/* some commands */'),
                    'undo' => array(),
                    'meta' => array(
                        'description' => 'test',
                        'appliedBy'   => 'tester'
                    )
                )
            ),
            /* data set #4 - a file with senseless meta information */
            array(
                'fileContent'    => "--//@crap test\n\n/* some commands */",
                'expectedResult' => array(
                    'do'   => array('', '/* some commands */'),
                    'undo' => array(),
                    'meta' => array(
                        'description' => '',
                        'appliedBy'   => '',
                        'crap'        => 'test'
                    )
                )
            ),
            /* data set #5 - a senseful file */
            array(
                'fileContent'    => "--//@description test\n--//@appliedBy tester\n"
                    . "CREATE DATABASE test;\n"
                    . "--//@UNDO\nDROP DATABASE test;",
                'expectedResult' => array(
                    'do'   => array('CREATE DATABASE test;'),
                    'undo' => array('DROP DATABASE test;'),
                    'meta' => array(
                        'description' => 'test',
                        'appliedBy'   => 'tester'
                    )
                )
            )
        );
    }

    /**
     * @covers DatabaseDeployTask::undoDeploy
     *
     * @dataProvider dataProviderForTestUndoDeploy
     *
     * @param integer $lastChangeAppliedInDb
     * @param array   $deltasFilesArray
     * @param array   $fileContents
     */
    public function testUndoDeploy($lastChangeAppliedInDb, $deltasFilesArray, $fileContents)
    {
        $dir = 'test';


        $out = $this->getMock(self::$_cut, array('getLastChangeAppliedInDb', 'getDeltasFilesArray', '_parse'));

        $out->expects($this->once())
            ->method('getLastChangeAppliedInDb')
            ->will($this->returnValue($lastChangeAppliedInDb));
        $out->expects($this->once())
            ->method('getDeltasFilesArray')
            ->will($this->returnValue($deltasFilesArray));

        $this->setProperty($out, '_dir', $dir);


        krsort($deltasFilesArray);

        $expectedFilePaths = array();
        $changeSets = array();
        $expectedResult = '';

        foreach ($deltasFilesArray as $fileChangeNumber => $fileName) {
            if ($fileChangeNumber > $lastChangeAppliedInDb) {

                $expectedFilePaths[$fileChangeNumber] = $dir . DIRECTORY_SEPARATOR . $fileName;

                $changeSets[$fileChangeNumber] = array(
                    'do'   => array(),
                    'undo' => explode("\n", $fileContents[$fileName]),
                    'meta' => array(
                        'description' => '',
                        'appliedBy'   => ''
                    )
                );

                $expectedResult .= '-- Fragment begins: ' . $fileChangeNumber . ' --' . "\n";
                $expectedResult .= $fileContents[$fileName] . "\n";
                $expectedResult .= 'DELETE FROM ' . DatabaseDeployTask::TABLE_NAME
                    . ' WHERE releaseNumber = ' . $fileChangeNumber . ';' . "\n";
                $expectedResult .= '-- Fragment ends: ' . $fileChangeNumber . ' --' . "\n";
            }
        }

        $i = 0;
        foreach ($deltasFilesArray as $fileChangeNumber => $fileName) {
            if ($fileChangeNumber > $lastChangeAppliedInDb) {
                $out->expects($this->at(2 + $i))
                    ->method('_parse')
                    ->with($this->equalTo($expectedFilePaths[$fileChangeNumber]))
                    ->will($this->returnValue($changeSets[$fileChangeNumber]));
                $i++;
            }
        }


        $result = $out->undoDeploy();

        $this->assertSame($expectedResult, $result);
    }

    /**
     * data provider for the test method testUndoDeploy()
     * @return array
     */
    public function dataProviderForTestUndoDeploy()
    {
        return array(
            /* data set #0 */
            array(
                'lastChangeAppliedInDb' => 0,
                'deltasFilesArray'      => array(),
                'fileContents'          => array()
            ),
            /* data set #1 */
            array(
                'lastChangeAppliedInDb' => 0,
                'deltasFilesArray'      => array(
                    1 => 'undofile1'
                ),
                'fileContents'          => array(
                    'undofile1' => '/* SQL undo file 1 */'
                )
            ),
            /* data set #2 */
            array(
                'lastChangeAppliedInDb' => 1,
                'deltasFilesArray'      => array(
                    1 => 'undofile1'
                ),
                'fileContents'          => array()
            ),
            /* data set #3 */
            array(
                'lastChangeAppliedInDb' => 2,
                'deltasFilesArray'      => array(
                    1 => 'undofile1'
                ),
                'fileContents'          => array()
            ),
            /* data set #4 */
            array(
                'lastChangeAppliedInDb' => 0,
                'deltasFilesArray'      => array(
                    1 => 'undofile1',
                    2 => 'undofile2'
                ),
                'fileContents'          => array(
                    'undofile1' => '/* SQL undo file 1 */',
                    'undofile2' => '/* SQL undo file 2 */'
                )
            ),
            /* data set #5 */
            array(
                'lastChangeAppliedInDb' => 1,
                'deltasFilesArray'      => array(
                    1 => 'undofile1',
                    2 => 'undofile2'
                ),
                'fileContents'          => array(
                    'undofile2' => '/* SQL undo file 2 */'
                )
            ),
            /* data set #6 */
            array(
                'lastChangeAppliedInDb' => 2,
                'deltasFilesArray'      => array(
                    1 => 'undofile1',
                    2 => 'undofile2'
                ),
                'fileContents'          => array()
            ),
            /* data set #7 */
            array(
                'lastChangeAppliedInDb' => 3,
                'deltasFilesArray'      => array(
                    1 => 'undofile1',
                    2 => 'undofile2'
                ),
                'fileContents'          => array()
            ),
            /* data set #8 */
            array(
                'lastChangeAppliedInDb' => 0,
                'deltasFilesArray'      => array(
                    2 => 'undofile2',
                    1 => 'undofile1'
                ),
                'fileContents'          => array(
                    'undofile1' => '/* SQL undo file 1 */',
                    'undofile2' => '/* SQL undo file 2 */'
                )
            ),
            /* data set #9 */
            array(
                'lastChangeAppliedInDb' => 1,
                'deltasFilesArray'      => array(
                    2 => 'undofile2',
                    1 => 'undofile1'
                ),
                'fileContents'          => array(
                    'undofile2' => '/* SQL undo file 2 */'
                )
            ),
            /* data set #10 */
            array(
                'lastChangeAppliedInDb' => 2,
                'deltasFilesArray'      => array(
                    2 => 'undofile2',
                    1 => 'undofile1'
                ),
                'fileContents'          => array()
            ),
            /* data set #11 */
            array(
                'lastChangeAppliedInDb' => 3,
                'deltasFilesArray'      => array(
                    2 => 'undofile2',
                    1 => 'undofile1'
                ),
                'fileContents'          => array()
            )
        );
    }

    /**
     * @covers DatabaseDeployTask::getDeltasFilesArray
     *
     * @dataProvider dataProviderForTestGetDeltasFilesArray
     *
     * @param array $fileNames
     * @param array $expectedResult
     */
    public function testGetDeltasFilesArray($fileNames, $expectedResult)
    {
        foreach ($fileNames as $fileName) {
            if ($this->_createTempFile($fileName) === false) {
                $this->fail('error creating temporary file "' . $fileName
                    . '" in temporary directory "' . $this->_dir . '"');
            }
        }


        $out = new self::$_cut();

        $this->setProperty($out, '_dir', $this->_dir);


        $result = $out->getDeltasFilesArray();


        /* here assertEquals() is used instead of assertSame(),
         * because the order of the filenames can vary depending on the file system */
        $this->assertEquals($expectedResult, $result);
    }

    /**
     * data provider for the test method testGetDeltasFilesArray()
     * @return array
     */
    public function dataProviderForTestGetDeltasFilesArray()
    {
        return array(
            /* data set #0 */
            array(
                'fileNames'      => array(),
                'expectedResult' => array()
            ),
            /* data set #1 */
            array(
                'fileNames'      => array('test.sql'),
                'expectedResult' => array()
            ),
            /* data set #2 */
            array(
                'fileNames'      => array(
                    '1.sql',
                    '2.sql'
                ),
                'expectedResult' => array(
                    1 => '1.sql',
                    2 => '2.sql'
                )
            ),
            /* data set #3 */
            array(
                'fileNames'      => array(
                    '00001.sql',
                    '00002.sql'
                ),
                'expectedResult' => array(
                    1 => '00001.sql',
                    2 => '00002.sql'
                )
            ),
            /* data set #4 */
            array(
                'fileNames'      => array(
                    '00001_structure.sql',
                    '00002_data.sql'
                ),
                'expectedResult' => array(
                    1 => '00001_structure.sql',
                    2 => '00002_data.sql'
                )
            ),
            /* data set #5 */
            array(
                'fileNames'      => array(
                    '1.sql',
                    '2.sql',
                    '00003.sql',
                    '00004.sql',
                    'do_00005.sql',
                    'undo_00006.sql',
                    '00007_do.sql',
                    '00008_undo.sql'
                ),
                'expectedResult' => array(
                    1 => '1.sql',
                    2 => '2.sql',
                    3 => '00003.sql',
                    4 => '00004.sql',
                    5 => 'do_00005.sql',
                    6 => 'undo_00006.sql',
                    7 => '00007_do.sql',
                    8 => '00008_undo.sql'
                )
            )
        );
    }

    /**
     * @covers DatabaseDeployTask::setDir
     */
    public function testSetDir()
    {
        $dir = 'test';

        $out = new self::$_cut();

        $out->setDir($dir);

        $this->assertAttributeSame($dir, '_dir', $out);
    }

    /**
     * @covers DatabaseDeployTask::setUrl
     */
    public function testSetUrl()
    {
        $url = 'test';

        $out = new self::$_cut();

        $out->setUrl($url);

        $this->assertAttributeSame($url, '_url', $out);
    }

    /**
     * @covers DatabaseDeployTask::setUsername
     */
    public function testSetUsername()
    {
        $username = 'test';

        $out = new self::$_cut();

        $out->setUsername($username);

        $this->assertAttributeSame($username, '_username', $out);
    }

    /**
     * @covers DatabaseDeployTask::setPassword
     */
    public function testSetPassword()
    {
        $password = 'test';

        $out = new self::$_cut();

        $out->setPassword($password);

        $this->assertAttributeSame($password, '_password', $out);
    }

    /**
     * @covers DatabaseDeployTask::setOutputFile
     */
    public function testSetOutputFile()
    {
        $outputFile = 'test';

        $out = new self::$_cut();

        $out->setOutputFile($outputFile);

        $this->assertAttributeSame($outputFile, '_outputFile', $out);
    }

    /**
     * @covers DatabaseDeployTask::setUndoOutputFile
     */
    public function testSetUndoOutputFile()
    {
        $undoOutputFile = 'test';

        $out = new self::$_cut();

        $out->setUndoOutputFile($undoOutputFile);

        $this->assertAttributeSame($undoOutputFile, '_undoOutputFile', $out);
    }

    /**
     * @covers DatabaseDeployTask::setVersionProperty
     */
    public function testSetVersionProperty()
    {
        $versionProperty = 'test';

        $out = new self::$_cut();

        $out->setVersionProperty($versionProperty);

        $this->assertAttributeSame($versionProperty, '_versionProperty', $out);
    }

    /**
     * @covers DatabaseDeployTask::setChangedProperty
     */
    public function testSetChangedProperty()
    {
        $changedProperty = 'test';

        $out = new self::$_cut();

        $out->setChangedProperty($changedProperty);

        $this->assertAttributeSame($changedProperty, '_changedProperty', $out);
    }

    /**
     * @covers DatabaseDeployTask::createFileSet
     */
    public function testCreateFileSet()
    {
        $out = new self::$_cut();

        $result = $out->createFileSet();

        $this->assertInstanceOf('FileSet', $result);

        $this->assertAttributeSame(array($result), 'filesets', $out);
    }
}