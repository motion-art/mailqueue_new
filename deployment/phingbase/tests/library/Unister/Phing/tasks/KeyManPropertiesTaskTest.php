<?php
/**
 * File for the class KeyManPropertiesTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class KeyManPropertiesTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class KeyManPropertiesTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (KeyManPropertiesTask)
     * @var string
     */
    protected static $_cut = 'KeyManPropertiesTask';


    /**
     * @covers KeyManPropertiesTask::main
     */
    public function testMainWithEmptyPropertiesList()
    {
        $propertiesList = array();


        $out = $this->getMock(self::$_cut, array('_getPropertiesList', '_getPropertiesValues'));

        $out->expects($this->once())
            ->method('_getPropertiesList')
            ->will($this->returnValue($propertiesList));
        $out->expects($this->never())
            ->method('_getPropertiesValues');


        $this->setExpectedException('BuildException', 'No keyman properties are found');


        $out->main();
    }

    /**
     * @covers KeyManPropertiesTask::main
     *
     * @dataProvider dataProviderForTestMain
     *
     * @param array $propertiesList
     * @param array $propertiesValues
     */
    public function testMain($propertiesList, $propertiesValues)
    {
        $projectMock = $this->getMock('Project', array('setProperty'));

        if (!empty($propertiesValues)) {
            $i = 0;
            foreach ($propertiesValues as $propertyName => $propertyValue) {
                $projectMock->expects($this->at($i++))
                    ->method('setProperty')
                    ->with($this->equalTo($propertyName), $this->equalTo($propertyValue))
                    ->will($this->returnValue(null));
            }
        } else {
            $projectMock->expects($this->never())
                ->method('setProperty');
        }


        $out = $this->getMock(self::$_cut, array('_getPropertiesList', '_getPropertiesValues'));

        $out->expects($this->once())
            ->method('_getPropertiesList')
            ->will($this->returnValue($propertiesList));
        $out->expects($this->once())
            ->method('_getPropertiesValues')
            ->with($this->equalTo($propertiesList))
            ->will($this->returnValue($propertiesValues));

        $this->setProperty($out, 'project', $projectMock);


        $out->main();
    }

    /**
     * data provider for the test method testMain()
     * @return array
     */
    public function dataProviderForTestMain()
    {
        return array(
            /* data set #0 */
            array(
                'propertiesList'   => array('prop1'),
                'propertiesValues' => array('prop1' => 'value1')
            ),
            /* data set #1 */
            array(
                'propertiesList'   => array('prop1'),
                'propertiesValues' => array('prop1' => null)
            ),
            /* data set #2 */
            array(
                'propertiesList'   => array('prop1', 'prop2'),
                'propertiesValues' => array(
                    'prop1' => 'value1',
                    'prop2' => 'value2'
                )
            ),
            /* data set #3 */
            array(
                'propertiesList'   => array('prop1', 'prop2'),
                'propertiesValues' => array(
                    'prop1' => null,
                    'prop2' => null
                )
            )
        );
    }

    /**
     * @covers KeyManPropertiesTask::setKey
     * @covers KeyManPropertiesTask::getKey
     */
    public function testSetKeyAndGetKey()
    {
        $key = 'test';

        $out = new self::$_cut();

        $out->setKey($key);
        $result = $out->getKey();

        $this->assertSame($key, $result);
    }

    /**
     * @covers KeyManPropertiesTask::_getPropertiesList
     *
     * @dataProvider dataProviderForTestGetPropertiesList
     *
     * @param string $propertiesListString
     * @param array  $expectedResult
     */
    public function testGetPropertiesList($propertiesListString, $expectedResult)
    {
        $key = 'test';

        $project = new Project();

        $project->setProperty($key . '.' . 'properties', $propertiesListString);


        $out = new self::$_cut();

        $this->setProperty($out, '_key', $key);

        $this->setProperty($out, 'project', $project);


        $result = $this->invoke($out, '_getPropertiesList');


        $this->assertSame($expectedResult, $result);
    }

    /**
     * data provider for the test method testGetPropertiesList()
     * @return array
     */
    public function dataProviderForTestGetPropertiesList()
    {
        return array(
            /* data set #0 */
            array(
                'propertiesListString' => null,
                'expectedResult'       => array(''),
            ),
            /* data set #1 */
            array(
                'propertiesListString' => '',
                'expectedResult'       => array(''),
            ),
            /* data set #2 */
            array(
                'propertiesListString' => 'prop1',
                'expectedResult'       => array('prop1')
            ),
            /* data set #3 */
            array(
                'propertiesListString' => 'prop1,prop2',
                'expectedResult'       => array('prop1', 'prop2')
            ),
            /* data set #4 */
            array(
                'propertiesListString' => 'prop2,prop1',
                'expectedResult'       => array('prop2', 'prop1')
            ),
            /* data set #5 */
            array(
                'propertiesListString' => 'prop1;prop2',
                'expectedResult'       => array('prop1;prop2')
            )
        );
    }

    /**
     * @covers KeyManPropertiesTask::_getPropertiesValues
     *
     * @dataProvider dataProviderForTestGetPropertiesValues
     *
     * @param array $propertiesList
     * @param array $expectedResult
     */
    public function testGetPropertiesValues($propertiesList, $expectedResult)
    {
        $key = 'test';

        $project = new Project();

        foreach ($propertiesList as $propertyName) {
            $nameKey = $key . '.' . $propertyName . '.name';
            $urlKey  = $key . '.' . $propertyName . '.url';

            $project->setProperty($nameKey, $propertyName);
            $project->setProperty($urlKey, $propertyName);
        }


        $out = $this->getMock(self::$_cut, array('_getContent'));

        $i = 0;
        foreach ($propertiesList as $propertyName) {
            $out->expects($this->at($i++))
                ->method('_getContent')
                ->with($this->equalTo($propertyName))
                ->will($this->returnValue($propertyName));
        }

        $this->setProperty($out, '_key', $key);

        $this->setProperty($out, 'project', $project);


        $result = $this->invoke($out, '_getPropertiesValues', array($propertiesList));


        $this->assertSame($expectedResult, $result);
    }

    /**
     * data provider for the test method testGetPropertiesValues()
     * @return array
     */
    public function dataProviderForTestGetPropertiesValues()
    {
        return array(
            /* data set #0 */
            array(
                'propertiesList' => array(),
                'expectedResult' => array()
            ),
            /* data set #1 */
            array(
                'propertiesList' => array(''),
                'expectedResult' => array(
                    '' => ''
                )
            ),
            /* data set #2 */
            array(
                'propertiesList' => array('prop1'),
                'expectedResult' => array(
                    'prop1' => 'prop1'
                )
            ),
            /* data set #3 */
            array(
                'propertiesList' => array('prop1', 'prop2'),
                'expectedResult' => array(
                    'prop1' => 'prop1',
                    'prop2' => 'prop2'
                )
            ),
            /* data set #4 */
            array(
                'propertiesList' => array('prop2', 'prop1'),
                'expectedResult' => array(
                    'prop2' => 'prop2',
                    'prop1' => 'prop1'
                )
            )
        );
    }

    /**
     * @covers KeyManPropertiesTask::_getNameKey
     */
    public function testGetNameKey()
    {
        $key = 'key';
        $name = 'name';

        $expectedResult = $key . '.' . $name . '.name';


        $out = new self::$_cut();

        $out->setKey($key);


        $result = $this->invoke($out, '_getNameKey', array($name));


        $this->assertSame($expectedResult, $result);
    }

    /**
     * @covers KeyManPropertiesTask::_getUrlKey
     */
    public function testGetUrlKey()
    {
        $key = 'key';
        $url = 'url';

        $expectedResult = $key . '.' . $url . '.url';


        $out = new self::$_cut();

        $out->setKey($key);


        $result = $this->invoke($out, '_getUrlKey', array($url));


        $this->assertSame($expectedResult, $result);
    }

    /**
     * @covers KeyManPropertiesTask::_getContent
     */
    public function testGetContent()
    {
        $url = 'test';

        $expectedStatus = 200;
        $expectedBody = 'body';


        /* create the mock object bypassing execution of the constructor of the original class */
        $responseMock = $this->_getMock('HTTP_Request2_Response', array('getStatus', 'getBody', 'getReasonPhrase'));
        $responseMock->expects($this->once())
            ->method('getStatus')
            ->will($this->returnValue($expectedStatus));
        $responseMock->expects($this->once())
            ->method('getBody')
            ->will($this->returnValue($expectedBody));
        $responseMock->expects($this->never())
            ->method('getReasonPhrase');

        /* create the mock object bypassing execution of the constructor of the original class */
        $requestMock = $this->_getMock('HTTP_Request2', array('send'));
        $requestMock->expects($this->once())
            ->method('send')
            ->will($this->returnValue($responseMock));


        $out = $this->getMock(self::$_cut, array('_getRequest'));

        $out->expects($this->once())
            ->method('_getRequest')
            ->with($this->equalTo($url))
            ->will($this->returnValue($requestMock));


        $result = $this->invoke($out, '_getContent', array($url));


        $this->assertSame($expectedBody, $result);
    }

    /**
     * @covers KeyManPropertiesTask::_getContent
     */
    public function testGetContentWithUnexpectedResponse()
    {
        $url = 'test';

        $expectedStatus = 503;
        $expectedReasonPhrase = 'Service unavailable';

        $expectedExceptionMessage = 'Unexpected HTTP status: ' . $expectedStatus . ' ' . $expectedReasonPhrase;


        /* create the mock object bypassing execution of the constructor of the original class */
        $responseMock = $this->_getMock('HTTP_Request2_Response', array('getStatus', 'getBody', 'getReasonPhrase'));
        $responseMock->expects($this->exactly(2))
            ->method('getStatus')
            ->will($this->returnValue($expectedStatus));
        $responseMock->expects($this->never())
            ->method('getBody');
        $responseMock->expects($this->once())
            ->method('getReasonPhrase')
            ->will($this->returnValue($expectedReasonPhrase));

        /* create the mock object bypassing execution of the constructor of the original class */
        $requestMock = $this->_getMock('HTTP_Request2', array('send'));
        $requestMock->expects($this->once())
            ->method('send')
            ->will($this->returnValue($responseMock));


        $out = $this->getMock(self::$_cut, array('_getRequest'));

        $out->expects($this->once())
            ->method('_getRequest')
            ->with($this->equalTo($url))
            ->will($this->returnValue($requestMock));


        $this->setExpectedException('BuildException', $expectedExceptionMessage);


        $this->invoke($out, '_getContent', array($url));
    }

    /**
     * @covers KeyManPropertiesTask::_getContent
     */
    public function testGetContentWithRequestException()
    {
        $url = 'test';


        /* create the mock object bypassing execution of the constructor of the original class */
        $requestMock = $this->_getMock('HTTP_Request2', array('send'));
        $requestMock->expects($this->once())
            ->method('send')
            ->will($this->throwException(new HTTP_Request2_Exception()));


        $out = $this->getMock(self::$_cut, array('_getRequest'));

        $out->expects($this->once())
            ->method('_getRequest')
            ->with($this->equalTo($url))
            ->will($this->returnValue($requestMock));


        $this->setExpectedException('BuildException', 'Error');


        $this->invoke($out, '_getContent', array($url));
    }
}