<?php
/**
 * File for the class NamespacePropertiesTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class NamespacePropertiesTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class NamespacePropertiesTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (NamespacePropertiesTask)
     * @var string
     */
    protected static $_cut = 'NamespacePropertiesTask';


    /**
     * Sets up the fixture.
     * This method is called before a test is executed.
     */
    public function setUp()
    {
        parent::setUp();

        if ($this->getName(false) === 'testSetPrefix') {
            $this->_saveClassProperties(self::$_cut);
        }
    }

    /**
     * Tears down the fixture.
     * This method is called after a test is executed.
     */
    public function tearDown()
    {
        if ($this->getName(false) === 'testSetPrefix') {
            $this->_resetClassProperties(self::$_cut, array('_prefix' => 'list', '_separator' => '.'));
        }

        parent::tearDown();
    }


    /**
     * @covers NamespacePropertiesTask::setKey
     */
    public function testSetKey()
    {
        $key = 'test';

        $out = new self::$_cut();

        $out->setKey($key);

        $this->assertAttributeSame($key, '_key', $out);
    }

    /**
     * @covers NamespacePropertiesTask::setPrefix
     */
    public function testSetPrefix()
    {
        $prefix = 'test';

        $out = new self::$_cut();

        $out->setPrefix($prefix);

        $this->assertSame($prefix, $this->getProperty($out, '_prefix'));
    }

    /**
     * @covers NamespacePropertiesTask::main
     *
     * @dataProvider dataProviderForTestMain
     *
     * @param string $key
     * @param array  $projectProperties
     * @param array  $expectedAdditionalProjectProperties
     */
    public function testMain($key, $projectProperties, $expectedAdditionalProjectProperties)
    {
        $project = new Project();

        foreach ($projectProperties as $propertyName => $propertyValue) {
            $project->setProperty($propertyName, $propertyValue);
        }

        $expectedProjectProperties = array_merge($projectProperties, $expectedAdditionalProjectProperties);


        $out = new self::$_cut();

        $this->setProperty($out, '_key', $key);

        $this->setProperty($out, 'project', $project);


        $out->main();


        $this->assertSame($expectedProjectProperties, $project->getProperties());
    }

    /**
     * data provider for the test method testMain()
     * @return array
     */
    public function dataProviderForTestMain()
    {
        $expectedAdditionalPropertiesPrefixPart1 = $this->getProperty('NamespacePropertiesTask', '_prefix');
        $expectedAdditionalPropertiesPrefix = $expectedAdditionalPropertiesPrefixPart1
            . $this->getProperty('NamespacePropertiesTask', '_separator');

        return array(
            /* data set #0 */
            array(
                'key'                                 => 'first',
                'projectProperties'                   => array(),
                'expectedAdditionalProjectProperties' => array()
            ),
            /* data set #1 */
            array(
                'key'                                 => 'first',
                'projectProperties'                   => array(
                    'firstkey' => 'value1'
                ),
                'expectedAdditionalProjectProperties' => array()
            ),
            /* data set #2 */
            array(
                'key'                                 => 'first',
                'projectProperties'                   => array(
                    'thefirstkey' => 'value1'
                ),
                'expectedAdditionalProjectProperties' => array()
            ),
            /* data set #3 */
            array(
                'key'                                 => 'first',
                'projectProperties'                   => array(
                    'theFirstKey' => 'value1'
                ),
                'expectedAdditionalProjectProperties' => array()
            ),
            /* data set #4 */
            array(
                'key'                                 => 'first',
                'projectProperties'                   => array(
                    $expectedAdditionalPropertiesPrefix . 'firstkey' => 'value1'
                ),
                'expectedAdditionalProjectProperties' => array(
                    $expectedAdditionalPropertiesPrefixPart1 . 'key' => 'value1'
                )
            ),
            /* data set #5 */
            array(
                'key'                                 => 'first',
                'projectProperties'                   => array(
                    'the' . $expectedAdditionalPropertiesPrefix . 'firstkey' => 'value1'
                ),
                'expectedAdditionalProjectProperties' => array()
            ),
            /* data set #6 */
            array(
                'key'                                 => 'first',
                'projectProperties'                   => array(
                    $expectedAdditionalPropertiesPrefix . 'FirstKey' => 'value1'
                ),
                'expectedAdditionalProjectProperties' => array()
            ),
            /* data set #7 */
            array(
                'key'                                 => 'first',
                'projectProperties'                   => array(
                    'key1' => $expectedAdditionalPropertiesPrefix . 'firstvalue'
                ),
                'expectedAdditionalProjectProperties' => array()
            ),
            /* data set #8 */
            array(
                'key'                                 => 'first',
                'projectProperties'                   => array(
                    'key1' => 'the' . $expectedAdditionalPropertiesPrefix . 'firstvalue'
                ),
                'expectedAdditionalProjectProperties' => array()
            ),
            /* data set #9 */
            array(
                'key'                                 => 'first',
                'projectProperties'                   => array(
                    'key1' => 'the' . $expectedAdditionalPropertiesPrefix . 'FirstValue'
                ),
                'expectedAdditionalProjectProperties' => array()
            ),
            /* data set #10 */
            array(
                'key'                                 => 'first',
                'projectProperties'                   => array(
                    $expectedAdditionalPropertiesPrefix . 'firstkey' => 'value1',
                    $expectedAdditionalPropertiesPrefix . 'secondkey' => 'value2'
                ),
                'expectedAdditionalProjectProperties' => array(
                    $expectedAdditionalPropertiesPrefixPart1 . 'key' => 'value1'
                )
            ),
            /* data set #11 */
            array(
                'key'                                 => 'first',
                'projectProperties'                   => array(
                    'the' . $expectedAdditionalPropertiesPrefix . 'firstkey' => 'value1',
                    'the' . $expectedAdditionalPropertiesPrefix . 'secondkey' => 'value2'
                ),
                'expectedAdditionalProjectProperties' => array()
            ),
            /* data set #12 */
            array(
                'key'                                 => 'first',
                'projectProperties'                   => array(
                    $expectedAdditionalPropertiesPrefix . 'FirstKey' => 'value1',
                    $expectedAdditionalPropertiesPrefix . 'SecondKey' => 'value2'
                ),
                'expectedAdditionalProjectProperties' => array()
            )
        );
    }
}