<?php
/**
 * File for the class ListenerTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class ListenerTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class ListenerTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (ListenerTask)
     * @var string
     */
    protected static $_cut = 'ListenerTask';


    /**
     * @covers ListenerTask::setName
     */
    public function testSetName()
    {
        $name = 'test';

        $out = new self::$_cut();

        $out->setName($name);

        $this->assertAttributeSame($name, '_name', $out);
    }

    /**
     * @covers ListenerTask::createOption
     */
    public function testCreateOption()
    {
        $out = new self::$_cut();

        $result = $out->createOption();

        $this->assertInstanceOf('Parameter', $result);

        $this->assertAttributeSame(array($result), '_options', $out);
    }

    /**
     * @covers ListenerTask::_findOrInitListener
     */
    public function testFindOrInitListenerWithoutListenerName()
    {
        $listenerName = null;


        $out = $this->getMock(self::$_cut, array('_requireListenerClassFile'));

        $out->expects($this->never())
            ->method('_requireListenerClassFile');


        $this->setExpectedException('BuildException', 'Listener not defined');


        $this->invoke($out, '_findOrInitListener', array($listenerName));
    }

    /**
     * @covers ListenerTask::_findOrInitListener
     */
    public function testFindOrInitListenerWhenListenerCanBeFoundInProject()
    {
        $listenerName = 'Syslog';
        $listenerClassName = $listenerName . 'Listener';

        $listener = new $listenerClassName();


        $project = new Project();

        $project->addBuildListener($listener);


        $out = $this->getMock(self::$_cut, array('_requireListenerClassFile'));

        $out->expects($this->never())
            ->method('_requireListenerClassFile');

        $this->setProperty($out, 'project', $project);


        $result = $this->invoke($out, '_findOrInitListener', array($listenerName));


        $this->assertSame($listener, $result);
    }

    /**
     * @covers ListenerTask::_findOrInitListener
     */
    public function testFindOrInitListenerWhenClassDoesNotExist()
    {
        $listenerName = 'Syslock';
        $listenerClassName = $listenerName . 'Listener';

        $expectedRelativeListenerFilePath = 'listener/' . $listenerClassName . '.php';


        $projectMock = $this->getMock('Project', array('addBuildListener'));
        $projectMock->expects($this->never())
            ->method('addBuildListener');


        $out = $this->getMock(self::$_cut, array('_requireListenerClassFile'));

        $out->expects($this->once())
            ->method('_requireListenerClassFile')
            ->with($this->equalTo($expectedRelativeListenerFilePath))
            ->will($this->returnValue(null));

        $this->setProperty($out, 'project', $projectMock);


        $this->setExpectedException('BuildException', 'Listener not found');


        $result = $this->invoke($out, '_findOrInitListener', array($listenerName));
    }

    /**
     * @covers ListenerTask::_findOrInitListener
     */
    public function testFindOrInitListener()
    {
        $listenerName = 'Syslog';
        $listenerClassName = $listenerName . 'Listener';

        $expectedRelativeListenerFilePath = 'listener/' . $listenerClassName . '.php';


        $projectMock = $this->getMock('Project', array('addBuildListener'));
        $projectMock->expects($this->once())
            ->method('addBuildListener')
            ->with($this->isInstanceOf($listenerClassName))
            ->will($this->returnValue(null));


        $out = $this->getMock(self::$_cut, array('_requireListenerClassFile'));

        $out->expects($this->once())
            ->method('_requireListenerClassFile')
            ->with($this->equalTo($expectedRelativeListenerFilePath))
            ->will($this->returnValue(null));

        $this->setProperty($out, 'project', $projectMock);


        $result = $this->invoke($out, '_findOrInitListener', array($listenerName));


        $this->assertInstanceOf($listenerClassName, $result);
    }

    /**
     * @covers ListenerTask::main
     *
     * @dataProvider dataProviderForTestMain
     *
     * @param array $options
     */
    public function testMain($options)
    {
        $name = 'test';


        $listener = $this->getMock('SyslogListener', array('setOption', '_log'));

        if (!empty($options)) {
            $i = 0;
            foreach ($options as $option) {
                $listener->expects($this->at($i++))
                    ->method('setOption')
                    ->with($this->equalTo($option->getName()), $this->equalTo($option->getValue()));
            }
        } else {
            $listener->expects($this->never())
                ->method('setOption');
        }


        $out = $this->getMock(self::$_cut, array('_findOrInitListener'));

        $out->expects($this->once())
            ->method('_findOrInitListener')
            ->with($this->equalTo($name))
            ->will($this->returnValue($listener));

        $this->setProperty($out, '_name', $name);
        $this->setProperty($out, '_options', $options);


        $out->main();
    }

    /**
     * data provider for the test method testMain()
     * @return array
     */
    public function dataProviderForTestMain()
    {
        $option1 = new Parameter();
        $option1->setName('name1');
        $option1->setValue('value1');

        $option2 = new Parameter();
        $option2->setName('name2');
        $option2->setValue('value2');

        return array(
            /* data set #0 */
            array(
                'options' => array()
            ),
            /* data set #1 */
            array(
                'options' => array($option1)
            ),
            /* data set #2 */
            array(
                'options' => array($option1, $option2)
            )
        );
    }
}