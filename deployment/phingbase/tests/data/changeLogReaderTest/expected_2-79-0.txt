Bugfix:      Bug 147085 - Suchzeile fehlt bei noresult Seite (Jessica Parth)
Bugfix:      Bug 154252 - MTB: Schreibfehler in den AGBs (Katharina Maschke)
Feature:     Bug 153466 - Anpassung Split 62 - Position von Kretszche auf Retail-LP (Nils Mammen)