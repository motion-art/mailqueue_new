<?xml version="1.0" encoding="UTF-8"?>
<!-- 
/**
 * Unister Deployment-Framework
 * 
 * DbDeploy-Targets
 *
 * @category   deployment
 * @package    dbdeploy
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id$
 */
 -->
 
<project name="db-deploy">

    <!-- Default Properties -->
    <property name="mysql.binary" value="/usr/bin/mysql" />
    <property name="db.server.port" value="3306" />
    <property name="db.server.default-character-set" value="latin1" />
    <property name="project.dbdeploydir" value="${project.builddir}/dbdeploy" />


    <!-- PUBLIC TARGETS -->
    
    <target name="db-deploy" depends="_prepare-phing" description="Migration der Datenbank">
        <foreach list="${db.list}" delimiter="," target="_db-deploy-loop" param="db" />
    </target>


    <!-- PRIVATE TARGETS -->

    <target name="_db-deploy-loop">
        <namespaceproperties key="${db}" prefix="db.server" />
        
        <available file="${destination}/${dbdeploy.path}/${db.server.deploypath}" property="deploypath_exists" value="ok"/>
        <if>
            <not><equals arg1="${deploypath_exists}" arg2="ok" /></not>
            <then>
                <fail msg="${destination}/${dbdeploy.path}/${db.server.deploypath} not found"/>
            </then>
        </if>
        
        <property name="db.server.fail-build-if-changes-exist" value="false" />
        
        <mkdir dir="${project.dbdeploydir}/${db.server.deploypath}" />
        <databasedeploy
            url="mysql:host=${db.server.host};port=${db.server.port};dbname=${db.server.name}"
            username="${db.server.username}"
            password="${db.server.password}"
            dir="${destination}/${dbdeploy.path}/${db.server.deploypath}"
            outputfile="${project.dbdeploydir}/${db.server.deploypath}/deploy.sql"
            undooutputfile="${project.dbdeploydir}/${db.server.deploypath}/undo.sql"
            versionproperty="dbversion"
            changedproperty="dbchanged"
            exceptionifchangesexist="${db.server.fail-build-if-changes-exist}"
        />
        
        <exec command="mktemp" outputProperty="credentialfile" checkreturn="true" />
        <copy 
            file="${phingbase.dir}/templates/mysql.credentials"
            tofile="${credentialfile}"
            overwrite="true">
            <filterchain>
                <expandproperties />
            </filterchain>
        </copy>
        <chmod file="${credentialfile}" mode="0600" />
        
        <listener name="PostBuildClean">
            <option name="mysql_credentials" value="${credentialfile}" />
        </listener>
        
        <exec
            command="${mysql.binary} --defaults-extra-file=${credentialfile} -h${db.server.host} -P ${db.server.port}
                --default-character-set=${db.server.default-character-set}
                ${db.server.name} &lt; ${project.dbdeploydir}/${db.server.deploypath}/deploy.sql"
            returnProperty="mysqlreturn" passthru="true" />
        
        <if>
            <not>
                <equals arg1="${mysqlreturn}" arg2="0" />
            </not>
            <then>
                <fail msg="Returncode of mysql was not 0" />
            </then>
        </if>
        
        <listener name="Syslog">
            <option name="databaseVersion" value="${dbversion}" />
            <option name="databaseChanged" value="${dbchanged}" />
        </listener>
    </target>
</project>
