<?php
/**
 * File for the class MinifyTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class MinifyTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class MinifyTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (MinifyTask)
     * @var string
     */
    protected static $_cut = 'MinifyTask';

    /**
     * names of test methods that need filesystem access
     * @var array
     */
    protected static $_methodsWithFileSystemAccess = array(
        'testMainWithFileSetWithoutException',
        'testMainWithFileSetWhenTypeIsUnknown',
        'testMainWithFileSetWithMinifyException'
    );


    /**
     * @covers MinifyTask::createFileSet
     */
    public function testCreateFileSet()
    {
        $out = new self::$_cut();

        $result = $out->createFileSet();

        $this->assertInstanceOf('FileSet', $result);

        $this->assertSame(1, count($this->getProperty($out, '_filesets')));
    }

    /**
     * @covers MinifyTask::setType
     */
    public function testSetType()
    {
        $type = 'test';

        $out = new self::$_cut();

        $out->setType($type);

        $this->assertAttributeSame($type, '_type', $out);
    }

    /**
     * @covers MinifyTask::setFailonerror
     */
    public function testSetFailonerror()
    {
        $failonerror = 'test';

        $out = new self::$_cut();

        $out->setFailonerror($failonerror);

        $this->assertAttributeSame($failonerror, '_failonerror', $out);
    }

    /**
     * @covers MinifyTask::init
     */
    public function testInit()
    {
        $out = new self::$_cut();

        $result = $out->init();

        $this->assertTrue($result);
    }

    /**
     * @covers MinifyTask::main
     */
    public function testMainWithoutFileSet()
    {
        $out = $this->getMock(self::$_cut, array('log'));

        $out->expects($this->never())
            ->method('log');


        $out->main();
    }

    /**
     * @covers MinifyTask::main
     */
    public function testMainWithEmptyFileSetWithoutException()
    {
        $includedFiles = array();
        $dir = './';


        $project = new Project();

        $directoryScannerMock = $this->getMock('DirectoryScanner', array('getIncludedFiles'));
        $directoryScannerMock->expects($this->once())
            ->method('getIncludedFiles')
            ->will($this->returnValue($includedFiles));

        $fileSetMock = $this->getMock('FileSet', array('getDirectoryScanner', 'getDir'));
        $fileSetMock->expects($this->once())
            ->method('getDirectoryScanner')
            ->with($this->equalTo($project))
            ->will($this->returnValue($directoryScannerMock));
        $fileSetMock->expects($this->once())
            ->method('getDir')
            ->with($this->equalTo($project))
            ->will($this->returnValue($dir));


        $out = $this->getMock(self::$_cut, array('log'));

        $out->expects($this->never())
            ->method('log');

        $this->setProperty($out, '_filesets', array($fileSetMock));

        $this->setProperty($out, 'project', $project);


        $out->main();
    }

    /**
     * data provider for the test method testMainWithFileSetWithoutException()
     * @return array
     */
    public function dataProviderForTestMainWithFileSetWithoutException()
    {
        return array(
                /* data set #0 */
                array(
                        'type'                => MinifyTask::TYPE_CSS,
                        'originalFileContent' => "body {\n  margin: 10px;\n}\n",
                        'minifiedFileContent' => 'body{margin:10px}'
                ),
                /* data set #1 */
                array(
                        'type'                => MinifyTask::TYPE_JS,
                        'originalFileContent' => "var test\n    = 'test';",
                        'minifiedFileContent' => "var test='test';"
                ),
                /* data set #2 */
                array(
                        'type'                => MinifyTask::TYPE_PHTML,
                        'originalFileContent' => "<html>\n\n<body>\n</body>\n<html>",
                        'minifiedFileContent' => '<html><body></body><html>'
                )
        );
    }

    /**
     * @covers MinifyTask::main
     *
     * @dataProvider dataProviderForTestMainWithEmptyFileSetWithException
     *
     * @param boolean $failOnError
     */
    public function testMainWithEmptyFileSetWithException($failOnError)
    {
        $expectedExceptionMessage = 'No directory specified for fileset.';


        $project = new Project();

        $fileSetMock = $this->getMock('FileSet', array('getDirectoryScanner', 'getDir'));
        $fileSetMock->expects($this->once())
            ->method('getDirectoryScanner')
            ->with($this->equalTo($project))
            ->will($this->throwException(new BuildException($expectedExceptionMessage)));
        $fileSetMock->expects($this->never())
            ->method('getDir');


        $out = $this->getMock(self::$_cut, array('log'));

        if (!$failOnError) {
            $out->expects($this->once())
                ->method('log')
                ->with($this->equalTo($expectedExceptionMessage), $this->equalTo(Project::MSG_WARN))
                ->will($this->returnValue(null));
        } else {
            $out->expects($this->never())
                ->method('log');
        }

        $this->setProperty($out, '_failonerror', $failOnError);
        $this->setProperty($out, '_filesets', array($fileSetMock));

        $this->setProperty($out, 'project', $project);


        if ($failOnError) {
            $this->setExpectedException('BuildException', $expectedExceptionMessage);
        }


        $out->main();
    }

    /**
     * data provider for the test method testMainWithEmptyFileSetWithException()
     * @return array
     */
    public function dataProviderForTestMainWithEmptyFileSetWithException()
    {
        return PhingbaseTest_Util_DataProvider::getDataProviderBooleanValues('failOnError');
    }

    /**
     * @covers MinifyTask::main
     *
     * @dataProvider dataProviderForTestMainWithFileSetWithoutException
     *
     * @param string $type
     * @param string $originalFileContent
     * @param string $minifiedFileContent
     */
    public function testMainWithFileSetWithoutException($type, $originalFileContent, $minifiedFileContent)
    {
        $fileName = __CLASS__ . '_' . $this->getName(false) . '_' . $type;

        if (($filePath = $this->_createTempFile($fileName, $originalFileContent)) === false) {
            $this->fail('error creating temporary file "' . $fileName
                . '" in temporary directory "' . $this->_dir . '"');
        }

        $includedFiles = array($fileName);

        $expectedLogMessage = 'Minifying file ' . $fileName;


        $project = new Project();

        $directoryScannerMock = $this->getMock('DirectoryScanner', array('getIncludedFiles'));
        $directoryScannerMock->expects($this->once())
            ->method('getIncludedFiles')
            ->will($this->returnValue($includedFiles));

        $fileSetMock = $this->getMock('FileSet', array('getDirectoryScanner', 'getDir'));
        $fileSetMock->expects($this->once())
            ->method('getDirectoryScanner')
            ->with($this->equalTo($project))
            ->will($this->returnValue($directoryScannerMock));
        $fileSetMock->expects($this->once())
            ->method('getDir')
            ->with($this->equalTo($project))
            ->will($this->returnValue($this->_dir));


        $out = $this->getMock(self::$_cut, array('log'));

        $out->expects($this->once())
            ->method('log')
            ->with($this->equalTo($expectedLogMessage))
            ->will($this->returnValue(null));

        $this->setProperty($out, '_type', $type);
        $this->setProperty($out, '_filesets', array($fileSetMock));

        $this->setProperty($out, 'project', $project);

        $out->main();

        $this->assertSame($minifiedFileContent, file_get_contents($filePath));
    }

    /**
     * @covers MinifyTask::main
     *
     * @dataProvider dataProviderForTestMainWithFileSetWhenTypeIsUnknown
     *
     * @param string|null $unknownType
     */
    public function testMainWithFileSetWhenTypeIsUnknown($unknownType)
    {
        $originalFileContent = 'test text';

        $fileName = __CLASS__ . '_' . $this->getName(false) . '_' . $unknownType;

        if (($filePath = $this->_createTempFile($fileName, $originalFileContent)) === false) {
            $this->fail('error creating temporary file "' . $fileName
                . '" in temporary directory "' . $this->_dir . '"');
        }

        $includedFiles = array($fileName);

        $expectedLogMessage1 = 'Minifying file ' . $fileName;

        $expectedExceptionMessage = 'Type unknown';

        $expectedLogMessage2 = 'Could not minify file ' . $fileName . ': ' . $expectedExceptionMessage;


        $project = new Project();

        $directoryScannerMock = $this->getMock('DirectoryScanner', array('getIncludedFiles'));
        $directoryScannerMock->expects($this->once())
            ->method('getIncludedFiles')
            ->will($this->returnValue($includedFiles));

        $fileSetMock = $this->getMock('FileSet', array('getDirectoryScanner', 'getDir'));
        $fileSetMock->expects($this->once())
            ->method('getDirectoryScanner')
            ->with($this->equalTo($project))
            ->will($this->returnValue($directoryScannerMock));
        $fileSetMock->expects($this->once())
            ->method('getDir')
            ->with($this->equalTo($project))
            ->will($this->returnValue($this->_dir));


        $out = $this->getMock(self::$_cut, array('log'));

        $out->expects($this->at(0))
            ->method('log')
            ->with($this->equalTo($expectedLogMessage1))
            ->will($this->returnValue(null));
        $out->expects($this->at(1))
            ->method('log')
            ->with($this->equalTo($expectedLogMessage2), $this->equalTo(Project::MSG_ERR))
            ->will($this->returnValue(null));

        $this->setProperty($out, '_type', $unknownType);
        $this->setProperty($out, '_filesets', array($fileSetMock));

        $this->setProperty($out, 'project', $project);


        $out->main();
    }

    /**
     * data provider for the test method testMainWithFileSetWhenTypeIsUnknown()
     * @return array
     */
    public function dataProviderForTestMainWithFileSetWhenTypeIsUnknown()
    {
        return array(
            /* data set #0 */
            array('unknownType' => null),
            /* data set #1 */
            array('unknownType' => 'txt'),
            /* data set #2 */
            array('unknownType' => 'bmp')
        );
    }

    /**
     * @covers MinifyTask::main
     */
    public function testMainWithFileSetWithMinifyException()
    {
        $originalFileContent = "var test\n    = 'test;";
        $type = MinifyTask::TYPE_JS;

        $fileName = __CLASS__ . '_' . $this->getName(false) . '_' . $type;

        if (($filePath = $this->_createTempFile($fileName, $originalFileContent)) === false) {
            $this->fail('error creating temporary file "' . $fileName
                . '" in temporary directory "' . $this->_dir . '"');
        }

        $includedFiles = array($fileName);

        $expectedLogMessage1 = 'Minifying file ' . $fileName;

        $expectedExceptionMessage = "JSMin: Unterminated String at byte 21: 'test;";
        //$expectedExceptionMessage = "Unterminated string literal.";

        $expectedLogMessage2 = 'Could not minify file ' . $fileName . ': ' . $expectedExceptionMessage;


        $project = new Project();

        $directoryScannerMock = $this->getMock('DirectoryScanner', array('getIncludedFiles'));
        $directoryScannerMock->expects($this->once())
            ->method('getIncludedFiles')
            ->will($this->returnValue($includedFiles));

        $fileSetMock = $this->getMock('FileSet', array('getDirectoryScanner', 'getDir'));
        $fileSetMock->expects($this->once())
            ->method('getDirectoryScanner')
            ->with($this->equalTo($project))
            ->will($this->returnValue($directoryScannerMock));
        $fileSetMock->expects($this->once())
            ->method('getDir')
            ->with($this->equalTo($project))
            ->will($this->returnValue($this->_dir));


        $out = $this->getMock(self::$_cut, array('log'));

        $out->expects($this->at(0))
            ->method('log')
            ->with($this->equalTo($expectedLogMessage1))
            ->will($this->returnValue(null));
        $out->expects($this->at(1))
            ->method('log')
            ->with($this->equalTo($expectedLogMessage2), $this->equalTo(Project::MSG_ERR))
            ->will($this->returnValue(null));

        $this->setProperty($out, '_type', $type);
        $this->setProperty($out, '_filesets', array($fileSetMock));

        $this->setProperty($out, 'project', $project);


        $out->main();
    }
}