<?php
/**
 * File for the class ParallelTargetTaskTest
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Henry Prang <henry.prang@unister-gmbh.de>
 */

/**
 * Unit tests for the class ParallelTargetTask
 *
 * @category   Phing
 * @package    Phing_Task
 * @copyright  Copyright (c) 2012, Unister GmbH
 */
class ParallelTargetTaskTest extends PhingbaseTest_TestCase_UnitTest
{
    /**
     * the class under test (ParallelTargetTask)
     * @var string
     */
    protected static $_cut = 'ParallelTargetTask';


    /**
     * @covers ParallelTargetTask::setList
     */
    public function testSetList()
    {
        $list = 'test';

        $out = new self::$_cut();

        $out->setList($list);

        $this->assertAttributeSame($list, '_list', $out);
    }

    /**
     * @covers ParallelTargetTask::setDelimiter
     */
    public function testSetDelimiter()
    {
        $delimiter = 'test';

        $out = new self::$_cut();

        $out->setDelimiter($delimiter);

        $this->assertAttributeSame($delimiter, '_delimiter', $out);
    }

    /**
     * @covers ParallelTargetTask::setTarget
     */
    public function testSetTarget()
    {
        $target = 'test';

        $out = new self::$_cut();

        $out->setTarget($target);

        $this->assertAttributeSame($target, '_target', $out);
    }

    /**
     * @covers ParallelTargetTask::setThreadCount
     */
    public function testSetThreadCount()
    {
        $threadCount = 200;


        /* create the mock object bypassing execution of the constructor of the original class */
        $managerMock = $this->_getMock('DocBlox_Parallel_Manager', array('setProcessLimit'));
        $managerMock->expects($this->once())
            ->method('setProcessLimit')
            ->with($this->equalTo($threadCount))
            ->will($this->returnValue(null));


        $out = new self::$_cut();

        $this->setProperty($out, '_manager', $managerMock);


        $out->setThreadCount($threadCount);
    }

    /**
     * @covers ParallelTargetTask::main
     */
    public function testMainWithoutFailures()
    {
        $target = 'test';
        $list = 'key1';
        $delimiter = ',';

        $expectedKeys = array('key1');


        $project      = new Project();
        $owningTarget = new Target();
        $location     = new Location();


        $propertyTaskMock = $this->getMock('PropertyTask', array('setName', 'setValue'));
        $propertyTaskMock->expects($this->once())
            ->method('setName')
            ->with($this->equalTo('key'))
            ->will($this->returnValue(null));
        $propertyTaskMock->expects($this->once())
            ->method('setValue')
            ->with($this->equalTo('key1'))
            ->will($this->returnValue(null));

        $taskMock = $this->getMock('PhingCallTask',
            array('setTarget', 'setProject', 'setOwningTarget', 'setLocation', 'createProperty'));
        $taskMock->expects($this->once())
            ->method('setTarget')
            ->with($this->equalTo($target))
            ->will($this->returnValue(null));
        $taskMock->expects($this->once())
            ->method('setProject')
            ->with($this->equalTo($project))
            ->will($this->returnValue(null));
        $taskMock->expects($this->once())
            ->method('setOwningTarget')
            ->with($this->equalTo($owningTarget))
            ->will($this->returnValue(null));
        $taskMock->expects($this->once())
            ->method('setLocation')
            ->with($this->equalTo($location))
            ->will($this->returnValue(null));
        $taskMock->expects($this->once())
            ->method('createProperty')
            ->will($this->returnValue($propertyTaskMock));

        /* create the mock object bypassing execution of the constructor of the original class */
        $workerMock = $this->_getMock('DocBlox_Parallel_Worker', array('getError'));
        $workerMock->expects($this->once())
            ->method('getError')
            ->will($this->returnValue(false));

        /* create the mock object bypassing execution of the constructor of the original class */
        $managerMock = $this->_getMock('DocBlox_Parallel_Manager', array('execute'));
        $managerMock->expects($this->once())
            ->method('execute')
            ->will($this->returnValue(null));


        $out = $this->getMock(self::$_cut,
            array('_getTask', '_getWorker', 'getProject', 'getOwningTarget', 'getLocation'));

        $out->expects($this->once())
            ->method('_getTask')
            ->will($this->returnValue($taskMock));

        $out->expects($this->once())
            ->method('getProject')
            ->will($this->returnValue($project));
        $out->expects($this->once())
            ->method('getOwningTarget')
            ->will($this->returnValue($owningTarget));
        $out->expects($this->once())
            ->method('getLocation')
            ->will($this->returnValue($location));

        $expectedWorkerCallback = array($taskMock, 'perform');

        $out->expects($this->once())
            ->method('_getWorker')
            ->with($this->equalTo($expectedWorkerCallback))
            ->will($this->returnValue($workerMock));

        $this->setProperty($out, '_manager', $managerMock);
        $this->setProperty($out, '_target', $target);
        $this->setProperty($out, '_list', $list);
        $this->setProperty($out, '_delimiter', $delimiter);


        $out->main();
    }

    /**
     * @covers ParallelTargetTask::main
     *
     * @dataProvider dataProviderForTestMainWithFailures
     *
     * @param string $list
     */
    public function testMainWithFailures($list)
    {
        $target = 'test';
        $delimiter = ',';

        $expectedKeys = explode($delimiter, $list);

        $expectedExceptionMessage = count($expectedKeys) . '/' . count($expectedKeys) . ' tasks failed';


        $project      = new Project();
        $owningTarget = new Target();
        $location     = new Location();


        $i = 0;
        $propertyTaskMocks = array();
        foreach ($expectedKeys as $key) {
            $propertyTaskMock = $this->getMock('PropertyTask', array('setName', 'setValue'));
            $propertyTaskMock->expects($this->once())
                ->method('setName')
                ->with($this->equalTo('key'))
                ->will($this->returnValue(null));
            $propertyTaskMock->expects($this->once())
                ->method('setValue')
                ->with($this->equalTo($expectedKeys[$i]))
                ->will($this->returnValue(null));

            $propertyTaskMocks[] = $propertyTaskMock;
            $i++;
        }

        $i = 0;
        $taskMocks = array();
        foreach ($expectedKeys as $key) {
            $taskMock = $this->getMock('PhingCallTask',
                array('setTarget', 'setProject', 'setOwningTarget', 'setLocation', 'createProperty'));
            $taskMock->expects($this->once())
                ->method('setTarget')
                ->with($this->equalTo($target))
                ->will($this->returnValue(null));
            $taskMock->expects($this->once())
                ->method('setProject')
                ->with($this->equalTo($project))
                ->will($this->returnValue(null));
            $taskMock->expects($this->once())
                ->method('setOwningTarget')
                ->with($this->equalTo($owningTarget))
                ->will($this->returnValue(null));
            $taskMock->expects($this->once())
                ->method('setLocation')
                ->with($this->equalTo($location))
                ->will($this->returnValue(null));
            $taskMock->expects($this->once())
                ->method('createProperty')
                ->will($this->returnValue($propertyTaskMocks[$i]));

            $taskMocks[] = $taskMock;
            $i++;
        }

        $workerMocks = array();
        foreach ($expectedKeys as $key) {

            /* create the mock object bypassing execution of the constructor of the original class */
            $workerMock = $this->_getMock('DocBlox_Parallel_Worker', array('getError'));
            $workerMock->expects($this->once())
                ->method('getError')
                ->will($this->returnValue(true));

            $workerMocks[] = $workerMock;
        }

        /* create the mock object bypassing execution of the constructor of the original class */
        $managerMock = $this->_getMock('DocBlox_Parallel_Manager', array('execute'));
        $managerMock->expects($this->once())
            ->method('execute')
            ->will($this->returnValue(null));


        $out = $this->getMock(self::$_cut,
            array('_getTask', '_getWorker', 'getProject', 'getOwningTarget', 'getLocation'));

        $i = 0;
        foreach ($expectedKeys as $key) {
            $out->expects($this->at(0 + $i * 5))
                ->method('_getTask')
                ->will($this->returnValue($taskMocks[$i]));
            $i++;
        }

        $out->expects($this->exactly(count($expectedKeys)))
            ->method('getProject')
            ->will($this->returnValue($project));
        $out->expects($this->exactly(count($expectedKeys)))
            ->method('getOwningTarget')
            ->will($this->returnValue($owningTarget));
        $out->expects($this->exactly(count($expectedKeys)))
            ->method('getLocation')
            ->will($this->returnValue($location));

        $i = 0;
        foreach ($expectedKeys as $key) {

            $expectedWorkerCallback = array($taskMocks[$i], 'perform');

            $out->expects($this->at(4 + $i * 5))
                ->method('_getWorker')
                ->with($this->equalTo($expectedWorkerCallback))
                ->will($this->returnValue($workerMocks[$i]));
            $i++;
        }

        $this->setProperty($out, '_manager', $managerMock);
        $this->setProperty($out, '_target', $target);
        $this->setProperty($out, '_list', $list);
        $this->setProperty($out, '_delimiter', $delimiter);


        $this->setExpectedException('BuildException', $expectedExceptionMessage);


        $out->main();
    }

    /**
     * data provider for the test method testMainWithFailures()
     * @return array
     */
    public function dataProviderForTestMainWithFailures()
    {
        return array(
            /* data set #0 */
            array(
                'list'      => 'key1',
                'delimiter' => ','
            ),
            /* data set #1 */
            array(
                'list'      => 'key1,key2',
                'delimiter' => ','
            ),
            /* data set #2 */
            array(
                'list'      => 'key1,key2',
                'delimiter' => ';'
            )
        );
    }

    /**
     * @covers ParallelTargetTask::_getTask
     */
    public function testGetTask()
    {
        $out = new self::$_cut();

        $result = $this->invoke($out, '_getTask');

        $this->assertInstanceOf('PhingCallTask', $result);
    }
}