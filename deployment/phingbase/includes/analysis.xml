<?xml version="1.0" encoding="UTF-8"?>
<!-- 
/**
 * Unister Deployment-Framework
 * 
 * Analysis-Targets
 *
 * pear channel-discover pear.unister.lan
 * pear install unister/qatools
 *
 * @category   deployment
 * @package    analysis
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Roman Lasinski <roman.lasinski@unister-gmbh.de>
 * @version    $Id$
 */
 -->
 
<project name="analysis targets">

    <!-- Default Properties -->
    <property name="project.php.include" value="**/*.php" />
    <property name="project.php.ignore" value="vendor,deployment" />
    <property name="project.php.exclude" value="vendor/**,deployment/**" />
    <property name="project.php.extensions" value="php phtml" />
    
    <property name="project.css.include" value="*/**.css" />
    <property name="project.css.ignore" value="" />
    <property name="project.css.exclude" value="" />
    <property name="project.css.extensions" value="css" />
    
    <property name="project.encoding" value="utf-8" />
    
    <property name="codesniffer.php.standard" value="UnisterPHP" />
    <property name="codesniffer.css.standard" value="UnisterCSS" />
    
    <property name="phpdoc.title" value="API Documentation" />

    <property name="compile.logfile" value="${project.reportdir}/compile.log" />

    
    <!-- PUBLIC TARGETS -->
    <target name="compile" depends="phplint"
        description="Syntax Überprüfung" />
        
    <target name="build-qa" depends="phpmd, phpdepend, phpcodesniffer, apidoc"
        description="Statische Analyse durchführen" />

    <target name="phplint" depends="_prepare-build" description="PHP Lint">
        <phplint level="debug" tofile="${compile.logfile}" haltonfailure="true">
            <fileset dir="${destination}"
                includes="${project.php.include}"
                excludes="${project.php.exclude}"
            />
        </phplint>
    </target>
    
    <target name="phpmd" depends="_create-build" description="PHP Mess Detector">
        <phpmd 
            ignorePatterns="${project.php.ignore}"
            allowedFileExtensions="${project.php.extensions}">
            <formatter type="xml" outfile="${project.reportdir}/pmd.xml"/>
            <fileset dir="${destination}"
                includes="${project.php.include}"
                excludes="${project.php.exclude}"
            />
        </phpmd>
    </target>
        
    <target name="phpdepend" depends="_create-build" description="PHP Depend">
        <phpdepend excludeDirectories="${project.php.ignore}">
            <logger type="phpunit-xml" outfile="${project.reportdir}/pdepend.xml"/>
            <logger type="jdepend-chart" outfile="${project.reportdir}/jdepend.svg"/>
            <logger type="overview-pyramid" outfile="${project.reportdir}/pyramid.svg"/>
            <logger type="summary-xml" outfile="${project.reportdir}/summary.xml"/>
            <fileset dir="${destination}"
                includes="${project.php.include}"
                excludes="${project.php.exclude}"
            />
        </phpdepend>
    </target>
        
    <target name="phpcodesniffer" depends="_create-build" description="Coding Standard - PHP">
        <phpcodesniffer
            ignorePatterns="${project.php.ignore}"
            allowedFileExtensions="${project.php.extensions}"
            standard="${codesniffer.php.standard}"
            format="checkstyle"
            showSniffs="true"
            showWarnings="true">
            <formatter type="checkstyle" outfile="${project.reportdir}/codesniffer.xml" />
            <fileset dir="${destination}"
                includes="${project.php.include}"
                excludes="${project.php.exclude}"
            />
        </phpcodesniffer>
    </target>
    
    <target name="phpcodesniffer-css" depends="_create-build" description="Coding Standard - CSS">
        <phpcodesniffer
            ignorePatterns="${project.css.ignore}"
            allowedFileExtensions="${project.css.extensions}"
            standard="${codesniffer.css.standard}"
            format="checkstyle"
            showSniffs="true"
            showWarnings="true"
            encoding="${project.encoding}">
            <formatter type="checkstyle" outfile="${project.reportdir}/codesniffer-css.xml" />
            <fileset dir="${destination}"
                includes="${project.css.include}"
                excludes="${project.css.exclude}"
            />            
        </phpcodesniffer>
    </target>
    
    <target name="phpcpd" depends="_create-build" description="Copy/Paste Detector (CPD) for PHP code">
    <phpcpd allowedFileExtensions="${project.php.extensions}">
        <fileset dir="${destination}"
             includes="${project.php.include}"
             excludes="${project.php.exclude}"
        />
        <formatter type="pmd" outfile="${project.reportdir}/php-cpd.xml" />
        </phpcpd>
    </target>
    
    <target name="imageoptimizer" depends="_create-build" description="ImageOptimizer">
        <exec 
            command="imageOptimizer --report-xml=${project.reportdir}/imageopt.xml --report-html=${project.reportdir}/imageopt ${destination}"
            passthru="true" /> 
    </target>
    
    <target name="apidoc" depends="_create-build" description="API Dokumentation erstellen">
        <phpdoc2 title="${phpdoc.title}"
            destdir="${project.docdir}">
            <fileset dir="${destination}"
                includes="${project.php.include}"
                excludes="${project.php.exclude}"
            />
        </phpdoc2>
    </target>
</project>
