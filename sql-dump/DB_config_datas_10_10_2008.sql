[portale]
INSERT INTO config_domains
(`Name`,`UserName`,`Password`,`From`,`Reply`)
VALUES
('schuelerprofile.de','community@schuelerprofile.de','community','community@schuelerprofile.de','community@schuelerprofile.de'),
('preisvergleich.de','community@preisvergleich.de','community','news@preisvergleich.de','news@preisvergleich.de'),
('uk.best-price.com','community@uk.best-price.com','community','community@uk.best-price.com','community@uk.best-price.com'),
('test-community.it','community@test-community.com','community','community@test-community.it','community@test-community.it'),
('test-community.jp','community@test-community.com','community','community@test-community.jp','community@test-community.jp'),
('test-community.ca','community@test-community.com','community','community@test-community.ca','community@test-community.ca'),
('test-community.ru','community@test-community.com','community','community@test-community.ru','community@test-community.ru'),
('test-community.es','community@test-community.com','community','community@test-community.es','community@test-community.es'),
('test-community.de','community@test-community.com','community','community@test-community.de','community@test-community.de'),
('unister.de','community@community-unister.de','community','info@community-unister.de','info@community-unister.de'),
('reisen.de','community@reisen.de','pF7kh9s','community@reisen.de','community@reisen.de'),
('hotelreservierung.de','service@hotelreservierung.de','iccy9mI5','service@hotelreservierung.de','service@hotelreservierung.de'),
('auto.de','community@auto.de','f7kb29H','community@auto.de','community@auto.de'),
('geld.de','service@geld.de','service','service@geld.de','service@geld.de'),
('fluege.de','community@reisen.de','pF7kh9s','community@reisen.de','community@reisen.de'),
('news_de','community@news.de','community','community@news.de','community@news.de'),
('cr.reisen.de','community@reisen.de','pF7kh9s','community@reisen.de','community@reisen.de'),
('service_fluege.de','service@fluege.de','bhD7dj2','service@fluege.de','service@fluege.de')

[mailservers]
INSERT INTO config_mailservers
(`Name`,`Address`,`Type`)
VALUES
('Mail1','192.168.198.150','HIGHPRIO'),
('Mail2','87.118.100.220','NORMAL'),
('Mail3','62.141.50.74','NORMAL')

[portale - mailserverusing]
INSERT INTO config_domain_mailserver
(SELECT DomainId, 2 AS Mailserver, 100 AS Weight FROM config_domains);

INSERT INTO config_domain_mailserver
(SELECT DomainId, 3 AS Mailserver, 100 AS Weight FROM config_domains)

INSERT INTO config_domain_mailserver
(SELECT DomainId, 1 AS Mailserver, 0 AS Weight FROM config_domains)