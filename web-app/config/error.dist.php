<?php

error_reporting(2047);

function ErrorHandlerFun($ErrNo, $ErrStr, $ErrFile, $ErrLine, $ErrVars)
{
	$ErrorName = "E_UNKNOWN";
	$ErrorNames = array(
		E_ERROR           => "E_ERROR",
		E_WARNING         => "E_WARNING",
		E_PARSE           => "E_PARSE",
		E_NOTICE          => "E_NOTICE",
		E_CORE_ERROR      => "E_CORE_ERROR",
		E_CORE_WARNING    => "E_CORE_WARNING",
		E_COMPILE_ERROR   => "E_COMPILE_ERROR",
		E_COMPILE_WARNING => "E_COMPILE_WARNING",
		E_USER_ERROR      => "E_USER_ERROR",
		E_USER_WARNING    => "E_USER_WARNING",
		E_USER_NOTICE     => "E_USER_NOTICE",
		E_STRICT          => "E_STRICT"
	);
	if (isset($ErrorNames[$ErrNo]))
		$ErrorName = $ErrorNames[$ErrNo];
	switch ($ErrNo)
	{
		case E_NOTICE      :
		case E_STRICT      :
		case E_WARNING     :
		case E_USER_NOTICE : break;

		default            :
		{
			ob_start();
			debug_print_backtrace();
			$out1 = ob_get_contents();
			ob_end_clean();

			$ErrorContent = "$ErrorName ($ErrNo) in \"$ErrFile\" at line $ErrLine. [$ErrStr]" . PHP_EOL ; // . $out1;

			if (!empty($ErrVars["file"]))
			{
				$ErrorContent .= " template: " . $ErrVars["file"];
			}

			{
				// Fehlermeldungen loggen
				/* (2011-07-04) Konstante LOG_DIR existiert nicht, daher absolute Pfadangabe...
				ini_set('error_log', LOG_DIR.'logger_'.date('Y-m-d').'.log');
				*/
				ini_set('error_log', '${application.vhost}/log/logger_' . date('Y-m-d') . '.log');
				@error_log($ErrorContent, 0);
			}
		}
	}
}

set_error_handler("ErrorHandlerFun");