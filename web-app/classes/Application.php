<?php

class Application {

	private $ControllerDirectory = array ();
	private $tree = array ();

	/**
	 * @param array $dir
	 */

	public function __construct ($dir = null)
	{
		if (isset($dir))
			$this->ControllerDirectory = $dir;
	}

	/**
	 * retrieves Modules Controller & Actions from given Directory
	 */

	private function _getResourcesFromDirectoryTree ()
	{
		if (!is_array ($this->ControllerDirectory))
			return false;

		foreach ($this->ControllerDirectory as $module => $dir)
		{
			try { $directory = new DirectoryIterator ($dir); }
				catch (Exception $e) { continue; }

			foreach ($directory as $file)
				if ($file->isFile () && $file->isReadable ())
					$this->_getActionsByFile ($file, $module);
		}
	}

	/**
	 * get Actions from File
	 *
	 * @param SplFileInfo $file
	 * @return bool
	 */

	private function _getActionsByFile (SplFileInfo $file, $module = 'default')
	{
		if (false === ($offset = strrpos($fileName = $file->getFilename(), '.')))
			return false;

		$className = @substr($fileName, 0, $offset);
		if (false === class_exists($className))
			try { Zend_Loader::loadClass($className, $file->getPath()); }
				catch (Exception $e) { return false; }

		if (false === ($offset = strrpos($className, 'Controller')))
			return false;

		$controller = @substr($className, 0, $offset);
		foreach (get_class_methods ($className) as $method)
			if (false !== ($offset = strrpos($method, 'Action')))
				$this->tree[$module][$controller][] = @substr($method, 0, $offset);

		return true;
	}


	/**
	 * get Resource Tree
	 *
	 * @param string $dir
	 * @return array
	 */

	public function getResourceTree ($dir = null)
	{
		if (isset($dir))
			$this->ControllerDirectory = $dir;

		if (0 == sizeof($this->tree))
			$this->_getResourcesFromDirectoryTree ();

		return $this->tree;
	}
	/**
	 * Updates Controller and Action to the database
	 */
	public function updateRessources()
	{
		$tree = $this->getResourceTree();
		
		
		foreach($tree['default'] as $controller => $actions){
			$controller = strtolower($controller);
			
			$sql = "INSERT INTO admin_ressource SET Ressource = '$controller'";
			try {
				Zend_registry::get('_DB')->query($sql);
			} catch (Exception $e) {
				//Eintrag schon da
			}
			$actionlist = '';
			
			foreach($actions as $action){
				$actionlist .= strtolower($action).',';	
			}
			$actionlist = substr($actionlist,0,-1);
			
			
			
			$sql = "UPDATE admin_ressource SET Action = '$actionlist' WHERE Ressource = '$controller'";
			try {
				Zend_registry::get('_DB')->query($sql);
			} catch (Exception $e) {
				//Eintrag schon da
			}
			
			
		}
	
	}
	
	
}
