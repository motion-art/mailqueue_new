#!/bin/sh
proc=`ps -Alf | grep "/cron/make_stats.php"`
anz=`echo -e "$proc" | grep -c "${application.vhost}/httpdocs"`
if test $anz -eq 0 
then
    /usr/bin/php -f ${application.vhost}/httpdocs/cron/make_stats.php &
fi
exit
