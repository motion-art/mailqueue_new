<?php

/**
 * general config settings
 */
include('cron_config.php');

$attachmentdir = dirname(__FILE__)."/";

# run
$MailQueue = new class_MailQueue();

$myattachments = array	(
							/**
							 * all fileinfos are known
							 */
							0 => array	(
											'AttachmentContent'	=> file_get_contents($attachmentdir."homer.jpg"),
											'AttachmentType'	=>'image/jpeg',
											'AttachmentFile'	=>'homer.jpg',
										),
							/**
							 * use mailqueue function to get fileinfos
							 */
							1 => $MailQueue->getAttachmentParamsFromFile($attachmentdir."ba_liest.gif"),
						);
						
$myattachments[] = $MailQueue->getAttachmentParamsFromFile($attachmentdir."Urlaubsantrag.xls");

$saved_attachments = $MailQueue->saveAttachments($myattachments);
$Params = array(
					'MemberTo_Id'		=>'19',
		 			'MemberFrom_Id'		=>'38',
		 			'Category'			=>'TEST',
		 			'Type'				=>'MAIL',
		 			'Subject'			=>'das ist eine testmail',
		 			'Text'				=>'das ist nur eine test mail also nur die ruhe',
		 			'Html'				=>'das ist nur eine <strong>test</strong> mail, also nur die <h2>ruhe</h2>',
		 			'Email'				=>'soeren.pestner@unister-gmbh.de',
		 			'FromName'			=>'Der Tester',
		 			'FromEmail'			=>'tester@unister-gmbh.de',
		 			'ReplyTo'			=>'lopotm@gmx.de',
		 			#'CC'				=>array('lopotm@gmx.de','lopotm@web.de','sof@müller.de'),
		 			#'BCC'				=>array('lopotm@gmx.de','lopotm@web.de','sof@müller.de'),
		 			#'Prio'				=>'1',
		 			'Attachment'		=>$saved_attachments,
		 			'Portal'			=>'schuelerprofile.de',
		 			'MailType'			=>'instant',
			 	);
$result = $MailQueue->sendMail($Params);
print_r("<pre>");
print_r($result);
print_r("</pre>");
exit;
