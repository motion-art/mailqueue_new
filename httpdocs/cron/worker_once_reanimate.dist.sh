#!/bin/sh
proc=`ps -Alf | grep "/cron/worker.php $1"`
anz=`echo -e "$proc" | grep -c "${application.vhost}/httpdocs"`
if test $anz -eq 0 
then
    curdate=`date -I`
    `/usr/bin/php -f ${application.vhost}/httpdocs/cron/worker.php $1 >> ${application.vhost}/log/mailerDebug$1-$curdate.log &`
fi
exit