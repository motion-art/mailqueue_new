<?php

/**
 * general config settings
 */
$cron_dir = dirname(__FILE__)."/";
include($cron_dir.'cron_config.php');

# run
$MailErrorCorrection = new class_MailErrorCorrection();

$result = $MailErrorCorrection->start();

# shutdown
exit;
