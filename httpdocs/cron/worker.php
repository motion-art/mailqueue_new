<?php
ini_set('memory_limit', '1024M');

/**
 * a mail mod have to be set
 */
if (isset($_GET['mod'])) {
    $_SERVER['argv'][1] = $_GET['mod'];
}
if (!isset($_SERVER['argv'][1])) {
    trigger_error('No Mod set', E_USER_ERROR);
} else {
    $mod = $_SERVER['argv'][1];
}

/**
 * general config settings
 */
$cronDir = dirname(__FILE__) . '/';
include($cronDir . 'cron_config.php');

// run
$mailObj = new class_SendMail();
$mailObj->_makeCronPortalConfig();
$mailObj->runSendMailCron($mod);

// shutdown
exit;
