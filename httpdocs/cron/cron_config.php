<?php

$cron_dir = dirname(__FILE__)."/";

ini_set("memory_limit", "256M");
/**
 * local
 */
$_SERVER['HTTP_HOST']  = "es-01";
include($cron_dir.'../init.php');


# db
$dbArray = Zend_Registry::get('_CONFIG')->database->toArray();
$dbArray['profiler'] = ("1" == $dbArray['profiler']);
$dbArray['options']['caseFolding'] = Zend_Db::CASE_LOWER;

/* (2011-07-04) PDO::MYSQL_ATTR_MAX_BUFFER_SIZE existiert nicht...
$dbArray['driver_options'][PDO::MYSQL_ATTR_MAX_BUFFER_SIZE] = 1024*1024*50;
*/

Zend_Registry::set('_DB', Zend_Db::factory(Zend_Registry::get('_CONFIG')->database->type, $dbArray));
Zend_Db_Table::setDefaultAdapter (Zend_Registry::get ('_DB'));
Zend_Registry::get ('_DB')->query ("SET NAMES utf8");

# init log
$writer = new Zend_Log_Writer_Stream(LOG_DIR.'cron_email_exception_logger_'.date('Y-m-d').'.log');
$writer->setFormatter(new Zend_Log_Formatter_Simple());
Zend_Registry::set ('_LOGGER'	, new Zend_Log ($writer));

