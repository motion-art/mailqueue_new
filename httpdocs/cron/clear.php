<?php
ini_set("memory_limit","512M");

$cron_dir = dirname(__FILE__)."/";
include($cron_dir . 'cron_config.php');

# run
$test = new Unister_Classes_Class_Clear();
$test->clearAttachments();
$test->clearMailDeleteBounce();

# shutdown
exit;
