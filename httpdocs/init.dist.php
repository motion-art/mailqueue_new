<?php

/**
 * @category  Application
 * @package   midoffice
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   v 1.0
 * @copyright Copyright (c) 2006-2008, Unister GmbH
 * @license   this code is property of company Unister GmbH and unterlays internal rules of company.
 * @link      http://rb-01/midoffice
 */


/**
 * Config Settings
 */
ini_set('memory_limit', '1024M');

date_default_timezone_set('Europe/Berlin');
define('LANGUAGE', 'GERMAN');
define('CHARSET', 'utf-8');
@setlocale(LC_TIME, 'de_DE.utf8');

// Sets run-mode and config_path to get application enviroment.
require_once 'runmode.php';

require_once ( CONFIG_DIR . 'error.php' );

if (!defined('RUN_MODE')) {
    error_log('ENVIRONMENT ERROR: constant run_mode not defined! ', 0);
    exit;
}

/**
 * crucial! - path for autoloader ( Zend_Loader );
 */
if (defined('LIB_DIR')) {
    ini_set('include_path', ini_get('include_path') . PATH_SEPARATOR . LIB_DIR);
} else {
    error_log('ENVIRONMENT ERROR: constant LIB_DIR not defined!', 0);
    exit;
}

if (file_exists(LIB_DIR . 'Zend/Loader.php')) {
    include 'Zend/Loader.php';
    Zend_Loader::registerAutoload();
} else {
    error_log('ENVIRONMENT ERROR: Zend_Loader not found! ', 0);
    exit;
}

if (!file_exists(CONFIG_DIR . 'config.ini')) {
    error_log('ENVIRONMENT ERROR: global config file not found : ' . CONFIG_DIR . 'config.ini', 0);
    exit;
}

$config    = new Zend_Config_Ini( CONFIG_DIR . 'config.ini', null, true );
Zend_Registry::set('_CONFIG', $config);

define('MAINTENANCE_MODE', $config->default->MAINTENANCE_MODE);

/**
 * Define PATH related constants taken form environment specific configuration
 */
define('HOME_DIR', dirname(__FILE__) . '/');

define('APP_DIR', HOME_DIR . $config->paths->app_dir);
define('LOG_DIR', '${application.vhost}/log/');
define('CACHE_DIR', HOME_DIR . $config->paths->cache_dir);
define('CLASS_DIR', APP_DIR . $config->loader->class_dir);
define('CONTROLLER_DIR', APP_DIR . $config->loader->controller_dir);

define('_DB', $config->registry->database_master);
define('_MAILDB', $config->registry->database_mail);

/**
 * Define SESSION related constants taken form environment specific configuration
 */
define('HTTP_HOST', $config->session->http_host);
define('DEEP_LINK_COOKIE', $config->session->deep_link_cookie);
define('SESSION_COOKIE_NAME', $config->session->cookie_name);
define('SESSION_LIFETIME', $config->session->cookie_lifetime);
define('SESSION_COOKIE_DOMAIN', $config->session->cookie_domain);

define('PERMANENT_SESSION_COOKIE_NAME', $config->session->permanent_cookie_name);
define('PERMANENT_SESSION_COOKIE_EXPIRY', $config->session->permanent_cookie_expirity);
define('PERMANENT_SESSION_COOKIE_KEY', $config->session->permanent_cookie_key);

/**
 * Important!!! - setting path for autoloader ( Zend_Loader ) to search classes;
 */
ini_set(
    'include_path',
    ini_get('include_path') . PATH_SEPARATOR . CLASS_DIR . PATH_SEPARATOR . CONTROLLER_DIR
);

/**
 * Define URL related settings taken form environment specific configuration
 */
function url_or_relative($strConfigUrl)
{
    if (substr($strConfigUrl, 0, 7) == 'http://') {
        return $strConfigUrl;
    }
    return $strUrl = 'http://' . $_SERVER['HTTP_HOST'] . '/' . $strConfigUrl;
}

define('HOME_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/' . $config->urls->home_url);
define('BASE_URL', $config->urls->base_url);
define('AJAX_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/' . $config->urls->ajax_url);
define('PICTURE_URL', url_or_relative($config->urls->picture_url));
define('STATIC_URL', url_or_relative($config->urls->static_url));
define('UPLOAD_URL', url_or_relative($config->urls->upload_url));