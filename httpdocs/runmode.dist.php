<?php

/**
 * @category  Application
 * @package   midoffice
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   v 1.0
 * @copyright Copyright (c) 2006-2008, Unister GmbH
 * @license   this code is property of company Unister GmbH and unterlays internal rules of company.
 * @link      http://rb-01/midoffice
 */



/**
 * Enviroment loader configuration
 *
 * sets run-mode and path to configuration dir.
 * Possible run-modes:
 *   online - base mode
 *   local  - development mode
 * X test   - ToDo: implement configuration for testing environment
 * envirnment configuration classes are in CONFIG_DIR and names "< RUN_MODE > . php"
 *
 * @category  Application
 * @package   midoffice
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   v 1.0
 * @copyright Copyright (c) 2006-2008, Unister GmbH
 * @license   this code is property of company Unister GmbH and unterlays internal rules of company.
 * @link      http://rb-01/midoffice
 */

define ( 'RUN_MODE', '${application.runmode}' );
// path to config dir. Needed to load environment configuration.
#if(!getenv('DOCUMENT_ROOT')){
define ( 'CONFIG_DIR', dirname(__FILE__) . '/../web-app/config/' );
define ( 'LIB_DIR',    dirname(__FILE__) . '/../web-lib/'        );
#} else {
#    define ( 'CONFIG_DIR', getenv('DOCUMENT_ROOT') . '/../projects/mailqueue_new/web-app/config/' );
#    define ( 'LIB_DIR',    getenv('DOCUMENT_ROOT') . '/../projects/mailqueue_new/web-lib/' 		  );
#}