function renderGoogle(google_ads, container, linkedLabel, label) {

    var google_num_ads = google_ads.length;

    if (google_num_ads <= 0) {
        return;
    }

    var ads     = '';

    for (var i = 0; i < google_num_ads; i++) {

        var last = ((i+1) == google_num_ads);
        var first = i == 0;

        ads = ads + renderGoogleAd(google_ads[i], last, first);
    }

    if (ads != '') {

        if (linkedLabel) {
           adBlock = '<span class="gads_label_new"><a href="https://www.google.com/adsense/support/bin/request.py?contact=abg_afc&gl=US&hideleadgen=1" class="adLabelLink">' + label + '</a></span><div class="gads_ads_new"><ul>' + ads + '</ul></div>';

        } else {
               adBlock = '<span class="gads_label_new">' + label + '</span><div class="gads_ads_new"><ul>' + ads + '</ul></div>';
        }

        if (document.getElementById(container)) {
            document.getElementById(container).innerHTML = adBlock;
        }
    }

    return;
}

function renderGoogleAd(google_ad, last, first) {

    var attrib = '';
    if(first) attrib = 'gads_li_first_new"';
    if (last) attrib = 'gads_li_last_new';
    if(google_ad.type.search(/.*text.*/) != -1) {
        var ad =  '<li class="gads_li_new_text ' + attrib +  '">';

        if (google_ad.line3) {
            google_ad.line2 = google_ad.line2 + ' ' + google_ad.line3;
        }
        ad +=  '<a class="gads_a_new" target="_blank" rel="nofollow" href="' + google_ad.url + '" onmousedown="logclick(\'google\');">';
        ad +=  '<span class="gads_title_new">' + google_ad.line1 + '</span>';
        ad +=  '<span class="gads_text_new">' + google_ad.line2 + '</span>';
        ad +=  '<span class="gads_link_new">' + google_ad.visible_url + '</span>';
        ad +=  '</a>';
        ad +=  '</li>';
    } else if (google_ad.type.search(/.*flash.*/) != -1) {
        ad ='<li class="gads_li_new_flash"><a target="_blank" onmousedown="logclick(\'google\');" rel="nofollow" href="' + google_ad.url +  '"><object height="' + google_ad.image_height + '" width="' + google_ad.image_width + '"><embed src="' + google_ad.image_url + '" type="application/x-shockwave-flash" allowscriptaccess="always" width="' + google_ad.image_width +  '" height="' + google_ad.image_height + '"></embed></object></a></li>';
    } else if ( google_ad.type.search(/.*image.*/) != -1) {
        ad = '<li class="gads_li_new_image"><a target="_blank" onmousedown="logclick(\'google\');" rel="nofollow" href="' + google_ad.url +  '"><img height="' + google_ad.image_height + '" width="' + google_ad.image_width + '" src="' + google_ad.image_url + '"/></a></li>';
    }

    return ad;
}
