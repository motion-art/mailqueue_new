<?php

/**
 * Client Interface
 *
 * @category  Application
 * @package   community base
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   v 2.0
 * @copyright Copyright (c) 2006-2008, Unister GmbH
 * @license   this code is property of company Unister GmbH and unterlays internal rules of company.
 * @link      http://www.unister-gmbh.de
 */

defined('APPLICATION_PATH')
    || define(
        'APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application')
);

defined('APPLICATION_ENV')
    || define('APPLICATION_ENV',
              (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV')
                    : (file_exists('runmode.php') ? include('runmode.php')
                        : 'local')
                        )
                    );

set_include_path(implode(PATH_SEPARATOR, array(
    dirname(dirname(__FILE__)) . '/library',
    get_include_path(),
)));

require_once 'Zend/Application.php';

try {

    $application = new Zend_Application(
        APPLICATION_ENV,
        APPLICATION_PATH . '/configs/application.ini'
    );

    $application->bootstrap()
                ->run();

} catch (Exception $e) {

    if (APPLICATION_ENV == 'local') {
        Zend_Debug::dump($e);
    } else {
        error_log($e->getMessage() . $e->getTraceAsString());
    }
}