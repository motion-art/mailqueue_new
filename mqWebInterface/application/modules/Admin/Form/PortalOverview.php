<?php
/**
 * Form to display an overview
 *
 * @category   mailqueue
 * @package    mqWebInterface
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Nico Schuetze <nico.schuetze@unister-gmbh.de>
 * @version    $Id:$
 */

class Admin_Form_PortalOverview extends Zend_Form
{
    protected $_startDate;
    protected $_endDate;
    protected $_portals;
    protected $_export;

    /**
     *
     * constructor to add required data to form
     * @param string $startDate
     * @param string $endDate
     * @param string $portals
     */
    public function __construct($startDate, $endDate, $portals, $export = false)
    {
        $this->_startDate = $startDate;
        $this->_endDate = $endDate;
        $this->_portals = $portals;
        $this->_export = $export;
        parent::__construct();
    }

    /**
     *
     * @return string selected portal
     */
    public function getSelectedPortal()
    {
        return $this->getElement('portal')->getValue();
    }

    public function setStartDate($startDate)
    {
        $this->_startDate = $startDate;
        return $this;
    }

    public function getStartDate()
    {
        return $this->_startDate;
    }

    public function setEndDate($endDate)
    {
        $this->_endDate = $endDate;
        return $this;
    }

    public function getEndDate()
    {
        return $this->_endDate;
    }


    /**
     *
     * add elements to form
     *
     */
    public function init()
    {
        $this->setMethod('post');

        $element = $this->createElement('text', 'startDate',
                                                    array('label' => 'StartDatum',
                                                          'attribs' => array('id' => 'input_startDate', ), ));

        $element->setDecorators(array(
                    'viewHelper',
                    'Errors',
                    'Description',
                    array('HtmlTag', array('tag' => 'div'))
                    ))
                ->setValue($this->_startDate);

        $this->addElement($element);

        $element = $this->createElement('text', 'endDate',
                                                    array('label' => 'EndDatum',
                                                        'attribs' => array('id' => 'input_endDate', ), ));

        $element->setDecorators(array(
                    'viewHelper',
                    'Errors',
                    'Description',
                    array('HtmlTag', array('tag' => 'div'))
                    ))
                ->setValue($this->_endDate);

        $this->addElement($element);

        $element = $this->createElement('select', 'portal',
                                                    array('label' => 'Portal',
                                                          'attribs' => array('id' => 'select_portal', ), ));

        $element->setDecorators(array(
                    'viewHelper',
                    'Errors',
                    'Description',
                    array('HtmlTag', array('tag' => 'div'))
                    ));

        foreach ($this->_portals as $portal) {
            $element->addMultioption($portal['portal'], $portal['portal']);
        }
        $this->addElement($element);

        if ($this->_export) {
            $buttontext = 'Exportieren';
        } else {
            $buttontext = 'Anzeigen';
        }
        $button = $this->createElement('submit', 'Anzeigen', array(
            'ignore' => true,
            'label'  => $buttontext
        ));
        $this->addElement($button);

    }
}
