<?php

class Admin_Form_Login extends Zend_Form
{

    public function init()
    {
        $this->setName('login');
        $this->setMethod('post');

        $validateStringLength = new Zend_Validate_StringLength(
            array(
                'min' => 0,
                'max' => 50
            )
        );
        $validateStringLength
            ->setMessage(
                'Ungültige Zeichenkette eingegeben.',
                Zend_Validate_StringLength::INVALID
            )
            ->setMessage(
                'Zeichenkette zu lang.',
                Zend_Validate_StringLength::TOO_LONG
            );

        $validateNotEmpty = new Zend_Validate_NotEmpty();
        $validateNotEmpty
            ->setMessage(
                'Pflichtfeld, muss ausgefüllt sein.'
            );

        $this->addElement(
            'text', 'username', array(
                'filters'    => array('StringTrim', 'StringToLower'),
                'validators' => array($validateStringLength, $validateNotEmpty),
                'required'   => true,
                'label'      => 'Nutzer:',
            )
        );

        $this->addElement(
         'password', 'password', array(
                'filters'    => array('StringTrim'),
                'validators' => array($validateStringLength, $validateNotEmpty),
                'required'   => true,
                'label'      => 'Passwort:',
            )
        );

        $this->addElement(
            'submit', 'login', array(
                'required' => false,
                'ignore'   => true,
                'label'    => 'Login',
            )
        );
    }

    public function getUsername()
    {
        return $this->getElement('username')->getValue();
    }

    public function getPassword()
    {
        return $this->getElement('password')->getValue();
    }
}