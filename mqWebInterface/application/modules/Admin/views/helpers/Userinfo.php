<?php

class Zend_View_Helper_Userinfo extends Zend_View_Helper_Abstract
{
    public function userinfo($username, $role = null)
    {
        $url = $this->view->url(
            array(
                'controller' => 'auth',
                'action' => 'logout'
            )
        );
        $output .= '<div class="loginInfo">' . $username;
        if ($role) {
            $output .= ' (' . $role . ')';
        } 
        //$output .= '<br><a href="' . $url . '">logout</a></div>';
        $output .= '<br><a href="' . $url . '"><img src=\'';
        $output .= $this->view->baseUrl('img', 'global', 'logout.png');
        $output .= '\' alt=\'logout\' title=\'logout\'></a></div>';
        return $output;
    }
}