<?php

class Admin_View_Helper_BounceTable extends Zend_View_Helper_Abstract
{
    public function bounceTable($data)
    {
        foreach ($data as $value) {
            echo '<tr><td>' . $value->getChanged() . '</td><td>' . $value->getTyp()
              . '</td><td>' . $value->getCat() . '</td><td>' . $value->getPortal()
              . '</td><td>' . $value->getEmail() . '</td><td>' . $value->getInc() . '</td></tr>' . PHP_EOL;
        }
    }
}
