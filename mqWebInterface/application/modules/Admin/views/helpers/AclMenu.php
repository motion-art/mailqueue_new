<?php
class Zend_View_Helper_AclMenu extends Zend_View_Helper_Navigation_Menu
{
    /**
     * View helper entry point:
     * Retrieves helper and optionally sets container to operate on
     *
     * @param  Zend_Navigation_Container $container  [optional] container to
     *                                               operate on
     * @return Zend_View_Helper_Navigation_Menu      fluent interface,
     *                                               returns self
     */
    public function AclMenu(Zend_Navigation_Container $container = null)
    {
        if (null !== $container) {
            $this->setContainer($container);
        }

        return $this;
    }

    /**
     * Renders menu
     *
     * Implements {@link Zend_View_Helper_Navigation_Helper::render()}.
     *
     * If a partial view is registered in the helper, the menu will be rendered
     * using the given partial script. Unlike
     * Zend_View_Helper_Navigation_Menu::render(), pages will be filtered based
     * on visibility and ACL before being passed to the view partial script.
     * If no partial is registered, the menu will be rendered as an 'ul' element
     * by the helper's internal method.
     *
     * @see renderPartial()
     * @see renderMenu()
     *
     * @param  Zend_Navigation_Container $container  [optional] container to
     *                                               render. Default is to
     *                                               render the container
     *                                               registered in the helper.
     * @return string                                helper output
     */
    public function render(Zend_Navigation_Container $container = null)
    {
        if (null === $container) {
            $container = $this->getContainer();
        }
        if ($partial = $this->getPartial()) {
            return $this->__filterRestrictedPages($container)
                        ->renderPartial($container, $partial);
        } else {
            $this->__filterRestrictedPages($container);
            return $this->renderMenu($container);
        }
    }

    /**
     * Filters out invisible and ACL based restricted pages
     *
     * @access protected
     * @param Zend_Navigation_Container $container
     * @return Zend_Navigation_Container
     */
    protected function __filterRestrictedPages(Zend_Navigation_Container $container)
    {
        // Need to clone the container as we will be removing pages from the
        // actual container within the recursive iterator loop. Removing
        // elements from the object we're iterating over *while* iterating will
        // trip up the iterator.
        $iterator = new RecursiveIteratorIterator(clone $container,
                                    RecursiveIteratorIterator::SELF_FIRST);

        foreach ($iterator as $page) {
            /* @var $page Zend_Navigation_Page_Mvc */
            if (!$this->accept($page)) {
                $container->removePage($page);
            }
        }

        return $this;
    }
}
