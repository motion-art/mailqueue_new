<?php

/**
 * maps Mail* model to database
 *
 * @category   mailqueue
 * @package    mqWebInterface
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Nico Schuetze <nico.schuetze@unister-gmbh.de>
 * @version    $Id:$
 */

class Admin_Model_MailMapper
{
    protected $_dbTable;
    private $_instant = false;


    /**
     * Get the DbTable
     * @return Admin_Model_DbTable_Mail
     */
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable();
        }
        return $this->_dbTable;
    }

    public function fetch($conditions)
    {
        $select = $this->getDbTable()->select();
        foreach ($conditions as $col => $value) {
            $select->where($col . ' = ?', $value);
        }
        $stmt = $select->query();

        $mailData = $stmt->fetchAll();

        $mails = array();
        foreach ($mailData as $data) {
            $mail = new Admin_Model_MailAbstract();
            $this->arrayToModel($data, $mail);

            $mails[] = $mail;
        }

        return $mails;
    }

    public function setDbTable()
    {
        $dbTable = new Admin_Model_DbTable_Mail($this->_instant);
        $this->_dbTable = $dbTable;
        return $this;
    }

    public function insert(Admin_Model_MailAbstract $mail)
    {
        $data = array();
        $this->modelToArray($mail, $data);
        $this->insertArray($data);
    }

    public function insertArray($data)
    {
        try {
            if (isset($data['instant'])) {
                $this->_instant = $data['instant'];
                unset($data['instant']);
            }
            return $this->getDbTable()->insert($data);
        } catch(Exception $e) {
            throw new Zend_Soap_Server_Exception($e->getMessage());
        }
    }

    public static function arrayToModel($data, Admin_Model_Mail $mail)
    {
        $mail->setMailId($data['mailId']);
        $mail->setMemberToId($data['memberToId']);
        $mail->setMemberFromId($data['memberFromId']);
        $mail->setCategory($data['category']);
        $mail->setType($data['type']);
        $mail->setSubject($data['subject']);
        $mail->setText($data['text']);
        $mail->setHtml($data['html']);
        $mail->setEmail($data['email']);
        $mail->setTry($data['try']);
        $mail->setFromName($data['fromName']);
        $mail->setFromEmail($data['fromEmail']);
        $mail->setReplyTo($data['replyTo']);
        $mail->setCc($data['cc']);
        $mail->setBcc($data['bcc']);
        $mail->setAttachment($data['attachment']);
        $mail->setMod10($data['mod10']);
        $mail->setPrio($data['prio']);
        $mail->setCharset($data['charset']);
        $mail->setPortal($data['portal']);
    }

    public static function rowToModel(Zend_Db_Table_Row $row, Admin_Model_Mail $mail)
    {
        self::arrayToModel($row->toArray(), $mail);
    }

    public static function modelToArray(Admin_Model_Mail $mail, &$data)
    {
        //$data['MailId'] = $mail->getMailId();
        $memberToId = $mail->getMemberToId();
        $data['MemberTo_Id'] = (isset($memberToId) ? $memberToId : 0);
        $memberFromId = $mail->getMemberFromId();
        $data['MemberFrom_Id'] = (isset($memberFromId) ? $memberFromId : 0);
        $data['Category'] = $mail->getCategory();
        $data['Type'] = $mail->getType();
        $data['Subject'] = $mail->getSubject();
        $data['Text'] = $mail->getText();
        $data['Html'] = $mail->getHtml();
        $data['Email'] = $mail->getEmail();
        $try = $mail->getTry();
        $data['Try'] = (isset($try) ? $try : 0);
        $data['FromName'] = $mail->getFromName();
        $data['FromEmail'] = $mail->getFromEmail();
        $data['ReplyTo'] = $mail->getReplyTo();
        $data['CC'] = $mail->getCc();
        $data['BCC'] = $mail->getBcc();
        $data['Attachment'] = $mail->getAttachment();
        $data['Mod10'] = rand(0, 9);
        $prio = $mail->getPrio();
        $data['Prio'] = (isset($prio) ? $prio : 0);
        $data['Charset'] = $mail->getCharset();
        $data['Portal'] = $mail->getPortal();
        $data['instant'] = $mail->getInstant();
    }

}