<?php

/**
 * Model for mail_instant und mail_compendium
 *
 *
 * @category	mailqueue
 * @package	    mqWebInterface
 * @copyright	Copyright (c) 2011 Unister GmbH
 * @author		Nico Schuetze <nico.schuetze@unister-gmbh.de>
 * @version		$Id:$
*/

class Admin_Model_Mail
{
    protected $_mailId;
    protected $_memberToId;
    protected $_memberFromId;
    protected $_category;
    protected $_type;
    protected $_subject;
    protected $_text;
    protected $_html = '';
    protected $_email;
    protected $_try;
    protected $_fromName;
    protected $_fromEmail;
    protected $_replyTo;
    protected $_cc;
    protected $_bcc;
    protected $_attachment;
    protected $_mod10;
    protected $_prio;
    protected $_charset = 'utf-8';
    protected $_portal;
    protected $_instant = false;


    public function getMailId()
    {
        return $this->_mailId;
    }

    public function setMailId($mailId)
    {
        $this->_mailId = $mailId;
        return $this;
    }

    public function getMemberToId()
    {
        return $this->_memberToId;
    }

    public function setMemberToId($memberToId)
    {
        $this->_memberToId = $memberToId;
        return $this;
    }

    public function getMemberFromId()
    {
        return $this->_memberFromId;
    }

    public function setMemberFromId($memberFromId)
    {
        $this->_memberFromId = $memberFromId;
        return $this;
    }

    public function getCategory()
    {
        return $this->_category;
    }

    public function setCategory($category)
    {
        $this->_category = $category;
        return $this;
    }

    public function getType()
    {
        return $this->_type;
    }

    public function setType($type)
    {
        $this->_type = $type;
        return $this;
    }

    public function getSubject()
    {
        return $this->_subject;
    }

    public function setSubject($subject)
    {
        $this->_subject = $subject;
        return $this;
    }

    public function getText()
    {
        return $this->_text;
    }

    public function setText($text)
    {
        $this->_text = $text;
        return $this;
    }

    public function getHtml()
    {
        return $this->_html;
    }

    public function setHtml($html)
    {
        $this->_html = $html;
        return $this;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function setEmail($email)
    {
        $this->_email = $email;
        return $this;
    }

    public function getTry()
    {
        return $this->_try;
    }

    public function setTry($try)
    {
        $this->_try = $try;
        return $this;
    }

    public function getFromName()
    {
        return $this->_fromName;
    }

    public function setFromName($fromName)
    {
        $this->_fromName = $fromName;
        return $this;
    }

    public function getFromEmail()
    {
        return $this->_fromEmail;
    }

    public function setFromEmail($fromEmail)
    {
        $this->_fromEmail = $fromEmail;
        return $this;
    }

    public function getReplyTo()
    {
        return $this->_replyTo;
    }

    public function setReplyTo($replyTo)
    {
        $this->_replyTo = $replyTo;
        return $this;
    }

    public function getCc()
    {
        return $this->_cc;
    }

    public function setCc($cc)
    {
        $this->_cc = $cc;
        return $this;
    }

    public function getBcc()
    {
        return $this->_bcc;
    }

    public function setBcc($bcc)
    {
        $this->_bcc = $bcc;
        return $this;
    }

    public function getAttachment()
    {
        return $this->_attachment;
    }

    public function setAttachment($attachment)
    {
        $this->_attachment = $attachment;
        return $this;
    }

    public function getMod10()
    {
        return $this->_mod10;
    }

    public function setMod10($mod10)
    {
        $this->_mod10 = $mod10;
        return $this;
    }

    public function getPrio()
    {
        return $this->_prio;
    }

    public function setPrio($prio)
    {
        $this->_prio = $prio;
        return $this;
    }

    public function getCharset()
    {
        return $this->_charset;
    }

    public function setCharset($charset)
    {
        $this->_charset = $charset;
        return $this;
    }

    public function getPortal()
    {
        return $this->_portal;
    }

    public function setPortal($portal)
    {
        $this->_portal = $portal;
        return $this;
    }

    public function getInstant()
    {
        return $this->_instant;
    }

    public function setInstant($instant)
    {
        $this->_instant = $instant;
        return $this;
    }

    public function toArray()
    {
        $result = array();
        $memberToId = $this->getMemberToId();
        $result['MemberTo_Id'] = (isset($memberToId) ? $memberToId : 0);
        $memberFromId = $this->getMemberFromId();
        $result['MemberFrom_Id'] = (isset($memberFromId) ? $memberFromId : 0);
        $result['Category'] = $this->getCategory();
        $result['Type'] = $this->getType();
        $result['Subject'] = $this->getSubject();
        $result['Text'] = $this->getText();
        $result['Html'] = $this->getHtml();
        $result['Email'] = $this->getEmail();
        $try = $this->getTry();
        $result['Try'] = (isset($try) ? $try : 0);
        $result['FromName'] = $this->getFromName();
        $result['FromEmail'] = $this->getFromEmail();
        $result['ReplyTo'] = $this->getReplyTo();
        $result['CC'] = $this->getCc();
        $result['BCC'] = $this->getBcc();
        $result['Attachment'] = $this->getAttachment();
        $result['Mod10'] = rand(0, 9);
        $prio = $this->getPrio();
        $result['Prio'] = (isset($prio) ? $prio : 0);
        $result['Charset'] = $this->getCharset();
        $result['Portal'] = $this->getPortal();
        $result['instant'] = $this->getInstant();
        return $result;
    }
}
