<?php

/**
 * Encapsulates the db table 'bounce_hardbounce'
 *
 *
 * @category	mailqueue
 * @package	    mqWebInterface
 * @copyright	Copyright (c) 2011 Unister GmbH
 * @author		Nico Schuetze <nico.schuetze@unister-gmbh.de>
 * @version		$Id:$
*/

class Admin_Model_DbTable_BounceHardbounce extends Zend_Db_Table_Abstract
{

    protected $_name = 'bounce_hardbounce';
    protected $_primary = array('typ', 'cat', 'portal', 'email');

}
