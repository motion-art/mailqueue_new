<?php

/**
 * Encapsulates the db table 'bounce_softbounce'
 *
 *
 * @category	mailqueue
 * @package	    mqWebInterface
 * @copyright	Copyright (c) 2011 Unister GmbH
 * @author		Nico Schuetze <nico.schuetze@unister-gmbh.de>
 * @version		$Id:$
*/

class Admin_Model_DbTable_BounceSoftbounce extends Zend_Db_Table_Abstract
{

    protected $_name = 'bounce_softbounce';
    protected $_primary = array('typ', 'cat', 'portal', 'email');

}
