<?php

/**
 * maps Bounce* model to database
 *
 * @category   mailqueue
 * @package    mqWebInterface
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Nico Schuetze <nico.schuetze@unister-gmbh.de>
 * @version    $Id:$
 */

class Admin_Model_BounceBaseMapper
{
    protected $_dbTable;

//    public abstract function getDbTable();
//    public abstract function getBlacklistCount($mailaddress);

    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }

        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data provided');
        }

        $this->_dbTable = $dbTable;
        return $this;
    }

    public function fetch($conditions)
    {
        $select = $this->getDbTable()->select();
        foreach ($conditions as $col => $value) {
            $select->where($col . ' = ?', $value);
        }
        $stmt = $select->query();

        $bounceData = $stmt->fetchAll();

        $bounces = array();
        foreach ($bounceData as $data) {
            $bounce = new Admin_Model_BounceBase();
            $this->arrayToModel($data, $bounce);

            $bounces[] = $bounce;
        }

        return $bounces;
    }

    /**
	 *
	 * retrieve all the *bounces
	 *
	 * @param string $typ typ of the mail
	 * @param string $portal name of the portal
	 * @return array list of abstractbounce models
	 */
    public function findByTypAndPortal($typ, $portal)
    {
        $select = $this->getDbTable()->select();
        $select->where('typ LIKE ?', $typ);
        $select->where('portal LIKE ?', $portal);
        $stmt = $select->query();
        $data = $stmt->fetchAll();

        $bounces = array();
        foreach ($data as $item) {
            $bounce = new Admin_Model_BounceBase();
            $this->arrayToModel($item, $bounce);
            $bounces[] = $bounce;
        }

        return $bounces;
    }

    /**
     *
     * retrieve all the *bounces
     *
     * @param string $portal name of the portal
     * @param string $startDate StartDate
     * @param string $endDate EndDate
     * @param string $order Order by
     * @return array list of abstract bounce models
     */
    public function findByPortal($portal, $startDate, $endDate, $order = null)
    {
        $select = $this->getDbTable()->select();
        if (('' != $portal) && ('<<alle>>' != $portal)) {
            $select->where('portal LIKE ? ', $portal);
        }
        if (($timestamp = strtotime($startDate)) && ($timestamp = strtotime($endDate))) {
            $select->where('changed BETWEEN \'' .  date('c', strtotime($startDate)) . '\' AND \''
              . date('c', strtotime($endDate)) . '\'');
        }
        if (null == $order) {
            $order = 'changed desc';
        }
        $select->order($order);
        $stmt = $select->query();
        $data = $stmt->fetchAll();

        $bounces = array();
        foreach ($data as $item) {
            $bounce = new Admin_Model_BounceBase();
            $this->arrayToModel($item, $bounce);
            $bounces[] = $bounce;
        }

        return $bounces;
    }

    /**
     *
     * retrieve all portals
     *
     * @param string $startDate StartDate
     * @param string $endDate EndDate
     * @return array list of portals
     */
    public function findPortals($startDate, $endDate)
    {
        $select = $this->getDbTable()->select();
        $select->from($this->getDbTable(), array('portal'))
               ->distinct();
        //if (($timestamp = strtotime($startDate)) && ($timestamp = strtotime($endDate))) {
        //    $select->where('changed BETWEEN \'' .  date('c', strtotime($startDate)) . '\' AND \''
        //      . date('c', strtotime($endDate)) . '\'');
        //}
        $select->where('portal not like ?', '%.de|%'); //sowas wie 'schuelerprofile.de|007' rausfiltern
        $select->order('portal');
        $stmt = $select->query();
        $data = $stmt->fetchAll();

        foreach ($data as $item) {
            $portals[] = $item;
        }
        return $portals;
    }

    /**
     *
     * retrieve an overview of bounces
     *
     * @param string $startDate StartDate
     * @param string $endDate EndDate
     * @return array overview
     */
    public function getOverview($startDate, $endDate)
    {
        $select = $this->getDbTable()->select();
        $select->from($this->getDbTable(), array('portal', 'anzahl' => 'sum(inc)'));
        if (($timestamp = strtotime($startDate)) && ($timestamp = strtotime($endDate))) {
            $select->where('changed BETWEEN \'' .  date('c', strtotime($startDate)) . '\' AND \''
              . date('c', strtotime($endDate)) . '\'');
        }
        $select->group('portal');
        $select->having('portal not like ?', '%.de|%'); //sowas wie 'schuelerprofile.de|007' rausfiltern
        $select->order('2 DESC');
        $stmt = $select->query();
        $data = $stmt->fetchAll();

        foreach ($data as $item) {
            $bounces[] = $item;
        }
        return $bounces;
    }

    public static function arrayToModel($data, Admin_Model_BounceBase $bounce)
    {
        $bounce->setCat($data['cat']);
        $bounce->setChanged($data['changed']);
        $bounce->setEmail($data['email']);
        $bounce->setInc($data['inc']);
        $bounce->setPortal($data['portal']);
        $bounce->setTyp($data['typ']);
    }

    public static function rowToModel(Zend_Db_Table_Row $row, Admin_Model_BounceBase $bounce)
    {
        self::arrayToModel($row->toArray(), $bounce);
    }

}
