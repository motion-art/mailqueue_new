<?php
/**
 * maps BounceHardbounce model to database
 *
 * @category   mailqueue
 * @package    mqWebInterface
 * @copyright  Copyright (c) 2011 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Nico Schuetze <nico.schuetze@unister-gmbh.de>
 * @version    $Id:$
 */

class Admin_Model_BounceHardbounceMapper extends Admin_Model_BounceBaseMapper
{
    /**
     *
     * Get the DbTable
     *
     * @return Admin_Model_DbTable_BounceHardbounce
     *
     */
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Admin_Model_DbTable_BounceHardbounce');
        }
        return $this->_dbTable;
    }


    public function getBlacklistCount($mailaddress)
    {
        //SELECT inc AS anzahl FROM bounce_hardbounce h WHERE h.email LIKE '%martinez@ol.com%'
        $select = $this->getDbTable()->select();
        $select->from($this->getDbTable(), array('anzahl' => 'inc'))
               ->where('email LIKE ? ', $mailaddress);
        $stmt = $select->query();
        $data = $stmt->fetchAll();
        if (count($data) > 0) {
            return $data[0]['anzahl'];
        } else {
            return 0;
        }
    }


}
