<?php

/**
 * Model for bounce_*
 *
 *
 * @category	mailqueue
 * @package	    mqWebInterface
 * @copyright	Copyright (c) 2011 Unister GmbH
 * @author		Nico Schuetze <nico.schuetze@unister-gmbh.de>
 * @version		$Id:$
*/

class Admin_Model_BounceBase
{
    protected $_changed;
    protected $_typ;
    protected $_cat;
    protected $_portal;
    protected $_email;
    protected $_inc;

    public function getChanged()
    {
        return $this->_changed;
    }

    public function setChanged($changed)
    {
        $this->_changed = $changed;
        return $this;
    }

    public function getTyp()
    {
        return $this->_typ;
    }

    public function setTyp($typ)
    {
        $this->_typ = $typ;
        return $this;
    }

    public function getCat()
    {
        return $this->_cat;
    }

    public function setCat($cat)
    {
        $this->_cat = $cat;
        return $this;
    }

    public function getPortal()
    {
        return $this->_portal;
    }

    public function setPortal($portal)
    {
        $this->_portal = $portal;
        return $this;
    }

    public function getEmail()
    {
        return $this->_email;
    }

    public function setEmail($email)
    {
        $this->_email = $email;
        return $this;
    }

    public function getInc()
    {
        return $this->_inc;
    }

    public function setInc($inc)
    {
        $this->_inc = $inc;
        return $this;
    }

}
