<?php

class Admin_Class_Plugin_Auth extends Zend_Controller_Plugin_Abstract
{
    private $_layout;
    
    public function __construct()
    {

    }
    
    /**
     * Set the Zend_Layout instance.
     * 
     * @param  Zend_Layout $value
     * @return Application_Plugin_Auth
     */
    public function setLayout(Zend_Layout $value)
    {
        $this->_layout = $value;
        return $this;
    }
    
    /**
     * Get the Zend_Layout instance.
     * 
     * @return Zend_Layout
     */
    public function getLayout()
    {
        return $this->_layout;
    }
    
    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
        if ($request->getControllerName() == 'auth') {
            return;
        }
        $this->setLayout(Zend_Layout::getMvcInstance());
        $user = Admin_Class_Auth_User::getInstance();
        if ($user->isLoggedIn()) {
            $this->_addLoginInfo($user->getUsername(), $user->getRole());
        } else {
            $this->_addLoginForm();
        }
        
    }
    
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $acl = new Admin_Class_Acl_Site();
        $allowed = $acl->isUserAllowed(
            Admin_Class_Auth_User::getInstance(),
            $request->getControllerName(),
            $request->getActionName()
        );
        if (!$allowed) {
            $request->setControllerName('index');
            $request->setActionName('index');
        }
    }
    
    private function _addLoginForm()
    {
        if (!$this->getLayout()->loginForm) {
            $url = $this->getLayout()->getView()->url(
                array(
                    'controller' => 'auth',
                    'action' => 'index'
                )
            );
            $form = new Admin_Form_Login();
            $form->setAction($url);
            $this->getLayout()->loginForm = $form;
        }
        return $this;
    }
    
    private function _addLoginInfo($username, $role = null)
    {
        if ($username) {
            $this->getLayout()->loginForm = $this->getLayout()->getView()->userinfo($username, $role);
        }
        return $this;
    }
}