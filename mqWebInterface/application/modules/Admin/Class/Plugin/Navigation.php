<?php
class Admin_Class_Plugin_Navigation extends Zend_Controller_Plugin_Abstract
{
    function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        $layout = Zend_Layout::getMvcInstance();
        $view = $layout->getView();
        $config = new Zend_Config_Xml(Zend_Registry::get('_CONFIG')->configFile->navigationXml);
        $acl = new Admin_Class_Acl_Site();
        
        $container = new Zend_Navigation($config);
        //$view->navigation($container);
        //print_r($acl);
        $view->navigation($container)->setAcl($acl)
                                     ->setRole(Admin_Class_Auth_User::getInstance()->getRole());
    }
}