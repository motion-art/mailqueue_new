<?php
class Admin_Class_Auth_User
{
    private $_role = null;
    private $_config = null;
    private $_session = null;

    private static $_instance = false;

    /**
     * Implement the singleton pattern...
     *
     * @return Admin_Class_Auth_User
     */
    public static function getInstance()
    {
        if (self::$_instance === false) {
            self::$_instance = new Admin_Class_Auth_User();
        }
        return self::$_instance;
    }

    /**
     * Set the userrole
     *
     * @param  Zend_Acl_Role | string $role
     * @return Admin_Class_Auth_User
     */
    public function setRole($role)
    {
        $this->_isAdmin($this->getUsername());

        if (is_string($role)) {
            $role = new Zend_Acl_Role($role);
        }
        $this->_role = $role;
        $this->_getNamespace()->role = $role;
        return $this;
    }

    /**
     * Get the userrole.
     *
     * If role uninitialized, function try to load from session.
     * If this also fail, use the default setting from configuration.
     *
     * @return Zend_Acl_Role
     */
    public function getRole()
    {
        if ($this->_role === null) {
            $role = $this->_getNamespace()->role;
            if (!$role) {
                $role = $this->getConfig()->default->role;
            }
            $this->setRole($role);
        }
        return $this->_role;
    }

    /**
     * Set the username
     *
     * @param  string $value
     * @return Admin_Class_Auth_User
     */
    public function setUsername($value)
    {
        $this->_getNamespace()->username = $value;
        return $this;
    }

    /**
     * Get the username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->_getNamespace()->username;
    }

    /**
     * Get the ldap uuid of the user
     *
     * If no user logged on return $config->default->role
     *
     * @return string
     */
    public function getUuid()
    {
        $username = $this->getUsername();
        if (empty($username) || ($this->getUsername() == $this->getConfig()->default->username)) {
            $username = $this->getConfig()->default->role;
        }
        return $username;
    }

    /**
     * Set the config instance
     *
     * @param  Zend_Config $value
     * @return Admin_Class_Auth_User
     */
    public function setConfig(Zend_Config $value)
    {
        $this->_config = $value;
        return $this;
    }

    /**
     * Get the config instance
     *
     * @return Zend_Config
     */
    public function getConfig()
    {
        return $this->_config;
    }

    /**
     * Try to to login the user with given username and password.
     *
     * @param string $username
     * @param string $password
     */
    public function logIn($username, $password)
    {
        $auth = Zend_Auth::getInstance();
        $config = $this->getConfig();

        $logPath = $this->getConfig()->ldap->logPath;
        $options[] = $this->getConfig()->ldap->server->toArray();
        unset($options['logPath']);
        unset($options['cryptKey']);

        $oldUsername = $username;
        if (!empty($oldUsername)) {
            $username = 'uid=' . $oldUsername . ',' . $options[0]['baseDn'];
        }

        $adapter = new Zend_Auth_Adapter_Ldap($options, $username, $password);
        $result = $auth->authenticate($adapter);

        $username = $oldUsername;
        if ($auth->hasIdentity()) {
            $this->setUsername($username)
                 ->_setLastTime(time());
            if ($this->_isAdmin($username)) {
                $this->setRole('admin');
            } else {
                $this->setRole('user');
            }
        }

        if ($logPath) {
            $messages = $result->getMessages();
            $logger = new Zend_Log();
            $logger->addWriter(new Zend_Log_Writer_Stream($logPath));
            $filter = new Zend_Log_Filter_Priority(Zend_Log::DEBUG);
            $logger->addFilter($filter);
            foreach ($messages as $i => $message) {
                if ($i-- > 1) { // $messages[2] und höher sind Log Nachrichten
                    $message = str_replace("\n", "\n  ", $message);
                    $logger->log('Ldap: [' . $i . '] ' . $message, Zend_Log::DEBUG);
                }
            }
        }
        return $auth->hasIdentity();
    }

    /**
     * Logout the user
     *
     * @return Admin_Class_Auth_User
     */
    public function logOut()
    {
        $this->_reset();
        return $this;
    }

    /**
     * Check if user is timeout.
     * Return true if session is timeout, else false
     *
     * @return bool
     */
    public function isSessionTimeout()
    {
        $now = time();
        if ($this->_getLastTime() + $this->getConfig()->timeout > $now) {
            $this->_setLastTime($now);
            return false;
        }
        return true;
    }

    /**
     * Check if user is logged in.
     *
     * @return bool
     */
    public function isLoggedIn()
    {
        if (($this->getRole() != $this->getConfig()->default->role) && ($this->getRole())) {
            return true;
        }
        return false;
    }

    /**
     * Check if user is a admin.
     *
     * Return true if user is admin, else false.
     *
     * @return bool
     */
    public function isAdmin()
    {
        return $this->_isAdmin($this->getUuid());
    }

    /**
     * Set the session_namespace
     *
     * @param  Zend_Session_Namespace|string $value
     * @return Branchmanager_Class_Auth_User
     */
    protected function _setNamespace($value)
    {
        if (is_string($value)) {
            $value = new Zend_Session_Namespace($value);
        }
        $this->_session = $value;
        return $this;
    }

    /**
     * Get the session namespace
     *
     * @return Zend_Session_Namespace
     */
    protected function _getNamespace()
    {
        return $this->_session;
    }

    /**
     * Constructor...
     * Singleton pattern implementation makes "new" unavailable
     */
    protected function __construct()
    {
        // new Zend_Config_Ini(APPLICATION_PATH . '/configs/config.ini', 'auth')
        $this->setConfig(Zend_Registry::get('_CONFIG'))
             ->_setNamespace('Auth_User');

        if ($this->isLoggedIn() && $this->isSessionTimeout()) {
            $this->_reset();
        }
    }

    /**
     * Magic method to clone...
     * Singleton pattern implementation makes "clone" unavailable
     */
    protected function __clone()
    {

    }

    /**
     * Get the last time action
     *
     */
    private function _getLastTime()
    {
        return (int)$this->_getNamespace()->lastTime;
    }

    /**
     * Set last time action
     *
     * @param string $value
     * @return Admin_Class_Auth_User
     */
    private function _setLastTime($value)
    {
        $this->_getNamespace()->lastTime = $value;
        return $this;
    }

    /**
     * Check if the user with the given ldap id is also a admin.
     *
     * Admins defined in config.ini
     *
     * @param  string $lpadUuid
     * @return bool
     */
    private function _isAdmin($lpadUuid)
    {
        $admins = $this->getConfig()->admin->toArray();
        return in_array($lpadUuid, $admins);
    }

    /**
     * Reset the values of the user.
     *
     * @return Admin_Class_Auth_User
     */
    private function _reset()
    {
        $this->setRole($this->getConfig()->default->role);
        $this->setUsername($this->getConfig()->default->username);
        return $this;
    }
}