<?php

class Admin_Class_Acl_Site extends Admin_Class_Acl_Abstract
{
    public function __construct()
    {
        $this->_initRoles()
             ->_initRessource()
             ->_initAllow();
    }

    private function _initRoles()
    {
        $roleGuest = new Zend_Acl_Role('guest');
        $this->addRole($roleGuest);

        $roleUser = new Zend_Acl_Role('user');
        $this->addRole($roleUser, $roleGuest);

        $roleAdmin = new Zend_Acl_Role('admin');
        $this->addRole($roleAdmin, $roleUser);

        return $this;
    }

    private function _initRessource()
    {
        $this->add(new Zend_Acl_Resource('index'));
        $this->add(new Zend_Acl_Resource('error'));
        $this->add(new Zend_Acl_Resource('softbouncereport'));
        $this->add(new Zend_Acl_Resource('hardbouncereport'));
        $this->add(new Zend_Acl_Resource('soap'));
        $this->add(new Zend_Acl_Resource('auth'));
        return $this;
    }

    private function _initAllow()
    {
        $this->allow('guest', 'index');
        $this->allow('guest', 'auth');
        $this->allow('guest', 'error');
        $this->allow('guest', 'soap');
        $this->allow('admin', 'softbouncereport');
        $this->allow('admin', 'hardbouncereport');

        return $this;
    }
}