<?php
abstract class Admin_Class_Acl_Abstract extends Zend_Acl
{
    public function isUserAllowed(Admin_Class_Auth_User $user, $resource = null, $privilege = null)
    {
        if ($this->has($resource)) {
            return $this->isAllowed($user->getRole(), $resource, $privilege);
        }
        return false;
    }
}