<?php

/**
 * WSDL-Basis-File
 *
 * @category	mailqueue
 * @package	    mqWebInterface
 * @copyright	Copyright (c) 2011 Unister GmbH
 * @author		Nico Schuetze <nico.schuetze@unister-gmbh.de>
 * @version		$Id:$
 */
class Admin_Class_Soap_Service
{
    /**
     * Gibt die Mail-Adressen aus der Blacklist zurück
     *
     * @param string $portal
     * @param array order
     * @return array
     */
    public function getBlacklist($portal, $order)
    {
        $bounce = new Admin_Class_Bounce_Hard();
        return $bounce->getByPortal($portal, $order);
    }

    /**
     * Liefert, wie oft eine Mail-Adresse in der Blacklist steht
     *
     * @param string $mailaddress
     * @return integer
     */
    public function getBlacklistCount($mailaddress)
    {
        $bounce = new Admin_Class_Bounce_Hard();
        return $bounce->getBlacklistCount($mailaddress);
    }

    /**
     * Fügt ein Portal zur Zugangsliste hinzu
     *
     * @param string $portal
     * @return boolean
     */
    public function addPortal($portal)
    {
        return true;
    }

    /**
     * Prüft, ob ein Portal Zugriffsrechte hat
     *
     * @param string $portal
     * @return boolean
     */
    public function isPortalAllowed($portal)
    {
        return true;
    }

    /**
     * Entzieht einem Portal die Zugriffsrechte
     *
     * @param string $portal
     * @return boolean
     */
    public function removePortal($portal)
    {
        return true;
    }

    /**
     * Verschickt eine Mail mit hoher Priorität
     *
     * @param string $category
     * @param string $type
     * @param string $subject
     * @param string $text
     * @param string $html
     * @param string $recipient
     * @param string $fromName
     * @param string $fromAddress
     * @param string $replyToAddress
     * @param string $ccAddresses
     * @param string $bccAddresses
     * @param string $charset
     * @param string $portal
     * @param string $templateName
     * @param array $attachments
     * @param int $timeToSend
     * @return boolean
     */
    public function sendMailInstant($category, $type, $subject, $text, $html, $recipients, $fromName, $fromAddress,
        $replyToAddress, $ccAddresses, $bccAddresses, $charset, $portal, $templateName, $attachments, $timeToSend)
    {
        $mail = new Admin_Model_Mail();

        $mail->setCategory($category);
        $mail->setType($type);
        $mail->setSubject($subject);
        $mail->setText($text);
        $mail->setHtml($html);
        $mail->setEmail($recipients);
        $mail->setFromName($fromName);
        $mail->setFromEmail($fromAddress);
        $mail->setReplyTo($replyToAddress);
        $mail->setCc($ccAddresses);
        $mail->setBcc($bccAddresses);
        $mail->setCharset($charset);
        $mail->setPortal($portal);
        $mail->setAttachment($attachments);
        $mail->setInstant(true);

        $mapper = new Admin_Model_MailMapper();
        return $mapper->insert($mail);
    }

    /**
     * Verschickt eine Mail mit hoher Priorität
     *
     * @param array $mail
     * @return boolean
     */
    public function sendMailArr($data)
    {
        $mapper = new Admin_Model_MailMapper();
        return $mapper->insertArray($data);
    }

    /**
     * Verschickt eine Mail mit normaler Priorität
     *
     * @param string $category
     * @param string $type
     * @param string $subject
     * @param string $text
     * @param string $html
     * @param string $recipient
     * @param string $fromName
     * @param string $fromAddress
     * @param string $replyToAddress
     * @param string $ccAddresses
     * @param string $bccAddresses
     * @param string $charset
     * @param string $portal
     * @param string $templateName
     * @param array $attachments
     * @param int $timeToSend
     * @return boolean
     */
    public function sendMailCompendium($category, $type, $subject, $text, $html, $recipient, $fromName, $fromAddress,
        $replyToAddress, $ccAddresses, $bccAddresses, $charset, $portal, $templateName, $attachments, $timeToSend)
    {
        $mail = new Admin_Model_MailCompendium();

        $mail->setCategory($category);
        $mail->setType($type);
        $mail->setSubject($subject);
        $mail->setText($text);
        $mail->setHtml($html);
        $mail->setEmail($recipients);
        $mail->setFromName($fromName);
        $mail->setFromEmail($fromAddress);
        $mail->setReplyTo($replyToAddress);
        $mail->setCc($ccAddresses);
        $mail->setBcc($bccAddresses);
        $mail->setCharset($charset);
        $mail->setPortal($portal);
        $mail->setAttachment($attachments);
        $mail->setInstant(false);

        $mapper = new Admin_Model_MailMapper();
        return $sender->insert($mapper);
    }
}
