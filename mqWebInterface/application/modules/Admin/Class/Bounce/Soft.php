<?php

class Admin_Class_Bounce_Soft extends Admin_Class_Bounce_Abstract
{

    public function __construct()
    {
        $this->_setMapper(new Admin_Model_BounceSoftbounceMapper);
    }

    protected function _setMapper(Admin_Model_BounceSoftbounceMapper $mapper)
    {
        $this->_mapper = $mapper;
    }

}
