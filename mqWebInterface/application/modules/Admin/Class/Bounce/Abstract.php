<?php

class Admin_Class_Bounce_Abstract
{
    protected $_startDate;
    protected $_endDate;
    protected $_mapper;

    protected function _getMapper()
    {
        return $this->_mapper;
    }

    public function getStartDate()
    {
        return $this->_startDate;
    }

    public function setStartDate($startDate)
    {
        $this->_startDate = $startDate;
        return $this;
    }

    public function getEndDate()
    {
        return $this->_endDate;
    }

    public function setEndDate($endDate)
    {
        $this->_endDate = $endDate;
        return $this;
    }

    public function getByPortal($portal, $order = null)
    {
        return $this->_mapper->findByPortal($portal, $this->_startDate, $this->_endDate, $order);
    }

    public function getPortals()
    {
        return $this->_mapper->findPortals($this->_startDate, $this->_endDate);
    }

    public function getGesamt($order = null)
    {
        return $this->_mapper->getOverview($this->_startDate, $this->_endDate);
    }

    public function getBlacklistCount($mailaddress)
    {
        return $this->_mapper->getBlacklistCount($mailaddress);
    }

}
