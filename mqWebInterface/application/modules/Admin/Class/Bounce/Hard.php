<?php

class Admin_Class_Bounce_Hard extends Admin_Class_Bounce_Abstract
{
    /**
     *
     * Enter description here ...
     */
    public function __construct()
    {
        $this->_setMapper(new Admin_Model_BounceHardbounceMapper);
    }

    /**
     *
     * @param Admin_Model_BounceHardbounceMapper $mapper
     */
    protected function _setMapper(Admin_Model_BounceHardbounceMapper $mapper)
    {
        $this->_mapper = $mapper;
    }

}