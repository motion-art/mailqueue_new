<?php
class Admin_Class_Soaptest
{
    /**
     * Add method
     *
     * @param integer $param1
     * @param integer $param2
     * @return string
     */
    public function mathssAdd($param1, $param2)
    {
        return $param1 + $param2;
    }

     /**
     * echo method
     *
     * @param integer $param1
     * @return string
     */
    public function myFunctions($param1)
    {
        return $param1;
    }

}