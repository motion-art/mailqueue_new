<?php

/**
 * HardbouncereportController
 *
 * @category	mailqueue
 * @package	    mqWebInterface
 * @copyright	Copyright (c) 2011 Unister GmbH
 * @author		Nico Schuetze <nico.schuetze@unister-gmbh.de>
 * @version		$Id:$
 */

class Admin_HardBouncereportController extends Admin_Controller_BouncereportController
{
    /**
     * (non-PHPdoc)
     * @see Admin_Controller_BouncereportController::indexAction()
     */
    public function indexAction()
    {
        $this->_initBouncereport();

//        $enableForm = (Zend_Registry::get('role') == 'admin');

        $hb = new Admin_Class_Bounce_Hard();
        $hb->setStartDate($this->_getStartDate());
        $hb->setEndDate($this->_getEndDate());

        $portals = $hb->getPortals();
        array_unshift($portals, array('portal' => '<<alle>>'));
        $overviewForm = new Admin_Form_PortalOverview($this->_getStartDate(), $this->_getEndDate(), $portals);

        if (($this->getRequest()->getPost()) && ($overviewForm->isValid($this->getRequest()->getPost()))) {
            $this->_setPortal($overviewForm->getSelectedPortal());
        }
        $this->view->overviewForm = $overviewForm;
        $this->view->bounces = $hb->getByPortal($this->_getPortal());
    }

    /**
     * (non-PHPdoc)
     * @see Admin_Controller_BouncereportController::exportAction()
     */
    public function exportAction()
    {
        $this->_initBouncereport();
        $portal = $this->_getParam('portal', '');
        if ($portal != '') {

            if ($portal == 'Gesamtübersicht') {
                $this->_helper->redirector('gesamt');
            }

            $this->_helper->layout->disableLayout();
            $this->_helper->viewRenderer->setNeverRender();
            $fileName = $this->_getParam('portal') . '_hardbounce.csv';

            header('Content-type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $fileName . '"');

            $this->_getCsv($this->_getParam('portal'), $this->_getStartDate(), $this->_getEndDate(), true);

        } else {
            $hb = new Admin_Class_Bounce_Hard();
            $hb->setStartDate($this->_getStartDate());
            $hb->setEndDate($this->_getEndDate());

            $portals = $hb->getPortals();
            array_unshift($portals, array('portal' => 'Gesamtübersicht'));
            $overviewForm = new Admin_Form_PortalOverview($this->_getStartDate(), $this->_getEndDate(), $portals, true);

            if (($this->getRequest()->getPost()) && ($overviewForm->isValid($this->getRequest()->getPost()))) {
                $this->_setPortal($overviewForm->getSelectedPortal());
            }
            $this->view->overviewForm = $overviewForm;
        }
    }

    /**
     * (non-PHPdoc)
     * @see Admin_Controller_BouncereportController::gesamtAction()
     */
    public function gesamtAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNeverRender();
        $fileName = 'gesamt_hardbounce.csv';

        header('Content-type: application/octet-stream');
        header('Content-Disposition: attachment; filename="' . $fileName . '"');

        $this->_getGesamtCsv(true);

    }

}
