<?php

/**
 * BouncereportController
 *
 * @category	mailqueue
 * @package	    mqWebInterface
 * @copyright	Copyright (c) 2011 Unister GmbH
 * @author		Nico Schuetze <nico.schuetze@unister-gmbh.de>
 * @version		$Id:$
 */

abstract class Admin_Controller_BouncereportController extends Zend_Controller_Action
{

    protected $_startDate;
    protected $_endDate;
    protected $_portal;

    protected function _getStartDate()
    {
        return $this->_startDate;
    }

    protected function _setStartDate($startDate)
    {
        $this->_startDate = $startDate;
        return $this;
    }

    protected function _getEndDate()
    {
        return  $this->_endDate;
    }

    protected function _setEndDate($endDate)
    {
        $this->_endDate = $endDate;
        return $this;
    }

    protected function _getPortal()
    {
        return  $this->_portal;
    }

    protected function _setPortal($portal)
    {
        $this->_portal = $portal;
        return $this;
    }

    /**
     *
     * indexaction
     * Stellt eine Übersicht über alle Bounces dar
     */
    public abstract function indexAction();

    /**
     *
     * exportAction
     * Stellt Export nach CSV portalweise zur Verfügung
     */
    public abstract function exportAction();

    /**
     *
     * gesamtAction
     * Stellt Übersicht-Export nach CSV zur Verfügung
     */
    public abstract function gesamtAction();

    public function tmpAction()
    {
        $bounce = new Admin_Class_Bounce_Hard();
        $bounces = $bounce->getBlacklistCount('test@testing.unister-gmbh.de');
        echo $bounces;
    }
    /**
     * Initialisiert den Controller
     */
    protected function _initBouncereport()
    {
        $request = $this->getRequest();
        $startDate = $request->getParam('startDate');
        $endDate = $request->getParam('endDate');
        $portal = $request->getParam('portal');

        if (null == $startDate) {
            $startDate = date('d.m.Y H:i', mktime(0, 0, 0, date('m'), date('d') - 1, date('Y')));
        }
        if (null == $endDate) {
            $endDate = date('d.m.Y H:i', mktime(23, 59, 59, date('m'), date('d'), date('Y')));
        }
        $this->_setStartDate($startDate);
        $this->_setEndDate($endDate);

        if (null != $portal) {
            $this->_setPortal($portal);
        }

        $this->view->action = $this->getRequest()->getActionName();
    }

    /**
     * Liefert alle Bounces eines Portals als CSV
     *
     * @param string $portal
     * @param boolean $hardbounce true: Hardbounces; false: Softbounces
     *
     */
    protected function _getCsv($portal, $startDate, $endDate, $hardbounce = true)
    {
        if ($hardbounce === true) {
            $bounce = new Admin_Class_Bounce_Hard();
        } else {
            $bounce = new Admin_Class_Bounce_Soft();
        }
        $bounce->setStartDate($startDate);
        $bounce->setEndDate($endDate);
        $bounces = $bounce->getByPortal($portal, array('inc desc', 'changed desc'));
        $out = fopen('php://output', 'w');
        fputcsv($out, array('E-Mail-Adresse', 'Anzahl', 'Typ', 'Kategorie', 'Letzte Änderung'));
        foreach ($bounces as $value) {
            fputcsv($out, array($value->getEmail(), $value->getInc(), $value->getTyp(),
                $value->getCat(), $value->getChanged()));
        }
        fclose($out);
    }

    /**
     * Liefert eine Übersicht über Bounces pro Portal
     *
     * @param boolean $hardbounce true: Hardbounces; false: Softbounces
     */
    protected function _getGesamtCsv($hardbounce = true)
    {
        if ($hardbounce === true) {
            $bounce = new Admin_Class_Bounce_Hard();
        } else {
            $bounce = new Admin_Class_Bounce_Soft();
        }
        //$bounce->setStartDate(date('d.m.Y H:i', mktime(0, 0, 0, date('m'), date('d') - 1, date('Y'))));
        //$bounce->setEndDate(date('d.m.Y H:i', mktime(23, 59, 59, date('m'), date('d'), date('Y'))));
        $bounces = $bounce->getGesamt();
        $out = fopen('php://output', 'w');
        fputcsv($out, array('Portal', 'Anzahl'));
        foreach ($bounces as $value) {
            fputcsv($out, $value);
        }
        fclose($out);
    }

}
