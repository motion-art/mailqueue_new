<?php

class Admin_AuthController extends Zend_Controller_Action
{

    public function init()
    {
        /* Initialize action controller here */
    }

    public function indexAction()
    {
        $form = new Admin_Form_Login();
        $request = $this->getRequest();
        if ($request->isPost() && $form->isValid($request->getPost())) {
            $auth = Admin_Class_Auth_User::getInstance()->logIn(
                $form->getUsername(),
                $form->getPassword()
            );
            if ($auth) {
                $this->_helper->redirector(
                    'index',
                    'index'
                );
            } else {
                $this->view->error = true;
            }
        } else if (!$form->isValid($request->getPost())) {
            $this->view->error = true;
        }
        $this->view->form = $form;
    }

    public function logoutAction()
    {
        Admin_Class_Auth_User::getInstance()->logOut();
        $this->_helper->redirector(
            'index',
            'index'
        );
    }
}
