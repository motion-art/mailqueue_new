<?php

/**
 * Standard-Controller
 * 
 * 
 * @author t.ehnert
 *
 */
class Admin_IndexController extends Zend_Controller_Action
{
    /**
     * Initialisieren aller benötigten Variable.
     * Überschreibt / ersetzt Zend_Controller_Action::init() 
     * 
     * @see    Zend_Controller_Action::init()
     * @return void
     */
    public function init()
    {
        /* Initialize action controller here */
    }

    /**
     * Index Action
     * 
     * Funktion verweist auf: project/index
     * 
     * @return void
     */
    public function indexAction()
    {
        // action body
        //$this->_helper->redirector('index', 'project');        
    }
}
