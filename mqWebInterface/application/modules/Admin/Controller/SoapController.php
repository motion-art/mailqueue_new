<?php

class Admin_SoapController extends Zend_Controller_Action
{

    //change this to your WSDL URI!
    private $_wsdlUri = 'http://developer01:8303/soap?wsdl';

    public function indexAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNeverRender();

        //if ($this->_getParam('wsdl', '') != '') {
        if (isset($_GET['wsdl'])) {
            //return the WSDL
            $this->_handleWSDL();
        } else {
            //handle SOAP request
            $this->_handleSOAP();
        }
    }

    private function _handleWSDL()
    {
        $autodiscover = new Zend_Soap_AutoDiscover();
        $autodiscover->setClass('Admin_Class_Soap_Service');
        $autodiscover->handle();
    }

    private function _handleSOAP()
    {
        $soap = new Zend_Soap_Server($this->_wsdlUri, array('cache_wsdl' => WSDL_CACHE_NONE));
        $soap->setClass('Admin_Class_Soap_Service');

        $soap->registerFaultException('Zend_Soap_Server_Exception');
        $soap->handle();
    }

    public function clientAction()
    {
        $client = new Zend_Soap_Client($this->_wsdlUri, array('cache_wsdl' => WSDL_CACHE_NONE));

        $mail = new Admin_Model_Mail();

        $mail->setCategory('test');
        $mail->setType('test');
        $mail->setSubject('Test vom Service als Objekt');
        $mail->setText('Test vom Service als Objekt');
        $mail->setEmail('nico.schuetze@unister.de');
        $mail->setFromName('Mailqueue WebService');
        $mail->setFromEmail('nico.schuetze@unister.de');
        $mail->setPortal('unister.de');
        echo $client->sendMailArr($mail->toArray());
    }

}
