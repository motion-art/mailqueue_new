<?php

class Core_Class_Filter_SeoLinkBuilder implements Zend_Filter_Interface
{
    protected $_whiteList      = array();
    protected $_blackList      = array();
    protected $_whiteListRegEx = null;
    protected $_blackListRegEx = null;

    protected $_titleList      = array();
    protected $_titleListRegEx = array();

    protected $_baseUrl        = null;
    protected $_view           = null;

    public function __construct(Zend_Config $config)
    {
        if (isset($config->nofollow)) {
            if (isset($config->nofollow->off)) {
                 $this->_whiteList = $config->nofollow->off->toArray();
                 $this->_whiteListRegEx = "~" . implode('|',$this->_whiteList). "~";
            }

            if (isset($config->nofollow->on)) {
                 $this->_blackList = $config->nofollow->on->toArray();
                 $this->_blackListRegEx = "~" . implode('|',$this->_blackList). "~";
            }
        }

        if (isset($config->title)) {
            foreach ($config->title as $title) {
                //speichert zu einem pattern die texte
                $this->_titleList[$title->pattern]['append']  = $title->append;
                $this->_titleList[$title->pattern]['prepend'] = $title->prepend;

                //alle pattern
                $this->_titleListRegEx[] = "~" .$title->pattern . "~";
            }
        }

        //base url holen
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper('ViewRenderer');
        $this->_view    = $viewRenderer->view;
        $this->_baseUrl = $viewRenderer->view->baseUrl();
    }

    /**
     * Filtert den gesamten context einer seite
     *
     * @param string $value
     * @return string
     */
    public function filter($value)
    {
        //hole alle Link Tags
        $search  = array("#<a (.*?)>(.*?)</a>#s");
        $value = preg_replace_callback($search, array($this,"_editOneATag"), $value);

        return $value;
    }

    /**
     * bearbeitet ein link tag
     * callback siehe filter methode
     *
     * @param array $treffer
     * @return string
     */
    private function _editOneATag($match)
    {
        $fullATag   = $match[0];

        $attributes = $this->_getATagAttribute($match[1]);
        $anker      = $match[2];

        $tagChange = false;

        // wenn schon rel-Attribute vorhanden
        // und kein href, dann nichts machen
        if (!isset($attributes['rel']) &&  isset($attributes['href'])) {
            //wenn nofollow
            if ($this->_isRelNoFollow($attributes['href'])) {
                $attributes['rel'] = '"nofollow"';
                $tagChange = true;
            }
        }

        // title anpassen
        if (!isset($attributes['title']) &&  isset($attributes['href'])) {
            $newTitle = $this->_getNewTitle($attributes['href'], $anker);
            if ($newTitle) {
                $attributes['title'] = '"' . $newTitle . '"';
                $tagChange = true;
            }
        }

        return ($tagChange) ? $this->_buildATag($attributes, $anker)
                            : $fullATag;
    }

    /**
     * gibt alle Atrribute eines Links als Array zurück
     *
     * @param string $tag <a .. >
     * @return array of attributes
     */
    private function _getATagAttribute($tag)
    {
         preg_match_all('/(\w+)\s*=\s*(?:(")(.*?)"|(\')(.*?)\')/s', $tag, $matches);
         $attributes = array();
         foreach ($matches[1] as $index => $attributeName) {
                $attributeName      = strtolower($attributeName);
                $attributeDelimiter = $matches[2][$index];
                $attributeValue     = $matches[3][$index];

                $attributes[$attributeName] = $attributeDelimiter
                                            . $attributeValue
                                            . $attributeDelimiter;
         }

         return $attributes;
    }

    /**
     * entscheidet ob Link auf nofollow
     * gesetzt werden soll
     *
     * @param string $href
     * @return boolean
     */
    private function _isRelNoFollow($href)
    {
        //delimiter entfernen
        $href = substr($href, 1, strlen($href)-2);

        // wenn relative url
        // baseUrl entfernen
        if (!preg_match("~^http://.*~",$href)) {
            $href = preg_replace("~^".$this->_baseUrl."~", "", $href);
        }

        //home url ausschliessen
        if (strlen($href) == 0) {
            //return false;
        }

        //wenn in blacklist dann rel="nofollow"
        if ($this->_blackList && preg_match($this->_blackListRegEx, $href)) {
            return true;
        }

        if ($this->_whiteList && preg_match($this->_whiteListRegEx, $href)) {
            return false;
        }

        return true;
    }

    /**
     * Gibt eine Title eines Links zurück
     *
     * @param  string $href
     * @param  string $anker
     * @return string|boolean
     */
    private function _getNewTitle($href, $anker)
    {
        if (!$this->_titleList) {
            return false;
        }

        //delimiter entfernen
        $href = substr($href, 1, strlen($href)-2);

        // wenn relative url
        // baseUrl entfernen
        if (!preg_match("~^http://.*~",$href)) {
            $href = preg_replace("~^".$this->_baseUrl."~", "", $href);
        }

        $check = preg_replace($this->_titleListRegEx, $this->_titleListRegEx, $href, 1);

        //$check gibt bei erfolg den regex zurück, sonst href - url
        //wenn rückgabe regex
        if (strpos($check,"~") !== 0) {
            return false;
        }

        //delimiter entfernen
        $check = substr($check, 1, strlen($check)-2);

        //wenn in list
        //muss eigendlich immer der Fall sein
        if (isset($this->_titleList[$check])) {
            $title  = $this->_titleList[$check]['prepend']
                    . ' ' . $this->_cleanAttrValue($anker) . ' '
                    . $this->_titleList[$check]['append'];
            return trim($title);
        }

        return false;
    }

    /**
     * liefert ein sauberes Attributevalue
     * anhand eines link-anker-textes
     * @param $value
     * @return string
     */
    private function _cleanAttrValue($value)
    {
        //wenn über mehrere Zeilen
        //inhalt des spans holen
        if (preg_match("#<span(.*?)>(.*?)</span>#s", $value, $matches)) {
            if (isset($matches[2])) {
                $value = $matches[2];
            }
        }

        $value = $this->_view->htmlAttributeFilter(strip_tags($value));
        return trim($value);
    }

    /**
     * baut einen kommplette Link zusammen
     * <a ...>BLA</a>
     *
     * @param  array  $attributes
     * @param  string $anker-text
     * @return string
     */
    private function _buildATag($attributes, $anker)
    {
        $return = '<a ';
        foreach ($attributes as $attr => $value) {
            $return .= $attr . '=' . $value . ' ';
        }

        return rtrim($return) . '>' . $anker . '</a>';
    }
}