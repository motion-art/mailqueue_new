<?php

/**
 * Helper zum Bauen eines Image-Tags für
 * http://www.websnapr.com/
 *
 * Beispiel - Konfiguration:
 * <pre>
 *  helper.snapshot.url = "http://unister.websnapr.com/?size=%s&url=%s"
 * </pre>
 *
 * @category   Core
 * @package    Core_View_Helper
 * @copyright  Unister GmbH
 */

class Core_View_Helper_Snapshot extends Zend_View_Helper_HtmlElement
{
    const SIZE_TINY = 'T';
    const SIZE_SMALL = 'S';
    const SIZE_MEDIUM = 'M';
    const SIZE_LARGE = 'L';

    protected $_config = null;

    public function __construct()
    {
        if (!isset(Zend_Registry::get('_CONFIG')->helper) || !isset(Zend_Registry::get('_CONFIG')->helper->snapshot)) {
            throw new Exception('Snapshot Helper Config expected');
        }

        $this->_config = Zend_Registry::get('_CONFIG')->helper->snapshot;
    }

    public function snapshot($url, $size = self::SIZE_TINY)
    {
        if (isset($this->_config->url)) {

            $attributes = array(
                'src' => sprintf($this->_config->url, $size, urlencode($url))
            );

            return '<img' . $this->_htmlAttribs($attributes) . ' />' . self::EOL;
        }

        Core_Class_Log::getInstance()->info('Snapshot-Url not defined');

        return '';
    }
}
