<?php

/**
 * Core_Class_Mail_Transport_Queue
 *
 * @package   Default
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: Queue.php 2442 2009-12-14 14:08:21Z daniel_troeger $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Mail_Transport_Queue extends Zend_Mail_Transport_Abstract
{
    /**
     * default table for attachments
     * @var string
     */
    const MAIL_ATTACHMENT_TABLENAME = 'attachments';

     /**
     * default table postfix
     * @var string
     */
    private  $_mailTable= 'instant';

    /**
     * portal name
     * @var string
     */
    private $_portal = null;

    /**
     * default prio
     * @var int
     */
    private $_priority = 1;

    /**
     * database
     * @var Zend_Db_Adapter_Pdo_Abstract
     */
    private $_db = null;

    /**
     * construct
     *
     * @param Zend_Db_Adapter_Pdo_Abstract $db
     * @param Zend_Config $config
     * @return Core_Class_Mail_Transport_Queue
     */
    public function __construct(Zend_Db_Adapter_Pdo_Abstract $db, $config = null)
    {
        $this->_db = $db;

        if ($config != null) {

            if ($config instanceof Zend_Config) {
                $config = $config->toArray();
            }
            $this->setOptions($config);
        }
    }

    /**
     * sets target
     *
     * @param string $name
     * @return Core_Class_Mail_Transport_Queue
     */
    public function setMailTable($name)
    {
        $allowed = array('instant', 'compendium');
        if (!in_array($name, $allowed)) {
            throw new Core_Class_Mail_Queue_Exception('unknown queue "'.$name.'"');
        }
        $this->_mailTable = $name;
        return $this;
    }

    /**
     * sets portal name
     *
     * @param string $name
     * @return Core_Class_Mail_Transport_Queue
     */
    public function setPortal($name)
    {
        $this->_portal = strtolower($name);
        return $this;
    }

    /**
     * sets priority
     *
     * @param int $priority
     * @return Core_Class_Mail_Transport_Queue
     */
    public function setPriority($priority)
    {
        $this->_priority = $priority;
        return $this;
    }

    /**
     * set options
     *
     * @param array $config
     * @return Core_Class_Mail_Transport_Queue
     */
    public function setOptions(array $config)
    {
        foreach ($config as $key => $value) {

            $method = 'set'.ucfirst($key);
            $this->$method($value);
        }
        return $this;
    }

    /**
     * saves E-Mail in mailqueue db via transaction
     *
     * @return void
     */
    protected function _sendMail()
    {
        try {

            $this->_db->beginTransaction();

            //save E-Mail Content
            $mailId = $this-> _saveEmailContent();

            //save E-Mail Attachments
            if($this->_mail->hasAttachments) {
                $this->_saveAttachements($mailId);
            }

            $this->_db->commit();

        } catch(Exception $e) {

            //rollback an delegate Exception
            $this->_db->rollBack();
            throw $e;
        }
    }

    /**
     * save E-Mail Content to mailqueue db
     * return insert id of E-Mail tuple
     *
     * @return int
     */
    private function _saveEmailContent()
    {
        $textpart = $this->_mail->getBodyText();
        $textpart->encoding = Zend_Mime::ENCODING_8BIT;
        $text = $textpart->getContent();

        $htmlpart = $this->_mail->getBodyHtml();
        $htmlpart->encoding = Zend_Mime::ENCODING_8BIT;
        $html = $htmlpart->getContent();

        $attachment = 0;
        if($this->_mail->hasAttachments) {
            $attachment = 1;
        }

        //TODO: implement full mailqueue functionality
        $insert = array(
            'Subject'           => $this->_mail->getSubject(),
            'Text'              => $text,
            'Html'              => $html,
            'Email'             => implode(";",$this->_mail->getRecipients()),
            'FromEmail'         => $this->_mail->getFrom(),
            'FromName'          => $this->_getName(),
            'ReplyTo'           => $this->_mail->getReturnPath(),
            'Portal'            => $this->_portal,
            'Charset'           => $this->_mail->getCharset(),
            'Mod10'             => rand(0,9),
            'Attachment'        => $attachment
//	        'Category'          => $this->_mail->Category,
//	        'Type'              => $this->_mail->Type,
//	        'Prio'              => $this->_priority,
//	        'CC'                => implode(";",$this->_mail->getCc()),
//	        'BCC'               => implode(";",$this->_mail->getBcc()),
//	        'Charset'           => $this->_mail->getCharset(),
//	        'MemberTo_Id'       => $this->_mail->MemberTo_Id,
//	        'MemberFrom_Id'     => $this->_mail->MemberFrom_Id,
         );

         $this->_db->insert('mail_'.$this->_mailTable, $insert);

         return $this->_db->lastInsertId();
    }

    /**
     * save attachements to mailqueue db
     * returns an array of attachment ids
     *
     * @return void
     */
    private function _saveAttachements($mailId)
    {
        $attachmentsIds = array();

        //check Mime Parts
        if (count($this->_mail->getParts()) > 0) {

            foreach ($this->_mail->getParts() as $part) {

                 $insert = array(
                      'AttachmentContent'  => $part->getContent(),
                      'AttachmentType'     => $part->type,
                      'AttachmentFile'     => $part->filename,
                    );

                $this->_db->insert(self::MAIL_ATTACHMENT_TABLENAME, $insert);
                $attachmentsIds[] = $this->_db->lastInsertId();
            }
        }

        $this->_saveEmailXAttachements($mailId, $attachmentsIds);

    }

    /**
     * save attachements - E-Mail reference to mailqueue db
     * returns an array of attachment ids
     * @return void
     */
    private function _saveEmailXAttachements($mailId, $attachementIds = array())
    {
        if (count($attachementIds) > 0) {

            foreach ( $attachementIds as $id) {
                $insert = array (
                       'Attachment_Id' => $id,
                       'Mail_Id'       => $mailId
                );

                $this->_db->insert(self::MAIL_ATTACHMENT_TABLENAME . '_mail_' . $this->_mailTable, $insert);
            }
        }
    }

    private function _getName()
    {
        $headers = $this->_mail->getHeaders();
        $fromHeader = $headers['From'][0];

        $position = strpos($fromHeader, '<');
        if ($position) {
            return trim(substr($fromHeader, 0, $position), ' "');
        } else {
            return $this->_mail->getFrom();
        }

    }
}
