<?php

/**
 * Core_Class_Mail_Queue_Exception
 *
 * @package   Default
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Mail_Queue_Exception extends Core_Exception
{
}
