<?php

require_once 'Zend/Log.php';

/**
 * Core_Class_ErrorHandler
 *
 * handles fatal errors
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: ErrorHandler.php 381 2010-07-08 10:11:48Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_ErrorHandler
{
    /**
     * log object
     * @var Zend_Log
     */
    static protected $_logger = null;

    /**
     * display errors
     * @var boolean
     */
    static protected $_showErrors = false;

    /**
     * error template
     * @var boolean
     */
    static protected $_errorTemplate = null;

    /**
     * register error handler
     *
     * @return void
     */
    static public function register()
    {
        ini_set('error_prepend_string', '<FATAL>');
        ini_set('error_append_string', '</FATAL>');

        ob_start(array('Core_Class_ErrorHandler', 'buffer'));
        set_error_handler(array('Core_Class_ErrorHandler', 'handle'));
    }

    static public function setLogger(Zend_Log $log)
    {
        self::$_logger = $log;
    }

    static public function setShowErrors($bool)
    {
        self::$_showErrors = $bool;
    }

    static public function setErrorTemplate($template)
    {
        self::$_errorTemplate = $template;
    }

    /**
     * handler for output buffer
     * gets fatal errors from buffer
     *
     * @param string $content
     * @return string
     */
    static public function buffer($content)
    {
        $output = $content;
        $matches = array();

        if (preg_match('|<FATAL>.*</FATAL>|s', $output, &$matches)) {
            $errors = '';
            header("HTTP/1.0 404 Not Found");
            foreach ($matches as $match) {
                self::handle(E_ERROR, trim(strip_tags($match)));
                if (self::$_showErrors) {
                    $errors .= '<li>' . trim(strip_tags($match)) . '</li>';
                }
            }
            if (self::$_showErrors) {
                return '<b>' . get_class() . ':</b><br /><ul>' . $errors . '</ul>';
            }

            if (self::$_errorTemplate != null && file_exists(self::$_errorTemplate)) {
                return file_get_contents(self::$_errorTemplate);
            }
            return;
        }

        return $output;
    }

    /**
     * handles reported errors
     *
     * @param int $code
     * @param string $message
     * @param string $filename
     * @param int $line
     * @param array $context
     * @return void
     */
    static public function handle($code, $message, $filename = null, $line = null, $context = null)
    {
        if (error_reporting() == 0) {
           return;
        }

        if (self::$_logger != null) {

            switch ($code) {
                case E_ERROR:
                    $method = 'emerg';
                    break;
                case E_USER_ERROR:
                    $method = 'err';
                    break;
                case E_USER_WARNING:
                case E_WARNING:
                    $method = 'warn';
                    break;
                case E_USER_NOTICE:
                case E_NOTICE:
                    $method = 'notice';
                    break;
                default:
                    $method = 'info';
                    break;
            }
            try {
                self::$_logger->$method(
                    $message
                    . ' - code: ' . $code
                    . ' - file: ' . $filename
                    . ' - line: ' . $line
                );
                return;

            } catch (Exception $e) {

                error_log($e->getMessage() . ' | ' . $e->getTraceAsString());
            }

        }
        error_log(sprintf('PHP %s: %s in %s on line %d', $code, $message, $filename, $line));
    }
}