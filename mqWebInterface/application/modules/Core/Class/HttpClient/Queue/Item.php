<?php

class Core_Class_HttpClient_Queue_Item
{
    private $_object = null;
    private $_method = null;
    private $_params = array();
    private $_handle = null;
    private $_result = null;
    private $_cache = null;
    private $_active = false;

    /**
     * construct
     *
     * @param mixed $object
     * @param string $method
     * @param array $params
     * @return Price_Class_Api_Queue_Item
     */
    public function __construct($object, $method, Zend_Cache_Core $cache, $params = array())
    {
        $this->_object = $object;
        $this->_method = $method;
        $this->_params = $params;
        $this->_cache = $cache;
    }

    public function isMulti()
    {
        if (method_exists($this->_object, 'getAdapter')) {
            return ($this->_object->getAdapter() instanceof Core_Class_HttpClient_Multi_Interface);
        }
        return ($this->_object instanceof Core_Class_HttpClient_Multi_Interface);
    }

    public function setActive($value)
    {
        $this->_active = (bool) $value;
    }

    public function isActive()
    {
        return $this->_active;
    }

    private function _getCacheKey()
    {
        return md5(get_class($this->_object) . $this->_method . serialize($this->_params));
    }

    public function isCached()
    {
        return $this->_cache->test($this->_getCacheKey());
    }

    public function execute()
    {
        if (!$this->_result = $this->_cache->load($this->_getCacheKey())) {
            $this->_result = call_user_func_array(array($this->_object, $this->_method), $this->_params);
        }
    }

    public function mapResult($result)
    {
        $this->_result = $this->_object->mapResult($result);
        $this->_cache->save($this->_result, $this->_getCacheKey());
    }

    public function getResult()
    {
        return $this->_result;
    }

    /**
     * get curl handle
     *
     * @return int
     */
    public function getHandle()
    {
        if ($this->_handle === null) {
            $this->_handle = $this->_object->getHandle($this->_method, $this->_params);
        }
        return $this->_handle;
    }

    public function __toString()
    {
        return $this->_object->getId();
    }
}
