<?php

interface Core_Class_HttpClient_Multi_Interface
{
    /**
     * returns http-handle (curl)
     *
     * @param string $url
     * @param array $data
     * @return int
     */
    public function getHandle($url, $data = array());

    /**
     * maps http-result to specific output format
     *
     * @param string $result
     * @return mixed
     */
    public function mapResult($result);
}
