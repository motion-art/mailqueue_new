<?php

class Core_Class_HttpClient_Profiler_Firebug
{
    protected $_items = array();
    protected $_label = null;
    protected $_message = null;
    protected $_enabled = false;

    public function __construct($label = null)
    {
        $this->_label = get_class($this);
        if ($label != null) {
            $this->_label = $label;
        }
    }

    public function setEnabled($value)
    {
        $this->_enabled = $value;
        return $this;
    }

    protected function _init()
    {
        if ($this->_message == null) {
            $this->_message = new Zend_Wildfire_Plugin_FirePhp_TableMessage($this->_label);
            $this->_message->setHeader(
                array(
                    'Time',
                    'Event',
                    'Code',
                    'Info'
                )
            );
            $this->_message->setBuffered(true);
            $this->_message->setDestroy(true);

            Zend_Wildfire_Plugin_FirePhp::getInstance()->send($this->_message);
        }
    }

    public function add($handle)
    {
        if (!$this->_enabled) {
            return;
        }

        $this->_init();
        $info = curl_getinfo($handle);

        $this->_totalTime += $info['total_time'];
        $this->_count++;

        $this->_message->setDestroy(false);
        $this->_message->addRow(
            array(
                (string) $info['total_time'],
                $info['url'],
                $info['http_code'],
                $info
            )
        );

        $this->_updateLabel();
    }

    protected function _updateLabel()
    {
        if ($this->_message != null) {

            $this->_message->setLabel(sprintf('%s (%d @ %f sec)', $this->_label, $this->_count, $this->_totalTime));
        }
    }
}
