<?php

abstract class Core_Class_HttpClient_Abstract
{
    /**
     * client-id
     * @var string
     */
    protected $_id = null;

    /**
     * service configuration
     * @var Zend_Config
     */
    protected $_config = null;

    /**
     * last query params
     * @var array
     */
    protected $_lastParams = array();

    protected $_profiler = null;

    /**
     * constructor
     *
     * @param Zend_Config $config
     * @return Core_Class_HttpClient_Abstract
     */
    public function __construct(Zend_Config $config)
    {
        $this->_config = $config;

        if (!$this->_config instanceof Zend_Config) {
            throw new Exception('Zend_Config expected');
        }

        if (!isset($this->_config->timeout) || $this->_config->timeout == 0) {
            throw new Exception('timeout not set');
        }

        if (!isset($this->_config->userAgent)) {
            $this->_config->userAgent = get_class($this);
        }
    }

    public function getProfiler()
    {
        if ($this->_profiler == null) {
            $this->_profiler = new Core_Class_HttpClient_Profiler_Firebug($this->getId());
        }
        if (isset($this->_config->profiler) && $this->_config->profiler) {
            $this->_profiler->setEnabled($this->_config->profiler);
        }
        return $this->_profiler;
    }

    /**
     * set timeout
     *
     * @param int $timeout
     * @return Core_Class_HttpClient_Abstract
     */
    public function setTimeout($timeout)
    {
        if ($timeout > 0) {
           $this->_config->timeout = $timeout;
        }

        return $this;
    }

    /**
     * set useragent
     *
     * @param string $userAgent
     * @return Core_Class_HttpClient_Abstract
     */
    public function setUserAgent($userAgent)
    {
        if (strlen($userAgent) > 0) {
           $this->_config->userAgent = $userAgent;
        }

        return $this;
    }

    /**
     * get params from last http-query
     *
     * @return array
     */
    public function getLastParams()
    {
        return $this->_lastParams;
    }

    public function getLastUrl()
    {
        return $this->_lastUrl;
    }

    /**
     * returns client-id
     *
     * @return string
     */
    public function getId()
    {
        if ($this->_id != null) {
            return $this->_id;
        }
        return get_class($this);
    }

    /**
     * set client-id
     *
     * @param string $id
     * @return Core_Class_HttpClient_Abstract
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * perform http-request with curl
     * use default application cache
     *
     * @param string $url
     * @param array $data
     * @return string
     */
    protected function _request($url, $data = array())
    {
        Core_Class_Statistic::api($this->getId(), Core_Class_Statistic::API_REQUEST);

        // get curl-handle for request
        $handle = $this->_getHandle($url, $data);

        // execute http request
        $result = curl_exec($handle);

        // profile response
        $this->getProfiler()->add($handle);

        // statistics
        Core_Class_Statistic::api($this->getId(), Core_Class_Statistic::API_RESPONSE, curl_getinfo($handle, CURLINFO_TOTAL_TIME), true);

        if (curl_errno($handle) == CURLE_OPERATION_TIMEOUTED) {
            Core_Class_Statistic::api($this->getId(), Core_Class_Statistic::API_TIMEOUT);
        }

        // close handle
        curl_close($handle);

        return $result;
    }

    protected function _getHandle($url, $data = array())
    {
        if (isset($this->_config->params)) {
            $data = array_merge($this->_config->params->toArray(), $data);
        }

        $this->_lastParams = $data;

        $params = $this->_buildQuery($data);
        $url = $url . '?' . implode('&', $params);

        $url = $this->_modifyUrl($url);

        $this->_lastUrl = $url;
        Core_Class_Log::getInstance()->debug('request: ' . $url);

        $handle = curl_init($url);
        curl_setopt($handle, CURLOPT_USERAGENT, $this->_config->userAgent);
        curl_setopt($handle, CURLOPT_TIMEOUT, $this->_config->timeout);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($handle, CURLOPT_FOLLOWLOCATION, 1);

        return $handle;
    }

    /**
     * bietet die möglichkeit einen hash auf die url zu berechnen und anzuhängen o.ä.
     *
     * @param string $url
     * @return string
     */
    protected function _modifyUrl($url)
    {
        return $url;
    }

    protected function _buildQuery($data)
    {
        $params = array();
        if (is_array($data)) {
            foreach ($data as $k => $v) {
                if (is_array($v)) {
                    foreach ($v as $sub) {
                        $params[] = $k . '=' . urlencode($sub);
                    }
                } else {
                    $params[] = $k . '=' . urlencode($v);
                }
            }
        }
        return $params;
    }
}
