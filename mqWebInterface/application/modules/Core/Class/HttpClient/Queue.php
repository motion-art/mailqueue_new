<?php

class Core_Class_HttpClient_Queue
{
    private $_items = array();
    private $_config = null;
    private $_profiler = null;
    private $_cache = null;

    public function __construct()
    {
        $this->_config = Zend_Registry::get('_CONFIG')->httpqueue;
        $this->_cache = Core_Class_Cache::getCache(Core_Class_Cache::CACHE_NAME_HTTP);
    }

    public function add($object, $method, $params = array())
    {
        $id = count($this->_items);
        $this->_items[$id] = new Core_Class_HttpClient_Queue_Item($object, $method, $this->_cache, $params);

        return $id;
    }

    public function getProfiler()
    {
        if ($this->_profiler == null) {
            $this->_profiler = new Core_Class_HttpClient_Profiler_Firebug(get_class($this));
        }
        if (isset($this->_config->profiler)) {
            $this->_profiler->setEnabled($this->_config->profiler);
        }
        return $this->_profiler;
    }

    public function count()
    {
        return count($this->_items);
    }

    public function getResult($id)
    {
        if (array_key_exists($id, $this->_items)) {
            return $this->_items[$id]->getResult();
        }
        return null;
    }

    public function execute()
    {
        $multi = false;
        $handle = curl_multi_init();

        foreach ($this->_items as $item) {

            if ($item->isMulti() && !$item->isCached()) {

                curl_multi_add_handle($handle, $item->getHandle());
                $item->setActive(true);
                $multi = true;

            } else {

                if (!$item->isMulti()) {
                    Core_Class_Log::getInstance()->debug('multi request not valid (multi interface expected): ' . (string) $item);
                }

                try {
                    $item->execute();
                } catch (Exception $e) {
                    Core_Class_Log::getInstance()->crit($e);
                }
            }
        }

        // no multi-requests
        if (!$multi) {
            curl_multi_close($handle);
            return;
        }

        $active = null;

        do {
            $mrc = curl_multi_exec($handle, $active);
        } while ($mrc == CURLM_CALL_MULTI_PERFORM);

        while ($active && $mrc == CURLM_OK) {
            if (curl_multi_select($handle) != -1) {
                do {
                    $mrc = curl_multi_exec($handle, $active);
                } while ($mrc == CURLM_CALL_MULTI_PERFORM);
            }
        }

        foreach ($this->_items as $item) {

            if ($item->isActive()) {

                curl_multi_remove_handle($handle, $item->getHandle());
                $this->getProfiler()->add($item->getHandle());
                $item->setActive(false);

                try {
                    $item->mapResult(curl_multi_getcontent($item->getHandle()));
                } catch (Exception $e) {
                    Core_Class_Log::getInstance()->crit($e);
                }
            }
        }

        curl_multi_close($handle);
    }
}
