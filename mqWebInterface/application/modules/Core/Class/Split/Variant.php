<?php

class Core_Class_Split_Variant
{
    protected $_id = null;
    protected $_info = null;
    protected $_test = false;
    protected $_enabled = false;
    protected $_chance = 0;
    protected $_prefix = null;
    protected $_module = null;
    protected $_template = null;

    public function __construct(SimpleXMLElement $config)
    {
        $this->_id      = (int) current($config->xpath('@id'));
        $this->_info    = (string) $config->info;
        if (isset($config->module)) {
            $this->_module  = (string) $config->module;
        }
        if (isset($config->template)) {
            $this->_template  = $config->template;
        }
        $this->_enabled = (int) current($config->xpath('@enabled'));
        $this->_test    = (int) current($config->xpath('@test'));
        $this->_chance  = (string) $config->chance;
        $this->_prefix  = (string) $config->prefix;
    }

    public function isModuleSplit()
    {
        return isset($this->_module);
    }

    public function isTemplateSplit()
    {
        return isset($this->_template);
    }

    public function getOriginal()
    {
        if ($this->isTemplateSplit()) {
            return (string) $this->_template->original;
        }
        return null;
    }

    public function getReplacement()
    {
        if ($this->isTemplateSplit()) {
            return (string) $this->_template->replacement;
        }
        return null;
    }

    public function getChance()
    {
        return $this->_chance;
    }

    public function isEnabled()
    {
        return $this->_enabled;
    }

    public function getId()
    {
        return $this->_id;
    }

    public function getModule()
    {
        return $this->_module;
    }

    public function getTrackingPrefix()
    {
        return $this->_prefix;
    }

    public function isTest()
    {
        return $this->_test;
    }
}
