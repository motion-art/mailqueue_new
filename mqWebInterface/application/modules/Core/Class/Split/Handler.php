<?php

class Core_Class_Split_Handler
{
    const DEFAULT_TRACKING_PREFIX = 'default';
    const MAX_VARIANT_RENEW = 10;

    protected $_enabled = true;
    protected $_baseModule = 'default';
    protected $_splits = array();
    protected $_active = null;
    protected $_scripts = null;

     /**
     * Singleton instance
     *
     * @var Core_Class_Split_Handler
     */
    protected static $_instance = null;

    /**
     *
     */
    protected function __construct()
    {}

    /**
     * Enforce singleton; disallow cloning
     *
     * @return void
     */
    private function __clone()
    {}

    /**
     * Singleton instance
     *
     * @return Core_Class_Split_Handler
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    /**
     * Setzt das Splittest XML File zum Modul
     * @param string $splitXml
     */
    public function setSplitXml($xml)
    {
        if (!$xml = simplexml_load_file($xml)) {
            throw new Exception('Could not parse SplitXML');
        }

        if (!$xml->xpath('/split_tests/split_test')) {
            throw new Exception('No section setup in SplitXML');
        }

        if (current($xml->xpath('@enabled')) == false) {
            $this->_enabled = false;
            return;
        }

        if ($splits = $xml->xpath('/split_tests/split_test')) {
            foreach ($splits as $split) {
                $this->_initSplit($split);
            }
        }
    }

    public function isEnabled()
    {
        return $this->_enabled;
    }

    public function setBaseModule($module)
    {
        $this->_baseModule = $module;
    }

    public function setManuallySwitch($id = false)
    {
        //wenn keine Id oder false
        if (!$id) {
            return;
        }

        if ($splits = $this->_getActiveSplits()) {
            foreach ($splits as $split) {
                $split->setTest($id);
            }
        }
    }

    /**
     * Gibt den Namen des Splittmoduls zurück oder
     * false wenn Defaultmodul
     * @return string|boolean
     */
    public function getSplitModeModuleName()
    {
        if (!$this->_enabled) {
            return false;
        }

        if (!$splits = $this->_getActiveSplits()) {
            return false;
        }

        $module = false;
        foreach ($splits as $split) {

            if ($variant = $split->getVariant((boolean) $module)) {
                if (!$module && $variant->isModuleSplit()) {

                    $module = $variant->getModule();
                }
            }
        }

        return $module;
    }

    /**
     * Gibt den TrackingPrefix zurück,
     * welcher aus dem Splittest besteht
     *
     * @return string
     */
    public function getTrackingPrefix()
    {
        if ($splits = $this->_getActiveSplits()) {

            $prefix = '';
            foreach ($splits as $split) {
                $prefix .= $split->getTrackingPrefix();
            }
            return $prefix;
        }

        return self::DEFAULT_TRACKING_PREFIX;
    }

    public function getViewScript($path)
    {
        if ($this->_scripts === null) {

            $this->_scripts = array();

            if ($variants = $this->_getActiveVariants()) {
                foreach ($variants as $variant) {
                    if ($variant->isTemplateSplit()) {
                        $this->_scripts[$variant->getOriginal()] = $variant->getReplacement();
                    }
                }
            }
        }

        if (array_key_exists($path, $this->_scripts)) {
            return $this->_scripts[$path];
        }

        return $path;
    }

    /**
     * Liefert den aktuellen Split,
     * der für das übergebene Basismodul,
     * gesetzt ist. Liefert false wenn
     * kein Modul gesetzt ist.
     *
     * @param $module
     * @return string
     */
    public function getSplitModuleByModule($module)
    {
        if ($variants = $this->_getActiveVariants()) {
            foreach ($variants as $variant) {
                if ($variant->isModuleSplit() && strcasecmp($this->_baseModule, $module) === 0) {
                    return $variant->getModule();
                }
            }
        }

        return $module;
    }

    public function getSplits()
    {
        return $this->_splits;
    }

    private function _initSplit($config)
    {
        $this->_splits[] = new Core_Class_Split_Group($config);
    }

    private function _getActiveSplits()
    {
        if ($this->_active === null) {

            $this->_active = array();

            foreach ($this->_splits as $split) {

                if ($split->isEnabled() && $split->isValid()) {

                    $this->_active[] = $split;

                    if (!$split->isMulti()) {
                        break;
                    }
                }
            }
        }

        return $this->_active;
    }

    private function _getActiveVariants()
    {
        if ($active = $this->_getActiveSplits()) {

            $result = array();

            foreach ($active as $split) {
                $result[] = $split->getVariant();
            }

            return $result;
        }

        return null;
    }
}
