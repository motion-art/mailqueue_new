<?php

class Core_Class_Split_Group
{
    const DEFAULT_SPLIT_ID = 'DEFAULT';

    protected $_validators = array();
    protected $_variants = array();
    protected $_info = null;
    protected $_cookieLiveTime = 3600;
    protected $_enabled = false;
    protected $_rate = 0;
    protected $_prefix = null;
    protected $_defaultPrefix = null;
    protected $_active = null;
    protected $_multi = false;

    public function __construct(SimpleXMLElement $xml)
    {
        if (!isset($xml->setup)) {
            throw new Exception('split setup expected');
        }
        $setup = $xml->setup;

        $this->_info            = (string) $setup->info;
        $this->_cookieLiveTime  = (string) $setup->cookie_liveTime;
        $this->_enabled         = (string) $setup->enabled;
        $this->_rate            = (string) $setup->splitting_rate;
        $this->_defaultPrefix   = (string) $setup->default_prefix;
        $this->_prefix          = (string) $setup->prefix;
        $this->_multi          = (int) $setup->multi;

        if ($this->_enabled) {
            if (!isset($xml->variant)) {
                $this->_enabled = false;

            } else {

                foreach ($xml->variant as $variant) {
                    $this->addVariant($variant);
                }

                if (isset($setup->validators) && isset($setup->validators->validator)) {
                    foreach ($setup->validators->validator as $validator) {
                        $this->addValidator((string) current($validator->xpath('@name')), $validator->options);
                    }
                }
            }
        }
    }

    public function isMulti()
    {
        return $this->_multi;
    }

    public function isValid()
    {
        foreach ($this->_validators as $validator) {
            if (!$validator->isValid()) {
                return false;
            }
        }

        return true;
    }

    public function isEnabled()
    {
        return $this->_enabled;
    }

    public function getTrackingPrefix()
    {
        if ($variant = $this->getVariant()) {
            return $this->_prefix . '_' . $variant->getTrackingPrefix();
        }
        return $this->_defaultPrefix;
    }

    public function setTest($id)
    {
        if ($id == self::DEFAULT_SPLIT_ID) {
            $this->_active = new Core_Class_Split_Variant_Default();
            $this->_writeCookieInfo(self::DEFAULT_SPLIT_ID, 3600);
            return $this;
        }
        if ($variant = $this->getVariantById($id)) {
            if ($variant->isTest()) {

                $this->_active = $variant;

                // für testing zwecke cookielaufzeit geringer stellen
                // und wenn split existiert
                $this->_writeCookieInfo($this->_active->getId(), 3600);
            }
        }

        return $this;
    }

    public function getVariant($ignoreModuleSplits = false, $ignoreCookies = false)
    {
        if (!$this->_enabled) {
            return null;
        }
        if ($this->_active !== null) {

            return $this->_active;
        }

        // wenn cookie gesetzt und nicht manuelle Splitwahl
        // dann auf splitmodul wechseln
        if (
            !$ignoreCookies
            && ($id = $this->_readCookieInfos()) !== false
            && ($this->_active === null)
        ) {
            if ($id == self::DEFAULT_SPLIT_ID) {
                $this->_active = false;
            } else {
                $this->_active = $this->getVariantById($id);
            }
        }

        // splittest nicht gesetzt, per Zufall bestimmen
        // cookie wird gesezt !!
        if ($this->_active === null) {

            if ($variant = $this->_getVariantByRandom()) {

                $this->_active = $variant;

                // splitMode in cookie speichern
                $this->_writeCookieInfo($variant->getId());

            } else {

                $this->_writeCookieInfo(self::DEFAULT_SPLIT_ID);
                $this->_active = false;
            }
        }

        if ($this->_active) {
            if (!$this->_active->isTest() && !$this->_active->isEnabled()) {
                $this->_active = false;
            }
        }

        if ($this->_active === null || !$this->_active) {
            $this->_active = new Core_Class_Split_Variant_Default();
        }

        return $this->_active;
    }

    public function addVariant($split)
    {
        $this->_variants[] = new Core_Class_Split_Variant($split);
    }

    public function getVariants()
    {
        return $this->_variants;
    }

    public function addValidator($validator, $options = null)
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();

        $validator = 'Core_Class_Split_Validator_' . ucfirst($validator);
        if (!@class_exists($validator)) {
            throw new Exception('split validator not exists');
        }
        $validator = new $validator($request);
        $validator->setOptions($options);

        $this->_validators[] = $validator;
    }


    /**
     * Setzt die Split Cookie Infos
     *
     * @param string $value
     */
    private function _writeCookieInfo($value, $livetime = null)
    {
        if (!$livetime) {
            $livetime = $this->_cookieLiveTime;
        }
        setcookie('split_' . $this->_prefix, $value , time() + $livetime, '/');
    }

    /**
     * Gibt eine SplitModeId zurück oder
     * false wenn kein cookie vorhanden
     * @return string|boolean
     */
    private function _readCookieInfos()
    {
        if (isset($_COOKIE['split_' . $this->_prefix])) {
            return $_COOKIE['split_' . $this->_prefix];
        } else {
            return false;
        }
    }

    /**
     * Berechnet per Zufall den Testmode und gibt
     * den Node aus der Splitest.xml oder
     * false bei default zurück
     * @param $splittingRate
     * @return array|boolean
     */
    private function _getVariantByRandom()
    {
        //split oder default
        $random = mt_rand(1, 1000);
        if (($this->_rate * 10) < $random) {
            return false;
        }

        //gewinner unter den Splits ermitteln
        $variants = $this->_getVariants(true);

        $desider = array();
        foreach ($variants as $variant) {
            $c =  $variant->getChance();
            for ($k=0 ; $k < $c ; $k++) {
                $desider[] = $variant;
            }
        }

        if (count($desider)) {
            $random = mt_rand(0, count($desider)-1);
            $winner = $desider[$random];
        } else {
            return false;
        }

        return $winner;
    }

    private function _getVariants($enabled = false)
    {
        $result = array();

        foreach ($this->_variants as $variant) {
            if (!$enabled || $variant->isEnabled()) {
                $result[] = $variant;
            }
        }

        return $result;
    }

    public function getVariantById($id)
    {
        foreach ($this->_variants as $variant) {
            if ($variant->getId() == $id) {
                return $variant;
            }
        }

        return null;
    }
}
