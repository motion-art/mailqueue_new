<?php

class Core_Class_Split_Variant_Default extends Core_Class_Split_Variant
{
    protected $_id = null;
    protected $_info = null;
    protected $_test = true;
    protected $_enabled = true;
    protected $_chance = 1;
    protected $_prefix = 'default';
    protected $_module = false;

    public function __construct()
    {}

    public function isModuleSplit()
    {
        return false;
    }

    public function isTemplateSplit()
    {
        return false;
    }
}
