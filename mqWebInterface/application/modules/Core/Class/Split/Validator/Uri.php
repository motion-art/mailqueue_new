<?php

class Core_Class_Split_Validator_Uri
    extends Core_Class_Split_Validator_Abstract
    implements Core_Class_Split_Validator_Interface
{
    public function isValid()
    {
        if (preg_match('/^\/index\/conversion/', $_SERVER['REQUEST_URI'])) {
            return true;
        }

        return preg_match(
            (string) $this->_options->pattern,
            $_SERVER['REQUEST_URI']
        );
    }
}
