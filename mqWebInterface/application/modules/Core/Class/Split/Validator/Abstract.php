<?php

abstract class Core_Class_Split_Validator_Abstract
{
    protected $_request = null;
    protected $_options = null;

    public function __construct(Zend_Controller_Request_Abstract $request)
    {
        $this->_request = $request;
    }

    public function setOptions(SimpleXMLElement $options)
    {
        $this->_options = $options;
    }
}
