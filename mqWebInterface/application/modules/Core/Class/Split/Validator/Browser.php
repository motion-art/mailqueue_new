<?php

class Core_Class_Split_Validator_Browser
    extends Core_Class_Split_Validator_Abstract
    implements Core_Class_Split_Validator_Interface
{
    public function isValid()
    {
        return preg_match(
            (string) $this->_options->pattern,
            $_SERVER['HTTP_USER_AGENT']
        );
        return true;
    }
}
