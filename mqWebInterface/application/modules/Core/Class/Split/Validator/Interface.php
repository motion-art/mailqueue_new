<?php

interface Core_Class_Split_Validator_Interface
{
    /**
     * returns http-handle (curl)
     *
     * @param string $url
     * @param array $data
     * @return int
     */
    public function isValid();
}
