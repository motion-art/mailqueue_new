<?php

class Core_Class_Seo_Network
{
    /**
     * Singleton instance
     *
     * @var Core_Class_Seo_Network
     */
    protected static $_instance = null;

    /**
     * @var Core_Class_Seo_Network_Adapter_Interface
     */
    protected $_network = null;

    public function __construct($options = null)
    {}

    /**
     * Returns an instance of Core_Class_Seo_Network
     *
     * Singleton pattern implementation
     *
     * @return Core_Class_Seo_Network
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }


    /**
     * Setzt den Contentnetzwerkadapter
     *
     * @param Core_Class_Seo_Network_Adapter_Interface $network
     * @return Core_Class_Seo_Network $this
     */
    public function setNetworkAdapter(Core_Class_Seo_Network_Adapter_Interface $network)
    {
        $this->_network = $network;
        return $this;
    }

    /**
     * Gibt den gesetzten Networkadapter zurück
     *
     * @return Core_Class_Seo_Network_Adapter_Interface
     */
    public function getNetworkAdapter()
    {
        return $this->_network;
    }

    /**
     * Gibt alle Rootpages zurück
     *
     * @return array of Core_Class_Seo_Network_Page_Interface
     */
    public function getRootPages()
    {
        return $this->_network->getRootPages();
    }

    /**
     * Gibt eine Seo-Page anhand ihrer Id zurück
     *
     * @param mixed $id
     * @return Core_Class_Seo_Network_Page_Interface
     */
    public function getPageById($id)
    {
        return $this->_network->getPageById($id);
    }

    /**
     * Gibt alle (spezielle) Rootpages zurück für das
     * Inhaltsverzeichnis zurück
     *
     * @return array of Core_Class_Seo_Network_Page_Interface
     */
    public function getDirectoryRootPages()
    {
        return $this->_network->getDirectoryRootPages();
    }

    /**
     * Gibt den Navigator zurück
     *
     * @return Zend_Navigation
     */
    public function getNavigator()
    {
        return $this->_network->getNavigator();
    }

    /**
     * setzt eine Page als aktiv
     *
     * @param int $id
     */
    public function setPageActive($id)
    {
        $this->_network->setPageActive($id);
    }
}
