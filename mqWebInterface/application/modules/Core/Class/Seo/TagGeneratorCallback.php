<?php

class Core_Class_Seo_TagGeneratorCallback
{
    public function formatLinks($treffer)
    {
        $pageId = $treffer[1];
        $anker = $treffer[2];
        $title = $treffer[3];
        $page = Core_Class_Seo_Network::getInstance()->getPageById($pageId);

        $values = array(
                    "anker" => $anker,
                    "title" => $title,
                    "link"  => $page->getHref(),
                  );

        return $values;
    }

    public function insertSearchTemplate($treffer)
    {
        return array();
    }
}