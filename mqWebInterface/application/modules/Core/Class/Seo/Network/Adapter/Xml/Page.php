<?php
/**
 * Die Klasse repräsentiert eine Seo-Page unter
 * zu Hilfenahme eines
 * Core_Class_Seo_Network_Adapter_Xml
 * @author r.gwizdziel
 * @version   $Id$
 * @copyright 2009 Unister GmbH
 */
class Core_Class_Seo_Network_Adapter_Xml_Page extends Core_Class_Seo_Network_Page_Abstract {

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Abstract#prepareData($data)
     */
    public function prepareData($data){
        if(! $data instanceof SimpleXMLElement ){
            throw new Core_Class_Seo_Network_Adapter_Xml_Exception("Page data type is not a SimpleXMLElement");
        }
        return $data;
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getId()
     */
    public function getId(){
        return (string) $this->_data['id'];
    }


    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getMetaDescription()
     */
    public function getMetaDescription(){
        return (string) $this->_data->metaDescription;
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getMetaKeywords()
     */
    public function getMetaKeywords(){
        return (string) $this->_data->metaKeywords;
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getMetaTitle()
     */
    public function getMetaTitle(){
        return (string) $this->_data->metaTitle;
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getLinkAnkerText()
     */
    public function getLinkAnkerText(){
        return (string) $this->_data->linkAnkerText;
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getLinkTitle()
     */
    public function getLinkTitle(){
        return (string) $this->_data->linkTitle;
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getLinkUrlSeoAdd()
     */
    public function getLinkUrlSeoAdd(){
        return $this->_encodeSeoAdd(
                        (string) $this->_data->linkUrlSeoAdd
                    );
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getPageHeadline()
     */
    public function getPageHeadline(){
        return (string) $this->_data->pageHeadline;
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Abstract#getPageRawContent()
     */
    public function getPageRawContent(){
        return (string) $this->_data->pageContent;
    }

    /**
     * Show only pages
     * with visible=1
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getSubPages()
     */
    public function getSubPages(){
        $allSubs = array();
        $subs = $this->_data->xpath('page[@visible="1"]');

        if( ! $subs ) return $allSubs;

        foreach($subs as $sub){
            $allSubs[] = new self($sub[0], $this->_linkBuilder , $this->_pageGenerator );
        }
        return $allSubs;
    }

    /**
     * Show all Subpages
     * visible = 1 und visible = 0
     * @return array
     */
    public function getAllSubPages(){
        $allSubs = array();
        $subs = $this->_data->xpath('page');

        if( ! $subs ) return $allSubs;

        foreach($subs as $sub){
            $allSubs[] = new self($sub[0], $this->_linkBuilder , $this->_pageGenerator );
        }
        return $allSubs;
    }

    /**
     * Helper - zum generieren des Seo Url Anhangs
     * @param string $name Seourl Anhang
     * @return string
     */
    private function _encodeSeoAdd($name){
        $string = htmlentities($name,ENT_COMPAT,'UTF-8');
        $string = preg_replace('/&([a-zA-Z])(acute|grave|circ|tilde);/','$1',$name);
        $string = html_entity_decode($string,ENT_COMPAT,'UTF-8');
        $string = preg_replace("/[^a-zA-ZäöüÄÖÜß&0-9|-]/i","-",$string);

        $map = array(
            'ä' => 'ae',
            'Ä' => 'Ae',
            'ß'=>'ss',
            'ö'=>'oe',
            'Ö'=>'Oe',
            'Ü'=>'Ue',
            'ü'=>'ue',
            '&'=>'und',
        );

        // Umlaute konvertieren
        $string = str_replace(array_keys($map), array_values($map), $string);
        $string = substr($string,0,200);
        $string.="---";
        $string = ereg_replace  ( "([\-]+)$"  , ""  , $string );
        $string = ereg_replace  ( "[\-]{2,}"  , "-"  , $string );
        return strtolower(urlencode($string));
    }

}
