<?php
class Core_Class_Seo_Network_Adapter_Xml_Importer_ImportFromCsv
{

    protected $_csv  = 'network.csv';
    protected $_xml  = 'network.xml';
    protected $_tree = null;
    protected $_elementBag = null;
    protected $_headlines = array();

    protected $_matchingNodes = array(
              "Artikelnummer"   => array("value"=> "id",              "isAttribute" =>1, "isCdata" => 0),
              "Mutterartikel"   => array("value"=> "parent",          "isAttribute" =>1, "isCdata" => 0),
              "visible"         => array("value"=> "visible",         "isAttribute" =>1, "isCdata" => 0),
              "Seitentitel"     => array("value"=> "metaTitle",       "isAttribute" =>0, "isCdata" => 0),
              "MetaDescription" => array("value"=> "metaDescription", "isAttribute" =>0, "isCdata" => 0),
              "MetaKeywords"    => array("value"=> "metaKeywords",    "isAttribute" =>0, "isCdata" => 0),
              "Ueberschrift"    => array("value"=> "pageHeadline",    "isAttribute" =>0, "isCdata" => 0),
              "AnkerText"       => array("value"=> "linkAnkerText",   "isAttribute" =>0, "isCdata" => 0),
              "LinkTitel"       => array("value"=> "linkTitle",       "isAttribute" =>0, "isCdata" => 0),
              "UrlText"         => array("value"=> "linkUrlSeoAdd",   "isAttribute" =>0, "isCdata" => 0),
              "ShortText"       => array("value"=> "pageContentShort","isAttribute" =>0, "isCdata" => 1),
              "Text"            => array("value"=> "pageContent",     "isAttribute" =>0, "isCdata" => 1),
              );

    /**
     * Setzt die zu importierend csv Datei
     * @param string $csv
     */
    public function setCvsInputFile($csv)
    {
        $this->_csv = $csv;
    }

    /**
     * Setzt die Xml datei in die das Netzwerk importiert wird
     * @param string $xml
     */
    public function setXmlOutputFile($xml)
    {
        $this->_xml = $xml;
    }


    public function run()
    {
        $this->_prepareCsv();
        $this->_createXml();
    }

    /**
     * nimmt die csv auseinander
     * ermittelt die beinhalteten Überschriften
     * und erstellt die Page - DomElemente (Konten page)
     * erstellt die Parent -> Kind Zuordungen
     */
    private function _prepareCsv(){
        $isFirstRow  = true;

        //durchlaufen der csv zeilenweise
        $handle = fopen($this->_csv,"r");
        while ( ($data = fgetcsv ($handle, 5000, ";",'"')) !== FALSE ) {
             //erste zeile - Überschriften sammeln
             if($isFirstRow){
                $isFirstRow = false;
                foreach($data as $key => $headline){
                    //existiert ein matching
                    if(!key_exists($headline,$this->_matchingNodes)){
                        throw new Exception("Headline $headline not defined");
                    }
                    //metadaten speichern
                    $this->_headlines[$key] = $this->_matchingNodes[$headline];
                }
             }
             //daten
             else{
                 $record = array();
                  foreach($data as $key => $entry){
                      $record[$this->_headlines[$key]['value']] = $entry;
                  }

                  //hierachiebaum erstellen
                 $elem = new DOMElement('page');
                 //speichert erstmal alle Seitendaten
                 $elem->recordBag = $record;
                 //zuordnung Seiten-Id => DomElement
                 $this->_elementBag[$record['id']] =  $elem ;

                 //zuordnung ParentId => Children DomElement
                 $this->_tree[$record['parent']][] =  $elem ;
             }
          }
          fclose($handle);
    }

    private function _createXml()
    {
        //xml-file generieren
        $xml = new DOMDocument('1.0', 'UTF-8');
        $xml->formatOutput = true;
        //oberste Element
        $root = $xml->appendChild(new DOMElement('contentnetwork'));

        //für jedes Element der obersten Ebene
        foreach($this->_tree as $parent=>$children){
            //rootelement bestimmen
            $parentElement =($parent)?$this->_elementBag[$parent]:$root;
            //kinden anhängen
            foreach($children as $child){

               $nodeData = $child->recordBag;

               //attribute anlegen
               $node = $parentElement->appendChild($child);
               foreach($this->_headlines as $headline){
                       //nur attribute anlegen
                       if($headline['isAttribute']){
                           $attributeName   = $headline['value'];
                           $attributeValue  = trim($nodeData[$attributeName]?$nodeData[$attributeName]:0);
                           $node->setAttribute($attributeName,$attributeValue);
                       }
               }

               //knoten anlegen
               foreach($this->_headlines as $headline){
                    //nur Konten anlegen
                    if(!$headline['isAttribute']){
                        $nodeName  = $headline['value'];
                        $nodeValue = trim($nodeData[$nodeName]);
                        //wenn nicht cdata
                        if(!$headline['isCdata']){
                            $node->appendChild(new DOMElement($nodeName))
                            ->appendChild(new DOMText($nodeValue));
                        }else{
                            $node->appendChild(new DOMElement($nodeName))
                            ->appendChild($node->ownerDocument->createCDATASection(stripslashes($nodeValue)));
                        }

                    }
               }

            }
        }
        $xml->save($this->_xml);
    }



}
