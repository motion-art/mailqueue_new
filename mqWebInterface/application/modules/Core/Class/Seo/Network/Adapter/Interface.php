<?php
interface Core_Class_Seo_Network_Adapter_Interface{

	/**
     * Konstruktor für den Adapter
     * 
     * @param $options Quellenspezifisch, z.Bsp. Dateiname zur Quelle
     * @param $linkBuilder
     * @param $pageGenerator
     */
    public function __construct($options = null, $linkBuilder = null, $pageGenerator = null);
	
    /**
     * Liefert alle Rootseiten zurück
     * Bedingung: Flag Visible ist true
     * 
     * @return array of Core_Class_Seo_Network_Page_Interface
     */
    public function getRootPages();

    /**
     * Liefert alle (spezielle) Rootseiten zum
     * Generieren eines Inhaltsverzeichnisses zurück
     * Flag Visible wird nicht beachtet
     * 
     * @return array of Core_Class_Seo_Network_Page_Interface
     */
    public function getDirectoryRootPages();
    
    /**
     * Liefert eine spezielle Seite anhand einer Id zurück
     * 
     * @param $id page id
     * @return Core_Class_Seo_Network_Page_Interface single page
     */
    public function getPageById($id);
    
    /**
     * Lädt die Quelle ein - je nach Adapter also als XML, aus einer DB etc.
     */
    public function loadPagesFromSource();
    
    /**
     * Seiteninformationen nachladen
     * 
     * @param pointer [Core_Class_Seo_Network_Page] $page
     */
    public function loadPageContent(&$page);
}
