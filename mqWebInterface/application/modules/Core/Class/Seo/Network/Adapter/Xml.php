<?php
/**
 * XML-Adapter eines SeoContentNetworks
 * @author r.gwizdziel
 * @version   $Id$
 * @copyright 2009 Unister GmbH
 */
class 
    Core_Class_Seo_Network_Adapter_Xml 
extends 
    Core_Class_Seo_Network_Adapter 
implements 
    Core_Class_Seo_Network_Adapter_Interface
{
    /**
     * XML-Inhalt
     * @var string
     */
    protected $_xml = null;
    
    /**
     * Dateiname der XML-Datei
     * @var string
     */
    protected $_xmlFile = null;

    /**
     * Konstruktor
     * 
     * options->xml = Pfad zur xml-Datei
     * Absolute Pfade beginnen mit "/"
     * ansonsten wird vom Modulpfad ausgegangen
     * 
     * @param Zend_Config $options
     */
    public function __construct($options = null, $linkBuilder = null, $pageGenerator = null){
        parent::__construct($options, $linkBuilder, $pageGenerator);
        
        //Dateiname für die XML
        $this->_xmlFile = (string) $options->xml;
        
        //XML direkt einlesen
        $this->loadPagesFromSource( );
    }
    
    /**
     * Lädt die xml-Datei in ein SimpleXmlElement Objekt
     * und sorgt für die Erstellung der Navigation
     * 
     * (non-PHPdoc)
     * @see application/modules/Dsl/Class/Seo/Network/Adapter/Core_Class_Seo_Network_Adapter_Interface#loadPagesFromSource()
     */
    public function loadPagesFromSource(){
        //wenn noch nicht geladen
        if(!$this->_xml){
            $xml = $this->_xmlFile;

            //wenn relativer pfad zum default Modul
            if($xml[0] != '/'){
                $xml = Core_Class_ResourcePath::getInstance()->getResource($xml);
            }
            
            //XML Datei existiert nicht
            if (!file_exists($xml) ){
                throw new Core_Class_Seo_Network_Adapter_Xml_Exception("Xml Datei existiert nicht: $xml");
            }   
            
            //Jetzt XML laden
            $this->_xml = simplexml_load_file($xml);

            //XML ungültig
            if(!$this->_xml){
                throw new Core_Class_Seo_Network_Adapter_Xml_Exception($xml .' file is not a valid xml file');
            }
            
            //Jetzt Struktur in einem simplen Array aufbauen
            $rootPages = $this->_xml->xpath('/contentnetwork/page');
            $rootNodes = array();
            foreach($rootPages as $page){
                $node     = $this->_createPage($page);
                $subNodes = $page->xpath('page');
                 
                foreach($subNodes as $subNode){
                    $this->_addSubPagesToArray($subNode,$node);
                }
                $rootNodes[] = $node;
            }
            
            //aus der Array-Struktur eine Navigation bauen 
            $this->_createNavigation($rootNodes);
        }
    }
    
   /**
     * Fügt Childrenseiten zu einer Array-Eintrag hinzu
     * wird von einer konkreten Quelle verwendet, NICHT vom Adapter selbst
     * 
     * @param array $page
     * @param array $parent
     */
    protected function _addSubPagesToArray($page, &$parent)
    {
        $node     = $this->_createPage($page);
        $subNodes = $page->xpath('page');

        foreach($subNodes as $subNode){
            $this->_addSubPagesToArray($subNode,$node);
        }
        $parent['pages'][] = $node;
    }
    
    /**
     * Fasst einen XML-Datensatz zu einem Array zusammen, der für Zend_Navigation nutzbar ist
     * Das Array wird an die gleiche Funktion des Parent-Objekts übergeben
     * 
     * @param $page
     * @return array
     */
    protected function _createPage($page){
        return parent::_createPage(
                array('id'              => (string)$page['id'],
                      'visible'         => (int)$page['visible'],
                      'label'           => (string)$page->linkAnkerText,
                      'params' => array('pageId'            => (string)$page['id'],
                                        'linkAnkerText'     => (string)$page->linkAnkerText,
                                        'linkTitle'         => (string)$page->linkTitle,
                                        'linkUrlSeoAdd'     => (string)$page->linkUrlSeoAdd,
                    )
                ));
    }
    
    /**
     * Pageinformationen ergänzen
     * 
     * (non-PHPdoc)
     * @see application/modules/Dsl/Class/Seo/Network/Core_Class_Seo_Network_Adapter#loadPageContent($page)
     */
    public function loadPageContent(&$page){
        if(!$this->_xml){
            //XML nicht geladen
            return false;
        }
        
        $id = $page->getId();
        
        //Abschnitt aus der XML für die korrekte Seite
        $xmlPage = $this->_xml->xpath('//page[@id='.$id.']');
        if (!$xmlPage[0]){
            return false;
        }
        
        $page->setParams(
            array_merge($page->params, 
                array(
                    'pageHeadline'      => (string)$xmlPage[0]->pageHeadline,
                    'pageContent'       => (string)$xmlPage[0]->pageContent,
                    'metaTitle'         => (string)$xmlPage[0]->metaTitle,
                    'metaKeywords'      => (string)$xmlPage[0]->metaKeywords,
                    'metaDescription'   => (string)$xmlPage[0]->metaDescription
                )
            ));
    }
}