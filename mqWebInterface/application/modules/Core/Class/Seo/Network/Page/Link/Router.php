<?php

/**
 * LinkBuilder
 * zum bauen von Links zu Seo-Seiten
 * anhand einer Route
 * @author r.gwizdziel
 * @version   $Id$
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Seo_Network_Page_Link_Router
    implements Core_Class_Seo_Network_Page_Link_Interface
{
    /**
     * Name der Route im Router
     * @var string
     */
    protected $_routeName = 'default';

    /**
     * Konstruktor
     * options->routeName 	= Name der Route
     * @param Zend_Config $options
     */
    public function __construct($options)
    {
        $this->_routeName = $options->routeName;
    }

    /**
     * Gibt die Url zur SeoPage zurück
     * @param Core_Class_Seo_Network_Page_Abstract $page
     * @return string url zur Page
     * (non-PHPdoc)
     * @see Network/Page/Link/Core_Class_Seo_Network_Page_Link_Interface#build($page)
     */
    public function build(Core_Class_Seo_Network_Page_Abstract $page)
    {

        $router = Zend_Controller_Front::getInstance()->getRouter();
        $url = $router->assemble(
                    array(
                        "pageId"		=> $page->getId(),
                        "linkUrlSeoAdd"	=> $page->getLinkUrlSeoAdd()
                    ),
                    $this->_routeName,
                    true
                );

        return $url;
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Link/Core_Class_Seo_Network_Page_Link_Interface#getDirectoryUrl()
     */
    public function getDirectoryUrl()
    {
        $router = Zend_Controller_Front::getInstance()->getRouter();
        $url = $router->assemble(
                    array(
                        "pageId"		=> "",
                        "linkUrlSeoAdd"	=> ""
                    ),
                    $this->_routeName,
                    true
                );

        return $url;
    }
}
