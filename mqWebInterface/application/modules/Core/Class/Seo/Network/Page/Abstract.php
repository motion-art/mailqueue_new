<?php

/**
 * Diese abstrakte Klasse implementiert die Funktionalität
 * einer Seo-Page mit LinkBuilders und PageContentGenerator
 * @author r.gwizdziel
 * @version   $Id$
 * @copyright 2009 Unister GmbH
 */

abstract class Core_Class_Seo_Network_Page_Abstract
extends Zend_Navigation_Page_Mvc
implements Core_Class_Seo_Network_Page_Interface
{
    /**
     * LinkBuilder
     * @var Core_Class_Seo_Network_Page_Link_Interface
     */
    protected $_linkBuilder = null;

    /**
     * PageContentGenerator
     * @var Core_Class_Seo_Network_Page_Generator_Interface
     */
    protected $_pageGenerator = null;

    /**
     * Referenz zum Netzwerkadapter
     * @var Core_Class_Seo_Network_Adapter
     */
    protected $_network = null;

    /**
     * Konstruktor
     * @param mixed $data ContentNetworkData
     * @param Core_Class_Seo_Network_Page_Link_Interface $linkBuilder
     * @param Core_Class_Seo_Network_Page_Generator_Interface $pageGenerator
     */
    public function __construct($data, $linkBuilder = null, $pageGenerator = null)
    {
        //Navigationsobjekt Konstruktor
        parent::__construct($data);

        // LinkBuilder setzen
        if ($linkBuilder instanceof Core_Class_Seo_Network_Page_Link_Interface) {
            $this->_linkBuilder = $linkBuilder;
        }

        // PageContentGenerator setzen
        if ($pageGenerator instanceof Core_Class_Seo_Network_Page_Generator_Interface) {
            $this->_pageGenerator = $pageGenerator;
        }
    }

    /**
     * Gibt den unbearbeiteten Page Content zurück
     * @return string
     */
    abstract function getPageRawContent();

    /**
     * Baut den Link mittels LinkBuilder zusammen
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getLinkUrl()
     */
    public function getLinkUrl()
    {
        if ($this->_linkBuilder) {
            return $this->_linkBuilder->build($this);
        }
        return $this->getLinkUrlSeoAdd();
    }

    /**
     * Baut den Seiten-Content mittels PageGenerator
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getPageContent()
     */
    public function getPageContent()
    {
        if ($this->_pageGenerator) {
            return $this->_pageGenerator->generate($this);
        } else {
            return $this->getPageRawContent();
        }
    }

    /**
     * Ermöglicht die Übergabe eines Netzwerkadapters
     *
     * @param reference $networkAdapter must be subclass of Core_Class_Seo_Network_Adapter
     */
    public function setNetworkAdapter(&$networkAdapter)
    {
        // Netzwerkadapter setzen
        if (is_subclass_of($networkAdapter, 'Core_Class_Seo_Network_Adapter')) {
            $this->_network = &$networkAdapter;
        }
    }

    /**
     * hängt einen Slash ans ende an, wenn nicht vorhanden
     * (non-PHPdoc)
     * @see library/Zend/Navigation/Page/Zend_Navigation_Page_Mvc#getHref()
     */
    public function getHref()
    {
        return rtrim(parent::getHref(),"/") . "/";
    }
}