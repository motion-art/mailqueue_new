<?php

interface Core_Class_Seo_Network_Page_Interface
{
    /**
     * Gibt die Id der Seite zurück
     * @return string
     */
    public function getId();

    /**
     * Gibt die Metadescription der Seite aus
     * <meta name="description" content="???" />
     * @return string
     */
    public function getMetaDescription();

    /**
     * Gibt die Metakeywords der Seite aus
     * <meta name="keywords" content="???" />
     * @return string
     */
    public function getMetaKeywords();

    /**
     * Gibt den Seitentitel aus
     * <title>???</title>
     * @return string
     */
    public function getMetaTitle();

    /**
     * Gibt den Ankertext des Links
     * zur Page aus
     * <a>???</a>
     * @return string
     */
    public function getLinkAnkerText();

    /**
     * Gibt den Title des Links
     * zur Page aus
     * <a href="..." title="???"
     * @return string
     */
    public function getLinkTitle();

    /**
     * Gibt den speziellen (SEO-) Zusatz für den Link
     * zur Page aus
     * <a href="../../???/.." ...
     * @return string
     */
    public function getLinkUrlSeoAdd();

    /**
     * Gibt den Url des Links
     * zur Page aus
     * <a href="???" ...
     * @return string
     */
    public function getLinkUrl();

    /**
     * Gibt die Überschrift der Page zurück
     * @return string
     */
    public function getPageHeadline();

    /**
     * Gibt den Inhalt der Seite zurück
     * @return string
     */
    public function getPageContent();

    /**
     * Gibt alle Unterseiten der Seite zurück
     * @return array of Core_Class_Seo_Network_Page_Interface
     */
    public function getSubPages();
}