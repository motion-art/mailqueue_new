<?php

/**
 * Interface: Ein Generator zum Generieren/Verarbeiten von
 * Content einer Seo-Seite
 * @author r.gwizdziel
 * @version   $Id$
 * @copyright 2009 Unister GmbH
 */

interface Core_Class_Seo_Network_Page_Generator_Interface
{
    /**
     * Liefert den generierten/verarbeiten Content einer Seo-Seite
     * siehe: Core_Class_Seo_Network_Page_Abstract#getPageContent()
     * @param Core_Class_Seo_Network_Page_Abstract $page
     * @return string
     */
    public function generate(Core_Class_Seo_Network_Page_Abstract $page);
}
