<?php

/**
 * Interface für LinkBuilder
 * zum bauen von Links zu Seo-Seiten
 * @author r.gwizdziel
 * @version   $Id$
 * @copyright 2009 Unister GmbH
 */

interface Core_Class_Seo_Network_Page_Link_Interface
{
    /**
     * Liefert den Link zu einer seo-seite zurï¿½ck
     * @param Core_Class_Seo_Network_Page_Abstract $page
     * @return string
     */
    public function build(Core_Class_Seo_Network_Page_Abstract $page);

    /**
     * Liefert die Url zum Inhaltsverzeichnis zurï¿½ck
     * @return string
     */
    public function getDirectoryUrl();
}
