<?php

/**
 * Klasse ist dafür zuständig, Tags mittels regulärer Ausdrücke
 * zu finden und diese durch Templates zu erstezen
 * @author r.gwizdziel
 * @version   $Id$
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Seo_Network_Page_Generator_Tags
    implements Core_Class_Seo_Network_Page_Generator_Interface
{
    /**
     * @var Zend_Config
     */
    protected $_options;

    /**
     * @var Zend_View_Abstract $_view
     */
    protected $_view;

    /**
     * Konstruktor
     * Beispiel für options:
     * options->tags->0->regex             = "/\[#(.*?);(.*?);(.*?)#\]/"
     * options->tags->0->template          = "seocontent/generator/link.phtml"
     * options->tags->0->callback.class    = "Core_Class_Seo_MyInterpreter"
     * options->tags->0->callback.function = "formatLinks"
     * Die aufgerufene Callback Funktion stellt die Daten für das
     * Template bereit. Die Callback Funktion bekommt als Parameter die Treffer
     * des regulären Ausdrucks
     * siehe preg_replace_callback
     * @param Zend_Config $options
     */
    public function __construct($options)
    {
        $this->_options = $options;

        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper( 'viewRenderer' );
        $this->_view  = $viewRenderer->view;
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Generator/Core_Class_Seo_Network_Page_Generator_Interface#generate($page)
     */
    public function generate(Core_Class_Seo_Network_Page_Abstract $page)
    {
        $data = $page->getPageRawContent();
        $data = $this->_run($data);
        return $data;
    }

   /**
    * Ersetzt Tags (reguläre Ausdrücke) durch Templates. Die Templates bekommen
    * als Daten den Rückgabeqwert der CallBackFunction
    * @param $data
    * @return string
    */
    private function _run($data)
    {
        try{
            foreach($this->_options->tags as $tag){
                $regex           = $tag->regex;
                $this->_callback = array(
                                       new $tag->callback->class,
                                       $tag->callback->function
                                   );
                $this->_template = $tag->template;

                $data = preg_replace_callback(
                    $regex,
                    array($this,'_replaceTag'),
                    $data
                );

            }
        } catch (Exception $e){
            Core_Class_Log::getInstance()->err($e);
            return null;
        }
        return $data;
    }

    /**
     * Übergibt $treffer an die CallBackFunction
     * und rendert das Template mit den Daten die die
     * CallBackFunction als Rückgabewert hat
     * @param array $treffer eines preg_replace_callback aufrufes
     * @return string
     */
    public function _replaceTag($treffer)
    {
        $values = call_user_func_array($this->_callback, array($treffer));
        return $this->_view->partial($this->_template, $values);
    }
}
