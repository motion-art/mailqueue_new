<?php

/**
 * Die Klasse repräsentiert eine Seo-Page unter
 * zu Hilfenahme eines
 * Core_Class_Seo_Network_Adapter_Xml
 * @author r.gwizdziel
 * @version   $Id$
 * @copyright 2009 Unister GmbH
 *
 * Grund-Daten des Objekts: id, visible
 * Params (bei Initialisierung dabei): pageId, linkAnkerText, linkTitle, linkUrlSeoAdd
 * Params (nachzuladener Content): pageHeadline, pageContent, metaTitle, metaDescription, metaKeywords
 */

class Core_Class_Seo_Network_Page
    extends Core_Class_Seo_Network_Page_Abstract
{
    /**
     * Konstruktor
     */
    public function __construct($data, $linkBuilder = null, $pageGenerator = null)
    {
        //Encodierung von "linkUrlSeoAdd" vornehmen, damit die Urls alle gleich aussehen
        $data['params']['linkUrlSeoAdd'] = $this->_encodeSeoAdd($data['params']['linkUrlSeoAdd']);

        //Konstruktor der abstrakten Klasse
        parent::__construct($data, $linkBuilder, $pageGenerator);
    }

    /**
     * Gibt die in $key angegebene Eigenschaft aus den Params zurück
     *
     * @param string $key
     * @return string
     */
    protected function _getParam($key)
    {
        if (array_key_exists($key, $this->params)){
            //Wert vorhanden
            return $this->params[$key];
        }

        //Inhalt nicht vorhanden, Inhalt jetzt nachladen
        if (!is_null($this->_network)) {
            $this->_network->loadPageContent($this); //direkt den Netzwerkadapter ansprechen und Content nachladen
        }

        //Inhalt zurückgeben
        return array_key_exists($key, $this->params) ? $this->params[$key] : '';
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getId()
     */
    public function getId()
    {
        return (string) $this->_getParam('pageId');
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getMetaDescription()
     */
    public function getMetaDescription(){
        return (string) $this->_getParam('metaDescription');
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getMetaKeywords()
     */
    public function getMetaKeywords()
    {
        return (string) $this->_getParam('metaKeywords');
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getMetaTitle()
     */
    public function getMetaTitle()
    {
        return (string) $this->_getParam('metaTitle');
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getLinkAnkerText()
     */
    public function getLinkAnkerText()
    {
        return (string) $this->_getParam('linkAnkerText');
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getLinkTitle()
     */
    public function getLinkTitle()
    {
        return (string) $this->_getParam('linkTitle');
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getLinkUrlSeoAdd()
     */
    public function getLinkUrlSeoAdd()
    {
        return (string) $this->_getParam('linkUrlSeoAdd');
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getPageHeadline()
     */
    public function getPageHeadline()
    {
        return (string) $this->_getParam('pageHeadline');
    }

    /**
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Abstract#getPageRawContent()
     */
    public function getPageRawContent()
    {
        return (string) $this->_getParam('pageContent');
    }

    /**
     * Show only pages
     * with visible=1
     *
     * (non-PHPdoc)
     * @see Network/Page/Core_Class_Seo_Network_Page_Interface#getSubPages()
     */
    public function getSubPages()
    {
        $list = $this->getPages();

        $pages = array();
        foreach ($list as $item) {
            if (!$item->visible) {
                continue;
            }
            $pages[] = $item;
        }
        return $pages;
    }

    /**
     * Show all Subpages
     * visible = 1 und visible = 0
     *
     * @return array
     */
    public function getAllSubPages()
    {
        $list = $this->getPages();

        $pages = array();
        foreach ($list as $item) {
            $pages[] = $item;
        }
        return $pages;
    }

    /**
     * Helper - zum generieren des Seo Url Anhangs
     * @param string $name Seourl Anhang
     * @return string
     */
    private function _encodeSeoAdd($name)
    {
        $string = htmlentities($name, ENT_COMPAT, 'UTF-8');
        $string = preg_replace('/&([a-zA-Z])(acute|grave|circ|tilde);/', '$1', $name);
        $string = html_entity_decode($string, ENT_COMPAT, 'UTF-8');
        $string = preg_replace("/[^a-zA-ZäöüÄÖÜß&0-9|-]/i", "-", $string);

        $map = array(
            'ä' => 'ae',
            'Ä' => 'Ae',
            'ß'=>'ss',
            'ö'=>'oe',
            'Ö'=>'Oe',
            'Ü'=>'Ue',
            'ü'=>'ue',
            '&'=>'und',
        );

        // Umlaute konvertieren
        $string = str_replace(array_keys($map), array_values($map), $string);
        $string = substr($string, 0, 200);
        $string.="---";
        $string = ereg_replace("([\-]+)$", "", $string);
        $string = ereg_replace("[\-]{2,}", "-", $string);
        return strtolower(urlencode($string));
    }
}