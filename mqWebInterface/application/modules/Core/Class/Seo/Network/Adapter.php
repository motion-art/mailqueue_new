<?php

class Core_Class_Seo_Network_Adapter
    implements Core_Class_Seo_Network_Adapter_Interface
{
    /**
     * Singleton instance
     *
     * @var Core_Class_Seo_Network
     */
    protected static $_instance = null;

    /**
     * Hält den kompletten Seitenstrukturbaum
     * @var Zend_Navigation
     */
    protected $_navigator = null;

    /**
     * @var Core_Class_Seo_Network_Link_Builder_Interface
     */
    protected $_linkBuilder = null;

    /**
     * @var Core_Class_Seo_Network_Page_Generator_Interface
     */
    protected $_pageGenerator = null;

    /**
     * Konstruktor
     *
     * @param $options
     * @param $linkBuilder
     * @param $pageGenerator
     */
    public function __construct($options = null, $linkBuilder = null, $pageGenerator = null)
    {
        $this->_linkBuilder = $linkBuilder;
        $this->_pageGenerator = $pageGenerator;
    }

    /**
     * Returns an instance of Core_Class_Seo_Network
     *
     * Singleton pattern implementation
     *
     * @return Core_Class_Seo_Network
     */
    public static function getInstance()
    {
        if (null === self::$_instance) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Erstellt die Navigation anhand einer Navigationsstruktur als Array
     * Ab hier wird dann Zend_Navigation genutzt
     *
     * @param array $tree
     */
    protected function _createNavigation($tree = array())
    {
        //Jetzt die Rootpages erstellen
        $rootPages = array();

        foreach ($tree as $rootPage) {
            //Unterelemente extrahieren aus dem RootElement
            $children = array_key_exists('pages', $rootPage) ? $rootPage['pages'] : array();
            unset($rootPage['pages']);

            $currentRootPage = new Core_Class_Seo_Network_Page(
                $rootPage,
                $this->_linkBuilder, $this->_pageGenerator
            );
            $currentRootPage->setNetworkAdapter($this);

            //Nach Kindern suchen und wenn vorhanden anhängen
            $this->_addChildrenToPage($children, $currentRootPage);

            $rootPages[] = $currentRootPage;
        }

        //Navigator erstellen
        $this->_navigator = new Zend_Navigation($rootPages);
    }

    /**
     * Sucht alle Children einer Page und fügt diese hinzu
     *
     * @param array $children
     * @param $parent
     */
    protected function _addChildrenToPage($children = array(), $parent)
    {
        foreach ($children as $child) {
            $childrenPages = array_key_exists('pages', $child) ? $child['pages'] : array();
            unset($child['pages']);

            $page = new Core_Class_Seo_Network_Page(
                $child,
                $this->_linkBuilder,
                $this->_pageGenerator
            );
            $page->setNetworkAdapter($this);

            if (count($childrenPages) > 0) {
                //wenn weitere Kinder vorhanden sind, dann diese durchgehen und hinzufügen
                $this->_addChildrenToPage($childrenPages, $page);
            }

            //aktuelle Page an die Elternpage anhängen
            $parent->addPage($page);
        }
    }

    /**
     * Fasst einen Datensatz zu einem Array zusammen, der für Zend_Navigation nutzbar ist
     *
     * @param $page
     * @return array
     */
    protected function _createPage($page = array())
    {
        return array_merge($page, array(
                                    'route' => 'seoContentNetwork',
                                    'type' => 'Core_Class_Seo_Network_Page',
                                    'root' => true,
                                ));
    }

    /**
     * Gibt alle Rottpages zurück
     * Eigenschaft Visible wird nicht beachtet
     *
     * (non-PHPdoc)
     * @see Network/Adapter/Core_Class_Seo_Network_Adapter_Interface#getDirectoryRootPages($linkBuilder, $pageGenerator)
     */
    public function getDirectoryRootPages()
    {
        $pages = array();
        foreach ($this->_navigator as $item) {
            $pages[] = $item;
        }
        return $pages;
    }

    /**
     * Gibt alle Rootpages zurück
     * Visible muss true sein
     *
     * @return array of Core_Class_Seo_Network_Page_Interface
     */
    public function getRootPages()
    {
        $pages = array();

        //bestimmen der Spezielle Rootpages
        //und ausschluss aus der Navi
        $config = Zend_Registry::get('_CONFIG');
        if (isset($config->seo->network->specialPage)) {
            $specialPageIds = $config->seo->network->specialPage->toArray();
        }

        foreach ($this->_navigator as $item) {
            if (!$item->visible || in_array($item->id,$specialPageIds)) {
                //Nur sichtbare Seiten
                continue;
            }
            $pages[] = $item;
        }
        return $pages;
    }

    /**
     * Gibt eine spezielle Seite zurück
     *
     * (non-PHPdoc)
     * @see application/modules/Core/Class/Seo/Network/Adapter/Core_Class_Seo_Network_Adapter_Interface#getPageById($id)
     */
    public function getPageById($id)
    {
        $page = $this->_navigator->findOneBy('id', $id);
        if (!is_null($page)) {
            //Seite existiert
            return $page;
        }

        //Seite existiert nicht
        throw new Core_Class_Seo_Network_Adapter_Exception('Page object for id '.$id.' not found.');
    }

    /**
     * Markiert eine Page als aktiv
     *
     * @param int $id
     */
    public function setPageActive($id)
    {
        $page = $this->_navigator->findOneBy('id', $id);
        $page->setActive();
    }

    /**
     * Gibt den Navigator zurück
     *
     * @return Zend_Navigation
     */
    public function getNavigator()
    {
        return $this->_navigator;
    }

    /**
     * (non-PHPdoc)
     * @see application/modules/Core/Class/Seo/Network/Adapter/Core_Class_Seo_Network_Adapter_Interface#loadPagesFromSource()
     */
    public function loadPagesFromSource()
    {}

    /**
     * (non-PHPdoc)
     * @see application/modules/Core/Class/Seo/Network/Adapter/Core_Class_Seo_Network_Adapter_Interface#loadPageContent($page)
     */
    public function loadPageContent(&$page)
    {}
}
