<?php

/**
 * Core_Class_Revision
 *
 * returns current global revision
 *
 * revision has to be updated by post commit hook
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id$
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Revision
{
    const FILENAME = 'revision';
    const DEFAULT_REVISION = 1;
    const REVISION_STRING = '__%d';

    static protected $_revision = null;

    static public function get()
    {
        if (self::$_revision == null) {
            $revision = 0;
            $filename = Core_Config :: getInstance()->APPLICATION_PATH . '/' . self::FILENAME;
            if (file_exists($filename)) {
                $revision = (int)trim(file_get_contents($filename));
            }
            if ($revision > 0) {
                self::$_revision = $revision;
            } else {
                self::$_revision = self::DEFAULT_REVISION;
            }
        }
        return self::$_revision;
    }

    static public function getString()
    {
        return sprintf(self::REVISION_STRING, self::get());
    }
}
