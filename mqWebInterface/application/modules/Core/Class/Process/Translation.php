<?php

class Core_Class_Process_Translation
{
    const FILENAME_PATTERN = '/(.*)\.phtml$/';

    private $_translation = null;

    public function __construct($filename)
    {
        $this->_translation = new Core_Class_Process_Translation_Tmx($filename);
    }

    public function update($path)
    {
        foreach (new DirectoryIterator($path) as $file) {
            if ($file->isDot()) {
                continue;
            }
            $match = array();
            if ($file->isDir()) {
                $this->update($file->getPathname());
            } elseif (preg_match(self::FILENAME_PATTERN, $file->getFilename(), $match)) {
                $this->_parse($file->getPathname()) . "\n";
            }
        }
    }

    private function _parse($script)
    {
        return preg_replace_callback(Core_Class_View_Stream_Plugin_Translation::TRANSLATION_PATTERN, array($this, 'match'), file_get_contents($script));
    }

    public function match($matches)
    {
        if (!is_array($matches) || count($matches) < 1) {
            throw new Exception('pattern error');
        }
        $this->_translation->add((string) $matches[1]);
    }

    public function save()
    {
        $this->_translation->save();
    }
}
