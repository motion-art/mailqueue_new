<?php

class Core_Class_Process_Split_Builder extends Core_Class_Process_Module_Builder
{
    const SPLIT_NAME_PATTERN = '%ssplit%s';

    protected $_configurationFiles = array(
        'Advertisment.ini' => 'Advertisment.ini',
        'Navigation.ini' => 'Navigation.ini',
        'Conversion.ini' => 'Conversion.ini',
        'Config.ini' => 'Config.ini'
    );

    public function __construct($id, $base, $info = '')
    {
        if (!$id) {
            throw new Exception('splitId expected');
        }

        $this->_id = ucfirst($id);
        $this->_name = strtolower(sprintf(self::SPLIT_NAME_PATTERN, $base, $this->_id));
        $this->_base = $base;
        $this->_info = $info;

        $this->_source = Core_Config :: getInstance()->MODULE_PATH . '/' . ucfirst($this->_base);
        $this->_destination = Core_Config :: getInstance()->MODULE_PATH . '/' . ucfirst($this->_name);

        if (!file_exists($this->_source)) {
            throw new Exception('base directory "' . $this->_source . '" not found');
        }
    }

    public function build()
    {
        if (file_exists($this->_destination)) {
            throw new Exception('split directory "' . $this->_destination . '" already exists');
        }

        // create base directories + config
        $this->_createBase();

        // scriptpath ini schreiben
        if (!file_exists($this->_source . '/Config/ModulePaths.ini')) {

            $config = new Zend_Config(array(), true);
            $config->path = new Zend_Config(array(), true);

        } else {

            $config   = new Zend_Config_Ini($this->_source . '/Config/ModulePaths.ini', null, array('allowModifications' => true));

            $scriptPaths = $config->path->scriptpath->toArray();
            $modulePaths = $config->path->publicpath->toArray();
        }

        $scriptPaths[] = '/' . ucfirst($this->_base) . '/views/scripts/';
        $config->path->scriptpath = $scriptPaths;

        $modulePaths[] = strtolower($this->_base);
        $config->path->publicpath = $modulePaths;
        $config->path->resourcepath = $modulePaths;
        $config->path->configpath = $modulePaths;

        $writer = new Zend_Config_Writer_Ini();
        $writer->write($this->_destination . '/Config/ModulePaths.ini', $config);

        // add module
        //$setup = new Core_Class_Process_Split_Config_Application(Core_Config :: getInstance()->APPLICATION_PATH . '/setup.xml');
        //$setup->addModule($this->_name);
        //$setup->save();

        // configure split
        $setup = new Core_Class_Process_Split_Config_Module($this->_source . '/Config/Splittest.xml', $this->_base);
        $setup->addSplit($this->_id, $this->_name, $this->_info);
        $setup->save();

        $this->_buildController($this->_source . '/Controller/', $this->_destination . '/Controller/');
    }

    public function delete()
    {
        if (!file_exists($this->_destination)) {
            throw new Exception('split directory "' . $this->_destination . '" not exists');
        }

        Core_Class_Log::getInstance()->info('delete module "' . $this->_name . '"');
        @exec ('rm -r ' . $this->_destination);

        // delete module
        //$setup = new Core_Class_Process_Split_Config_Application(Core_Config :: getInstance()->APPLICATION_PATH . '/setup.xml');
        //$setup->removeModule($this->_name);
        //$setup->save();

        // delete split
        $setup = new Core_Class_Process_Split_Config_Module($this->_source . '/Config/Splittest.xml');
        $setup->removeSplit($this->_id);
        if ($setup->hasSplits()) {
            $setup->save();
        } else {
            $setup->delete();
        }
    }
}
