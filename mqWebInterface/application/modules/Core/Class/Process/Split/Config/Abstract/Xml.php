<?php

abstract class Core_Class_Process_Split_Config_Abstract_Xml
{
    protected $_document = null;
    protected $_filename = null;

    public function __construct($filename)
    {
        if (!file_exists($filename)) {
            throw new Exception('config not found');
        }

        $this->_filename = $filename;

        $this->_document = new DOMDocument('1.0');
        $this->_document->preserveWhiteSpace = false;
        $this->_document->formatOutput = true;
        $this->_document->load($this->_filename);

    }

    public function save()
    {
        $this->_document->save($this->_filename);
    }
}
