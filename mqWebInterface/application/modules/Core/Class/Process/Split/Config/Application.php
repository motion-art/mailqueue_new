<?php

class Core_Class_Process_Split_Config_Application extends Core_Class_Process_Split_Config_Abstract_Xml
{
    public function removeModule($name)
    {
        $xpath = new DOMXPath($this->_document);
        $name = ucfirst($name);

        $entries = $xpath->query('/setup/module/name[.=\'' . $name . '\']/parent::*');

        if ($entry = $entries->item(0)) {
            Core_Class_Log::getInstance()->info('delete splitmodule in setup.xml: "' . $name . '"');
            $this->_document->documentElement->removeChild($entry);
        } else {
            Core_Class_Log::getInstance()->crit('splitmodule not found in setup.xml: "' . $name . '"');
        }

    }

    public function addModule($name)
    {
        Core_Class_Log::getInstance()->info('add splitmodule to setup.xml: "' . $name . '"');

        $element = $this->_document->createElement('module');
        $moduleNode = $this->_document->documentElement->appendChild($element);

        $element = $this->_document->createElement('name', ucfirst($name));
        $moduleNode->appendChild($element);
    }
}
