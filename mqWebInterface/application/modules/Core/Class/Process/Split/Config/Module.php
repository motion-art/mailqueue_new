<?php

class Core_Class_Process_Split_Config_Module extends Core_Class_Process_Split_Config_Abstract_Xml
{
    private $_header = array(
        'default_stats_prefix' => 'default',
        'enabled' => 1,
        'cookie_liveTime' => 86400,
        'splitting_rate' => 50
    );

    private $_root = null;

    public function __construct($filename, $base = '')
    {
        $this->_document = new DOMDocument('1.0');
        $this->_document->preserveWhiteSpace = false;
        $this->_document->formatOutput = true;

        if (!file_exists($filename)) {
            Core_Class_Log::getInstance()->info('create splittest.xml');
            $this->_root = $this->_document->appendChild($this->_document->createElement('split_tests'));
            $this->_createHeader($base);
        } else {
            $this->_document->load($filename);
            $this->_root = $this->_document->documentElement;
        }

        $this->_filename = $filename;
    }

    public function addSplit($id, $name, $info)
    {
        Core_Class_Log::getInstance()->info('add splitsection in splittest.xml: "' . $id . '"');

        $element = $this->_document->createElement('split');
        $splitId = $this->_document->createAttribute('id');
        $splitId->appendChild($this->_document->createTextNode($id));
        $element->appendChild($splitId);

        $enable      = $this->_document->createAttribute('enabled');
        $enable->appendChild($this->_document->createTextNode(0));
        $element->appendChild($enable);

        $forTest      = $this->_document->createAttribute('test');
        $forTest->appendChild($this->_document->createTextNode(0));
        $element->appendChild($forTest);


        $split   = $this->_root->appendChild($element);

        $element = $this->_document->createElement('module', $name);
        $split->appendChild($element);

        $element = $this->_document->createElement('stats_prefix', 'split'. $id);
        $split->appendChild($element);

        $element = $this->_document->createElement('info', $info);
        $split->appendChild($element);

        $element = $this->_document->createElement('chance', 1);
        $split->appendChild($element);
    }

    private function _createHeader($base)
    {
        $element = $this->_document->createElement('setup');
        $split   = $this->_root->appendChild($element);

        $element = $this->_document->createElement('info', $base);
        $split->appendChild($element);

        foreach ($this->_header as $key => $value) {
            $element = $this->_document->createElement($key, $value);
            $split->appendChild($element);
        }
    }

    public function removeSplit($id)
    {
        $xpath = new DOMXPath($this->_document);
        $entries = $xpath->query("/split_tests/split[@id='" . $id . "']");

        if ($entry = $entries->item(0)) {
            Core_Class_Log::getInstance()->info('delete splitsection in splittest.xml: "' . $id . '"');
            $this->_root->removeChild($entry);
        } else {
            Core_Class_Log::getInstance()->crit('splitsection not found in splittest.xml: "' . $id . '"');
        }
    }

    public function hasSplits()
    {
        $xpath = new DOMXPath($this->_document);
        return $xpath->query('/split_tests/split')->item(0);
    }

    public function delete()
    {
        Core_Class_Log::getInstance()->info('delete split configuration');
        @unlink($this->_filename);
    }
}
