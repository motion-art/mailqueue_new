<?php

/**
 * Core_Class_Process_ChangeDb
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: ChangeDb.php 2181 2009-09-08 13:12:10Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 *
 * - migrates database to newest version by parsing all new deltafiles
 *   and executing queries
 * - the information about current version is saved in database
 * - required:
 * 		- instance of Zend_Db_Adapter_Abstract
 * 		- path to deltafiles
 * - optional:
 * 		- custom deltafile pattern (default: /^([0-9]+)_[0-9]+.sql$/)
 * 		- custom tablename (default: application)
 *
 * db structure
 * ------------
 * CREATE TABLE IF NOT EXISTS `application` (
 *   `version` int(4) unsigned default NULL
 * ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
 *
 * inital version = 1 expected
 *
 *
 * deltafile example
 * -----------------
 * filename: 45_1248309494.sql
 * version: 45
 *
 * content:
 * CREATE TABLE cms_lock (id INT UNSIGNED, tname VARCHAR(55), user_id INT UNSIGNED NOT NULL, date VARCHAR(255) NOT NULL, PRIMARY KEY(id, tname)) ENGINE = INNODB;
 * --//@UNDO
 * --DROP TABLE cms_lock;
 *
 * @TODO: UNDO
 */

class Core_Class_Process_ChangeDb
{
    /**
     * default deltafile pattern
     * @var string
     */
    const DEFAULT_FILE_PATTERN = '/^([0-9]+)_[0-9]+.sql$/';

    /**
     * default version tablename
     * @var string
     */
    const DEFAULT_TABLENAME = 'application';

    /**
     * database
     * @var Zend_Db_Adapter_Abstract
     */
    protected $_db = null;

    /**
     * current db version
     * @var integer
     */
    protected $_version = null;

    /**
     * path to deltafiles
     * @var string
     */
    protected $_path = null;

    /**
     * pattern for deltafiles
     * @var string
     */
    protected $_pattern = null;

    /**
     * tablename for version information
     * @var string
     */
    protected $_tablename = null;

    /**
     * constructor
     *
     * @param Zend_Db_Adapter_Abstract $db database adapter
     * @param string                   $path path to deltafiles
     * @param string                   $tablename tablename for version information [optional]
     * @param string                   $pattern pattern for deltafiles [optional]
     */
    public function __construct(Zend_Db_Adapter_Abstract $db, $path, $tablename = null, $pattern = null)
    {
        $this->_pattern = self::DEFAULT_FILE_PATTERN;
        $this->_tablename = self::DEFAULT_TABLENAME;

        if (!$db instanceof Zend_Db_Adapter_Abstract) {
            throw new Exception('instance of Zend_Db_Adapter_Abstract expected');
        }
        if ($path == null || !file_exists($path)) {
            throw new Exception('path "' . $path . '" not found');
        }
        if ($pattern != null) {
            $this->_pattern = $pattern;
        }
        if ($tablename != null) {
            $this->_tablename = $tablename;
        }
        $this->_db = $db;
        $this->_path = $path;
    }

    /**
     * executes changes from deltafiles
     *
     * @return void
     */
    public function execute()
    {
        Core_Class_Log::getInstance()->info('[execute] begin version ' . $this->getVersion());

        $files = $this->getDeltaFiles();
        if (count($files) == 0) {
            Core_Class_Log::getInstance()->info('[execute] no database changes found');
            return;
        }

        foreach ($files as $version => $file) {

            Core_Class_Log::getInstance()->info('[execute] ' . $file);
            $queries = explode(';', file_get_contents($file));

            if (count($queries) <= 0) {
                throw new Exception('no query found in "' . $file . '"');
            }

            foreach ($queries as $query) {
                $query = $this->_filterQuery($query);
                if ($this->_isValidQuery($query)) {

                    Core_Class_Log::getInstance()->info('[execute] ' . $query);

                    try {
                        $this->_db->query($query);
                        Core_Class_Log::getInstance()->info('[execute] OK');

                    } catch (Exception $e) {

                        throw new Exception($e->getMessage());
                    }
                }
            }

            $this->_updateVersion($version);
        }

        Core_Class_Log::getInstance()->info('[execute] end version ' . $version);
    }

    /**
     * validates query
     *
     * @param string $query
     * @return boolean
     */
    protected function _isValidQuery($query)
    {
        return (substr($query, 0, 2) != '--') && strlen($query) > 0;
    }

    /**
     * filter sql query
     *
     * @param string $query
     * @return string
     */
    protected function _filterQuery($query)
    {
        return trim($query);
    }

    /**
     * read directory contents and compare with current version
     *
     * @return array
     */
    public function getDeltaFiles()
    {
        $files = array();
        foreach (new DirectoryIterator($this->_path) as $file) {
            if ($file->isFile()) {
                $matches = array();
                if (preg_match($this->_pattern, $file->getFilename(), $matches)) {
                    if ($this->getVersion() < $matches[1]) {
                        $files[$matches[1]] = $file->getPathname();
                    }
                }
            }
        }
        ksort($files);
        return $files;
    }

    /**
     * update version number in database
     *
     * @param integer $version
     * @return void
     */
    protected function _updateVersion($version)
    {
        try {
            $this->_db->query('UPDATE ' . $this->_tablename . ' SET version = ?', $version);
        } catch (Exception $e) {
            throw new Exception('could not update migrated version in database');
        }
    }

    /**
     * select current version from db
     *
     * @return integer
     */
    public function getVersion()
    {
        if ($this->_version == null) {
            try {
                $this->_version = (int)$this->_db->fetchOne('SELECT version FROM ' . $this->_tablename);
            } catch (Exception $e) {
                throw new Exception('could not select version from database table "' . $this->_tablename . '"');
            }
            if ($this->_version <= 0) {
                throw new Exception('database version is not defined or zero');
            }
        }
        return $this->_version;
    }
}
