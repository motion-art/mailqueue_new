<?php

class Core_Class_Process_Module_Builder
{
    protected $_configurationFiles = array(
        'Advertisment.dist.ini' => 'Advertisment.ini',
        'Navigation.dist.ini' => 'Navigation.ini',
        'Config.ini' => 'Config.ini',
        'Conversion.dist.ini' => 'Conversion.ini'
    );

    public function __construct($name, $base)
    {
        $this->_name = strtolower($name);
        $this->_base = strtolower($base);

        $this->_source = Core_Config :: getInstance()->MODULE_PATH . '/' . ucfirst($this->_base);
        $this->_destination = Core_Config :: getInstance()->MODULE_PATH . '/' . ucfirst($this->_name);

        if (!file_exists($this->_source)) {
            throw new Exception('base directory "' . $this->_source . '" not found');
        }
    }

    public function build()
    {
        if (file_exists($this->_destination)) {
            throw new Exception('module directory "' . $this->_destination . '" already exists');
        }

        $this->_createBase();

        $config   = new Zend_Config(array(), true);

        $scriptPaths = array();
        $scriptPaths[] = '/' . ucfirst($this->_base) . '/views/scripts/';

        $modulePaths = array();
        $modulePaths[] = strtolower($this->_base);

        $config->path = array();
        $config->path->scriptpath = $scriptPaths;
        $config->path->publicpath = $modulePaths;
        $config->path->resourcepath = $modulePaths;
        $config->path->configpath = $modulePaths;

        $writer = new Zend_Config_Writer_Ini();
        $writer->write($this->_destination . '/Config/ModulePaths.ini', $config);

        $this->_buildController($this->_source . '/Controller/', $this->_destination . '/Controller/');
    }

    protected function _createBase()
    {
        Core_Class_Log::getInstance()->info('create base directories');

        @mkdir($this->_destination . '/Controller/', 0777, true);
        if (!file_exists($this->_destination . '/Controller/')) {
            throw new Exception('could not create split directory');
        }

        @mkdir($this->_destination . '/views/scripts/', 0777, true);
        @mkdir($this->_destination . '/Config/', 0777, true);

        foreach ($this->_configurationFiles as $from => $to) {
            @copy($this->_source . '/Config/' . $from, $this->_destination . '/Config/' . $to);
        }
    }

    protected function _buildController($source, $destination)
    {
        Core_Class_Log::getInstance()->info('extend base-controller');

        $directory = new DirectoryIterator($source);

        foreach ($directory as $file) {
            if ($file->isFile() && $file->isReadable()) {

                $baseController = basename($file->getFilename(), '.php');

                $class = new Zend_CodeGenerator_Php_Class();
                $class->setName(ucfirst($this->_name) . '_' . $baseController);
                $class->setExtendedClass(ucfirst($this->_base) . '_' . $baseController);

                $docblock = new Zend_CodeGenerator_Php_Docblock(array(
                    'shortDescription' => $this->_name,
                    'longDescription'  => 'This is a class generated with module builder.',
                    'tags'             => array(
                        array(
                            'name'        => 'category',
                            'description' => 'Application',
                        ),
                        array(
                            'name'        => 'package',
                            'description' => $this->_name,
                        ),
                        array(
                            'name'        => 'author',
                            'description' => 'Unister GmbH <teamleitung-dev@unister-gmbh.de>',
                        ),
                        array(
                            'name'        => 'version',
                            'description' => '$Id$',
                        ),
                        array(
                            'name'        => 'copyright',
                            'description' => 'Copyright (c) 2006-2009, Unister GmbH',
                        ),
                        array(
                            'name'        => 'license',
                            'description' => 'license.txt',
                        ),
                    ),
                ));

                $file = new Zend_CodeGenerator_Php_File();
                $file->setClass($class);
                $file->setDocblock($docblock);
                $file->setRequiredFiles(array(ucfirst($this->_base) . '/Controller/' . $baseController . '.php'));

                file_put_contents($destination . '/' . $baseController . '.php', $file->generate());
            }
        }
    }
}
