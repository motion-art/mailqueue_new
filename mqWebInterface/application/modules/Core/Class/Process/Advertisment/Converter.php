<?php
/**
 * SHORT DESCRIPTION
 *
 * LONG DESCRIPTION
 *
 * @category    CATEGORY
 * @package     CATEGORY_PACKAGE
 * @copyright   Copyright (c) 2010 Unister GmbH
 * @author      Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author      r.lasinski
 * @version     $Id$
 * @since       12.05.2010
 */

class Core_Class_Process_Advertisment_Converter
{
    public function __construct($db)
    {
        $this->_db = $db;
        $this->_dataHolder = array();
    }

    public function addModule($module, $default = false)
    {
        $this->_modules[$module] = $default;
    }

    public function run()
    {
        foreach (array_reverse($this->_modules) as $name => $default) {

            $module = null;
            if (!$default) {
                $module = $name;
            }

            Core_Class_Log::getInstance()->info('convert module "'.$name.'"');
            $filename = Core_Config :: getInstance()->MODULE_PATH . '/' . ucfirst($name) . '/Config/Advertisment.ini';

            if (!file_exists($filename)) {
                Core_Class_Log::getInstance()->warn('advertisment.ini for module "'.$name.'" not found');
                continue;
            }

            $config = new Zend_Config_Ini($filename, null, array('skipExtends' => false));

            foreach ($config as $runmode => $from) {

                $this->_data = array();
                $this->_module = $module;
                $this->_runmode = $runmode;
                if ($runmode != 'local') {
                    $this->_runmode = $runmode;
                } else {
                    $this->_runmode = null;
                }
                $this->_addRules($from);
                $this->_setExtends();

                foreach ($this->_data as $data) {
                   $this->_insert($data);
                }
            }
        }
    }
    
    
    private function _insert($data)
    {
        $runmode = $data['runmode'];
        //wenn noch nicht enthalten in der globalen section
        $data['runmode'] = null;
        if(!in_array($data , $this->_dataHolder, true)) {
            //runmode wieder auf original setzen
            $data['runmode'] = $runmode;
            //wenn noch nicht in der spezielle section enthalten
            if(!in_array($data , $this->_dataHolder, true)) {
                $this->_dataHolder[] = $data;
                $this->_db->insert('advertisement', $data);
            }
        }
    }

    private function _setExtends()
    {
        foreach ($this->_extends as $destination => $source) {
            foreach ($this->_data as $data) {
                if ($data['controller'] == $source) {
                    $data['controller'] = $destination;
                    $this->_data[] = $data;
                }
            }
        }
    }

    private function _addRules($node, $path = array())
    {
        foreach ($node as $subNode => $rule) {

            if (!$rule instanceof Zend_Config) {

                $this->_addRule($subNode, $rule, $path);

            } else {

                if (count($path) > 1 && !is_numeric($subNode)) {

                    $this->_addRule($subNode, $rule->toArray(), $path);

                } else {
                    $newPath = $path;
                    $newPath[] = $subNode;
                    $this->_addRules($rule, $newPath);
                }
            }
        }
    }

    private function _addRule($key, $value, $path)
    {
        if ($path[0] != 'default') {
            $controller = $path[0];
        }
        if ($path[1] != 'default') {
            if (!is_numeric($path[1])) {
                $action = $path[1];
            } else {
                $id = $path[1];
            }
        }
        if (isset($path[2])) {
            $id = $path[2];
        }

        if (!$value) {
            $value = 0;
        }

        if ($key == 'extends') {
            $this->_extends[$controller] = $value;
            return;
        }

        $data = array(
            'module' => $this->_module,
            'controller' => $controller,
            'action' => $action,
            'key' => $key,
            'id' => $id,
            'runmode' => $this->_runmode
        );

        if (is_array($value)) {
            $data['value'] = serialize($value);
            $data['isSerialized'] = true;
        } else {
            $data['value'] = $value;
        }

        if (strpos($key, 'google_') !== false) {
            $data['plugin'] = 'google';
        } elseif (in_array($key, array('pop', 'top', 'sky', 'ca', 'nuggad', 'top_placeholder', 'sky_placeholder'))) {
            $data['plugin'] = 'adition';
        } else {
            $data['plugin'] = 'default';
        }

        $this->_data[] = $data;
    }
}