<?php

require('Minify/CssMin.php');
require('Minify/JsMin.php');

class Core_Class_Process_Minify
{
    private $_typeInfo = array(
                            'js'=> 'application/x-javascript',
                            'css'=> 'text/css'
                         );
    private $_compressionLevel = 9; //compress at maximum
    private $_compressInfo = array(
                                    array('encoding' => 'deflate', 'fn' => 'gzdeflate', 'ext' => '.zd'),
                                    array('encoding' => 'x-gzip', 'fn' => 'gzencode', 'ext' => '.zg'),
                                    array('encoding' => 'x-compress', 'fn' => 'gzcompress', 'ext' => '.zc')
                             );
    private $_varExt = '.var';

    public function __construct($path)
    {
        Zend_Controller_Front::getInstance()->setRequest('Zend_Controller_Request_Simple');

        $this->_path = $path;
        $this->_config = Core_Class_Config::get('Minify', 'default');

        $this->_output = 'minify';
    }

    public function run()
    {
        foreach ($this->_config->modules as $module) {
            $this->_build($module);
        }
    }

    private function _build($module)
    {
        $this->_initPublicPaths($module);

        foreach ($this->_config->minify as $group) {

            Core_Class_Log::getInstance()->info('group ' . $group->filename);

            $path = Core_Class_BasePath::getInstance()->getPath($group->resource, $module);
            if (!file_exists($path)) {
                throw new Exception('minify path not found: ' . $path);
            }

            $output = '';
            $class = $group->class;

            foreach ($group->partial as $file) {
                Core_Class_Log::getInstance()->info('include partial ' . $file);
                $content = file_get_contents(
                                Core_Class_BasePath::getInstance()->getPath($group->type, $module, $file)
                           );

                if ($group->replaceUri) {
                    $matches = array();
                    if (preg_match_all('/url\((.+)\)/', $content, $matches)) {
                        foreach ($matches[1] as $match) {
                            $path = Core_Class_BasePath::getInstance()->getPath($group->type, $module, $file);
                            $path = realpath(dirname($path) . '/' . $match);
                            $path = str_replace(Core_Config::getInstance()->PUBLIC_PATH, '', $path);

                            Core_Class_Log::getInstance()->info('replace uri "' . $match . '" => "' . $path . '"');
                            $content = str_replace($match, $path, $content);
                        }
                    }
                }

                $output .= $content . PHP_EOL;
            }

            $destination = Core_Class_BasePath::getInstance()->getPath($group->resource, $module) . $group->filename;

            file_put_contents(
                $destination,
                call_user_func(array($class, 'minify'), $output)
            );

            $this->_compress($destination);
        }
    }

    private function _initPublicPaths($module)
    {
        Core_Class_BasePath::getInstance()->reset();

        $config = Core_Config :: getInstance()->MODULE_PATH
                . '/' . ucfirst($module)
                . '/Config/ModulePaths.ini';

        if (file_exists($config)) {
            $paths = new Zend_Config_Ini($config, 'path');
            if (isset($paths->publicpath)) {

                foreach ($paths->publicpath as $module) {

                    Core_Class_BasePath::addModule($module);
                }
            }
        }
    }

    private function _compress($filename)
    {
        Core_Class_Log::getInstance()->info('compress ' . $filename);

        $fileInfo = pathinfo($filename);

        // compress
        $content = file_get_contents($filename);
        foreach ($this->_compressInfo as $info) {
            $mtd = $info['fn'];
            $out = $mtd($content, $this->_compressionLevel);
            file_put_contents($fileInfo['dirname'] . '/' . $fileInfo['basename'] . $info['ext'], $out);
        }

        // make .var file
        $contentType = $this->_typeInfo[$fileInfo['extension']];
        $qs = 0.9;
        $out = '';
        foreach ($this->_compressInfo as $info) {
            $out .= "URI: {$fileInfo['basename']}{$info['ext']}" . PHP_EOL;
            $out .= "Content-Type: {$contentType}; qs=$qs" . PHP_EOL;
            $out .= "Content-Encoding: {$info['encoding']}"  . PHP_EOL  . PHP_EOL;
            $qs -= 0.1;
        }

        $out .= "URI: {$fileInfo['basename']}" . PHP_EOL;
        $out .= "Content-Type: {$contentType}; qs=$qs";

        file_put_contents($filename . $this->_varExt, $out);
    }
}
