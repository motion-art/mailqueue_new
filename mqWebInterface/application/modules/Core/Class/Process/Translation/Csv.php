<?php

class Core_Class_Process_Translation_Csv
{
    private $_filename = null;
    private $_text = array();

    public function __construct($filename)
    {
        if (file_exists($filename)) {
            $this->_import($filename);
        }

        $this->_filename = $filename;
    }

    private function _import($filename)
    {
        $file = fopen($filename, 'r');
        while (($data = fgetcsv($file, 1000, ',')) !== FALSE ) {
            $translation = array();
            $translation['de'] = $data[1];
            $translation['en'] = $data[2];
            $translation['fr'] = $data[3];
            $this->add($data[0], $translation);
        }
        fclose($file);
    }

    public function add($text, $translations = array())
    {
        if (!array_key_exists($text, $this->_text)) {
            if (count($translations) == 0) {
                Core_Class_Log::getInstance()->info('[translation] new: ' . $text);
            }
            $this->_text[$text] = $translations;
        }
    }

    public function save()
    {
        $file = fopen($this->_filename, 'w');
        foreach ($this->_text as $language => $translation) {
            fwrite($file, '"' . implode('","', array($language) + $translation) . '"' . "\n");
        }
        fclose($file);
    }
}
