<?php

class Core_Class_Process_Translation_Tmx
{
    private $_filename = null;
    private $_document = null;
    private $_root = null;
    private $_text = array();
    private $_languages = array('de', 'en', 'fr', 'es', 'pt');

    public function __construct($filename)
    {
        if (file_exists($filename)) {
            $this->_import($filename);
        }

        $this->_filename = $filename;

        $this->_document = new DOMDocument('1.0', 'utf-8');
        $this->_root = $this->_document->createElement('tmx');
        $this->_root->setAttribute('version', '1.4');
        $this->_document->appendChild($this->_root);

        $this->_body = $this->_document->createElement('body');
        $this->_root->appendChild($this->_body);
    }

    private function _import($filename)
    {
        $this->_document = new DOMDocument('1.0', 'utf-8');
        $this->_document->load($filename);

        foreach ($this->_document->getElementsByTagName('tu') as $tu) {
            $translation = array();
            foreach ($tu->getElementsByTagName('tuv') as $tuv) {
                $translation[$tuv->getAttribute('xml:lang')] = $tuv->getElementsByTagName('seg')->item(0)->nodeValue;
            }
            $this->add($tu->getAttribute('tuid'), $translation);
        }
    }

    public function add($text, $translations = array())
    {
        if (!array_key_exists($text, $this->_text)) {
            if (count($translations) == 0) {
                Core_Class_Log::getInstance()->info('[translation] new: ' . $text);
            }
            $this->_text[$text] = $translations;
        }
    }

    public function save()
    {
        foreach ($this->_text as $text => $translation) {
            $this->_addTextUnit($text, $translation);
        }

        $this->_document->formatOutput = true;
        $this->_document->save($this->_filename);
    }

    private function _addTextUnit($unit, $translation = array())
    {
        $tu = $this->_document->createElement('tu');
        $tu->setAttribute('tuid', $unit);

        foreach ($this->_languages as $language) {
            $tuv = $this->_document->createElement('tuv');
            $tuv->setAttribute('xml:lang', $language);
            if (array_key_exists($language, $translation)) {
                $seg = $this->_document->createElement('seg', $translation[$language]);
            } else {
                $seg = $this->_document->createElement('seg', $unit);
            }
            $tuv->appendChild($seg);
            $tu->appendChild($tuv);
        }

        $this->_body->appendChild($tu);
    }
}
