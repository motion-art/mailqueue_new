<?php

/**
 * Core_Class_Log
 *
 * builds cache object from config with fallback to filecache
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: Cache.php 348 2010-07-02 08:50:38Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Log
{
    protected $_logger = null;
    static protected $_instance = null;

    static public function getInstance()
    {
        if (self::$_instance == null) {
            self::$_instance = new self();
        }

        return self::$_instance;
    }

    public function __call($method, $params)
    {
        switch (count($params)) {
            case 0:
                throw new Exception('Missing log message');
            case 1:
                $message = array_shift($params);
                $extras = null;
                break;
            default:
                $message = array_shift($params);
                $extras  = array_shift($params);
                break;
        }

        return $this->_getLogger()->$method($message, $extras);
    }

    protected function _getLogger()
    {
        if (!Zend_Registry::isRegistered(Core_Class_Bootstrap::REGISTRY)) {
            throw new Exception(Core_Class_Bootstrap::REGISTRY . ' not found in Registry');
        }
        if (!Zend_Registry::get(Core_Class_Bootstrap::REGISTRY)->hasResource('logger')) {
            throw new Exception('Resource "logger" expected');
        }

        return Zend_Registry::get(Core_Class_Bootstrap::REGISTRY)->getResource('logger');
    }
}