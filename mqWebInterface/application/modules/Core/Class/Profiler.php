<?php

/**
 * Core_Class_Profiler
 *
 * application profiler
 * used from Profiler Controller Plugin (Core_Class_Plugin_Profiler)
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: Profiler.php 381 2010-07-08 10:11:48Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Profiler
{
    /**
     * instance
     * @var Core_Class_Profiler
     */
    protected static $_instance = null;

    /**
     * marker
     * @var array
     */
    protected $_marker = array();

    /**
     * singleton
     *
     * @return Core_Class_Profiler
     */
    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new Core_Class_Profiler();
        }
        return self::$_instance;
    }

    /**
     * begin marker
     *
     * @param string $name
     * @return void
     */
    public function start($name)
    {
        if (isset($name) && strlen($name)>0) {
            $this->_marker[$name] = array();
            $this->_marker[$name]['start'] = microtime(true);
        }
    }

    /**
     * end marker
     *
     * @param string $name
     * @return integer
     */
    public function end($name)
    {
        if (isset($name) && strlen($name)>0 && isset($this->_marker[$name])) {
            $this->_marker[$name]['end'] = microtime(true);
            return ($this->_marker[$name]['end'] - $this->_marker[$name]['start']);
        }
        return 0;
    }

    /**
     * returns profiling statistic
     *
     * @return array
     */
    public function getResults()
    {
        if (count($this->_marker) > 0) {
            foreach ($this->_marker as &$marker) {

                if (isset($marker['start']) && isset($marker['end'])) {
                    $marker = $marker['end'] - $marker['start'];
                } else {
                    $marker = microtime(true) - $marker['start'];
                }
            }
        }
        return $this->_marker;
    }
}