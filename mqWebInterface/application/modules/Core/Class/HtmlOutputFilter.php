<?php

/**
 * Klasse filtert den HtmlOutput der depatchers
 * @author r.gwizdziel
 */

class Core_Class_HtmlOutputFilter
{
    /**
     * Optionen
     * @var Zend_Config
     */
    protected $_options = null;

    public function __construct(Zend_Config $options)
    {
        $this->_options = $options;
    }

    /**
     * Filtert $data entsprechend der definierten Filter
     *
     * @param string $data unfilterd html output
     * @return string filterd html output
     */
    public function start($data)
    {
        if (!$this->_options->enable) {
            return $data;
        }

        foreach ($this->_options->filters as $filter) {
            if (!class_exists($filter->class)) {
                Core_Class_Log::getInstance()->warn($filter->class . ' not found');
                continue;
            }

            $filterClass = new $filter->class($filter->options);
            if (!$filterClass instanceof Zend_Filter_Interface) {
                Core_Class_Log::getInstance()->warn($filter->class . ' must be implement Zend_Filter_Interface');
                continue;
            }

           $data = call_user_func_array(array($filterClass, 'filter'), array($data));
        }

        return $data;
    }
}