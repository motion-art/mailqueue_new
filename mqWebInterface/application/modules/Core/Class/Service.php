<?php

class Core_Class_Service
{
    static public function factory($name)
    {
        if (!isset(Zend_Registry::get('_CONFIG')->service) || !isset(Zend_Registry::get('_CONFIG')->service->$name)) {
            throw new Exception('service configuration for "' . $name . '" expected');
        }

        $config = Zend_Registry::get('_CONFIG')->service->$name;
        $class = $config->class;

        if (!class_exists($class)) {
            throw new Exception('service-class "' . $class . '" not found');
        }

        return new $class($config);
    }
}
