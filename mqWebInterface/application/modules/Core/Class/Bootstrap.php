<?php

class Core_Class_Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    const REGISTRY = '_BOOTSTRAP';

    public function __construct($application)
    {
        parent::__construct($application);

        if (!$this->hasPluginResource('logger')) {
            $this->registerPluginResource('logger');
        }
        if (!$this->hasPluginResource('config')) {
            $this->registerPluginResource('config');
        }
        if (!$this->hasPluginResource('router')) {
            $this->registerPluginResource('router');
        }

        Zend_Registry::set(self::REGISTRY, $this);
    }

    public function run()
    {
        $front   = $this->getResource('FrontController');
        $default = $front->getDefaultModule();
        if (null === $front->getControllerDirectory($default)) {
            throw new Zend_Application_Bootstrap_Exception(
                'No default controller directory registered with front controller'
            );
        }

        $front->setParam('bootstrap', $this);
        $front->returnResponse(true);

        $response = $front->dispatch();
        echo $this->_filter($response);
    }

    private function _filter($response)
    {
        $config = $this->getResource('config');

        if (isset($config->outputFilter)) {
            $filter = new Core_Class_HtmlOutputFilter(
                $config->outputFilter
            );
            return $filter->start($response);
        }

        return $response;
    }
}