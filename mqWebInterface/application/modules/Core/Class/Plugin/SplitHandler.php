<?php

/**
 * Core_Class_Plugin_ViewHandler
 * Plugin setzt einen zusätzlichen Templatepfad.
 * Wird im Modul das Template nicht gefunden, wird im
 * anderen Modulen gesucht.
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id$
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Plugin_SplitHandler extends Zend_Controller_Plugin_Abstract
{
    /**
     * (non-PHPdoc)
     * @see Controller/Plugin/Zend_Controller_Plugin_Abstract#routeShutdown($request)
     */
    public function routeStartup(Zend_Controller_Request_Abstract $request)
    {
        $module = Zend_Controller_Front::getInstance()->getDefaultModule();
        $splitXmlFile = Core_Config :: getInstance()->MODULE_PATH
                      . '/' . ucfirst($module) . '/Config/Splittest.xml';

        //wenn splittest xml existiert
        if (file_exists($splitXmlFile)) {

            try {
                $splitHandler = Core_Class_Split_Handler::getInstance();
                $splitHandler->setSplitXml($splitXmlFile);

                $switch = $request->getParam('split', false);
                $splitHandler->setManuallySwitch($switch);
                $splitHandler->setBaseModule($module);

                //wenn nicht Defaultmodul, Splittmodul setzen
                if ($splitHandler->isEnabled()) {

                    Core_Class_Log::getInstance()->debug('Split: ' . $splitHandler->getTrackingPrefix());

                    if ($splitModule = $splitHandler->getSplitModeModuleName()) {

                        $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
                        $bootstrap->getResource('Module')->setDefaultModule($splitModule);
                    }
                }
            } catch (Exception $e) {

                Core_Class_Log::getInstance()->crit($e);
            }
        }
    }
}
