<?php

/**
 * Core_Class_Plugin_ModulePathHandler
 * Plugin setzt einen zusätzlichen Templatepfad.
 * Wird im Modul das Template nicht gefunden, wird im
 * anderen Modulen gesucht.
 *
 * setzt zusätzliche public-Pfade
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: LayoutParts.php 1489 2009-07-13 14:06:53Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Plugin_ModulePathHandler extends Zend_Controller_Plugin_Abstract
{
    /**
     * (non-PHPdoc)
     * @see Controller/Plugin/Zend_Controller_Plugin_Abstract#routeShutdown($request)
     */
    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
        $viewRenderer = Zend_Controller_Action_HelperBroker::getStaticHelper( 'viewRenderer' );
        $view         = $viewRenderer->view;

        if (!$module = $request->getModuleName()) {
            $module = Zend_Controller_Front::getInstance()->getDefaultModule();
        }

        $config = Core_Config :: getInstance()->MODULE_PATH
                . '/' . ucfirst($module)
                 . '/Config/ModulePaths.ini';

        if (file_exists($config)) {

            $paths = new Zend_Config_Ini(
                        $config,
                        'path'
                    );

            if (isset($paths->scriptpath)) {

                foreach ($paths->scriptpath as $path) {

                    $addPath = Core_Config :: getInstance()->MODULE_PATH
                             . $path;

                    if (in_array($addPath, $view->getScriptPaths())) {
                        continue;
                    }

                    $view->addScriptPath($addPath);
                }
            }

            if (isset($paths->publicpath)) {

                foreach ($paths->publicpath as $module) {

                    Core_Class_BasePath::addModule($module);
                }
            }

            if (isset($paths->configpath)) {

                foreach ($paths->configpath as $module) {

                    Core_Class_Config::addModule($module);
                }
            }
        }
    }
}
