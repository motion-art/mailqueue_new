<?php

class Core_Class_Plugin_Source extends Zend_Controller_Plugin_Abstract
{
    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
        if ($source = $request->getParam('s')) {
            Core_Class_Source::getInstance()->setSource($source);

            if (Core_Class_Source::getInstance()->isFirst()) {
                Core_Class_Statistic::clickin();
            }
        }

        $source = Core_Class_Source::getInstance()->getSource();
        if (!is_null($source)) {
            //source in der url vorhanden
            if (@class_exists($class = 'Core_Class_Plugin_Source_'.ucfirst(strtolower($source)))) {
                //Klasse für Source existiert
                $sourceTracking = new $class($request);
                $sourceTracking->track();
            } else {
                //es existiert keine klasse für die quelle
                Core_Class_Log::getInstance()->debug('unknown source '.$source);
            }
        }
    }
}
