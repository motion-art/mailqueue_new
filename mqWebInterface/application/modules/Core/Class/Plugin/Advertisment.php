<?php
/**
 * Price_Class_Plugin_LayoutParts
 *
 * @package   Price
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: LayoutParts.php 1489 2009-07-13 14:06:53Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Plugin_Advertisment extends Zend_Controller_Plugin_Abstract
{
    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        if (isset(Zend_Registry::get('_CONFIG')->advertisment)) {

            $layout = Zend_Layout::getMvcInstance();
            $view   = $layout->getView();

            // init prefix paths
            if (isset(Zend_Registry::get('_CONFIG')->advertisment->prefixPath)) {
                $prefixpath = Zend_Registry::get('_CONFIG')->advertisment->prefixPath->toArray();
                foreach ($prefixpath as $path) {
                    $view->advertisment()->addPrefixPath( $path );
                }

            }

            // init plugins
            if (isset(Zend_Registry::get('_CONFIG')->advertisment->initPlugins)) {
                $plugins = Zend_Registry::get('_CONFIG')->advertisment->initPlugins->toArray();
                $view->advertisment()->initPlugins($plugins);
            }
        }
    }
}
