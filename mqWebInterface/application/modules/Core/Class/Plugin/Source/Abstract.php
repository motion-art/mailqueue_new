<?php
/**
 * Datei für Klasse Core_Class_Plugin_Source_Abstract
 *
 * @package			Core
 * @subpackage		Plugin
 * @author    		Stefan Junge <stefan.junge@unister-gmbh.de>
 * @version   		$Id: $
 * @copyright 		Copyright (c) 2010, Unister GmbH
 * @link			http://www.unister-gmbh.de
 */

/**
 * Abstrakte Klasse für die Source Plugins. Diese Klasse wird von der jeweiligen
 * Source abgeleitet und stellt gemeinsam genutze Funktionen zur Verfügung, um
 * auf die Config der Source zugreifen zu können und die Config-Werte zu verarbeiten.
 *
 * @package			Core
 * @subpackage		Plugin
 * @author    		Stefan Junge <stefan.junge@unister-gmbh.de>
 * @copyright 		Copyright (c) 2010, Unister GmbH
 * @link			http://www.unister-gmbh.de
 */

abstract class
    Core_Class_Plugin_Source_Abstract
implements
    Core_Class_Plugin_Source_Interface {

    /**
     * current request
     * @var string
     */
    protected $_request = null;

    /**
     * view / layout
     */
    protected $_layout = null;

    /**
     * namespace
     */
    protected $_session = null;

    /**
     * config
     * @var Zend_Config
     */
    protected $_sourceConfig = null;

    /**
     * sourceValues
     * @var Zend_Config
     */
    protected $_sourceValues = null;

    /**
     * timestamp for ini section date check
     * @var null|int
     */
    protected $_timestamp = null;

    /**
     * Standardkondtruktor
     *
     * @param $request
     * @return unknown_type
     */
    public function __construct($request)
    {
        $this->_request = $request;
        $this->_sourceValues = new Zend_Config(array(), true); //mit Modifikation
        $this->_layout = Zend_Layout::getMvcInstance();
        $this->_session = new Zend_Session_Namespace('Source_Plugins');

        //Konfiguration einlesen
        $this->_readConfig();
    }

    /**
     * Liest die Config für die aktuelle Source ein
     */
    protected function _readConfig()
    {
        $config = null;

        //Source aus Klassenname ermitteln
        $source = str_replace('Core_Class_Plugin_Source_', '', get_class($this));

        //für Modulpath
        if (!$module = Zend_Controller_Front::getInstance()->getRequest()->getModuleName()) {
            $module = Zend_Controller_Front::getInstance()->getDefaultModule();
        }

        $sections = array(); //Sections, die verwendet werden
        $filename = Core_Config :: getInstance()->MODULE_PATH . '/' . ucfirst($module) . '/Config/Source/'.$source.'.ini';
        if (file_exists($filename)) {
            try {
                $config = new Zend_Config_Ini($filename);

                //Alle Sektions parsen
                foreach ($config as $sectionName => $value) {
                    if ($this->_isSectionActive($sectionName)) {
                        //Sektion ist im Zeitfenster, also laden
                        $sections[$sectionName] = $value;
                    }
                }

                //nochmal Zend_Config lasen, dieses Mal mit default-Section
                $config = new Zend_Config_Ini($filename, 'default', array('allowModifications' => true));
                foreach ($sections as $key => $section) {
                    $config->merge($section);
                    $config->setExtend( 'default', $key );
                }

            } catch (Exception $e) {
                Core_Class_Log::getInstance()->crit($e);
            }
        }

        if ($config === null) {
            //Keine Configdaten --> leere Config
            $config = new Zend_Config(array());
        }

        $this->_sourceConfig = $config;
    }

    /**
     * Schließt das Tracking ab und übergibt die vorhandenen Configwerte an den Advertisment-Helper
     *
     * (non-PHPdoc)
     * @see application/modules/Core/Class/Plugin/Source/Core_Class_Plugin_Source_Interface#finish()
     */
    protected function _finish()
    {
        if (count($this->_sourceValues) > 0) {

            // merge advertisment config
            if (isset($this->_sourceValues->advertisment)) {
                 Core_Class_Advertisment_Config::getInstance()->setSourceConfig($this->_sourceValues->advertisment);
            }

            // merge application config
            if (isset($this->_sourceValues->config)) {
                Zend_Registry::get('_CONFIG')->merge($this->_sourceValues->config);
            }
            
            // merge conversion config
            if (isset($this->_sourceValues->conversion)) {
                Core_Class_Config::setOverrideParams($this->_sourceValues->conversion, 'conversion');
            }
        }
    }

    /**
     * holt sich den key anhand von $id aus dem Request oder der Session
     * Fügt den Wert in die SourceValues hinzu und ggf. auch in die Session (falls noch nicht vorhanden)
     *
     * @param unknown_type $id
     * @return unknown_type
     */
    protected function _checkKey($id)
    {
        if (is_null($id)) {
            //id nicht angegeben
            return;
        }


        //Key aus Request ermittln
        $key = $this->_request->getParam($id);
        if (is_null($key)) {
            //Key nicht im Request vorhanden, aus der Session holen
            if (isset($this->_session->$id)) {
                //Key in der Session vorhanden
                $key = $this->_session->$id;
            } else {
                //Key weder im Request, noch in der Session
                return;
            }
        }

        if (isset($this->_sourceConfig) &&
            isset($this->_sourceConfig->default)) {

            $this->_sourceValues->merge($this->_sourceConfig->default);
        }

        if (isset($this->_sourceConfig) &&
            isset($this->_sourceConfig->$id) &&
            isset($this->_sourceConfig->$id->$key)) {
            //key existiert

            //Key zur Session hinzufügen bzw. überschreiben
            $this->_session->$id = $key;

            //SoruceValues ergänzen
            $this->_sourceValues->merge($this->_sourceConfig->$id->$key);
        }
    }

    /**
     * prüft den übergebenen Namen einer Sektion aus Zend_Config, ob dieser zur aktuellen Uhrzeit passt
     *
     * @param string $sectionName
     * @return boolean
     */
    protected function _isSectionActive($sectionName) {
        if ($sectionName == 'default') {
            //Default nicht übernehmen
            return false;
        }

        //Sectionname nach Zeiten filtern
        $sectionParams = $this->_sectionNameToDate($sectionName);
        if (is_null($sectionParams)) {
            //Kein gültiger Section-Name
            return false;
        }

        if (is_null($this->_timestamp)) {
            //Aktuelle Zeit noch nicht gesetzt, jetzt setzen
            $this->_timestamp = strftime('%Y-%m-%d %H:%M:%S ', time());
        }

        //boolean => Sektion im Zeitfenster oder nicht
        return $sectionParams['beginDate'] <= $this->_timestamp &&
               $sectionParams['endDate'] > $this->_timestamp;
    }

    /**
     * Wandelt den String in einen Zeitspanne von-bis um
     *
     * @param string $sectionName
     * @return null|array
     */
    protected function _sectionNameToDate($sectionName) {
        //2010-04-10|04-2010-04-12|20
        $sectionName = substr($sectionName,0,27);
        if (strlen($sectionName) != 27) {
            //Inkompatibel, Section muss 27 Zeichen lang sein.
            Core_Class_Log::getInstance()->warn('Advertisment: Section-Name is incompatible '.$sectionName);
            return;
        }

        $pattern = '/^(20[1-9][0-9]|[2-9][1-9][0-9]{2})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]).([0-1][0-9]|2[0-3]).(20[1-9][0-9]|[2-9][1-9][0-9]{2})-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1]).([0-1][0-9]|2[0-3])/';
        if (preg_match($pattern, $sectionName, $matches) == 0) {
            //Format der Zeile stimmt nicht
            return;
        }

        //Datumswert aus dem String ermitteln
        $beginDate = strftime(substr($sectionName,0,10) . ' '.substr($sectionName,11,2).':00:00');
        $endDate = strftime(substr($sectionName,14,10) . ' '.substr($sectionName,25,2).':00:00');

        if ($beginDate > $endDate) {
            //Begin liegt vor dem Ende //ungültig
            Core_Class_Log::getInstance()->warn('Advertisment: beginDate ist larger than endDate '.$sectionName);
            return;
        }

        $result = array(
            'beginDate' => $beginDate,
            'endDate' => $endDate,
        );

        return $result;
    }
}
