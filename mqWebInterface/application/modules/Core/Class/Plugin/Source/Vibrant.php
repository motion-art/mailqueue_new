<?php
/**
 * Datei für Klasse Core_Class_Plugin_Source_Vibrant
 *
 * @package			Core
 * @subpackage		Plugin
 * @author    		Stefan Junge <stefan.junge@unister-gmbh.de>
 * @version   		$Id: $
 * @copyright 		Copyright (c) 2010, Unister GmbH
 * @link			http://www.unister-gmbh.de
 */

/**
 * Klasse für die Vibrant-Werbung
 *
 * @package			Core
 * @subpackage		Plugin
 * @author    		Stefan Junge <stefan.junge@unister-gmbh.de>
 * @copyright 		Copyright (c) 2010, Unister GmbH
 * @link			http://www.unister-gmbh.de
 */

class
    Core_Class_Plugin_Source_Vibrant
extends
    Core_Class_Plugin_Source_Abstract {

    /**
     * Auswerten der erforderlichen Vibrant-Keys
     *
     * (non-PHPdoc)
     * @see Source/Price_Class_Source_Interface#track()
     */
    public function track(){

        $this->_checkKey('vibid');

        $this->_finish();
    }
}
