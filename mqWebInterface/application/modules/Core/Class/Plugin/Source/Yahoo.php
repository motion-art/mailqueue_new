<?php
/**
 * Datei für Klasse Core_Class_Plugin_Source_Yahoo
 *
 * @package			Core
 * @subpackage		Plugin
 * @author    		Stefan Junge <stefan.junge@unister-gmbh.de>
 * @version   		$Id: $
 * @copyright 		Copyright (c) 2010, Unister GmbH
 * @link			http://www.unister-gmbh.de
 */

/**
 * Klasse für die Yahoo Werbung
 *
 * @package			Core
 * @subpackage		Plugin
 * @author    		Stefan Junge <stefan.junge@unister-gmbh.de>
 * @copyright 		Copyright (c) 2010, Unister GmbH
 * @link			http://www.unister-gmbh.de
 */

class
    Core_Class_Plugin_Source_Yahoo
extends
    Core_Class_Plugin_Source_Abstract {

    /**
     * Auswerten der erforderlichen Yahoo-Keys
     *
     * (non-PHPdoc)
     * @see Source/Core_Class_Source_Interface#track()
     */
    public function track(){

        $this->_checkKey('adid');

        $this->_checkKey('koid');

        $this->_checkKey('kaid');

        $this->_finish();
    }
}
