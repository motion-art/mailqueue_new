<?php
/**
 * Datei für Interface Core_Class_Plugin_Source_Interface
 *
 * @package			Core
 * @subpackage		Plugin
 * @author    		Stefan Junge <stefan.junge@unister-gmbh.de>
 * @version   		$Id: $
 * @copyright 		Copyright (c) 2010, Unister GmbH
 * @link			http://www.unister-gmbh.de
 */

/**
 * Interface für die Advertisment-Klassen
 *
 * @package			Core
 * @subpackage		Plugin
 * @author    		Stefan Junge <stefan.junge@unister-gmbh.de>
 * @copyright 		Copyright (c) 2010, Unister GmbH
 * @link			http://www.unister-gmbh.de
 */

interface Core_Class_Plugin_Source_Interface {

    /**
     * Standardkonstructor
     */
    public function __construct($request);

    /**
     * Funktion zum Validieren der Request-Parameter
     */
    public function track();
}
