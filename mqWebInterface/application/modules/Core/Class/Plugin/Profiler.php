<?php

/**
 * Core_Class_Plugin_Profiler
 *
 * use Core_Class_Profiler_Writer to write profiler data
 * default profile-block "Total" starts with preDispatch and stops
 * with dispatchLoopShutdown
 *
 * to set user blocks:
 * Core_Class_Profiler::getInstance()->start($name);
 * Core_Class_Profiler::getInstance()->end($name);
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: Profiler.php 316 2010-06-24 13:02:55Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Plugin_Profiler extends Zend_Controller_Plugin_Abstract
{
    /**
     * writer object
     * @var Core_Class_Profiler_Writer_Interface
     */
    protected $_writer = null;

    /**
     * minimum seconds per block to log
     * @var integer
     */
    protected $_seconds = 1;

    /**
     * log memory usage
     * @var boolean
     */
    protected $_logMemory = false;

    public function __construct(Core_Class_Profiler_Writer_Interface $writer)
    {
        if (!$writer instanceof Core_Class_Profiler_Writer_Interface) {
            throw new Exception('Profiler_Writer must implement Core_Class_Profiler_Writer_Interface');
        }
        $this->_writer = $writer;
        $this->_writer->init($this);
    }

    public function setMinSeconds($value)
    {
        $this->_seconds = (float)$value;
    }

    public function setLogMemory($value)
    {
        $this->_logMemory = (bool)$value;
    }

    public function preDispatch(Zend_Controller_Request_Abstract $request)
    {
        // Start Profiling
        Core_Class_Profiler::getInstance()->start('Total');
    }

    public function dispatchLoopShutdown()
    {
        // End Profiling
        Core_Class_Profiler::getInstance()->end('Total');

        foreach ((array)Core_Class_Profiler::getInstance()->getResults() as $name => $seconds) {
            if ($this->_seconds <= $seconds)
                $this->_writer->log($name, $seconds);
        }

        // MemoryUsage
        if ($this->_logMemory)
            $this->_writer->log('MemoryPeakUsage', memory_get_peak_usage(true)/1024);

        $this->_writer->finish();
    }
}
