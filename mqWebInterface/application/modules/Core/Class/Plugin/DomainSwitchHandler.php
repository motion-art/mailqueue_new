<?php

/**
 * Core_Class_Plugin_DomainSwitchHandler
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id$
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Plugin_DomainSwitchHandler extends Zend_Controller_Plugin_Abstract
{
    /**
     * Config konfigurierbarer switcher zwischen modulen
     * Benutzt die Config:
     * z.B.
     * domain.switch.host[]    = "strom.preisvergleich.de"
     * domain.switch.module[]  = "preisvergleich"
     * domain.switch.host[]    = "strom.geld.de"
     * domain.switch.module[]  = "geld"
     * direkter wechsel:
     * ...?switchDomain=strom.preisvergleich.de
     * (non-PHPdoc)
     * @see Controller/Plugin/Zend_Controller_Plugin_Abstract#routeShutdown($request)
     */
    public function routeStartup(Zend_Controller_Request_Abstract $request)
    {
        $config = Zend_Registry::get('_CONFIG');

        if (!$config->domain->switch) {
            //do nothing, keine seo-config gefunden / seo-Netzwerk nicht eingebunden
            return;
        }

        $requestDomain =  $_SERVER["HTTP_HOST"];
        $hardSwitch = $request->getParam('switchDomain', false);

        $switchSession = new Zend_Session_Namespace('switchDomain');

        if ($hardSwitch) {
            if ($hardSwitch !== 'off') {
                $switchSession->domain = $hardSwitch;
            } else {
                unset($switchSession->domain);
            }
        }

        if (isset($switchSession->domain)) {
            $requestDomain = $switchSession->domain;
        }

        $domains = $config->domain->switch->toArray();

        $key = array_search(strtolower($requestDomain), $domains['host']);

        if ($key === false) {
            if ($config->domain->default) {
                $key = array_search((string)$config->domain->default, $domains['host']);
            } else {
                Core_Class_Log::getInstance()->debug("SwitchDomain: kein Switch");
                return;
            }
        }

        if ($key !== false) {
            $bootstrap = Zend_Controller_Front::getInstance()->getParam('bootstrap');
            $bootstrap->getResource('Module')->setDefaultModule($domains['module'][$key]);
        } else {
            throw new Exception('no domain for domain-switch found');
        }

        Core_Class_Log::getInstance()->debug('SwitchDomain: ' . $domains['module'][$key]);
    }
}
