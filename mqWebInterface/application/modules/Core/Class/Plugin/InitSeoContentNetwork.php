<?php

class Core_Class_Plugin_InitSeoContentNetwork
extends Zend_Controller_Plugin_Abstract
{

    /**
     * Benutzt eine Config um das SeContentnetwork zu konfigurieren:
     * ;************************
     * ;SeoContentNetwork
     * ;************************
     * seo.network.adapter.class           = "Core_Class_Seo_Network_Adapter_Xml"
     * seo.network.adapter.options.xml     = "Resource/Seo/ContentNetwork.xml"
     *
     * ;************************
     * ;LinkBuilder
     * ;************************
     * seo.network.page.linkBuilder.class  	            = "Core_Class_Seo_Network_Page_Link_Router"
     * seo.network.page.linkBuilder.options.routeName 	= "seoContentNetwork"
     *
     * ;************************
     * ;PageGenerator
     * ;************************
     * seo.network.page.generator.class					           = "Core_Class_Seo_Network_Page_Generator_Tags"
     * seo.network.page.generator.options.tags.0.regex             = "/\[#(.*?);(.*?);(.*?)#\]/"
     * seo.network.page.generator.options.tags.0.template          = "seocontent/generator/link.phtml"
     * seo.network.page.generator.options.tags.0.callback.class    = "Power_Class_Seo_TagGeneratorCallback"
     * seo.network.page.generator.options.tags.0.callback.function = "formatLinks"
     *
     * seo.network.page.generator.options.tags.1.regex             = "/(\[\*inc1\*\])/"
     * seo.network.page.generator.options.tags.1.template          = "seocontent/generator/search.phtml"
     * seo.network.page.generator.options.tags.1.callback.class    = "Power_Class_Seo_TagGeneratorCallback"
     * seo.network.page.generator.options.tags.1.callback.function = "insertSearchTemplate"
     * (non-PHPdoc)
     * @see Controller/Plugin/Zend_Controller_Plugin_Abstract#routeShutdown($request)
     */
    public function routeShutdown(Zend_Controller_Request_Abstract $request)
    {
        $config = Zend_Registry::get('_CONFIG');

        if (!$config->seo->network->adapter->class) {
            //do nothing, keine seo-config gefunden / seo-Netzwerk nicht eingebunden
            return $request;
        }

        $seoLinkBuilder = null;
        $seoPageGenerator = null;

        try {
            // linkbuilder aus config holen, wenn vorhanden
            if ($config->seo->network->page->linkBuilder->class) {
                $linkBuilder = (string) $config->seo->network->page->linkBuilder->class;

                $seoLinkBuilder	= new $linkBuilder(
                    $config->seo->network->page->linkBuilder->options
                );
            }

            // pageGenerator aus config holen, wenn vorhanden
            if ($config->seo->network->page->generator->class) {
                $pageGenerator = (string) $config->seo->network->page->generator->class;

                $seoPageGenerator = new $pageGenerator(
                    $config->seo->network->page->generator->options
                );
            }

            // adapter aus config holen
            $apapter = (string) $config->seo->network->adapter->class;
            $seoPageAdapter	= new $apapter(
                $config->seo->network->adapter->options,
                $seoLinkBuilder,
                $seoPageGenerator
            );

            Core_Class_Seo_Network::getInstance()
                ->setNetworkAdapter($seoPageAdapter);

        } catch (Exception $e) {
            Core_Class_Log::getInstance()->err($e);
        }
    }
}
