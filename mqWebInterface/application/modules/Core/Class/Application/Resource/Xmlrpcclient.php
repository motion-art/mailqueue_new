<?php


/**
 * XmlRpcClient resource
 *
 * Example configuration:
 * <pre>
 *   resources.xmlrpcclient.url = "http://localhost/service.php"
 * </pre>
 *
 * @category   Core
 * @package    Core_Class_Application
 * @subpackage Resource
 * @copyright  Unister GmbH
 */
class Core_Class_Application_Resource_Xmlrpcclient
    extends Zend_Application_Resource_ResourceAbstract
{
    /**
     * @see Zend_Application_Resource_ResourceAbstract
     */
    public function init()
    {
        $options = $this->getOptions();

        if (!isset($options['url'])) {
            throw new Exception('xmlrpc-url not defined');
        }

        $client = new Zend_XmlRpc_Client($options['url']);

        return $client;
    }
}