<?php

class Core_Class_Application_Resource_Pagecache
    extends Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
        $config = $this->getOptions();

        if ($config['enabled'] == true) {

            if (!isset($config['frontendOptions'])) {
                throw new Exception('pagecache frontendOptions required');
            }
            if (!isset($config['backendOptions'])) {
                throw new Exception('pagecache backendOptions required');
            }

            $frontendOptions = $config['frontendOptions'];
            if (isset($config['pages'])) {
                foreach ($config['pages'] as $page) {
                    $frontendOptions['regexps'][$page['uri']] = array(
                        'cache' => true
                    );
                    if (isset($page->exclude)) {
                        $frontendOptions['regexps'][$page['uri']]['exclude'] = $page['exclude'];
                    }
                    if (isset($page->options)) {
                        $frontendOptions['regexps'][$page['uri']] = array_merge($frontendOptions['regexps'][$page['uri']], $page['options']);
                    }
                }
            }
            if (!isset($frontendOptions['default_options'])) {
                $frontendOptions['default_options'] = array();
            }
            $frontendOptions['default_options']['cache'] = false;
            $frontendOptions['default_options']['cache_with_cookie_variables'] = true;
            $frontendOptions['default_options']['cache_with_get_variables'] = true;
            $frontendOptions['default_options']['make_id_with_cookie_variables'] = false;

            $cache = Zend_Cache::factory(
                'Core_Class_Cache_Frontend_Page',
                 $config['backend'],
                 $frontendOptions,
                 $config['backendOptions'],
                 true
            );
            $cache->start();

            Zend_Registry::set('Zend_Cache_Frontend_Page', $cache);

            return $cache;
        }
    }
}
