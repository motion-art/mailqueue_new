<?php

class Core_Class_Application_Resource_Config
    extends Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
        $config = new Zend_Config(array(), true);
        Zend_Registry::set('_CONFIG', $config);

        return $config;
    }
}
