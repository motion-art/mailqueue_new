<?php

class Core_Class_Application_Resource_Xmlrpcserver
    extends Zend_Application_Resource_ResourceAbstract
{
    const CACHE_FILENAME = 'xmlrpc.cache';

    /**
     * Default option values.
     *
     * @var array
     */
    protected $_defaultOptions = array(
        'caching' => true
    );

    public function init()
    {
        Zend_XmlRpc_Server_Fault::attachFaultException('Core_Class_XmlRpc_Exception');
        Zend_XmlRpc_Server_Fault::attachObserver('Core_Class_XmlRpc_Observer_Logger');

        $options = $this->mergeOptions($this->_defaultOptions, $this->getOptions());
        $cacheFile = Core_Config::getInstance()->CACHE_PATH . DIRECTORY_SEPARATOR . self::CACHE_FILENAME;

        $server = new Zend_XmlRpc_Server();

        if (!$options['caching'] || !Zend_XmlRpc_Server_Cache::get($cacheFile, $server)) {

            if (isset($options['service']) &&
                is_array($options['service']) &&
                count($options['service']) > 0
            ) {

                foreach ($options['service'] as $service) {
                    try {
                        $server->setClass($service['class'], $service['namespace']);
                    } catch (Exception $e) {
                        throw new Core_Class_XmlRpc_Exception($e->getMessage());
                    }
                }

                if ($options['caching']) {
                    Zend_XmlRpc_Server_Cache::save($cacheFile, $server);
                }

            } else {
                throw new Core_Class_XmlRpc_Exception('no services defined in config');
            }
        }

        return $server;
    }
}