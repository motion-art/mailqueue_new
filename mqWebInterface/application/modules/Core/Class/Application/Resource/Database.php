<?php

class Core_Class_Application_Resource_Database
    extends Zend_Application_Resource_ResourceAbstract
{
    protected $_defaultOptions = array(
        'adapterNamespace' => 'Core_Class_Db_Adapter',
        'type' => 'pdo_mysql',
        'profiler' => false,
        'cache' => false
    );

    public function init()
    {
        $options = $this->mergeOptions($this->_defaultOptions, $this->getOptions());

        $db = Zend_Db::factory($options['type'], $options);

        if ($options['profiler'] == true) {
            $profiler = new Zend_Db_Profiler_Firebug('All DB Queries');
            $profiler->setEnabled(true);
            $db->setProfiler($profiler);
        }

        Zend_Db_Table_Abstract::setDefaultAdapter($db);

        if (isset($options['cache']) && $options['cache']) {
            Zend_Db_Table_Abstract::setDefaultMetadataCache(
                Core_Class_Cache::getCache(Core_Class_Cache::CACHE_NAME_DATABASE)
            );
        }

        Zend_Registry::set('_DB', $db);

        return $db;
    }
}
