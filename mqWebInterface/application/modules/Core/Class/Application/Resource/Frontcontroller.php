<?php

class Core_Class_Application_Resource_Frontcontroller
    extends Zend_Application_Resource_Frontcontroller
{
    const CONTROLLER_DIRECTORY = 'Controller';

    public function init()
    {
        $front = parent::init();
        $front->setModuleControllerDirectoryName(self::CONTROLLER_DIRECTORY);

        return $front;
    }
}
