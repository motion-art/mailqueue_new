<?php

class Core_Class_Application_Resource_Logger
    extends Zend_Application_Resource_ResourceAbstract
{
    /**
     * php parse error filename
     * @var string
     */
    const PHP_ERROR_LOG = 'php_%s_logger.log';

    /**
     * application error filename
     * @var string
     */
    const APP_ERROR_LOG = 'php_%s_app_logger.log';

    const TYPE_CLI = 'cli';
    const TYPE_APP = 'app';

    protected $_defaultOptions = array(
        'php' => array(
            'level' => 'DEBUG',
            'enabled' => true,
            'show' => false
        ),
        'application' => array(
            'type' => self::TYPE_APP,
            'level' => 'DEBUG',
            'firebug' => false
        )
    );

    public function init()
    {
        $prefix = 'http';
        $config = $this->mergeOptions(
            $this->_defaultOptions,
            $this->getOptions()
        );

        $logClass = new ReflectionClass('Zend_Log');

        // php log
        if ($config['php']['enabled']) {

            $phpLogger = new Zend_Log();

            $log = new Zend_Log_Writer_Stream(
                Core_Config::getInstance()->LOGFILE_PATH
                . DIRECTORY_SEPARATOR
                . sprintf(self::PHP_ERROR_LOG, $prefix)
            );
            $log->setFormatter(new Zend_Log_Formatter_Simple());

            if (!$logClass->hasConstant($config['php']['level'])) {
                throw new Exception('log-level "' . $config['php']['level'] . '" not found');
            }
            $log->addFilter(new Zend_Log_Filter_Priority($logClass->getConstant($config['php']['level'])));

            if (isset($config['php']['template'])) {
                Core_Class_ErrorHandler::setErrorTemplate(Core_Config::getInstance()->APPLICATION_PATH . DIRECTORY_SEPARATOR . $config['php']['template']);
            }

            $phpLogger->addWriter($log);

            Core_Class_ErrorHandler::setLogger($phpLogger);
            Core_Class_ErrorHandler::setShowErrors($config['php']['show']);
            Core_Class_ErrorHandler::register();
        }

        // application log
        $logger = new Zend_Log();

        if ($config['application']['type'] == self::TYPE_CLI) {

            $log = new Zend_Log_Writer_Stream('php://output');
            $log->setFormatter(new Core_Class_Log_Formatter_Cli());
            $logger->addWriter($log);

        } else {

            $log = new Zend_Log_Writer_Stream(
                Core_Config::getInstance()->LOGFILE_PATH
                . DIRECTORY_SEPARATOR
                . sprintf(self::APP_ERROR_LOG, $prefix)
            );
            $log->setFormatter(new Zend_Log_Formatter_Simple());

            if (!$logClass->hasConstant($config['application']['level'])) {
                throw new Exception('log-level "' . $config['application']['level'] . '" not found');
            }
            $log->addFilter(new Zend_Log_Filter_Priority($logClass->getConstant($config['application']['level'])));
            $logger->addWriter($log);
        }

        if ($config['application']['firebug']) {

            $logger->addWriter(new Zend_Log_Writer_Firebug());
        }

        Zend_Registry::set('_LOGGER', $logger);

        return $logger;
    }
}
