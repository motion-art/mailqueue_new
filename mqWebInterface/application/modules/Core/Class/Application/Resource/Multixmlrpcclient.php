<?php

/**
 * MultiXmlRpcClient resource
 *
 * Manager
 *
 * Example configuration:
 * <pre>
 *   resources.multixmlrpcclient.name.url = "http://localhost/service.php"
 * </pre>
 *
 * @category   Core
 * @package    Core_Class_Application
 * @subpackage Resource
  * @copyright  Unister GmbH
 */
class Core_Class_Application_Resource_Multixmlrpcclient
    extends Zend_Application_Resource_ResourceAbstract
{
    protected $_clients = array();

    protected $_defaultOptions = array(
        'client' => array(
            'class' => 'Zend_Http_Client',
            'options' => array(
                'maxredirects'    => 5,
                'strictredirects' => false,
                'useragent'       => 'Zend_Http_Client',
                'timeout'         => 5,
                'adapter'         => 'Zend_Http_Client_Adapter_Socket',
                'httpversion'     => Zend_Http_Client::HTTP_1,
                'keepalive'       => false,
                'storeresponse'   => true,
                'strict'          => true,
                'output_stream'   => false,
                'encodecookies'   => true
            )
        )
    );

    /**
     * @see Zend_Application_Resource_ResourceAbstract
     */
    public function init()
    {
        return $this;
    }

    /**
     * liefert XmlRpC-Client Instanz für übergebenen Namen zurück
     *
     * @param string $name
     * @return Zend_XmlRpc_Client
     * @throws Exception
     */
    public function getClient($name)
    {
        $name = strtolower($name);

        if (!isset($this->_clients[$name])) {

            $options = $this->getOptions();

            if(!isset($name) || !isset($options[$name]) || !isset($options[$name]['url'])) {
                throw new Exception('name or url not defined');
            }

            $options = $this->mergeOptions($this->_defaultOptions, $options[$name]);

            if (!class_exists($options['client']['class'])) {
                throw new Exception('httpclient "'.$options['client']['class'].'" not found');
            }

            $httpClient = new $options['client']['class']();
            $httpClient->setConfig($options['client']['options']);

            if (isset($options['client']['auth'])) {
                $auth = $options['client']['auth'];
                $httpClient->setAuth(
                    $auth['username'],
                    $auth['password'],
                    $auth['type']
                );
            }

            $this->_clients[$name] = new Zend_XmlRpc_Client($options['url'], $httpClient);

            if (isset($options['skipSystemLookup'])) {
                $this->_clients[$name]->setSkipSystemLookup($options['skipSystemLookup']);
            }
        }

        return $this->_clients[$name];
    }
}