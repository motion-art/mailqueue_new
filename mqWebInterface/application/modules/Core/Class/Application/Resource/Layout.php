<?php

class Core_Class_Application_Resource_Layout
    extends Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
        $options = $this->getOptions();

        $config = array(
            'layoutPath'    => Core_Config :: getInstance()->APPLICATION_PATH . '/layouts/',
            'contentKey'    => 'content',
            'layout'        => 'default'
        );
        if (count($options) > 0) {
            $config = array_merge($config, $options);
        }

        $layout = Zend_Layout::startMvc($config);

        return $layout;
    }
}
