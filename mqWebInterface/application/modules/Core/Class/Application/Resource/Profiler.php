<?php

class Core_Class_Application_Resource_Profiler
    extends Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
        $front = $this->getBootstrap()->bootstrap('frontController')->getResource('frontController');

        $writer = new Core_Class_Profiler_Writer_Firebug();
        $profiler = new Core_Class_Plugin_Profiler($writer);
        $profiler->setMinSeconds(0);
        $profiler->setLogMemory(true);
        $front->registerPlugin($profiler);

        return $profiler;
    }
}
