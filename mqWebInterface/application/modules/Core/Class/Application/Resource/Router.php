<?php

class Core_Class_Application_Resource_Router
    extends Zend_Application_Resource_ResourceAbstract
{
    const CONFIG_SECTION = 'routes';

    protected $_config = null;
    protected $_router = null;

    public function init()
    {
        $this->_config = new Zend_Config(array(), true);

        $options = $this->getOptions();

        $router = new Zend_Controller_Router_Rewrite();
        $this->_router = $router;

        if (isset($options['default'])) {
            $defaultRouter = $options['default'];
            $router->addRoute('default',
                call_user_func_array(
                    array($defaultRouter, 'getInstance'),
                    array(
                        new Zend_Config(array())
                    )
                )
            );
        }

        $this->_updateConfig();

        $this->getBootstrap()
             ->bootstrap('frontController')
             ->getResource('frontController')
             ->setRouter($router);

        return $router;
    }

    public function merge($config)
    {
        $this->_config->merge($config);
        $this->_updateConfig();
    }

    protected function _updateConfig()
    {
        if (array_key_exists(self::CONFIG_SECTION, $this->_config->toArray())) {
            try {
                $this->_router->addConfig($this->_config, self::CONFIG_SECTION);
            } catch (Exception $e) {
                 $this->getBootstrap()->bootstrap('logger')->getResource('logger')->crit($e);
            }
        }
    }
}
