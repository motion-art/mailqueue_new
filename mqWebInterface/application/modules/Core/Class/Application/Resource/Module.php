<?php

class Core_Class_Application_Resource_Module
    extends Zend_Application_Resource_ResourceAbstract
{
    protected $_registeredModules = array();
    protected $_setup = null;

    public function init()
    {
        $config = $this->getOptions();

        if (isset($config['init'])) {
            foreach ($config['init'] as $module) {
                if (is_array($module)) {
                    $this->_initModule($module['name'], $module['mvc']);
                } else {
                    $this->_initModule($module);
                }
            }
        }

        if (isset($config['errorHandler'])) {
            $front = $this->getBootstrap()->bootstrap('frontController')->getResource('frontController');
            $front->registerPlugin(
                new Zend_Controller_Plugin_ErrorHandler($config['errorHandler'])
            );
        }

        return $this;
    }

    public function setDefaultModule($name)
    {
        $config = Core_Config :: getInstance()->MODULE_PATH . '/' . ucfirst($name) . '/Config/application.ini';

        if (file_exists($config)) {
            $config = new Zend_Config_Ini($config, Core_Config::getInstance()->RUNMODE);

            foreach ($config->resources as $resource => $options) {

                $bootstrap = $this->getBootstrap();

                if (!$bootstrap->hasResource($resource)) {
                    $bootstrap->registerPluginResource($resource, $options->toArray());
                }

                $bootstrap->bootstrap($resource)->getPluginResource($resource)->setOptions($options->toArray())->init();
            }
        }

        $this->_initModule($name);

        $front = $this->getBootstrap()->bootstrap('frontController')->getResource('frontController');
        $front->setDefaultModule($name)
              ->setParam('prefixDefaultModule', $name);
    }

    protected function _initModule($name, $initMvc = true)
    {
        $moduleDirectory = Core_Config :: getInstance()->MODULE_PATH . '/' . ucfirst($name);

        if (!file_exists($moduleDirectory)) {
            throw new Exception('module "' . $name . '" not exists');
        }

        $this->_getModuleConfig($name);
        Core_Class_Config::addModule($name);

        // add controller directory
        if ($initMvc) {
            $front = $this->getBootstrap()->bootstrap('frontController')->getResource('frontController');
            $front->addControllerDirectory($moduleDirectory . '/Controller', strtolower($name));

            // merge module route config
            if (file_exists($moduleDirectory . '/Config/Route.ini')) {

                $this->getBootstrap()
                     ->bootstrap('router')
                     ->getPluginResource('router')
                     ->merge(
                           new Core_Class_Config_Ini(
                               $moduleDirectory . '/Config/Route.ini',
                               Core_Config::getInstance()->RUNMODE
                           )
                      );
            }

            $view = $this->getBootstrap()->bootstrap('view')->getResource('view');

            //add view filters
            if ($view instanceof Zend_View_Interface && file_exists($moduleDirectory . '/views/filters')) {
                $view->addFilterPath($moduleDirectory . '/views/filters', ucfirst($name) . '_View_Filter');
            }

            // add view helper
            if ($view instanceof Zend_View_Interface && file_exists($moduleDirectory . '/views/helpers')) {
                $view->addHelperPath($moduleDirectory . '/views/helpers', ucfirst($name) . '_View_Helper');
            }

            // add action helper
            if (file_exists($moduleDirectory . '/Class/Controller/Action/Helper')) {
                Zend_Controller_Action_HelperBroker::addPrefix(ucfirst($name) . '_Class_Controller_Action_Helper');
            }
        }
    }

    protected function _getModuleConfig($name)
    {
        $config = $this->getBootstrap()->bootstrap('config')->getResource('config');

        $name = strtolower($name);
        if (!in_array($name, $this->_registeredModules)) {

            $moduleDirectory = Core_Config :: getInstance()->MODULE_PATH . '/' . ucfirst($name);

            // merge module config
            if (file_exists($moduleDirectory . '/Config/Config.ini')) {
                $config->merge(new Core_Class_Config_Ini($moduleDirectory . '/Config/Config.ini', Core_Config::getInstance()->RUNMODE));
            }

            $this->_registeredModules[] = $name;
        }
    }
}
