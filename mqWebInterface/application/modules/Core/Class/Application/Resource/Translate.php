<?php

class Core_Class_Application_Resource_Translate
    extends Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
        $config = $this->getOptions();

        $params = array();
        $language = 'de';
        $path = Core_Config::getInstance()->LANGUAGES_PATH;

        if (isset($config)) {
            if ($config['scan']) {
                $params['scan'] = Zend_Translate::LOCALE_DIRECTORY;
            }
            $params['logUntranslated'] = (isset($config['logUntranslated']) ? $config['logUntranslated'] : false);
            if (isset($config['path'])) {
                $path .= '/' . $config['path'];
            }

            if (isset($config['default'])) {
                $language = $config['default'];
            }
        }

        $translate = new Zend_Translate(
            (isset($config['adapter']) ? $config['adapter'] : 'gettext'),
            $path,
            $language,
            $params
        );

        Zend_Registry::set('Zend_Translate', $translate);

        Core_Class_View_Stream_Wrapper::addPlugin('Core_Class_View_Stream_Plugin_Translation');

        return $translate;
    }
}
