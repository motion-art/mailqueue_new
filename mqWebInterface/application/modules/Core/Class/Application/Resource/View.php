<?php

class Core_Class_Application_Resource_View
    extends Zend_Application_Resource_ResourceAbstract
{
    public function init()
    {
        return $this->getView();
    }

    public function getView()
    {
        $options = $this->getOptions();

        $view = new Core_Class_View_Stream();

        if (isset($options['filter'])) {
            foreach ($options['filter'] as $filter) {
                $view->addFilter($filter);
            }
        }

        if (isset($options['wrapperPlugin'])) {
            foreach ($options['wrapperPlugin'] as $plugin) {
                $view->addPlugin($plugin);
            }
        }

        if (isset($options['doctype'])) {
            $view->doctype($options['doctype']);
        }

        $viewRenderer = new Zend_Controller_Action_Helper_ViewRenderer();
        $viewRenderer->setView($view);

        Zend_Controller_Action_HelperBroker::addHelper($viewRenderer);

        return $view;
    }
}
