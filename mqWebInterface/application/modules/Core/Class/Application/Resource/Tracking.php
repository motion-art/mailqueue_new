<?php

class Core_Class_Application_Resource_Tracking
    extends Zend_Application_Resource_ResourceAbstract
{
    /**
     * Default option values.
     *
     * @var array
     */
    protected $_defaultOptions = array(
        'enabled' => true,
        'adapter' => array(
            'db' => array(
                'enabled' => false,
                'table' => null
            ),
            'output' => array(
                'enabled' => false
            )
        ),
        'track' => array(
            'site' => array(
                'enabled' => false,
                'options' => array(
                    'duration' => true,
                    'memoryUsage' => true,
                    'peakMemoryUsage' => true
                )
            )
        )
    );

    /**
     * Instance of the tracking object.
     *
     * @var Unister_Tracking_Tracking
     */
    protected $_resource = null;

    /**
     * Initialize Tracking
     *
     * Options:
     *  enabled: Set Tracking on or off
     *  table: Tracking table
     *  track.site.enabled: Enable profiling for the whole application (keyword: Site)
     *  track.site.options: Options for profiler
     */
    public function init()
    {
        $resource = $this->_getTrackingResource();

        $options = $this->_getOptions();
        $resource->setEnabled($options['enabled']);

        if ($options['adapter']['output']['enabled']) {
            $adapter = new Unister_Tracking_Tracking_Save_Output();
            $resource->registerShutdown($adapter);
        }

        if ($options['adapter']['db']['enabled']) {
            $db = $this->getBootstrap()->bootstrap('database')->getResource('database');
            $adapter = new Unister_Tracking_Tracking_Save_Zend_Db($db, $options['adapter']['db']['table']);
            $resource->registerShutdown($adapter);
        }

        return $resource;
    }

    protected function _getTrackingResource()
    {
        if ($this->_resource == null) {

            $options = $this->_getOptions();
            $this->getBootstrap()->getApplication()->getAutoloader()->registerNamespace('Unister_Tracking');

            if (!class_exists('Unister_Tracking_Tracking')) {
                throw new Exception('Unister Tracking Library expected');
            }

            $this->_resource = Unister_Tracking_Tracking::getInstance();

            if (isset($options['defaultKeywords'])) {
                $this->_resource->setDefaultKeywords(
                    (array) $options['defaultKeywords']
                );
            }

            if ($options['track']['site']['enabled']) {
                $this->_resource->startProfile(
                    'Site',
                    true,
                    $options['track']['site']['options']
                );
            }
        }
        return $this->_resource;
    }

    /**
     * Get the options merged with default options.
     *
     * @return Array
     */
    protected function _getOptions()
    {
        return $this->mergeOptions($this->_defaultOptions, $this->getOptions());
    }
}