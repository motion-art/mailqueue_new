<?php

class Core_Class_Statistic
{
    const SEPARATOR = '_';
    const PATTERN = '%s_CS_%s';

    const API_REQUEST = 'request';
    const API_TIMEOUT = 'timeout';
    const API_RESPONSE = 'response';

    static private $_config = null;

    static public function log($id)
    {
        if (self::_isValid()) {
            self::_log(array($id));
        }
    }

    static private function _isValid()
    {
        $config = self::_getConfig();
        if (isset($config->filter)) {
            foreach ($config->filter as $filter) {
                if (preg_match("`$filter`", $_SERVER['REQUEST_URI'])) {
                    return false;
                }
            }
        }
        return true;
    }

    static private function _isIgnored()
    {
        $config = self::_getConfig();

        // ignore
        if (isset($config->ignore)) {

            // ignore by user agent
            if (isset($config->ignore->useragent)) {
                foreach ($config->ignore->useragent as $agent) {
                    if (strpos($_SERVER['HTTP_USER_AGENT'], $agent) !== false) {
                        return true;
                    }
                }
            }

            // ignore by ip
            if (isset($config->ignore->ip)) {
                foreach ($config->ignore->ip as $ip) {
                    if (strpos($_SERVER['REMOTE_ADDR'], $ip) !== false) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    static public function track($id)
    {
        if (self::_isIgnored()) {
            return false;
        }

        $logInfo = array();
        $groupInfo = array();
        $compatibilityInfo = array();

        // split
        $logInfo[] = Core_Class_Split_Handler::getInstance()->getTrackingPrefix();

        // source
        if ($source = Core_Class_Source::getInstance()->getSource()) {
            $logInfo[] = $source;
            $groupInfo[] = $source;
            $compatibilityInfo[] = $source;
        }

        // source - group
        if ($group = Core_Class_Source::getInstance()->getGroup()) {
            $groupInfo[] = $group;
            $groupInfo[] = $id;
            self::_log($groupInfo);
        }

        // id
        $logInfo[] = $id;
        $compatibilityInfo[] = $id;

        self::_log($logInfo);

        // compatibility
        self::_log($compatibilityInfo);
    }

    static private function _getConfig()
    {
        if (self::$_config === null) {
            if (!$config = Zend_Registry::get('_CONFIG')->tracking) {
                throw new Exception('tracking config expected');
            }

            if (!$config->directory) {
                throw new Exception('tracking directory expected');
            }
            self::$_config = $config;
        }

        return self::$_config;
    }

    static private function _log(array $logInfo)
    {
        $config = self::_getConfig();

        $directory = Core_Config :: getInstance()->BASE_PATH . '/' . $config->directory;
        $filename = sprintf(
            self::PATTERN,
            implode(self::SEPARATOR, $logInfo),
            date('Y-m-d-H')
        );

        $umask = umask(0);
        if (!file_exists($directory)) {
            @mkdir($directory, 0777);
        }
        @chmod($directory, 0777);
        umask($umask);

        if ($fileHandler = @fopen($directory . '/' . $filename, 'a')) {
            fwrite($fileHandler, '.');
            fclose($fileHandler);
        }
    }

    static public function clickin()
    {
        if (self::_isIgnored()) {
            return false;
        }

        self::track('clickin');

        if ($source = Core_Class_Source::getInstance()->getSource()) {
            self::_track(
                array(
                    'Conversion',
                    'Campaign',
                    ucfirst($source),
                    'CI'
                )
            );
        }
    }

    static public function clickout($type)
    {
        if (self::_isIgnored()) {
            return false;
        }

        self::track($type);

        if ($source = Core_Class_Source::getInstance()->getSource()) {
            self::_track(
                array(
                    'Conversion',
                    'Campaign',
                    ucfirst($source),
                    'CO',
                    ucfirst($type)
                )
            );
        }
    }

    static public function search($id, $value = 1)
    {
        if (self::_isIgnored()) {
            return false;
        }

        self::_track(
            array(
                'Search',
                ucfirst($id)
            ),
            true,
            $value
        );
    }

    static public function api($name, $key, $value = 1, $avg = false)
    {
        if (self::_isIgnored()) {
            return false;
        }

        self::log(strtolower($name . '_' . $key));

        self::_track(
            array(
                'API',
                ucfirst($name),
                ucfirst($key)
            ),
            true,
            $value,
            $avg
        );
    }

    static private function _track($params, $default = true, $value = 1, $avg = false)
    {
        if (@class_exists('Unister_Tracking_Tracking')) {

            if (Core_Class_Split_Handler::getInstance()->isEnabled()) {
                Unister_Tracking_Tracking::getInstance()->track(
                    array_merge(array('Split', Core_Class_Split_Handler::getInstance()->getTrackingPrefix()), $params),
                    $default,
                    $value,
                    $avg
                );
            }
            Unister_Tracking_Tracking::getInstance()->track(
                array_merge(array('Default'), $params),
                $default,
                $value,
                $avg
            );
        }
    }
}
