<?php

class Core_Class_Config
{
    private static $_config = array();
    private static $_modules = array();
    
    private static $_overrideParams = array();

    public static function setOverrideParams(Zend_Config $config, $name)
    {
        self::$_overrideParams[$name] = $config;
    }

    public static function addModule($module)
    {
        $module = strtolower($module);
        self::$_modules[$module] = $module;
    }

    /**
     * get config as zend_config
     * @param string $config
     * @param string $section
     * @param string $path - empty or 'path/' with ending slash
     *
     * @return Zend_Config
     */
    public static function get($config, $section = null, $path = '')
    {
        $config = strtolower($config);
        
        $filename = $path . ucfirst($config);
        $key = md5($filename . $section);

        $request = Zend_Controller_Front::getInstance()->getRequest();
        if ($request == null || !$module = $request->getModuleName()) {
            $module = Zend_Controller_Front::getInstance()->getDefaultModule();
        }

        if (!array_key_exists($key, self::$_config)) {
            self::$_config[$key] = self::_initConfig($filename, $section, $module);
        }
        //merge override config params
        if (isset(self::$_overrideParams[$config])) {
            self::$_config[$key]->merge(self::$_overrideParams[$config]);
        }
            
        return self::$_config[$key];
    }

    private static function _initConfig($config, $section = null, $module = null)
    {
        if ($section === null) {
            $section = Core_Config::getInstance()->RUNMODE;
        }

        $cacheKey = md5($config . $section . $module . Core_Class_Revision::get());
        $cache = Core_Class_Cache::getCache(Core_Class_Cache::CACHE_NAME_CONFIG);

        if (!$result = $cache->load($cacheKey)) {

            $result = new Zend_Config(array(), true);

            $modules = self::$_modules;
            if ($module != null) {
                $modules[$module] = $module;
            }

            try {
                foreach ($modules as $module) {
                    $filename = Core_Config :: getInstance()->MODULE_PATH . '/' . ucfirst($module) . '/Config/' . $config . '.ini';
                    if (file_exists($filename)) {
                        $result->merge(new Core_Class_Config_Ini($filename, $section));
                    }
                }
            } catch (Exception $e) {
                Core_Class_Log::getInstance()->err($e);
            }

            $cache->save($result, $cacheKey);
        }

        return $result;
    }
}