<?php

/**
 * Core_Class_Mail
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Mail
{
    /**
     * default main view script
     * @var string
     */
    const DEFAULT_MAIN_SCRIPT = 'includes/mail/main';

    /**
     * default view suffix
     * @var string
     */
    const DEFAULT_VIEW_SUFFIX = 'phtml';

    /**
     * default character set
     * @var string
     */
    const DEFAULT_CHARSET = 'utf-8';

    /**
     * view
     * @var Zend_View_Abstract
     */
    protected $_view = null;

    /**
     * config
     * @var Zend_Config
     */
    protected $_config = null;

    /**
     * database
     * @var Zend_Db_Adapter_Abstract
     */
    protected $_db = null;

    /**
     * instance
     * @var Core_Class_Mail
     */
    static protected $_instance = null;

    /**
     * construct
     */
    protected function __construct()
    {
        if (!Zend_Registry::get ('_CONFIG')->mail) {
             throw new Exception('mail-config expected');
        }
        if (!Zend_Registry::get ('_CONFIG')->mail->from) {
             throw new Exception('mail-config email expected');
        }

        $this->_config = Zend_Registry::get ('_CONFIG')->mail;
    }

    /**
     * singleton
     *
     * @return Core_Class_Mail
     */
    static public function getInstance()
    {
        if (self::$_instance == null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * sends mail with transport queue
     * could throw exceptions
     *
     * @param string $subject
     * @param string $name
     * @param array $params
     * @params string $recipient
     * @params array $attachments
     *         OPTIONAL, can contain Zend_Mime_Part Objects or some binary strings
     * @return void
     */
    public function sendMail($subject, $tplPath, $params, $recipient, $attachments = null)
    {
        //validate essential params
        if (!isset($this->_config->portal)) {
            throw new Exception ('mail-config portal expected');
        }

        if (!isset($recipient)) {
            throw new Exception ('recipient is expected');
        }

        $text = $this->_getMailText($tplPath, $params, 'text');
        $html = $this->_getMailText($tplPath, $params, 'html');

        // getting E-Mail Charset
        if (!isset($this->_config->charset)) {
            $charset = self::DEFAULT_CHARSET;
        } else {
            $charset = $this->_config->charset;
        }

        // Build E-Mail with ZendMail
        $mail = new Zend_Mail($charset);

        $mail->setBodyHtml($html);
        $mail->setBodyText($text);
        $mail->setFrom($this->_config->from);
        $mail->addTo($recipient);
        $mail->setSubject($subject);

        //add attchement(s)
        if ($attachments != null && is_array($attachments) && count($attachments) > 0 )  {

            foreach ($attachments as $filename => $attachment) {
                $this->_addAttachment($mail, $attachment, $filename);
            }
        }

        //send to mailqueue db, using special db transport adapert
        $transport = new Core_Class_Mail_Transport_Queue($this->_getDb());
        $transport->setPortal($this->_config->portal);

        $mail->send($transport); //throws Exceptions
    }

    /**
     * init mail database
     *
     * @return void
     */
    protected function _getDb()
    {
        if ($this->_db === null) {
            if (!isset($this->_config->db)) {
                throw new Exception('mail.db-config expected');
            }
            $dbConfig = $this->_config->db->toArray();
            $dbConfig['adapterNamespace'] = 'Core_Class_Db_Adapter';
            $this->_db = Zend_Db::factory($dbConfig['type'], $dbConfig);
        }
        return $this->_db;
    }

    /**
     * get viewscript for context
     *
     * @param string $name
     * @param string $context
     * @return string
     */
    protected function _getViewScript($name, $context = 'html')
    {
        $suffix = self::DEFAULT_VIEW_SUFFIX;
        if (strpos($name, $suffix)) {

            return str_replace($suffix, $context . '.' . $suffix, $name);
        }
        return $name . '.' . $context . '.' . $suffix;
    }

    /**
     * render mailscript
     *
     * @param string $name
     * @param array $params
     * @param string $context
     * @return string
     */
    protected function _getMailText($tplPath, $params = array(), $context = 'html')
    {
        $this->getView()->clearVars();
        $this->getView()->assign($params);
        $this->getView()->content = $this->getView()->render($this->_getViewScript($tplPath, $context));

        if (!isset($this->_config->layoutPath)) {
            $layoutPath = self::DEFAULT_MAIN_SCRIPT;
        } else {
            $layoutPath = $this->_config->layoutPath;
        }

        return $this->getView()->render($layoutPath . '.' . $context . '.' . self::DEFAULT_VIEW_SUFFIX);
    }

    /**
     * add attachment to zend_mail object
     *
     * @param objectreferenc $mail
     * @param string or zend_mime_part $attachment
     * @return void
     */
    private function _addAttachment($mail, $attachment, $filename)
    {
        Core_Class_Log::getInstance()->debug($filename);

        if ($attachment instanceof Zend_Mime_Part) {
            $attachment->encoding = Zend_Mime::ENCODING_8BIT;

            //expect zend_mime_part filename
            if (!isset($attachment->filename)) {

                if (!isset($filename) || !is_string($filename)) {
                    throw new Exception('attachment has no filename but it is expected');
                }
                $attachment->filename = $filename;
            }

            $mail->addAttachment($attachment);

        } else {

            if (!isset($filename) || !is_string($filename)) {
                throw new Exception('attachment has no filename but it is expected');
            }

            $mail->createAttachment( $attachment,
                                     Zend_Mime::TYPE_OCTETSTREAM,
                                     Zend_Mime::DISPOSITION_ATTACHMENT,
                                     Zend_Mime::ENCODING_8BIT,
                                     $filename);
        }
    }

    public function getView()
    {
        if ($this->_view === null) {
            $renderer = Zend_Controller_Action_HelperBroker::getStaticHelper('viewRenderer');
            if (!$renderer->view instanceof Zend_View_Interface) {
                throw new Exception('could not found view instance');
            }
            $this->_view = clone $renderer->view;
        }
        return $this->_view;
    }

    public function setView($view)
    {
        if (!$view instanceof Zend_View_Interface) {
            throw new Exception('instance of Zend_View_Interface expected');
        }
        $this->_view = $view;
    }
}