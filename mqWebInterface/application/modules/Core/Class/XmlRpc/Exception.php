<?php
/**
 *
 * Exceptions thrown by xmlrpc
 *
 */
class Core_Class_XmlRpc_Exception extends Zend_XmlRpc_Server_Exception
{
}