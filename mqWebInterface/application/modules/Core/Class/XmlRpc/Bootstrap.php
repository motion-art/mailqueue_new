<?php

class Core_Class_XmlRpc_Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
    public function __construct($application)
    {
        parent::__construct($application);

        if (!$this->hasPluginResource('logger')) {
            $this->registerPluginResource('logger');
        }
        if (!$this->hasPluginResource('config')) {
            $this->registerPluginResource('config');
        }

        Zend_Registry::set(Core_Class_Bootstrap::REGISTRY, $this);
    }

    public function run()
    {
        $server = $this->bootstrap('xmlrpcserver')->getResource('xmlrpcserver');
        echo $server->handle();
    }
}
