<?php

/**
 * Core_Class_BasePath
 *
 * returns basepaths from Config.ini
 *
 * helper.resource.[KEY].path = [RELATIVE PATH]
 * helper.resource.[KEY].base = [BASE URL] [optional]
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id$
 * @copyright 2009 Unister GmbH
 */

class Core_Class_BasePath
{
    /**
     * Konfigurationsknoten (helper.CONFIG_KEY)
     * @var string
     */
    const CONFIG_KEY = 'resource';

    const MODULE_DIRECTORY = 'module';

    /**
     * Singleton
     * @var Core_Class_BasePath
     */
    static private $_instance = null;

    /**
     * Verwendete Modulnamen
     * @var array
     */
    static private $_modules = array();

    /**
     * Default Modulname
     * @var string
     */
    private $_defaultModule = null;

    /**
     * Konfiguration
     * @var Zend_Config
     */
    private $_config = null;

    /**
     * Cache
     * @var Zend_Cache_Core
     */
    private $_cache = null;

    protected function __construct()
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();

        if ($request == null || !$this->_defaultModule = $request->getModuleName()) {
            $this->_defaultModule = Zend_Controller_Front::getInstance()->getDefaultModule();
        }

        if (
            !isset(Zend_Registry::get('_CONFIG')->helper)
            || (
                isset(Zend_Registry::get('_CONFIG')->helper) &&
                !isset(Zend_Registry::get('_CONFIG')->helper->{self::CONFIG_KEY})
        )) {
            Core_Class_Log::getInstance()->info('basePath-Config not found');
            $this->_config = new Zend_Config(array());
        } else {
            $this->_config = Zend_Registry::get('_CONFIG')->helper->{self::CONFIG_KEY};
        }

        $this->_cache = Core_Class_Cache::getCache(Core_Class_Cache::CACHE_NAME_PATH);
    }

    /**
     * Fügt ein Modul hinzu
     * Beim finden von Resourcen wird dieses mit durchsucht
     *
     * @param Modulname string $module
     */
    static public function addModule($module)
    {
        self::$_modules[] = strtolower($module);
    }

    /**
     * Löscht alle Modulnamen
     */
    public function reset()
    {
        self::$_modules = array();
    }

    /**
     * Singleton
     */
    static public function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * Gibt den vollständigen Pfad zur Resource zurück
     *
     * @param Typ der Resource (helper.CONFIG_KEY.$typ) string $type
     * @param Modulname string $module
     * @param Dateiname string $resource
     * @return string
     */
    public function getPath($type = null, $module = null, $resource = null)
    {
        return
            Core_Config :: getInstance()->PUBLIC_PATH .
            DIRECTORY_SEPARATOR .
            $this->_getRelativePath($type, $module, $resource);
    }

    /**
     * Gibt die Url zur Resource zurück
     *
     * @param Typ der Resource (helper.CONFIG_KEY.$typ) string $type
     * @param Modulname string $module
     * @param Dateiname string $resource
     * @return string
     */
    public function getUrl($type = null, $module = null, $resource = null)
    {
        return
            $this->_getBaseUrl($type) .
            $this->_removeScriptName(Zend_Controller_Front::getInstance()->getBaseUrl()) .
            DIRECTORY_SEPARATOR .
            $this->_getRelativePath($type, $module, $resource);
    }

    /**
     * Gibt die Konfiguration des ResourceTyps zurück
     *
     * @param Zend_Config $type
     */
    private function _getConfigByType($type)
    {
        if (!$type) {
            return null;
        }
        if (strlen($type) > 0
             && ($config = $this->_config->$type) != null
        ) {
            return $config;

        } else {

            Core_Class_Log::getInstance()->info('basePath ResourceType "' . $type . '" doesn\'t exists');
        }
        return null;
    }

    /**
     * Gibt die CacheId der Resource zurück
     *
     * @param array $arguments
     */
    private function _getCacheId(array $arguments)
    {
        $activeModule = Zend_Controller_Front::getInstance()->getDefaultModule();

        return md5(serialize($arguments) . Core_Class_Revision::get() . $activeModule);
    }

    /**
     * Gibt den relativen Pfad zum ResourceTyp-Pfad zurück
     *
     * @param Typ der Resource (helper.CONFIG_KEY.$typ) string $type
     * @param Modulname string $module
     * @param Dateiname string $resource
     * @return string
     */
    private function _getRelativePath($type = null, $module = null, $resource = null)
    {
        if ($config = $this->_getConfigByType($type)) {

            if ($config instanceof Zend_Config) {
                $path = $config->path;
            } else {
                $path = $config;
            }

            // ersetze modul mit splitmodul, wenn split aktiv
            $module = Core_Class_Split_Handler::getInstance()->getSplitModuleByModule($module);

            if ($resource != null) {

                return $this->_getResourcePath($path, $resource, $module);

            } else {

               $base = '';
               if ($module != null) {
                   $base =
                        self::MODULE_DIRECTORY .
                        DIRECTORY_SEPARATOR .
                        $module .
                        DIRECTORY_SEPARATOR;
               }
               return
                    $base .
                    $path .
                    DIRECTORY_SEPARATOR;
            }
        }

        return $resource;
    }

    /**
     * Ermittelt den Basispfad zum übergebenen vollständigen Resourcepfad
     *
     * @param string $resource
     * @return string
     */
    public function getBasename($resource)
    {
        foreach ($this->_config as $path) {
            $path .= '/';
            if ($pos = strripos($resource, $path)) {

                return substr($resource, $pos + strlen($path));
            }
        }

        return basename($resource);
    }

    /**
     * Ermittelt den Pfad zur übergebenen Resource
     *
     * @param Basispfad string $path
     * @param Dateiname string $resource
     * @param Modulname string $module
     * @return string
     */
    private function _getResourcePath($path, $resource, $module = null)
    {
        if (empty($resource)) {
            return '';
        }

        $cacheId = $this->_getCacheId(func_get_args());

        if (!($resourcePath = $this->_cache->load($cacheId))) {

            if ($module === null) {
                $module = $this->_defaultModule;
            }

            $resourcePath =
                self::MODULE_DIRECTORY .
                DIRECTORY_SEPARATOR .
                $module .
                DIRECTORY_SEPARATOR .
                $path .
                DIRECTORY_SEPARATOR .
                $resource;

            if (!file_exists(
                    Core_Config :: getInstance()->PUBLIC_PATH .
                    DIRECTORY_SEPARATOR .
                    $resourcePath
            )) {

                 foreach (array_reverse(self::$_modules) as $baseModule) {

                    $resourcePath =
                        self::MODULE_DIRECTORY .
                        DIRECTORY_SEPARATOR .
                        $baseModule .
                        DIRECTORY_SEPARATOR .
                        $path .
                        DIRECTORY_SEPARATOR .
                        $resource;

                    if (file_exists(
                        Core_Config :: getInstance()->PUBLIC_PATH .
                        DIRECTORY_SEPARATOR .
                        $resourcePath
                    )) {
                        break;
                    }
                }
            }

            $this->_cache->save($resourcePath, $cacheId);
        }

        return $resourcePath;
    }

    private function _getBaseUrl($type = null)
    {
        if ($config = $this->_getConfigByType($type)) {
            if (isset($config->base)) {

                return $config->base;
            }
        }
        return '';
    }

    private function _removeScriptName($url)
    {
        if (!isset($_SERVER['SCRIPT_NAME'])) {
            return $url;
        }

        if (($pos = strripos($url, basename($_SERVER['SCRIPT_NAME']))) !== false) {
            $url = substr($url, 0, $pos);
        }

        $url = rtrim($url, '/\\');

        return $url;
    }
}