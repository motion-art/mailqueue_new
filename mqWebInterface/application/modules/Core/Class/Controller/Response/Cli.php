<?php

/**
 * Core_Class_Controller_Response_Cli
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: Cli.php 287 2010-06-07 12:39:32Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Controller_Response_Cli extends Zend_Controller_Response_Abstract
{
    /**
     * Flag; if true, when header operations are called after headers have been
     * sent, an exception will be raised; otherwise, processing will continue
     * as normal. Defaults to false.
     *
     * @see canSendHeaders()
     * @var boolean
     */
    public $headersSentThrowsException = false;

    /**
     * Magic __toString functionality
     *
     * @return string
     */
    public function __toString()
    {
        if ($this->isException() && $this->renderExceptions()) {
            $exceptions = '';
            foreach ($this->getException() as $e) {
                $exceptions .= $e->__toString() . "\n";
            }
            return $exceptions;
        }
        if (is_array($this->_body)) {
            return implode(chr(10), $this->_body);
        } else {
            return $this->_body;
        }
    }
}
