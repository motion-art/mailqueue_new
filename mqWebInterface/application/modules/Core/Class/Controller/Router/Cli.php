<?php

/**
 * Core_Class_Controller_Router_Cli
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: Cli.php 287 2010-06-07 12:39:32Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Controller_Router_Cli extends Zend_Controller_Router_Abstract implements Zend_Controller_Router_Interface {

    public function assemble ($userParams, $name = null, $reset = false, $encode = true) {}

    public function route (Zend_Controller_Request_Abstract $dispatcher) {}
}
