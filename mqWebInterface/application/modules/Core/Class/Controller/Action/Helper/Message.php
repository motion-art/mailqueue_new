<?php

/**
 * Core_Class_Controller_Action_Helper_Message
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: Message.php 287 2010-06-07 12:39:32Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Controller_Action_Helper_Message extends Zend_Controller_Action_Helper_Abstract
{
    public function direct($key, $params = null)
    {
        $request = $this->getRequest();
        $controller = strtolower($request->getControllerName());
        $module = strtolower($request->getModuleName());

        if ($controller != null && $module != null) {

            return Core_Class_Message::getInstance()->getMessage($module, $controller, $key, $params);
        }

        return '';
    }
}
