<?php

class Core_Class_Controller_Action_Helper_Advertisement extends Zend_Controller_Action_Helper_Abstract
{
    public function direct(Zend_Config $config)
    {
        $this->setConfig($config);
    }

    public function setConfig(Zend_Config $config)
    {
        Core_Class_Advertisment_Config::getInstance()->setConfig($config);
        return $this;
    }
}