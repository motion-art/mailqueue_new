<?php

/**
 * Core_Class_Controller_Action_Helper_BasePath
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: BasePath.php 287 2010-06-07 12:39:32Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Controller_Action_Helper_BasePath extends Zend_Controller_Action_Helper_Abstract
{
    public function direct($type = null)
    {
        return Core_Class_BasePath::getInstance()->getPath($type);
    }
}
