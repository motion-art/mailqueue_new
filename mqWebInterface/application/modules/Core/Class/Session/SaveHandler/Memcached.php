<?php

/**
 * Core_Class_Session_SaveHandler_Memcached
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: Memcached.php 287 2010-06-07 12:39:32Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Session_SaveHandler_Memcached implements Zend_Session_SaveHandler_Interface
{
    /**
     * memcache object
     * @var Memcache
     */
    private $_memcache = null;

    /**
     * config
     * @var array
     */
    private $_config = array();

    /**
     * construct
     *
     * @param array $config
     * @return Core_Class_Session_SaveHandler_Memcached
     */
    public function __construct($config)
    {
        if (!isset($config['key'])) {
            throw new Exception('memcached session unique prefix-key required');
        }
        if (!isset($config['host'])) {
            throw new Exception('memcached session host required');
        }
        if (!isset($config['port'])) {
            throw new Exception('memcached session port required');
        }
        if (!isset($config['lifetime'])) {
            throw new Exception('memcached session lifetime required');
        }

        $this->_config = $config;
        $this->_memcache = new Memcache;
        if (!$this->_memcache->addServer($config['host'], $config['port'])) {
            throw new Exception('memcached session couldnt connect');
        }
    }

    /**
     * read from session
     *
     * @param string $id
     * @return mixed
     */
    public function read($id)
    {
        return $this->_memcache->get($this->_makeId($id));
    }

    /**
     * write to session
     *
     * @param string $id
     * @param mixed $data
     * @return boolean
     */
    public function write($id, $data)
    {
        return $this->_memcache->set($this->_makeId($id), $data, false, $this->_config['lifetime']);
    }

    /**
     * delete session
     *
     * @param string $id
     * @return boolean
     */
    public function destroy($id)
    {
        return $this->_memcache->delete($this->_makeId($id));
    }

    /**
     * garbage collector
     * - no action
     *
     * @param int $maxlifetime
     * @return boolean
     */
    public function gc($maxlifetime)
    {
        return true;
    }

    /**
     * open session
     * - no action
     *
     * @param string $save_path
     * @param string $name
     * @return boolean
     */
    public function open($save_path, $name)
    {
        return true;
    }

    /**
     * close session
     * - no action
     *
     * @return boolean
     */
    public function close()
    {
        return true;
    }

    /**
     * returns unique id for key
     *
     * @param string $id
     * @return string
     */
    private function _makeId($id)
    {
        return md5($this->_config['key'] . $id);
    }
}
