<?php

/**
 * Core_Class_Profiler_Writer_Firebug
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: Firebug.php 287 2010-06-07 12:39:32Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Profiler_Writer_Firebug implements Core_Class_Profiler_Writer_Interface
{
    /**
     * firebug table
     * @var Zend_Wildfire_Plugin_FirePhp_TableMessage
     */
    private $_tableMessage = null;

    /**
     * initialize
     *
     * @return void
     */
    public function init($profiler)
    {
        Zend_Wildfire_Plugin_FirePhp::getInstance()->send(get_class($profiler) . ' initialized');
        if ($this->_tableMessage === null) {
            $this->_tableMessage = new Zend_Wildfire_Plugin_FirePhp_TableMessage(get_class($profiler));
            $this->_tableMessage->setHeader(
                array('Marker', 'Value')
            );
        }
    }

    /**
     * log entry
     *
     * @return void
     */
    public function log($name, $value)
    {
        if ($this->_tableMessage === null) {
            $this->init();
        }
        $this->_tableMessage->addRow(array($name, $value));
    }

    /**
     * finish logging
     *
     * @return void
     */
    public function finish()
    {
        if ($this->_tableMessage != null) {
            Zend_Wildfire_Plugin_FirePhp::getInstance()->send($this->_tableMessage);
        }
    }
}
