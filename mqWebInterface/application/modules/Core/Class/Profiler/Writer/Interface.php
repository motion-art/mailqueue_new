<?php

/**
 * Core_Class_Profiler_Writer_Interface
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: Interface.php 287 2010-06-07 12:39:32Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

interface Core_Class_Profiler_Writer_Interface
{
    public function init($profiler);
    public function log($name, $value);
    public function finish();
}
