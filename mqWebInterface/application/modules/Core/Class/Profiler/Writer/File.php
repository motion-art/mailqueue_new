<?php

/**
 * Core_Class_Profiler_Writer_File
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: File.php 287 2010-06-07 12:39:32Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Profiler_Writer_File implements Core_Class_Profiler_Writer_Interface
{
    /**
     * directory
     * @var string
     */
    private $_path = null;

    /**
     * entries
     * @var array
     */
    private $_data = array();

    public function __construct($path)
    {
        if (!file_exists($path)) {
            throw new Exception('Profiler directory does not exists');
        }
        if (!is_writable($path)) {
            throw new Exception('Profiler directory not writeable');
        }
        $this->_path = $path;
    }

    /**
     * initialize
     *
     * @return void
     */
    public function init($profiler)
    {
    }

    /**
     * log entry
     *
     * @return void
     */
    public function log($name, $value)
    {
        $this->_data[$name] = $value;
    }

    /**
     * finish logging
     *
     * @return void
     */
    public function finish()
    {
        if (count($this->_data) > 0) {

            $this->_data['url'] = $_SERVER['REQUEST_URI'];

            $logstring = '';
            foreach ($this->_data as $key => $value) {
                $logstring.='[' . $key . '] ' . $value . "\n";
            }

            file_put_contents(
                $this->_path . '/profiler.log',
                $logstring . "\n",
                FILE_APPEND
            );
        }
    }
}
