<?php

abstract class Core_Class_Service_Abstract extends Core_Class_HttpClient_Abstract
{
    protected function _getAdditionalParams()
    {
        return array();
    }

    public function request($params, $url = false)
    {
        if (!$url) {
            $url = $this->_config->baseUrl;
        }
        return $this->_request($url, array_merge($this->_getAdditionalParams(), $params));
    }

    public function getHandle($name, $params = array())
    {
        $method = '_' . $name;
        if (!method_exists($this, $method)) {
            throw new Exception('query method not implemented');
        }

        $params = call_user_func_array(array($this, $method), $params);

        return $this->_getHandle($this->_config->baseUrl, array_merge($this->_getAdditionalParams(), $params));
    }
}
