<?php

/**
 * Service Klasse für http://code.google.com/intl/de-DE/apis/ajaxsearch/web.html
 *
 * Beispiel Konfiguration:
 * <pre>
 * service.googleajax.class            = Price_Class_Service_GoogleAjax
 * service.googleajax.baseUrl          = http://ajax.googleapis.com/ajax/services/search/web
 * service.googleajax.timeout          = 2
 * service.googleajax.params.rsz       = large
 * service.googleajax.params.hl        = en
 * service.googleajax.params.gl        = uk
 * service.googleajax.params.lr        = lang_en
 * service.googleajax.params.start     = 0
 * service.googleajax.params.v         = 1.0
 * service.googleajax.params.safe      = active
 * service.googleajax.profiler         = false
 * service.googleajax.params.key       = "API-KEY"; http://code.google.com/intl/de-DE/apis/ajaxsearch/signup.html
 * service.googleajax.blacklist[]       = blogspot
 * service.googleajax.blacklist[]       = amazon
 * service.googleajax.blacklist[]       = billiger
 * </pre>
 *
 * @category   Core
 * @package    Core_Class_Service
 * @copyright  Unister GmbH
 */

class Core_Class_Service_GoogleAjax
    extends
        Core_Class_Service_Abstract
    implements
        Core_Class_HttpClient_Multi_Interface
{
    protected function _getAdditionalParams()
    {
        $this->_params['userip'] = $_SERVER['REMOTE_ADDR'];
        return $this->_params;
    }

    public function search($query)
    {
        $result = $this->request($this->_search($query));
        return $this->mapResult($result);
    }

    public function mapResult($result)
    {
        $result = json_decode($result);

        if (isset($result->responseData) && isset($result->responseData->results)) {

            $this->_filter($result->responseData->results);

            return $result->responseData->results;
        }

        return array();
    }

    protected function _search($query)
    {
        return array(
            'q' => $query
        );
    }

    public function getTop($query)
    {
        $result = $this->request(
            array(
                'q' => $query,
                'num' => 100
            )
        );

        $result = json_decode($result);

        if (isset($result->responseData) && isset($result->responseData->results)) {

            $results = $result->responseData->results;

            shuffle($results);
            $results = array_slice($results, 0, 10);

            $this->_filter($results);

            return $results;
        }

        return array();
    }

    private function _filter(&$results)
    {
        if (isset($this->_config->blacklist)) {

            foreach ($results as $key => $result) {

                $host= parse_url($result->url, PHP_URL_HOST);
                $domain = explode('.', $host);
                   $domain = $domain[count($domain) - 2];
                if (in_array($domain, $this->_config->blacklist->toArray())) {
                    unset($results[$key]);
                }
            }
        }
    }
}
