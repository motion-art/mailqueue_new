<?php

class Core_Class_Service_Google extends Core_Class_Service_Abstract
{
    protected $_id = 'google';
    protected $_params = array();

    static protected $_logger = null;
    static protected $_logEnabled = false;

    protected function _getAdditionalParams()
    {
        $this->_params['ip'] = $this->_getRemoteIp();
        $this->_params['useragent'] = $_SERVER['HTTP_USER_AGENT'];
        return $this->_params;
    }

    private function _getRemoteIp()
    {
        $ip = $_SERVER['REMOTE_ADDR'];
        // ipv6 -> ipv4
        if ($ip == '::1') {
            $ip = '192.168.1.1';
        }

        return $ip;
    }

    public function search($query)
    {
        $result = $this->request(
            array(
                'q' => $query
            )
        );

        if ($xml = simplexml_load_string($result)) {
            if ($xml instanceof SimpleXMLElement) {

                $results = $xml->xpath('ADS/AD');

                foreach ($results as $result) {
                    $result->visible_url = html_entity_decode(current($result->xpath('@visible_url')), ENT_COMPAT, 'UTF-8');
                }

                if (self::$_logEnabled) {
                    $this->_logAlternatives($query, $results);
                }
                return $results;
            }
        }
        return array();
    }

    protected function _logAlternatives($query, $results)
    {
        $alternatives = array();

        foreach ($results as $result) {

            $url = (string) current($result->xpath('@visible_url'));
            if ($pos = strpos($url, '/')) {
                $alternatives[] = substr($url, $pos + 1);
            }
        }

        $info = array();
        $info[] = $_SERVER['REQUEST_URI'];
        $info[] = $query;

        if (count($alternatives) > 0) {
            $info[] = implode('#', $alternatives);
        } else {
            $info[] = 'not found';
        }

        self::getLogger()->info(implode('|', $info));
    }

    public function set($name, $value)
    {
        if (strlen($value) > 0) {
            $this->_params[$name] = $value;
        }
    }

    static public function enableLog()
    {
        self::$_logEnabled = true;
    }

    static public function setLogger($logger)
    {
        self::$_logger = $logger;
    }

    static public function getLogger()
    {
        if (self::$_logger === null) {

            self::$_logger = new Zend_Log();

            $log = new Zend_Log_Writer_Stream(Core_Config::getInstance()->LOGFILE_PATH . '/optimize_keywords_' . date('Y-m-d') . '.log');
            $log->setFormatter(new Zend_Log_Formatter_Simple());

            self::$_logger->addWriter($log);
        }
        return self::$_logger;
    }
}
