<?php

/**
 * Core_Class_Message
 *
 * returns message from Messages.ini
 *
 * [CONTROLLER]
 * [MESSAGEKEY] = [MESSAGE]
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: Message.php 381 2010-07-08 10:11:48Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Message
{
    /**
     * config filename
     * @var string
     */
    const CONFIG = 'messages';

    /**
     * instance
     * @var Core_Class_Message
     */
    static protected $_instance = null;

    /**
     * messages
     * @var array
     */
    protected $_messages = array();

    /**
     * returns message for module, section and key
     *
     * @param string $module
     * @param string $section
     * @param string $key
     * @param array $params
     * @return string
     */
    public function getMessage($module, $section, $key, $params = array())
    {
        $messages = $this->_getMessages($section);

        if ($messages instanceof Zend_Config && isset($messages->$key)) {

            return vsprintf($messages->$key, (array) $params);
        }

        Core_Class_Log::getInstance()->err('message key '.$key.' doesn\'t exists');
        return '';
    }

    /**
     * singleton
     *
     * @return Core_Class_Message
     */
    static public function getInstance()
    {
        if (self::$_instance === null) {

            self::$_instance = new self();
        }
        return self::$_instance;
    }

    /**
     * initialize messages for module
     *
     * @param string $module
     * @return void
     */
    protected function _getMessages($section)
    {
        if (!isset($this->_messages[$section])) {

            $this->_messages[$section] = Core_Class_Config::get(self::CONFIG, $section);
        }

        return $this->_messages[$section];
    }
}
