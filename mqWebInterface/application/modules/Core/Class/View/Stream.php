<?php

class Core_Class_View_Stream extends Zend_View_Abstract
{
    /**
     * Erstellt eine neue Instanz des Views
     *
     * @param mixed $config (Optional) Zusätzliche Konfigurationseinstellungen
     */
    public function __construct($config = array())
    {
        if (!(bool) ini_get('short_open_tag')) {
            if (false === in_array('zend.view', stream_get_wrappers())) {
                stream_wrapper_register('zend.view', 'Zend_View_Stream');
            }
            Core_Class_View_Stream_Wrapper::setShortTagsEnabled(false);
        }

        if (false === in_array(Core_Class_View_Stream_Wrapper::STREAM_PREFIX, stream_get_wrappers())) {
            stream_wrapper_register(Core_Class_View_Stream_Wrapper::STREAM_PREFIX, 'Core_Class_View_Stream_Wrapper');
        }

        parent::__construct($config);
    }

    public function addPlugin($plugin)
    {
        Core_Class_View_Stream_Wrapper::addPlugin($plugin);
    }

    /**
     * Nutzt den Ps eigenen Stream, zum Cachen der übersetzten Views
     *
     * @see    View/Zend_View_Abstract#_run()
     * @return void
     */
    protected function _run()
    {
        include Core_Class_View_Stream_Wrapper::STREAM_PREFIX . '://' . func_get_arg(0);
    }
}
