<?php

require_once 'Smarty/libs/Smarty.class.php';

/**
 * Core_Class_View_Smarty
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: Smarty.php 287 2010-06-07 12:39:32Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_View_Smarty extends Zend_View_Abstract implements Zend_View_Interface
{
    /**
     * smarty
     * @var Smarty
     */
    protected $_smarty = null;

    /**
     * smarty options
     * @var array
     */
    protected $_options = array();

    /**
     * assigned variables
     * @var array
     */
    protected $_vars = array();

    /**
     * registered plugins
     * @var array
     */
    protected $_plugins = null;

    public function __construct($config = array())
    {
        $this->_options = $config;
    }

    /**
     * register plugin directory
     * @param string $dir
     * @return void
     */
    public function addPluginDir($dir)
    {
        $this->_options['plugins_dir'][] = $dir;
    }

    /**
     * render view script
     * @return string
     */
    protected function _run()
    {
        $this->strictVars(true);

        $aPath = $this->getScriptPaths();
        foreach ($aPath as $sPath) {
            $aScript = str_split(func_get_arg(0), strlen($sPath));
            if ($aScript[0] == $sPath) {
                $file = $aScript[1];
                $path = $aScript[0];
            }
        }

        if (strlen($file) > 0 && strlen($path) > 0) {
            $this->getEngine()->template_dir = $path;

            if (sizeof($this->_vars) > 0) {
                $this->getEngine()->assign($this->_vars);
            }
            $this->getEngine()->assign ( 'this', $this );
            echo $this->getEngine()->fetch($file);
        }

        echo '';
    }

    /**
     * set options
     *
     * @param array $options
     * @return void
     */
    public function setOptions ($options = array())
    {
        if (!is_array($options)) {
            throw new Core_Class_View_Exception ('argument is not a array');
        }
        foreach ($options as $key => $value) {
            $this->_options[$key] = $value;
        }
    }

    /**
     * returns smarty object
     *
     * @return Smarty
     */
    public function getEngine()
    {
        if ($this->_smarty === null) {
            $this->_smarty = new Smarty;
            if (is_array($this->_options)) {
                foreach ($this->_options as $key => $value) {
                    $this->_smarty->{$key} = $value;
                }
            }
        }
        return $this->_smarty;
    }

    /**
     * assign variable
     *
     * @param string $key
     * @param string $val
     * @return void
     */
    public function __set($key, $val)
    {
        $this->_vars[$key] = $val;
    }

    /**
     * returns assigned variable
     *
     * @return mixed
     */
    public function __get($key)
    {
        return isset($this->_vars[$key]) ? $this->_vars[$key] : null;
    }

    public function __isset($key)
    {
        return isset($this->_vars[$key]);
    }

    public function __unset($key)
    {
        unset($this->_vars[$key]);
    }

    /**
     * assign view variable
     *
     * @param string|array $spec
     * @param mixed $value
     * @return void
     */
    public function assign($spec, $value = null)
    {
        if (is_array($spec) && null === $value) {
            $this->_vars = array_merge($this->_vars, $spec);
            return;
        }
        $this->_vars[$spec] = $value;
    }

    /**
     * clear all view variables
     *
     * @return void
     */
    public function clearVars()
    {
        $this->_vars = null;
    }
}
