<?php

class Core_Class_View_Stream_Wrapper
{
    const STREAM_PREFIX = 'core.view.stream';

    /**
     * Position
     *
     * @var integer
     */
    protected $_pos = 0;

    /**
     * Data for streaming.
     *
     * @var string
     */
    protected $_data;

    /**
     * Stream stats.
     *
     * @var array
     */
    protected $_stat;

    protected $_cache = null;

    /**
     * Whether or not to use streams to mimic short tags
     *
     * @var boolean
     */
    static private $_shortTagsEnabled = true;

    static private $_plugins = array();

    public function __construct()
    {
        $this->_cache = Core_Class_Cache::getCache(Core_Class_Cache::CACHE_NAME_VIEW);
    }

    static public function setEnabledShortTags($value)
    {
        self::$_shortTagsEnabled = $value;
    }

    static public function addPlugin($class)
    {
        if (!class_exists($class)) {
            Core_Class_Log::getInstance()->warn('plugin "' . $class . '" not found');
            return;
        }
        try {
            Core_Class_Log::getInstance()->debug('StreamWrapperPlugin "' . $class . '" loaded');
            self::$_plugins[] = new $class();
        } catch (Exception $e) {
            Core_Class_Log::getInstance()->crit($e);
        }
    }

    /**
     * Opens the script file and converts markup.
     *
     * @param  mixed $path         Pfad
     * @param  mixed $mode         Modus
     * @param  mixed $options      Optionen
     * @param  mixed &$opened_path Geöffneter Pfad
     * @return boolean
     */
    public function stream_open($path, $mode, $options, &$opened_path)
    {
        // Get the view script source
        $path = str_replace(self::STREAM_PREFIX . '://', '', $path);

        foreach (self::$_plugins as $plugin) {
            $path = $plugin->getPath($path);
        }

        $locale = null;
        if (Zend_Registry::isRegistered('Zend_Translate')) {
            $locale = Zend_Registry::get('Zend_Translate')->getLocale();
        }
        $key = md5($locale . $path . Core_Class_Revision::get());

        if (!$this->_data = $this->_cache->load($key)) {

            if (!self::$_shortTagsEnabled) {
                $this->_data = file_get_contents('zend.view://' . $path);
            } else {
                $this->_data = file_get_contents($path);
            }

            foreach (self::$_plugins as $plugin) {
                try {
                    $this->_data = $plugin->filter($this->_data);
                } catch (Exception $e) {
                    Core_Class_Log::getInstance()->crit($e);
                }
            }

            $this->_cache->save($this->_data, $key);
        }

        return true;
    }

    /**
     * Included so that __FILE__ returns the appropriate info
     *
     * @return array
     */
    public function url_stat()
    {
        return $this->_stat;
    }

    /**
     * Reads from the stream.
     *
     * @param  integer $count Anzahl zu lesender Zeichen
     * @return string
     */
    public function stream_read($count)
    {
        $ret         = substr($this->_data, $this->_pos, $count);
        $this->_pos += strlen($ret);
        return $ret;
    }

    /**
     * Tells the current position in the stream.
     *
     * @return integer
     */
    public function stream_tell()
    {
        return $this->_pos;
    }

    /**
     * Tells if we are at the end of the stream.
     *
     * @return boolean
     */
    public function stream_eof()
    {
        return $this->_pos >= strlen($this->_data);
    }

    /**
     * Stream statistics.
     *
     * @return array
     */
    public function stream_stat()
    {
        return $this->_stat;
    }

    /**
     * Seek to a specific point in the stream.
     *
     * @param  mixed $offset Offset
     * @param  mixed $whence Aktion
     * @return boolean
     */
    public function stream_seek($offset, $whence)
    {
        switch ($whence) {
            case SEEK_SET:
                if ($offset < strlen($this->_data) AND $offset >= 0) {
                    $this->_pos = $offset;
                    return true;
                } else {
                    return false;
                }
                break;

            case SEEK_CUR:
                if ($offset >= 0) {
                    $this->_pos += $offset;
                    return true;
                } else {
                    return false;
                }
                break;

            case SEEK_END:
                if ((strlen($this->_data) + $offset) >= 0) {
                    $this->_pos = (strlen($this->_data) + $offset);
                    return true;
                } else {
                    return false;
                }
                break;

            default:
                return false;
                break;
        }
    }
}
