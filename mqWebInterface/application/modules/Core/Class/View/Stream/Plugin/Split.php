<?php

class Core_Class_View_Stream_Plugin_Split
    implements Core_Class_View_Stream_Plugin_Interface
{
    public function filter($data)
    {
        return $data;
    }

    public function getPath($path)
    {
        $replacement = $this->_getPath($path);
        if (file_exists($replacement)) {

            return $replacement;
        }

        return $path;
    }

    private function _getPath($path)
    {
        $path = str_replace(Core_Config::getInstance()->BASE_PATH, '', $path);
        return Core_Config::getInstance()->BASE_PATH . Core_Class_Split_Handler::getInstance()->getViewScript($path);
    }
}
