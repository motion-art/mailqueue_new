<?php

class Core_Class_View_Stream_Plugin_Translation
    implements Core_Class_View_Stream_Plugin_Interface
{
    const TRANSLATION_PATTERN = '/\[\[([^\]\|]*)(\|\|([^\]]*))?\]\]/';
    const REPLACE_PATTERN = '<?=sprintf(\'%s\', %s)?>';
    const TRANSLATION_REGISTRY_KEY = 'Zend_Translate';

    public function filter($data)
    {
        if (!Zend_Registry::isRegistered(self::TRANSLATION_REGISTRY_KEY)) {
            throw new Exception('Zend_Translate not found in Registry');
        }

        $callback = array($this, '_translate');
        return preg_replace_callback(self::TRANSLATION_PATTERN, $callback, $data);
    }

    public function getPath($path)
    {
        return $path;
    }

    /**
     * Übersetzt das was zwischen [[ und ]] steht
     *
     * @param  array $matches Feld mit Treffern des regulären Ausdruckes
     * @return string Übersetzter Text
     */
    private function _translate(array $matches)
    {
        $translate = Zend_Registry::get(self::TRANSLATION_REGISTRY_KEY);

        if (isset($matches[3])) {
            return sprintf(self::REPLACE_PATTERN, $this->_filter($translate->_($matches[1])), $matches[3]);
        } else {
            return $translate->_($matches[1]);
        }
    }

    private function _filter($value)
    {
        return str_replace("'", "\'", $value);
    }
}