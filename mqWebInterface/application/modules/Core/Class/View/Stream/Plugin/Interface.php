<?php

interface Core_Class_View_Stream_Plugin_Interface
{
    public function filter($data);
    public function getPath($path);
}
