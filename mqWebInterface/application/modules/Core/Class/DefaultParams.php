<?php

/**
 * Core_Class_DefaultParams
 *
 * wrapper for config :
 * Zend_Registry::get('_CONFIG')->params->[NAME]
 *
 * params.[NAME] = [VALUE]
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: DefaultParams.php 381 2010-07-08 10:11:48Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_DefaultParams
{
    /**
     * returns default param
     *
     * @param string $name
     * @return mixed
     */
    static public function get($name)
    {
        $config = Zend_Registry::get('_CONFIG')->params;
        if (isset($config) && isset($config->$name)) {
            return $config->$name;
        }
        return false;
    }
}