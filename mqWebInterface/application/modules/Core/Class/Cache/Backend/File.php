<?php

class Core_Class_Cache_Backend_File extends Zend_Cache_Backend_File
{
    /**
     * Set the cache_dir (particular case of setOption() method)
     *
     * @param  string  $value
     * @param  boolean $trailingSeparator If true, add a trailing separator is necessary
     * @throws Zend_Cache_Exception
     * @return void
     */
    public function setCacheDir($value, $trailingSeparator = true)
    {
        if (!file_exists($value)) {
            mkdir($value, 0777, true);
        }

        return parent::setCacheDir($value, $trailingSeparator);
    }
}