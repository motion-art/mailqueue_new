<?php

/**
 * Core_Class_Form_Abstract
 *
 * loads form from Zend_Config_Xml file depending on module name
 * extends config to get multioptions from db (Doctrine_Query_Abstract)
 *
 *	<query>
 *		<class>[CLASS]</class>
 *		<method>[METHOD]</method>
 *		<key>[KEY]</key>
 *		<value>[VALUE]</value>
 *	</query>
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: Abstract.php 287 2010-06-07 12:39:32Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

abstract class Core_Class_Form_Abstract extends Zend_Form
{
    /**
     * module name
     * @var string
     */
    protected $_module = null;

    public function __construct($module, $config = null)
    {
        if (strlen($module) == 0) {
            throw new Core_Class_Form_Exception('Module expected');
        }
        $this->_module = $module;

        $this->_addPrefixPathModule($module, '/Class/Form/Element/', 'element');
        $this->_addPrefixPathModule($module, '/Class/Form/Decorator/', 'decorator');

        $configPath = Core_Config :: getInstance()->MODULE_PATH . '/' . ucfirst($module) . '/Form/' . $config;
        if ($config != null && file_exists($configPath)) {

            $this->setConfig(new Zend_Config_Xml($configPath));
        }

        parent::__construct();
    }

    /**
     * load form options from Zend_Config
     * @param Zend_Config $config
     * @return self
     */
     public function setConfig(Zend_Config $config)
    {
        $this->setOptions($config->toArray());

          $this->_setElementOptions();
          $this->_addPrefixPathModule($this->_module, '/Class/Filter/', 'filter', 'addElementPrefixPath');
          $this->_addPrefixPathModule($this->_module, '/Class/Validate/', 'validate', 'addElementPrefixPath');
          $this->_addPrefixPathModule($this->_module, '/Class/Form/Decorator/', 'decorator', 'addElementPrefixPath');

          return $this;
    }

    /**
     * initialize element multioptions from option "query"
     *
     * @return void
     */
    protected function _setElementOptions()
    {
        foreach ($this->getElements() as $element) {
            if (isset($element->query) && $element instanceof Zend_Form_Element_Multi && class_exists($element->query['class']) && method_exists($element->query['class'], $element->query['method'])) {

                $opt = array();
                $key = $element->query['key'];
                $value = $element->query['value'];

                $class = new $element->query['class'];
                $query = call_user_func_array(array($class, $element->query['method']), null);

                if ($query instanceof Doctrine_Query_Abstract) {

                    $options = $query->execute();

                    foreach ($options as $option) {
                        $opt[$option->$key] = $option->$value;
                    }

                    if (is_array($opt) && count($opt)>0) {
                        $element->addMultiOptions($opt);
                    }
                }
                unset($element->query);
            }
        }
    }

    /**
     * add prefix path for form or element
     *
     * @param string $module
     * @param string $path
     * @param string $type
     * @param string $method
     * @return void
     */
    protected function _addPrefixPathModule($module, $path, $type, $method = 'addPrefixPath')
    {
        $path = ucfirst($module) . $path;
        $prefix = str_replace('/', '_', $path);

        if (file_exists(Core_Config :: getInstance()->MODULE_PATH . '/' . $path)) {
            $this->$method($prefix, $path, $type);
        }
    }
}
