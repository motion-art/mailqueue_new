<?php

/**
 * enviroment core configuration
 *
 * sets run-mode and path to configuration dir.
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: Abstract.php 316 2010-06-24 13:02:55Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

abstract class Core_Class_Config_Abstract
{
    /**
     * instance
     * @var Core_Config
     */
    protected static $_instance = null;

    /**
     * path templates
     * @var array
     */
    protected $_options = array (
        'public_path' => ':path/../httpdocs',
        'base_path' => ':path/..',
        'cache_path' => ':path/../cache',
        'logfile_path' => ':path/../log',
        'module_path' => ':path/../application/modules',
        'application_path' => ':path/../application',
        'languages_path' => ':path/../languages',
        'library_path' => ':path/../library',
        'runmode' => ':runmode'
    );

    /**
     * runmode
     * @var string
     */
    protected $_defaultRunmode = 'local';

    /**
     * path placeholders
     * @var array
     */
    protected $_placeholder = array (
        'path' => ':path',
        'runmode' => ':runmode'
    );

    public function __construct ()
    {
        $this->_init();
    }

    public function __get ($var)
    {
        if (isset($this->_options[strtolower($var)])) {
            return $this->_options[strtolower($var)];
        }
    }

    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new Core_Config();
        }
        return self::$_instance;
    }

    protected function _init ()
    {
        if (!is_array($this->_options)) {
            throw new Core_Class_Config_Exception(get_class($this) . '::_options should be an array');
        }
        foreach ($this->_options as $key => $value) {

            if (false !== strpos($value, $this->_placeholder['path']) && defined('APPLICATION_PATH')) {
                $this->_options[$key] = realpath(str_replace($this->_placeholder['path'], APPLICATION_PATH, $value));
            }
            elseif (false !== strpos($value, $this->_placeholder['runmode'])) {
                if (defined('APPLICATION_ENV')) {
                    $this->_options[$key] = str_replace($this->_placeholder['runmode'], APPLICATION_ENV, $value);
                } else {
                    $this->_options[$key] = $this->_defaultRunmode;
                }
            }
        }
    }
}
