<?php

/**
 * Core_Class_Config_Exception
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: Exception.php 287 2010-06-07 12:39:32Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Config_Exception extends Core_Exception
{
}
