<?php
/**
 * Core_Class_Config_Ini
 *
 * @category    Core
 * @package     Core_Config
 * @copyright   Copyright (c) 2010 Unister GmbH
 * @author      Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author      r.lasinski
 * @version     $Id$
 * @since       30.04.2010
 */

class Core_Class_Config_Ini extends Zend_Config_Ini
{
    const CACHE_PATH = 'config_static';

    static protected $_caching = true;

    public function __construct($filename, $section = null, $options = false)
    {
        if (empty($filename)) {
            throw new Zend_Config_Exception('Filename is not set');
        }

        if (self::$_caching) {
            $key = md5(serialize(func_get_args()));

            if (!$this->_getCache($key, filemtime($filename))) {
                parent::__construct($filename, $section, $options);
                $this->_setCache($key);
            }
        } else {
            parent::__construct($filename, $section, $options);
        }
    }

    static public function setCaching($value)
    {
        self::$_caching = (bool) $value;
    }

    private function _getCacheFilename($key)
    {
        $path = Core_Config::getInstance()->CACHE_PATH .
                DIRECTORY_SEPARATOR .
                self::CACHE_PATH .
                DIRECTORY_SEPARATOR;

        if (!file_exists($path)) {
            mkdir($path, 0777, true);
            chmod($path, 0777);
        }
        return $path . $key;
    }

    private function _setCache($key)
    {
        $cache = array();
        $cache['_allowModifications'] = $this->_allowModifications;
        $cache['_loadedSection'] = $this->_loadedSection;
        $cache['_data'] = $this->_data;

        $filename = $this->_getCacheFilename($key);
        file_put_contents($filename, serialize($cache));
        chmod($filename, 0666);
    }

    private function _getCache($key, $time)
    {
        $filename = $this->_getCacheFilename($key);
        if (file_exists($filename) && filemtime($filename) >= $time) {
            $cache = unserialize(file_get_contents($filename));

            $this->_allowModifications = $cache['_allowModifications'];
            $this->_loadedSection = $cache['_loadedSection'];
            $this->_index = 0;
            $this->_data = $cache['_data'];
            $this->_count = count($this->_data);

            return true;
        }
        return false;
    }
}
