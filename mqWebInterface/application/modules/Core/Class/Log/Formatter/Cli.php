<?php

class Core_Class_Log_Formatter_Cli implements Zend_Log_Formatter_Interface
{
    const LOG_FORMAT = "\033[1;37m%timestamp%\033[0;37m \033[1;%color%m[%priorityName%] (%priority%): %message%\033[0;37m";

    public function format($event)
    {
        switch ($event['priority']) {

            case Zend_Log::EMERG:
            case Zend_Log::ALERT:
            case Zend_Log::CRIT:
            case Zend_Log::ERR:
               $event['color'] = '31';
               break;
            case Zend_Log::WARN:
               $event['color'] = '33';
               break;
            case Zend_Log::INFO:
               $event['color'] = '32';
               break;
            case Zend_Log::DEBUG:
               $event['color'] = '37';
               break;
        }

        $output = self::LOG_FORMAT . PHP_EOL;
        foreach ($event as $name => $value) {

            if ((is_object($value) && !method_exists($value,'__toString'))
                || is_array($value)) {

                $value = gettype($value);
            }

            $output = str_replace("%$name%", $value, $output);
        }
        return $output;
    }
}
