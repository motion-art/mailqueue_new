<?php

class Core_Class_Filter_SeoMetaTagBuilder implements Zend_Filter_Interface
{
    protected $_robotsList = array();
    protected $_defaultRobotsTag = "noindex,follow";
    protected $_enabled = true;

    public static $robotTagValue = false;

    public function __construct(Zend_Config $config)
    {
        if (!$this->_prepareConfig($config)) {
            $this->_enabled = false;
        }
    }

    public function filter($value)
    {
        if (!$this->_enabled) {
            return $value;
        }

        return preg_replace('#</title>#','</title>' . "\n" . '<meta name="robots" content="'
               . $this->_getRobotsTagContent()
               . '" />' . "\n", $value);
    }

    protected function _getRobotsTagContent()
    {
        if (self::$robotTagValue !== false) {
            return self::$robotTagValue;
        }

        // komplette BaseUrl holen:
        // incl. schema und host
        $request = Zend_Controller_Front::getInstance()->getRequest();

        // genutzt für vergleiche auf schema und hostebene
        $baseUrl = $request->getScheme() . '://'
                 . $request->getHttpHost()
                 . $request->getBaseUrl() . '/';

        $totalUri      = $request->getRequestUri();
        $quotedBaseUrl = preg_quote($request->getBaseUrl(), '#');

        // uri ohne basepath
        // wenn basepath vorhanden
        // genutzt für vergleiche auf uri ohne base url
        if ($request->getBaseUrl()) {
            $uri = preg_replace("#^$quotedBaseUrl#", '', $totalUri);
        } else {
            $uri = $totalUri;
        }

        // uri ohne führenden Slash
        $uri = ltrim($uri, '/');

        foreach ($this->_robotsList as $regex => $robotsTagContent) {

            if (preg_match($regex, $uri)) {
                return $robotsTagContent;
            }

            if (preg_match($regex, $baseUrl)) {
                return $robotsTagContent;
            }
        }

        return $this->_defaultRobotsTag;
    }

    protected function _prepareConfig(Zend_Config $config)
    {
        //setzen des Default Robotstag-contents
        if (isset($config->default->robotsTag)) {
            $this->_defaultRobotsTag = $config->default->robotsTag;
        }

        if (!isset($config->robotsTags)) {
            return false;
        }

        $robotsTagsList = $config->robotsTags->toArray();
        foreach ($robotsTagsList as $robotTag) {
            $value = $robotTag['value'];
            foreach ($robotTag['uri'] as $uri) {
                $this->_robotsList['~'.$uri.'~'] = $value;
            }
        }

        return true;
    }

    /**
     * Setzt einen festen RobotTagValue,welcher nicht mehr
     * überschrieben wird durch die ausführung des Filters
     * @param string $robotTagValue
     */
    public static function setFixedRobotTagValue($robotTagValue)
    {
        self::$robotTagValue = $robotTagValue;
    }
}