<?php

class Core_Class_Source
{
    const SESSION_NAMESPACE = 'Core_Source';

    static private $_instance = null;

    private $_session = null;
    private $_config = null;

    public function __construct()
    {
        $this->_session = new Zend_Session_Namespace(self::SESSION_NAMESPACE);
        $this->_session->clicks++;

        $this->_config = Zend_Registry::get('_CONFIG')->source;
    }

    public function isFirst()
    {
        return ($this->_session->clicks == 1);
    }

    public function getSource()
    {
        return $this->_session->source;
    }

    public function setSource($source)
    {
        if ($this->_validateSource($source)) {
            if ($this->_session->source != $source) {
                $this->_session->clicks = 1;
            }
            $this->_session->source = $source;
        } else {
            $this->_session->source = null;
        }
    }

    public function getGroup()
    {
        return $this->_session->group;
    }

    public function hasGroup()
    {
        return isset($this->_session->group);
    }

    public function setGroup($group)
    {
        if ($this->_validateGroup($group)) {
            $this->_session->group = $group;
        } else {
            $this->_session->group = null;
        }
    }

    private function _validateIdByFiltername($id, $filtername)
    {
        if (isset($this->_config) && isset($this->_config->filter) && isset($this->_config->filter->$filtername)) {
            if (!in_array($id, $this->_config->filter->$filtername->toArray())) {
                return false;
            }
        }
        return true;
    }

    private function _validateSource($source)
    {
        return $this->_validateIdByFiltername($source, 'id');
    }

    private function _validateGroup($group)
    {
        return $this->_validateIdByFiltername($group, 'group');
    }

    static public function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }
}
