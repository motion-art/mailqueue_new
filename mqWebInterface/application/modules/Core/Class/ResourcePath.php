<?php

class Core_Class_ResourcePath
{
    protected static $_instance = null;
    /**
     * default value
     * @var string
     */
    protected $_resourceDir     = 'Resource';
    protected $_modules         = array();

    protected function __construct()
    {
        $this->_init();
    }

   /**
    * instance
    * @var Core_ResourcePath
    */
    public static function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    protected function _init()
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        if ($request==null || !$module = $request->getModuleName()) {
            $module = Zend_Controller_Front::getInstance()->getDefaultModule();
        }

        $config = Core_Config :: getInstance()->MODULE_PATH
                . '/' . ucfirst($module)
                 . '/Config/ModulePaths.ini';
        $mainConfig = Zend_Registry::get('_CONFIG');

        if ($mainConfig->moduleResourceDirectory) {
            $this->_resourceDir = $mainConfig->moduleResourceDirectory;
        }

        if (file_exists($config)) {
            $paths = new Zend_Config_Ini(
                        $config,
                        'path');
            try {
                if ($paths->resourcepath) {
                    $configPaths = 	$paths->resourcepath->toArray();
                    $this->_modules = array_merge($this->_modules, $configPaths);
                }
            } catch(Exception $e) {
                Core_Class_Log::getInstance()->warn($e);
            }
        }
        //default modul hinzufügen
        $this->_modules[] = $module;
    }


    /**
     * Gibt eine Resource modulspezifisch zurück
     * @param string $file
     * @param string $module speziell Modul
     * @param string $resource subdirectory in Module
     * @return string|boolean
     */
    public function getResource($file, $module = null, $resource = null)
    {
        $resourcePath = false;
        $resourceDir = ($resource) ? $resource : $this->_resourceDir;

        // wenn spezielles Module gesetzt
        if ($module) {
            return Core_Config::getInstance()->MODULE_PATH . '/'
                          . ucfirst($module) .'/'
                          . $resourceDir . '/'
                          . $file;
        }

        // last in first out of paths
        foreach (array_reverse($this->_modules) as $module) {

            $resourcePath = Core_Config::getInstance()->MODULE_PATH . '/'
                          . ucfirst($module) .'/'
                          . $resourceDir . '/'
                          . $file;

            if (file_exists($resourcePath)) {
                //file found
                break;
            }
        }

        return $resourcePath;
    }
}