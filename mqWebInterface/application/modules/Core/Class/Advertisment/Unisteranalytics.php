<?php
/**
 * Advertisement Plugin zur Nutzung von Unister analytics
 */
class Core_Class_Advertisment_Unisteranalytics extends Core_Class_Advertisment_Abstract implements Core_Class_Advertisment_Interface
{
    /**
     * Rendere unisterAnalytics standardmaessig in den Header
     * @see Class/Advertisment/Core_Class_Advertisment_Abstract#renderHeader()
     */
    public function renderHeader()
    {
        $config = $this->_getAdConfig();

        if (isset($config['unisteranalytics']) && $config['unisteranalytics']) {
            return $this->render('unisteranalytics.phtml');
        }
    }

    public function __toString()
    {
        return '';
    }
}