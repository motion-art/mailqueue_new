<?php
/**
 * @category    Core
 * @package     Core_Advertisment
 * @copyright   Copyright (c) 2010 Unister GmbH
 * @author      Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author      r.lasinski
 * @version     $Id$
 * @since       10.05.2010
 */

class Core_Class_Advertisment_Config_Ini implements Core_Class_Advertisment_Config_Interface
{
    const DEFAULT_CONFIG = 'advertisment';

    private $_config = null;

    public function __construct(Zend_Config $config = null)
    {
        $this->_config = $config;
    }

    public function get(Zend_Controller_Request_Abstract $request, $ids = array())
    {
        $module = $request->getModuleName();
        $controller = $request->getControllerName();
        $action = $request->getActionName();

        $moduleConfig = Core_Class_Config::get(self::DEFAULT_CONFIG);

        $adConfig = new Zend_Config(array('default' => array()), true);
        $config = $adConfig->default;

        // global default
        if (isset($moduleConfig->default)) {

            $config->merge($moduleConfig->default);
        }

        foreach ($ids as $key) {
            // key extension
            if ($key != null && isset($moduleConfig->default) && isset($moduleConfig->default->$key) && isset($moduleConfig->default->$key->extends)) {

                $extends = $moduleConfig->default->$key->extends;

                if (isset($moduleConfig->default->$extends)) {
                    $config->merge($moduleConfig->default->$extends);
                }
            }

            // global key
            if ($key != null && isset($moduleConfig->default) && isset($moduleConfig->default->$key)) {

                $config->merge($moduleConfig->default->$key);
            }
        }

        // controller extension
        if (isset($moduleConfig->$controller) && isset($moduleConfig->$controller->extends)) {

            $extends = $moduleConfig->$controller->extends;

            if (isset($moduleConfig->$extends)) {

                if (isset($moduleConfig->$extends->default)) {
                    $config->merge($moduleConfig->$extends->default);
                    foreach ($ids as $key) {
                        if (isset($moduleConfig->$extends->default->$key)) {
                            $config->merge($moduleConfig->$extends->default->$key);
                        }
                    }
                }
            }
        }

        // controller default
        if (isset($moduleConfig->$controller) && isset($moduleConfig->$controller->default)) {

            $config->merge($moduleConfig->$controller->default);
        }

        // controller default key
        foreach ($ids as $key) {
            if (isset($moduleConfig->$controller) && isset($moduleConfig->$controller->default) && isset($moduleConfig->$controller->default->$key)) {

                $config->merge($moduleConfig->$controller->default->$key);
            }
        }

        // controller action
        if (isset($moduleConfig->$controller) && isset($moduleConfig->$controller->$action)) {

            $config->merge($moduleConfig->$controller->$action);
        }

        // controller action key
        foreach ($ids as $key) {
            if (isset($moduleConfig->$controller) && isset($moduleConfig->$controller->$action) && isset($moduleConfig->$controller->$action->$key)) {

                $config->merge($moduleConfig->$controller->$action->$key);
            }
        }

        return $adConfig;
    }
}