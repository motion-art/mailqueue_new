<?php
/**
 * @category    Core
 * @package     Core_Advertisment
 * @copyright   Copyright (c) 2010 Unister GmbH
 * @author      Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author      r.lasinski
 * @version     $Id$
 * @since       10.05.2010
 */

class Core_Class_Advertisment_Config_Db implements Core_Class_Advertisment_Config_Interface
{
    private $_config = null;

    public function __construct(Zend_Config $config = null)
    {
        $this->_db = Zend_Registry::get('_DB');
        $this->_config = $config;
    }

    public function get(Zend_Controller_Request_Abstract $request, $ids = array())
    {
        $module = $request->getModuleName();
        $controller = $request->getControllerName();
        $action = $request->getActionName();

        $if = '(0)';
        foreach ($ids as $key => $id) {
            $if = 'IF(id = ' . $id . ', ' . $key . ', ' . $if . ')';
        }

        $query = $this->_db
                    ->select()
                    ->from(
                        'advertisement',
                        array(
                            'r1' => 'IF(module IS NOT NULL, 1, 0)',
                            'r2' => 'IF(id IS NOT NULL, 2, 0)',
                            'r3' => 'IF(controller IS NOT NULL, 4, 0)',
                            'r4' => 'IF(action IS NOT NULL, 8, 0)',
                            'r5' => 'IF(runmode IS NOT NULL, 16, 0)',
                            'orderId' => $if,
                            'id',
                            'plugin',
                            'key',
                            'value',
                            'isSerialized'
                        )
                    )
                    ->where('module = ? OR module IS NULL', $module)
                    ->where('controller = ? OR controller IS NULL', $controller)
                    ->where('action = ? OR action IS NULL', $action)
                    ->where('runmode = ? OR runmode IS NULL', Core_Config::getInstance()->RUNMODE);

        if (count($ids) > 0) {
            $query->where('id IN (?) OR id IS NULL', $ids);
        }

        $query->order('(r1+r2+r3+r4+r5) DESC, orderId DESC');

        $config = $query->query()->fetchAll();
        $result = array();
        $currentIds = array();

        foreach ($config as $conf) {

            $id = md5($conf['plugin'] . $conf['key']);

            if (!isset($result[$conf['plugin']])) {
                $result[$conf['plugin']] = array();
            }

            if (!isset($result[$conf['plugin']][$conf['key']])) {
                if (isset($conf['id'])) {
                    if (($key = array_search($conf['id'], $ids)) !== false) {
                        if (!isset($currentIds[$id])) {
                            $currentIds[$id] = -1;
                        }
                        if ($key > $currentIds[$id]) {
                            $currentIds[$id] = $key;
                            $result[$conf['plugin']][$conf['key']] = ($conf['isSerialized']?unserialize($conf['value']):$conf['value']);
                        }
                    }
                } elseif (!isset($result[$conf['plugin']][$conf['key']])) {
                    $result[$conf['plugin']][$conf['key']] = ($conf['isSerialized']?unserialize($conf['value']):$conf['value']);
                }
            }
        }

        return new Zend_Config($result, true);
    }
}