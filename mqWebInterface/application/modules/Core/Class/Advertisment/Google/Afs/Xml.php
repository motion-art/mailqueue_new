<?php

class Core_Class_Advertisment_Google_Afs_Xml extends Core_Class_Advertisment_Abstract implements Core_Class_Advertisment_Interface
{
    /**
     * Standard Template
     * @var string
     */
    protected $_defaultTemplate = 'google/afs/block_xml.phtml';

    /**
     * Aktuelles Result des Google-Requests
     * @var SimpleXML
     */
    protected $_result = null;

    /**
     * Anzahl Ad-Blöcke
     * @var int
     */
    protected $_ads = 0;

    /**
     * Anzahl Ad-Zeilen
     * @var int
     */
    protected $_count = 0;

    /**
     * Enthält Informationen zum letzten Request
     * @var array
     */
    protected $_info = array();

    /**
     * Uri des letzten Requests
     * @var string
     */
    protected $_url = null;
    protected $_configName = 'google';

    /**
     * Gibt die erforderlichen Ad-Zeilen zurück
     *
     * @return array
     */
    private function _getAds()
    {
        if (!$id = $this->getId()) {
            $id = $this->_ads;
        }

        $this->_ads++;

        $config = $this->_getAdConfigById($id);

        $start = $this->_count;
        if (isset($config['start'])) {
            $start = $config['start'];
        }
        $this->_count += $config['count'];

        return array_slice(
            $this->_getResult(),
            $start,
            $config['count']
        );
    }

    private function _getResult()
    {
        if ($this->_result === null) {
            $this->_result = $this->_query();
        }
        return $this->_result;
    }

    protected function _getAdConfigById($id)
    {
        $config = $this->_getAdConfig();

        if (!isset($config['google_ads']) || !array_key_exists($id, $config['google_ads'])) {
            throw new Exception('config for google block id: "' . $id . '" not found -> ignored');
        }

        if (!is_array($config['google_ads'][$id])) {
            $result = array();
            $result['count'] = (int) $config['google_ads'][$id];
        } else {
            $result = $config['google_ads'][$id];
        }

        // Keine Anzahl angegeben
        if (!isset($result['count']) || (int) $result['count'] == 0) {
            throw new Exception('config for google block id: "' . $id . '" invalid (count == 0) -> ignored');
        }

        return $result;
    }

    /**
     * Gibt die Gesamtzahl der erforderlichen Ad-Zeilen zurück
     *
     * @return int
     */
    public function getTotalCount()
    {
        $config = $this->_getAdConfig();
        $sum = 0;

        foreach ($config['google_ads'] as $adConfig) {

            if (!is_array($adConfig)) {
                $sum += (int) $adConfig;
            } else {
                $sum += $adConfig['count'];
            }
        }

        return $sum;
    }

    protected function _query()
    {
        $service = Core_Class_Service::factory('google');

        $config = $this->_getAdConfig();
        $count = $this->getTotalCount();

        // different parameters
        $service->set('client', $config['google_afs_client']);
        $service->set('ad',     'w' . $count);
        $service->set('adtest', $config['google_adtest']);

        // channel
        if (isset($config['google_afs_channel'])) {
            $service->set('channel', implode(',', (array) $config['google_afs_channel']));
        }

        // language + region
        if (isset($config['google_language']) && $config['google_language']) {
            $service->set('hl', $config['google_language']);
        }
        if (isset($config['google_region'])) {
            $service->set('gl', $config['google_region']);
        }

        // adsafe
        if (isset($config['google_adsafe'])) {
            $service->set('adsafe', $config['google_adsafe']);
        }

        // category browsing
        if (isset($config['google_afs_browsing'])) {

            $service->set('qry_ctxt',   $this->_view->navi()->getNavigationPath());
            $service->set('qry_lnk',    $this->_view->navi()->getActiveNavigationNode());
            $query = trim($this->_view->navi()->getNavigationPath(' ') . ' ' . $this->_view->navi()->getActiveNavigationNode());

        // search browsing
        } elseif (isset($config['google_afs_search_browsing'])) {

            if ($navigationPath = $this->_view->navi()->getNavigationPath()) {
                $service->set('qry_ctxt', $navigationPath . '|' . $this->_view->navi()->getActiveNavigationNode());
            }
            //$service->set('qry_lnk', $config['google_afs_query']);
            $query = $config['google_afs_query'];

        // search by keyword
        } else {
            if (isset($config['google_afs_ctxt'])) {
               $service->set('qry_ctxt', $config['google_afs_ctxt']);
            }
            if (isset($config['google_afs_lnk'])) {
               $service->set('qry_lnk', $config['google_afs_lnk']);
            }
            $query = $config['google_afs_query'];
        }

        // ad page
        if (isset($config['google_page']) && $config['google_page'] > 0) {
            $service->set('adpage', $config['google_page']);
        } else {
            $service->set('adpage', 1);
        }

        $result = $service->search($query);

        $this->_info = $service->getLastParams();
        $this->_url = $service->getLastUrl();

        return $result;
    }

    /**
     * Gibt die Response des Google-Requests (SimpleXMLElement) als String zurück
     *
     * @return array
     */
    protected function _formatResult()
    {
        $result = '';
        if (count($this->_result) > 0) {
            $dom = dom_import_simplexml(reset($this->_result))->ownerDocument;
            $dom->formatOutput = true;
            $result = $dom->saveXML();
        }
        return $result;
    }

    public function renderFooter()
    {
        $request = Zend_Controller_Front::getInstance()->getRequest();
        if ($request->getParam('GAdsInfo')) {
            return $this->_partial(
                'google/afs/info.phtml',
                array(
                    'info' => $this->_info,
                    'url' => $this->_url,
                    'result' => $this->_formatResult(),
                )
            );
        }
        return '';
    }

    public function render($template = null)
    {
        if (!$this->_isEnabled()) {
            return '';
        }

        try {

            if ($template === null && $this->_defaultTemplate === null) {
                throw new Exception('no default template defined');
            }

            if ($template === null) {
                $template = $this->_defaultTemplate;
            }

            return $this->_partial(
                $template,
                null,
                array(
                    'config' => $this->_getAdConfig(),
                    'id' => $this->getId(),
                    'ads' => $this->_getAds()
                )
            );

        } catch (Exception $e) {

            Core_Class_Log::getInstance()->crit($e);
        }
    }

    protected function _isEnabled()
    {
        $config = $this->_getAdConfig();

        return (bool) $config['google_afs'];
    }
}
