<?php

class Core_Class_Advertisment_Google_Afs_Js extends Core_Class_Advertisment_Google_Js_Abstract implements Core_Class_Advertisment_Interface
{
    protected $_defaultTemplate = 'google/afs/block_js.phtml';

    public function renderHeader()
    {
        if (count($this->_ads) > 0) {
            return $this->_partial('google/afs/head.phtml',
                array(
                    'ads' => $this->_ads,
                    'script' => $this->getScriptPath()
                )
            );
        }
        return '';
    }

    public function renderTop()
    {
        return $this->_partial('google/afs/request.phtml',
            array(
                'config' => $this->_getBaseConfig()
            )
        );
    }

    protected function _isEnabled()
    {
        $config = $this->_getBaseConfig();

        return (bool) $config['google_afs'];
    }
}
