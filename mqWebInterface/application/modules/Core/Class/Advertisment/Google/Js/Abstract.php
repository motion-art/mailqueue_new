<?php

abstract class Core_Class_Advertisment_Google_Js_Abstract extends Core_Class_Advertisment_Abstract
{
    const CONTAINER_NAME = 'google_ad_%s';
    /**
     * Registrierte AdBlöcke
     * @var array
     */
    protected $_ads = array();

    /**
     * Anzahl bisher angezeigter Ads (Rows)
     * @var int
     */
    protected $_count = 0;
    protected $_configName = 'google';

    /**
     * Registriere AdBlock und gebe Konfiguration zurück
     *
     * @return array
     */
    protected function _getAdConfig()
    {
        // Nutze den numerischen Index der registrierten AdBlöcke als Id, wenn keine gesetzt
        if (!$id = $this->getId()) {
            $id = count($this->_ads);
        }

        $config = $this->_getBaseConfig();

        // Wenn keine Konfiguration für den Block existiert
        if (!isset($config['google_ads']) || !array_key_exists($id, $config['google_ads'])) {
            throw new Exception('config for google block id: "' . $id . '" not found -> ignored');
        }

        if (!is_array($config['google_ads'][$id])) {
            $result = array();
            $result['count'] = (int) $config['google_ads'][$id];
        } else {
            $result = $config['google_ads'][$id];
        }

        $config = $this->_mergeOptions($result, $config);

        // Keine Anzahl angegeben
        if (!isset($config['count']) || (int) $config['count'] == 0) {
            throw new Exception('config for google block id: "' . $id . '" invalid (count == 0) -> ignored');
        }

        $start = $this->_count;
        if (isset($config['start'])) {
            $start = $config['start'];
        }
        $this->_count += $config['count'];

        $this->_ads[] = array(
            'container' => $this->_getContainerName($id),
            'start' => $start,
            'end' => $start + $config['count'],
            'count' => $config['count'],
            'label' => isset($config['label'])?$config['label']:null,
            'active_label' => isset($config['active_label'])?$config['active_label']:null
        );

        $config['container'] = $this->_getContainerName($id);

        return $config;
    }

    /**
     * Gibt die feste Konfiguration zurück, unabhängig vom aktuellen AdBlock
     *
     * @return array
     */
    protected function _getBaseConfig()
    {
        return parent::_getAdConfig();
    }

    /**
     * Gibt den Containernamen zurück
     *
     * @param string $id
     * @return string
     */
    private function _getContainerName($id)
    {
        return sprintf(self::CONTAINER_NAME, $id);
    }

    /**
     * Gibt die benötigten AdRows zurück
     *
     * @return int
     */
    public function getTotalCount()
    {
        return $this->_count;
    }

    /**
     * Gibt den Scriptpath (Google JavaScript zum rendern der AdBlöcke)
     * aus der Konfiguration oder direkt vom BaseUrl-Helper zurück
     *
     * @return string
     */
    public function getScriptPath()
    {
        if ($this->_config->scriptpath) {
            return $this->_config->scriptpath;
        }

        return $this->_view->baseUrl('ads', null, 'google.js');
    }
}
