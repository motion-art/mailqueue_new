<?php

class Core_Class_Advertisment_GoogleAfs
{
    const REQUEST_TYPE_JS = 'js';
    const REQUEST_TYPE_XML = 'xml';

    protected $_plugin = null;

    public function __construct(Core_View_Helper_Advertisment $helper, Zend_View_Abstract $view, Zend_Config $config = null)
    {
        if (isset($config->type) && $config->type == self::REQUEST_TYPE_JS) {
            $this->_plugin =  new Core_Class_Advertisment_Google_Afs_Js($helper, $view, $config);
        } else {
            $this->_plugin =  new Core_Class_Advertisment_Google_Afs_Xml($helper, $view, $config);
        }
    }

    public function __call($method, $args)
    {
        return call_user_func_array(array($this->_plugin, $method), $args);
    }

    public function __toString()
    {
        return $this->render();
    }
}
