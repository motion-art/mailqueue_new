<?php

class Core_Class_Advertisment_GoogleMultiSplit extends Core_Class_Advertisment_Abstract implements Core_Class_Advertisment_Interface
{
    public function renderTop()
    {
        $config = $this->_getAdConfig();

        if (isset($config['google_multiSplit'])){
            return $this->render('google/multiSplit.phtml');
        }
    }
}
