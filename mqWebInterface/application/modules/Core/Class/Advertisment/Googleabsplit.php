<?php

class Core_Class_Advertisment_Googleabsplit extends Core_Class_Advertisment_Abstract implements Core_Class_Advertisment_Interface
{
    public function renderPreHtml()
    {
        $config = $this->_getAdConfig();

        if(isset($config['control'])){
            return $this->render('google/abSplit/control.phtml');
        }

    }

    public function renderFooter()
    {
        $config = $this->_getAdConfig();

        if(isset($config['track'])){
            return $this->render('google/abSplit/track.phtml');
        }

        if(isset($config['conversion'])){
            return $this->render('google/abSplit/conversion.phtml');
        }
    }
}