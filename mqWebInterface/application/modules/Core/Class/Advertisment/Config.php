<?php

class Core_Class_Advertisment_Config
{
    /**
     * instance
     * @var Newsfrontend_Class_Advertisment
     */
    static protected $_instance = null;

    /**
     * merged config
     * @var array
     */
    protected $_configs = array();

    /**
     * source config to merge with configs
     * @var Zend_Config
     */
    protected $_sourceConfig = null;

    protected $_config = null;

    /**
     * gets specific advertisment configuration
     *
     * @return array
     */
    public function get($ids = array())
    {
        // Benutze bereits gesetzte Konfigruration
        if ($this->_config != null) {
            if (!is_null($this->_sourceConfig)) {
                $this->_config->merge($this->_sourceConfig);
            }
            return $this->_config->toArray();
        }

        // Beziehe Konfiguration vom Adapter
        $cacheId = md5(
            serialize(Zend_Controller_Front::getInstance()->getRequest()->getUserParams()),
            serialize($ids)
        );

        if (!isset($this->_configs[$cacheId])) {

            try {
                $this->_configs[$cacheId] = $this->_adapter->get(
                    Zend_Controller_Front::getInstance()->getRequest(),
                    $ids
                );
            } catch (Exception $e) {
                Core_Class_Log::getInstance()->crit($e);
                $this->_configs[$cacheId] = new Zend_Config(array(), true);
            }
        }

        // merge source config
        if (!is_null($this->_sourceConfig)) {
            $this->_configs[$cacheId]->merge($this->_sourceConfig);
        }

        return $this->_configs[$cacheId]->toArray();
    }

    public function setAdapter($config)
    {
        $class = $config->name;
        if (!class_exists($class)) {
            throw new Exception('adapter not found');
        }
        $this->_adapter = new $class($config->config);
    }

    /**
     * singleton
     *
     * @return Newsfrontend_Class_Advertisment
     */
    static public function getInstance()
    {
        if (self::$_instance === null) {

            self::$_instance = new self();
        }
        return self::$_instance;
    }

    protected function __construct()
    {
        $this->_adapter = new Core_Class_Advertisment_Config_Ini();
    }

    /**
     * nimmt die akuellen Configwerte der Source entgegen
     * @param Zend_Config $config
     */
    public function setSourceConfig(Zend_Config $config)
    {
        $this->_sourceConfig = $config;

        return $this;
    }

    /**
     * setzt die Konfiguration
     * @param Zend_Config $config
     */
    public function setConfig(Zend_Config $config)
    {
        $this->_config = $config;

        return $this;
    }
}