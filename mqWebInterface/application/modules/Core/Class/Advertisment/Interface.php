<?php

interface Core_Class_Advertisment_Interface
{
    /**
     * Hook direkt vor <html>
     *
     * @return string
     */
    public function renderPreHtml();

    /**
     * Hook für Rendering im <head>
     *
     * @return string
     */
    public function renderHeader();

    /**
     * Hook direkt nach dem <body>
     *
     * @return string
     */
    public function renderTop();

    /**
     * Hook für den Footer (nach </body>)
     *
     * @return string
     */
    public function renderFooter();

    /**
     * Rendert den AdBlock unter Verwendung des übergebenen Templates,
     * ansonsten wird das Standard-Template verwendet
     *
     * @param string $template
     */
    public function render($template = null);
}
