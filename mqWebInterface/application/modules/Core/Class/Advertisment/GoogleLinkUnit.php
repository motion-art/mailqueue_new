<?php

class Core_Class_Advertisment_GoogleLinkUnit extends Core_Class_Advertisment_Google_Js_Abstract implements Core_Class_Advertisment_Interface
{
    protected $_defaultTemplate = 'google/linkunit/block.phtml';

    public function renderHeader()
    {
        return $this->_partial('google/linkunit/head.phtml',
            array(
                'ads' => $this->_ads,
                'script' => $this->getScriptPath()
            )
        );
    }

    public function getScriptPath()
    {
        if ($this->_config->scriptpath) {
            return $this->_config->scriptpath;
        }

        return $this->_view->baseUrl('ads', null, 'googleLinkUnit.js');
    }
}
