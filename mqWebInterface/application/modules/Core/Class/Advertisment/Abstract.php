<?php

abstract class
    Core_Class_Advertisment_Abstract
implements
    Core_Class_Advertisment_Interface
{
    const VIEW_BASE_DIR = 'advertisment';

    /**
     * Ad Id
     * @var string
     */
    protected $_id = null;

    /**
     * View Helper (Advertisment)
     * @var Core_View_Helper_Advertisment
     */
    protected $_helper = null;

    /**
     * View
     * @var Zend_View_Abstract
     */
    protected $_view = null;

    /**
     * Konfiguration des Plugins
     * @var Zend_Config
     */
    protected $_config = null;

    /**
     * Standard Ad-Template
     * @var string
     */
    protected $_defaultTemplate = null;

    /**
     * Parameter
     * @var array
     */
    protected $_params = array();
    protected $_configName = null;

    public function __construct(Core_View_Helper_Advertisment $helper, Zend_View_Abstract $view, Zend_Config $config = null)
    {
        if ($config instanceof Zend_Config) {
            $this->_config = $config;
        } else {
            $this->_config = new Zend_Config(array());
        }
        $this->_helper = $helper;
        $this->_view = $view;
    }

    public function render($template = null)
    {
        if (!$this->_isEnabled()) {
            return '';
        }

        try {

            if ($template === null && $this->_defaultTemplate === null) {
                throw new Exception('no default template defined');
            }

            if ($template === null) {
                $template = $this->_defaultTemplate;
            }

            return $this->_partial(
                $template,
                null,
                array(
                    'config' => $this->_getAdConfig(),
                    'id' => $this->getId()
                )
            );

        } catch (Exception $e) {

            Core_Class_Log::getInstance()->crit($e);
        }
    }

    public function __toString()
    {
        return $this->render();
    }

    /**
     * Ermittle aktuelle Konfiguration des AdBlocks
     *
     * @return array
     */
    protected function _getAdConfig()
    {
        if ($this->_configName == null) {
            $classnameParts = explode('_', get_class($this));
            $this->_configName = strtolower(array_pop($classnameParts));
        }
        return array_merge($this->_params, $this->_helper->getConfig($this->_configName));
    }

    public function renderPreHtml()
    {
        return '';
    }

    public function renderHeader()
    {
        return '';
    }

    public function renderTop()
    {
        return '';
    }

    public function renderFooter()
    {
        return '';
    }

    /**
     * Setzt Konfigurationsparameter zur Laufzeit
     *
     * @param string $key
     * @param mixed $value
     * @return void
     */
    public function setParam($key, $value)
    {
        $this->_params[$key] = $value;
    }

    /**
     * Gibt einen bestimmten Parameter zurück
     *
     * @param string $key
     * @return mixed
     */
    public function getParam($key)
    {
        if (array_key_exists($key, $this->_params)) {
            return $this->_params[$key];
        }
        return null;
    }

    /**
     * Setzt das Plugin zurück
     *
     * @return void;
     */
    public function reset()
    {
        $this->_id = null;
    }

    /**
     * Setzt AdBlock-Id
     *
     * @param $id
     * @return void
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * Gibt AdBlock-Id zurück
     *
     * @return string
     */
    public function getId()
    {
        return $this->_id;
    }

    protected function _isEnabled()
    {
        return true;
    }

    protected function _getScriptPath($template)
    {
        return self::VIEW_BASE_DIR . DIRECTORY_SEPARATOR . $template;
    }

    protected function _partial($template, $module = null, $params = null)
    {
        return $this->_view->partial(
            $this->_getScriptPath($template),
            $module,
            $params
        );
    }

    protected function _mergeOptions(array $array1, $array2 = null)
    {
        if (is_array($array2)) {
            foreach ($array2 as $key => $val) {
                if (is_array($array2[$key])) {
                    $array1[$key] = (array_key_exists($key, $array1) && is_array($array1[$key]))
                                  ? $this->_mergeOptions($array1[$key], $array2[$key])
                                  : $array2[$key];
                } else {
                    $array1[$key] = $val;
                }
            }
        }
        return $array1;
    }
}
