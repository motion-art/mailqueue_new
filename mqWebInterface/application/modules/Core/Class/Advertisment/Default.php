<?php

class Core_Class_Advertisment_Default extends Core_Class_Advertisment_Abstract
{
    protected $_rendered = array();

    public function __construct($template, $helper, $view, $config = null)
    {
        if ($config instanceof Zend_Config) {
            $this->_config = $config;
        } else {
            $this->_config = new Zend_Config(array());
        }
        $this->_helper = $helper;
        $this->_view = $view;
        $this->_defaultTemplate = $template . '.phtml';
    }

    public function renderFooter()
    {
        return $this->render();
    }

    public function render($template = null)
    {
        if ($this->_isRendered($template)) {
            return '';
        }
        $this->_rendered[] = $template;

        return parent::render($template);
    }

    protected function _isRendered($template = null)
    {
        return in_array($template, $this->_rendered);
    }
}
