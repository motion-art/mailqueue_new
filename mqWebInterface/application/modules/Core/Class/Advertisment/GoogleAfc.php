<?php

class Core_Class_Advertisment_GoogleAfc extends Core_Class_Advertisment_Google_Js_Abstract implements Core_Class_Advertisment_Interface
{
    protected $_defaultTemplate = 'google/afc/block.phtml';

    public function renderHeader()
    {
        if (count($this->_ads) > 0) {
            return $this->_partial('google/afc/head.phtml',
                array(
                    'ads' => $this->_ads,
                    'script' => $this->getScriptPath()
                )
            );
        }
        return '';
    }

    /**
     * Start google Ad Section for Section Targeting.
     *
     * @param $name Name of the Section, if no name is submitted an unnamed section will be started
     * @param $weight Weight for the section if, if no weigt is submitted an unweighted section will be started,
     * posible values are 'ignore'(0.0), 'low'(0.1), 'medium'(0.5), 'height'(1.0) or numeric values betweeen 0.0 and 1.0
     * @return String the HTML Sourcecode for starting the Section
     */
    public function startSection($name = null, $weight = null)
    {
        $params = array();

        if ($name != null) {
            $params[] = 'name=' . $name;
        }
        if ($weight != null) {
            $params[] = 'weight=' . $weight;
        }
        return '<!-- google_ad_section_start(' . implode(', ', $params) . ') -->';
    }

    /**
     * End google Ad Section
     *
     * @param $name Name of the Section, if no name is submitted the unnamed section will be closed
     * @return String the HTML Sourcecode for ending the Section
     */
    public function endSection($name = null)
    {
        $params = array();

        if ($name != null) {
            $params[] = 'name=' . $name;
        }
        return '<!-- google_ad_section_end(' . implode(', ', $params) . ') -->';
    }

    protected function _isEnabled()
    {
        $config = $this->_getBaseConfig();

        return (bool) $config['google_afc'];
    }
}
