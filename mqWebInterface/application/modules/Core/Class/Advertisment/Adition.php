<?php

class
    Core_Class_Advertisment_Adition
extends
    Core_Class_Advertisment_Abstract
implements
    Core_Class_Advertisment_Interface
{
    const ID_TOP = 'top';
    const ID_SKY = 'sky';
    const ID_POP = 'pop';
    const ID_CA = 'ca';
    const ID_TOP_PLACEHOLDER = 'top_placeholder';
    const ID_SKY_PLACEHOLDER = 'sky_placeholder';

    /**
     * Registrierte Adition Werbeblöcke
     * @var array
     */
    protected $_ads = array();

    protected $_globalOptions = array(
        'ng_nuggn' => '',
        'ng_nuggsid' => '',
        'ng_nuggtg' => '',
        'adition_customerid' => '',
        'adition_contentunits' => '',
        'wl_publisherid' => '',
        'wl_websiteid' => '',
        'wl_profilingunit' => '',
        'tg_contentclass' => '',
        'tg_customerid' => '',
        'tg_websiteid' => ''
    );

    /**
     * Verwendete Adition-Ids
     * @var array
     */
    protected $_ids = array();

    public function renderHeader()
    {
        $return = '';
        $config = $this->_getAdConfig();
        $this->setGlobalOptions($config);

        // Top Ad
        if (isset($config[self::ID_TOP]) && $config[self::ID_TOP]) {
            $this->_ids[] = $config[self::ID_TOP];
            if (isset($config[self::ID_TOP_PLACEHOLDER]) && $config[self::ID_TOP_PLACEHOLDER]) {
                $return .= $this->_partial('adition/top_placeholder.phtml');
            }
        }

        // Sky Ad
        if (isset($config[self::ID_SKY]) && $config[self::ID_SKY]) {
            $this->_ids[] = $config[self::ID_SKY];
            if (isset($config[self::ID_SKY_PLACEHOLDER]) && $config[self::ID_SKY_PLACEHOLDER]) {
                $return .= $this->_partial('adition/sky_placeholder.phtml');
            }
        }

        // Pop Ad
        if (isset($config[self::ID_POP]) && $config[self::ID_POP]) {
            $this->_ids[] = $config[self::ID_POP];
        }

        // Render Global Tag
        $this->setGlobalOption('adition_contentunits', implode(',', $this->_ids));

        if (isset($config['agof_szm'])) {
            $this->setGlobalOption('tg_contentclass', $config['agof_szm']);
        }
        if (isset($config['nuggad'])) {
            $this->setGlobalOption('ng_nuggtg', $config['nuggad']);
        }

        if (count($this->_ids) > 0) {
            $return .= $this->_partial(
                'adition/global.phtml',
                null,
                array(
                    'params' => http_build_query($this->getGlobalOptions())
                )
            );
        }

        return $return;
    }

    public function getGlobalOptions()
    {
        return $this->_globalOptions;
    }

    public function setGlobalOptions($options)
    {
        foreach ($options as $option => $value) {
            $this->setGlobalOption($option, $value);
        }
    }

    public function setGlobalOption($option, $value)
    {
        if (array_key_exists($option, $this->_globalOptions)) {
            $this->_globalOptions[$option] = $value;
        }
    }

    protected function _renderAd($type)
    {
        return $this->render('adition/' . strtolower($type) . '.phtml');
    }

    /**
     * Rendert einen Content Ad
     *
     * @param string $type
     */
    public function ca($type)
    {
        if (empty($type)) {
            throw new Exception('ContentAd Type expected');
        }
        $config = $this->_getAdConfig();

        if (isset($config['ca']) && isset($config['ca'][$type])) {

            $id = count($this->_ads) + 1;
            $id = $type . $id;

            $this->_ads[$id] = $type;
            $this->_ids[] = $config['ca'][$type];

            return $this->_partial(
                'adition/ca_container.phtml',
                null,
                array(
                    'id' => $id
                )
            );
        }
    }

    public function renderFooter()
    {
        $config = $this->_getAdConfig();

        // render top + sky
        $aditions = (isset($config[self::ID_TOP])? $this->_renderAd(self::ID_TOP):'')
                  . (isset($config[self::ID_SKY])? $this->_renderAd(self::ID_SKY):'');

        foreach($this->_ads as $id => $type) {

            $aditions .= $this->_partial(
                'adition/ca.phtml',
                null,
                array(
                    'id' => $id,
                    'config' => $config,
                    'type' => $type
                )
            );
        }

        // render pop
        $aditions .= (isset($config[self::ID_POP])? $this->_renderAd(self::ID_POP):'');

        if (count($this->_ads) > 0) {

            $aditions .= $this->_partial(
                'adition/move_ads.phtml',
                null,
                array(
                    'ads' => $this->_ads
                )
            );
        }

        return $aditions;
    }
}
