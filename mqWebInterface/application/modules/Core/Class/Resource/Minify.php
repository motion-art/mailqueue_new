<?php

class Core_Class_Resource_Minify
{
    protected $_config = null;

    static private $_instance = null;

    protected function __construct()
    {
        $this->_config = Zend_Registry::get('_CONFIG')->minify;
        $this->_groups = Core_Class_Config::get('Minify', 'default')->minify;
    }

    static public function getInstance()
    {
        if (self::$_instance === null) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function isEnabled()
    {
        return (isset($this->_config->enabled) && $this->_config->enabled);
    }

    public function getGroupFile($filename)
    {
        if (!$this->isEnabled()) {
            return $filename;
        }

        $base = Core_Class_BasePath::getInstance()->getBasename($filename);

        foreach ($this->_groups as $group) {

            foreach ($group->partial as $partial) {

                if ($partial == $base) {

                    $var = Core_Class_BasePath::getInstance()->getPath($group->resource, null, $group->filename) . '.var';

                    if (file_exists($var)) {

                        return Core_Class_BasePath::getInstance()->getUrl($group->resource, null, $group->filename) . '.var';

                    } else {

                        Core_Class_Log::getInstance()->debug('groupfile "' . $var . '" not found');
                        return $filename;
                    }

                    break;
                }
            }
        }

        Core_Class_Log::getInstance()->debug('resource "' . $filename . '" not found in group');

        return $filename;
    }
}
