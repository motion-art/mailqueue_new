<?php

/**
 * Core_Class_Cache
 *
 * builds cache object from config with fallback to filecache
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: Cache.php 389 2010-07-09 13:05:40Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_Class_Cache
{
    const DEFAULT_LIFETIME = 1800;

    const CACHE_NAME_DEFAULT = 'data';
    const CACHE_NAME_VIEW = 'view';
    const CACHE_NAME_DATABASE = 'database';
    const CACHE_NAME_CONFIG = 'config';
    const CACHE_NAME_PATH = 'path';
    const CACHE_NAME_HTTP = 'http';

    static function getCache($name = null)
    {
        if (!Zend_Registry::isRegistered(Core_Class_Bootstrap::REGISTRY)
             || !Zend_Registry::get(Core_Class_Bootstrap::REGISTRY)->hasResource('cachemanager')
        ) {
            return self::_getFallbackCache($name);
        }

        $cachemanager = Zend_Registry::get(Core_Class_Bootstrap::REGISTRY)->getResource('cachemanager');

        if ($name == null || !$cachemanager->hasCache($name)) {
            Core_Class_Log::getInstance()->debug('cache "' . $name . '" not found; default used');
            $name = self::CACHE_NAME_DEFAULT;
        }
        return $cachemanager->getCache($name);
    }

    /**
     * returns default cache object
     *
     * @param string $name
     * @param int $lifetime
     * @return Zend_Cache_Core
     */
    static function getDefaultCache($name = null, $lifetime = self::DEFAULT_LIFETIME)
    {
        Core_Class_Log::getInstance()->info('method "getDefaultCache" deprecated');

        return self::getCache($name);
    }

    static protected function _getFallbackCache($name = null)
    {
        if (isset(Zend_Registry::get('_CONFIG')->cache)) {

            return self::getCacheFromConfig($name, Zend_Registry::get('_CONFIG')->cache);
        }

        return self::_getFileCache($name);
    }

    /**
     * returns cache object from config
     *
     * @param string $name
     * @param Zend_Config $config
     * @param int $lifetime
     * @return Zend_Cache_Core
     */
    static function getCacheFromConfig($name, Zend_Config $config = null, $lifetime = self::DEFAULT_LIFETIME)
    {
        Core_Class_Log::getInstance()->info('method "getCacheFromConfig" deprecated');

        if ($config == null || !isset($config->backend) || !isset($config->backendOptions)) {

            return self::getDefaultCache($name);
        }

        $frontendOptions = array();

        if (isset($config->frontendOptions)) {
            $frontendOptions = $config->frontendOptions->toArray();
        }

        $frontendOptions['lifetime'] = $lifetime;
        $frontendOptions['automatic_serialization'] = true;
        $frontendOptions['caching'] = self::getCaching();
        $frontendOptions['cache_id_prefix'] = $name;

        return Zend_Cache::factory(
            'Core',
            $config->backend,
            $frontendOptions,
            $config->backendOptions->toArray()
        );
    }

    public static function getCacheDir($name)
    {
        return Core_Config :: getInstance()->CACHE_PATH . '/' . $name;
    }

    static function _getFileCache($name, $lifetime = self::DEFAULT_LIFETIME)
    {
        if (!file_exists(self::getCacheDir($name))) {

            mkdir(self::getCacheDir($name), 0777, true);
        }

        $frontendOptions = array(
           'lifetime' => $lifetime,
           'automatic_serialization' => true,
           'caching' => self::getCaching()
        );

        $backendOptions = array(
            'cache_dir' => self::getCacheDir($name)
        );

        $cache = Zend_Cache::factory('Core',
                                     'File',
                                     $frontendOptions,
                                     $backendOptions);
        return $cache;
    }

    public static function getCaching()
    {
        if (isset(Zend_Registry::get('_CONFIG')->cache) && Zend_Registry::get('_CONFIG')->cache->enabled == true) {
            return true;
        }
        return false;
    }

    public static function getKey($id)
    {
        return md5($id);
    }
}