<?php

/**
 * Core_LanguageController
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: LanguageController.php 292 2010-06-08 08:33:46Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_LanguageController extends Zend_Controller_Action
{
    public function changeAction()
    {
        $this->_helper->viewRenderer->setNoRender();

        $locale = Zend_Registry::get('Zend_Locale');
        $language = $this->_getParam('lang');

        if ($locale->isLocale($language)) {
            $locale->setLocale($language);
            $session_lang = new Zend_Session_Namespace('Zend_Locale_Language');
            $session_lang->language = $locale->getLanguage();
        }

        $this->_redirect($_SERVER['HTTP_REFERER']);
    }
}
