<?php

/**
 * Core_View_Helper_RevisionHeadScript
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id$
 * @copyright 2009 Unister GmbH
 */

class Core_View_Helper_RevisionHeadScript extends Zend_View_Helper_HeadScript
{
    public function revisionHeadScript($mode = Zend_View_Helper_HeadScript::FILE, $spec = null, $placement = 'APPEND', array $attrs = array(), $type = 'text/javascript')
    {
        return parent::headScript($mode, $spec, $placement, $attrs, $type);
    }

    public function __call($method, $args)
    {
        if (preg_match('/^(?P<action>set|(ap|pre)pend|offsetSet)(?P<mode>File|Script)$/', $method, $matches)) {
            if (1 > count($args)) {
                require_once 'Zend/View/Exception.php';
                throw new Zend_View_Exception(sprintf('Method "%s" requires at least one argument', $method));
            }

            $action  = $matches['action'];
            $mode    = strtolower($matches['mode']);
            $type    = 'text/javascript';
            $attrs   = array();

            if ('offsetSet' == $action) {
                $index = array_shift($args);
                if (1 > count($args)) {
                    require_once 'Zend/View/Exception.php';
                    throw new Zend_View_Exception(sprintf('Method "%s" requires at least two arguments, an index and source', $method));
                }
            }

            $content = $args[0];

            if (isset($args[1])) {
                $type = (string) $args[1];
            }
            if (isset($args[2])) {
                $attrs = (array) $args[2];
            }

            switch ($mode) {
                case 'script':
                    $item = $this->createData($type, $attrs, $content);
                    if ('offsetSet' == $action) {
                        $this->offsetSet($index, $item);
                    } else {
                        $this->$action($item);
                    }
                    break;
                case 'file':
                default:

                    $content = Core_Class_Resource_Minify::getInstance()->getGroupFile($content);
                    $content .= Core_Class_Revision::getString();

                    if (!$this->_isDuplicate($content)) {
                        $attrs['src'] = $content;
                        $item = $this->createData($type, $attrs);
                        if ('offsetSet' == $action) {
                            $this->offsetSet($index, $item);
                        } else {
                            $this->$action($item);
                        }
                    }
                    break;
            }

            return $this;
        }

        return parent::__call($method, $args);
    }
}
