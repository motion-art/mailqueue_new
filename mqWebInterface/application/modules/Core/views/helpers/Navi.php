<?php

class Core_View_Helper_Navi extends Zend_View_Helper_Abstract
{
    public function navi()
    {
        return $this;
    }

    protected function _getNavigation()
    {
        $container = Zend_Registry::get('Zend_Navigation');
        if (!$container instanceof Zend_Navigation) {
            throw new Exception('Zend_Navigation expected');
        }

        return $container;
    }

    public function getActiveNavigationNode()
    {
        if ($active = $this->findActive($this->_getNavigation())) {
            if ($active->getId() != 'home') {
                return $this->_filter($active->getLabel());
            }
        }
        return '';
    }

    private function _filter($label)
    {
        $alnumFilter = new Zend_Filter_Alnum();
        $alnumFilter->setAllowWhiteSpace(true);

        return $alnumFilter->filter($label);
    }

    public function getNavigationPath($separator = '|')
    {
        $container = $this->_getNavigation();

        if (!$active = $this->findActive($container)) {
            return '';
        }

        $result = array();

        while ($parent = $active->getParent()) {
            if ($parent instanceof Zend_Navigation_Page && $parent->getId() != 'home') {
                $result[] = $this->_filter($parent->getLabel());
            }

            if ($parent === $container) {
                break;
            }

            $active = $parent;
        }

        return implode($separator, array_reverse($result));
    }

    public function findActive(Zend_Navigation_Container $container)
    {
        $found  = null;
        $iterator = new RecursiveIteratorIterator($container,
                RecursiveIteratorIterator::CHILD_FIRST);

        foreach ($iterator as $page) {
            if ($page->isActive(false)) {
                return $page;
            }
        }

        return $found;
    }
}
