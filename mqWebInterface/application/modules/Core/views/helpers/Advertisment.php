<?php

class Core_View_Helper_Advertisment extends Zend_View_Helper_Abstract
{
    const DEFAULT_PLUGIN_NAMESPACE = 'Core_Class_Advertisment';

    /**
     * user ad-key (e.g. categoryId)
     * @var string
     */
    protected $_ids = array();

    /**
     * advertisment helper config
     * @var array
     */
    protected $_config = null;

    /**
     * user params
     * @var array
     */
    protected $_params = array();

    /**
     * registered plugin prefix paths
     * @var array
     */
    protected $_pluginprefix = array();

    /**
     * registered plugins
     * @var array
     */
    protected $_plugins = array();

    /**
     * loads initial config
     * @return Core_View_Helper_Advertisment
     */
    public function __construct()
    {
        if (isset(Zend_Registry::get('_CONFIG')->advertisment)) {
            $this->_config = Zend_Registry::get('_CONFIG')->advertisment;
        } else {
            $this->_config = new Zend_Config(array());
        }

        if (isset($this->_config->configAdapter)) {
            Core_Class_Advertisment_Config::getInstance()->setAdapter($this->_config->configAdapter);
        }

        $this->_pluginprefix[] = self::DEFAULT_PLUGIN_NAMESPACE;
    }

    /**
     * renders partial with assigned config
     *
     * @param string $partial
     * @param array $param
     * @param string $controller
     * @param string $module
     * @return string
     */
    public function advertisment()
    {
        return $this;
    }

    /**
     * add prefix path for plugin classes
     *
     * @param string $path the path to insert
     * @param int $pos the position where the path should be inserted. Default at the front of the array.
     */
    public function addPrefixPath($path, $pos = 0)
    {
        if ($pos) {
            $this->_pluginprefix = array_merge(array_slice($this->_pluginprefix,0, $pos-1),
                                               array($path),
                                               array_slice($this->_pluginprefix, $pos-1));
        } else {
            array_unshift($this->_pluginprefix, $path);
        }
    }

    /**
     * remove prefix path for plugin classes
     *
     * @param string $path the path to remove
     */
    public function removePrefixPath($path)
    {
        $pos = array_search($path, $this->_pluginprefix);
        if ($pos !== false) {
            unset($this->_pluginprefix[$pos]);
        }
    }

    public function initPlugins($plugins = array())
    {
        foreach ($plugins as $plugin) {

            $this->_getPlugin($plugin);
        }
    }

    /**
     * füge KategorieId hinzu
     *
     * @param int $id
     * @return Core_View_Helper_Advertisment
     */
    public function addCategoryId($id)
    {
        $this->_ids[] = $id;
        return $this;
    }

    /**
     * merge config with user-params
     * @return array
     */
    public function getConfig($plugin = null)
    {
        $mainConfig = Core_Class_Advertisment_Config::getInstance()->get($this->_ids);

        $config = $mainConfig['default'];
        if (isset($mainConfig[$plugin])) {
            $config = array_merge($config, $mainConfig[$plugin]);
        }
        if (count($this->_params) > 0) {
            $config = array_merge($config, $this->_params);
        }

        return $config;
    }

    /**
     * sets param
     *
     * @param string $key
     * @param string $value
     * @return void
     */
    public function setParam($key, $value)
    {
        $this->_params[$key] = $value;
    }

    /**
     * param-filter
     * use alnum-filter with whitespaces
     *
     * @param string $name
     * @return string
     */
    public function filter($name)
    {
        $alnumFilter = new Zend_Filter_Alnum();
        $alnumFilter->setAllowWhiteSpace(true);

        return $alnumFilter->filter($name);
    }

    public function renderPreHtml()
    {
           $result = '';
        foreach ($this->_plugins as $plugin) {
            $result .= $plugin->renderPreHtml();
        }

        return $result;
    }

    public function renderHeader()
    {
           $result = '';
        foreach ($this->_plugins as $plugin) {
            $result .= $plugin->renderHeader();
        }

        return $result;
    }

    public function renderTop()
    {
        $result = '';
        foreach ($this->_plugins as $plugin) {
            $result .= $plugin->renderTop();
        }

        return $result;
    }

    public function renderFooter()
    {
        $result = '';
        foreach ($this->_plugins as $plugin) {
            $result .= $plugin->renderFooter();
        }

        return $result;
    }

    public function __call($method, $args)
    {
        $plugin = $this->_getPlugin($method);
        $plugin->reset();

        if (isset($args[0]) && is_array($args[0])) {
            foreach ($args[0] as $key => $value) {
                $plugin->setParam($key, $value);
            }
        }

        if (isset($args[1])) {
            $plugin->setId($args[1]);
        }

        return $plugin;
    }

    private function _getPlugin($name)
    {
        $pluginId = strtolower($name);
        if (!isset($this->_plugins[$pluginId])) {

            foreach ($this->_pluginprefix as $prefix) {
                $className = $prefix . '_' . ucfirst($name);
                if (@class_exists($className)) {
                   $this->_plugins[$pluginId] = new $className($this, $this->view, (isset($this->_config->$pluginId)?$this->_config->$pluginId:null));
                   return $this->_plugins[$pluginId];
                }
            }
            $this->_plugins[$pluginId] = new Core_Class_Advertisment_Default($name, $this, $this->view, (isset($this->_config->$pluginId)?$this->_config->$pluginId:null));

        }

        return $this->_plugins[$pluginId];
    }
}