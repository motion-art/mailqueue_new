<?php

/**
 * Core_View_Helper_GetSeoDirectoryUrl
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id$
 * @copyright 2009 Unister GmbH
 */

class Core_View_Helper_GetSeoContentNetwork extends Zend_View_Helper_Abstract
{
    /**
     * Flag, ob SpecialIds bereits eingelesen wurden
     * 
     * @var bool
     */
	private $_specialPageIdsInitialized = false;

	/**
	 * Liste der SpecialIds
	 * 
	 * @var array
	 */
    private $_specialPageIds = array();
	
    public function getSeoContentNetwork(){
        return $this;
    }

    /**
     * Gibts die RootPages zurück
     * welche auf jeder Seite angezeigt werden
     * 
     * @return unknown_type
     */
    public function getRoots()
    {
        return Core_Class_Seo_Network::getInstance()->getRootPages();
    }

    /**
     * Gibt die Url zum Inhaltsverzeichnis des
     * Seo-Contentnetworks zurück
     * 
     * @return unknown_type
     */
    public function getDirectoryUrl()
    {
        $LinkBuilder = Core_Class_Seo_Network::getInstance()
                     ->getLinkBuilder();

        $url = "";

        if($LinkBuilder) {
            $url = $LinkBuilder->getDirectoryUrl();
        }else{
            Core_Class_Log::getInstance()->warn("Core_View_Helper_GetSeoDirectoryUrl kein LinkBuilder gefunden.");
        }

        return $url;
    }
    
    /**
     * Gibt eine spezialseite zurück
     * definiert in der config:
     * seo.network.specialPage.[name]   = [id]
     * 
     * @param string name
     * @return Core_Class_Seo_Network_Page_Interface 
     */
    public function getSpecialPageByName($name){
        if (!$this->_specialPageIdsInitialized) {
        	//zuerst die SpeicalIds laden
        	$this->_initSpecialPages();
        }
    	
    	if(!isset($this->_specialPageIds[strtolower($name)])){
            throw new Core_Class_Seo_Network_Exception("Special page $name not exists");
        }
        
        return Core_Class_Seo_Network::getInstance()->getPageById($this->_specialPageIds[strtolower($name)]);
    }
    
    /**
     * Liest die SpecialIds ein
     * 
     * Steuerung per Flag, damit erst bei der ersten Anforderung
     * die SpecialIds geladen werden
     */
    private function _initSpecialPages() {
        $this->_specialPageIdsInitialized = true;
    	$config     = Zend_Registry::get('_CONFIG');
        
        if(isset($config->seo->network->specialPage)){
            $this->_specialPageIds = $config->seo->network->specialPage->toArray();
        }
    }
}
