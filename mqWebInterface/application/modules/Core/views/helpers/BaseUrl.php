<?php

/**
 * Core_View_Helper_BaseUrl
 *
 * @package   Core
 * @author    Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @version   $Id: BaseUrl.php 287 2010-06-07 12:39:32Z roman_lasinski $
 * @copyright 2009 Unister GmbH
 */

class Core_View_Helper_BaseUrl
{
    public function baseUrl($type = null, $module = null, $resource = null)
    {
        return Core_Class_BasePath::getInstance()->getUrl($type, $module, $resource);
    }
}
