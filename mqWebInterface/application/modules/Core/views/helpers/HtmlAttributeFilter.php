<?php

class Core_View_Helper_HtmlAttributeFilter extends Zend_View_Helper_Abstract
{
    const REPLACE_PATTERN = '/["<>]/';

    public function htmlAttributeFilter($value)
    {
        return preg_replace(self::REPLACE_PATTERN, '', $value);
    }
}
