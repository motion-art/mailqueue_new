<?php


class Unister_Classes_Class_MailErrorCorrection
{

    /**
     * Keine Stelle gefunden, wo die Variable genutzt wird...
     * @deprecated
     * @var array
     */
    protected $_mailDatas = array();

    /**
     * So wie es scheint, wird diese Variable noch in Unister_Classes_Model_MailErrorCorrection genutzt, daher public
     * @var array
     */
    public $emailLogs = array();

    /**
     * So wie es scheint, wird diese Variable noch in Unister_Classes_Model_MailErrorCorrection genutzt, daher public
     * @var array
     */
    public $emailUpdates = array();

    /**
     * @var Zend_Validate
     */
    protected $_emailValidator = null;

    /**
     * @var Zend_Filter
     */
    protected $_emailFilter = null;

    /**
     * @var unknown_type
     */
    protected $_existentValidator = null;

    /**
     * return sendmail-model
     *
     */
    protected function _getMailErrorCorrectionModel()
    {
        return new model_MailErrorCorrection();
    }

    public function start()
    {
        $deniedPortal = array();
        $mails = $this->_getMailErrorCorrectionModel()->fetchMailsForCron($Mod, $deniedPortal);
        if (count($mails) > 0) {
            $this->_setValidators();
            $changes = false;
            foreach ($mails as $key => &$value) {
                if ($this->_proofMailStrings('email', $value, true) === false) {
                    $changes = true;
                }
                if ($this->_proofMailStrings('cc', $value, false) === false) {
                    $changes = true;
                }
                if ($this->_proofMailStrings('bcc', $value, false) === false) {
                    $changes = true;
                }
                if ($this->_proofMailStrings('fromemail', $value, false) === false) {
                    $changes = true;
                }

                if ($changes === true) {
                    if (strlen($value['email']) == 0) {
                        $this->emailUpdates['markToDelete'][$value['mailtype']][] = $value['mailid'];
                        $this->emailLogs[] = array(
                                                'Portal'   => $value['portal'],
                                                'Category' => $value['category'],
                                                'Type'     => $value['type'],
                                                'Email'    => 0,
                                                'Server'   => 0,
                                                'Deleted'  => 1,
                                            );
                    } else {
                        $this->emailUpdates['markToUpdate'][$value['mailtype']][$value['mailid']]['Email']
                            = 'Email = \'' . $value['email'] . '\'';
                        $this->emailUpdates['markToUpdate'][$value['mailtype']][$value['mailid']]['CC']
                            = 'CC = \'' . $value['cc'] . '\'';
                        $this->emailUpdates['markToUpdate'][$value['mailtype']][$value['mailid']]['BCC']
                            = 'BCC = \'' . $value['bcc'] . '\'';
                        $this->emailUpdates['markToUpdate'][$value['mailtype']][$value['mailid']]['FromEmail']
                            = 'FromEmail = \'' . $value['fromemail'] . '\'';
                        $this->emailUpdates['markToResend'][$value['mailtype']][] = $value['mailid'];
                    }
                } else {
                    $this->emailLogs[] = array(
                        'Portal'    => $value['portal'],
                        'Category'  => $value['category'],
                        'Type'      => $value['type'],
                        'Email'     => 0,
                        'Server'    => 1,
                        'Deleted'   => 0,
                    );
                    $this->emailUpdates['markToResend'][$value['mailtype']][] = $value['mailid'];
                }
            }
        } else {
            return false;
        }
        return $this->_getMailErrorCorrectionModel()->doCorrectionJobs($this);
    }

    protected function _setValidators()
    {
        $this->_emailValidator = new Zend_Validate();
        $this->_emailValidator->addValidator(new Zend_Validate_NotEmpty(), true);
        $this->_emailValidator->addValidator(new Zend_Validate_EmailAddress(), true);

        $this->_existentValidator = new Zend_Validate();
        $this->_existentValidator->addValidator(new Zend_Validate_NotEmpty(), true);

        $this->_emailFilter = new Unister_Classes_Class_Filter_Idna();
    }

    /**
     * proofs mailstring - validation
     *
     * @param string $mailfield
     * @return boolean
     */
    protected function _proofMailStrings($mailfield, &$datas, $expired = false)
    {
        $return = true;
        if (strlen($datas[$mailfield]) > 0) {
            $tmpDatas = array();
            $tmpDatas = explode(';', $datas[$mailfield]);
            foreach ($tmpDatas as $key => $address) {
                if (!$this->_emailValidator->isValid($address) || $address[0] == '-') {
                    unset($tmpDatas[$key]);
                    $this->emailLogs[] = array(
                        'Portal'    => $datas['portal'],
                        'Category'  => $datas['category'],
                        'Type'      => $datas['type'],
                        'Email'     => 1,
                        'Server'    => 0,
                        'Deleted'   => 0,
                    );
                    $return = false;
                } else {
                    // Punycode filtern (Umlaute im Domainpart ersetzen)...
                    $address = $this->_emailFilter->filter($address);
                    $tmpDatas[$key] = trim($address);
                }
            }
            $datas[$mailfield] = implode(';', $tmpDatas);
        } elseif ($expired === true) {
            $return = false;
        }
        return $return;
    }
}
