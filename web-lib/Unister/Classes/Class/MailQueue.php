<?php


class Unister_Classes_Class_MailQueue {
	
	/**
	 * memberid of acceptor
	 *
	 * @var integer
	 */
	var $MemberTo_Id = null;
	
	/**
	 * memberid of sender
	 *
	 * @var integer
	 */
	var $MemberFrom_Id = null;
	
	/**
	 * email category
	 *
	 * @var string
	 */
	var $Category = null;
	
	/**
	 * type of email category
	 *
	 * @var string
	 */
	var $Type = null;
	
	/**
	 * email subject
	 *
	 * @var string
	 */
	var $Subject = null;
	
	/**
	 * email textpart
	 *
	 * @var string
	 */
	var $Text = null;
	
	/**
	 * email htmlpart
	 *
	 * @var string
	 */
	var $Html = null;
	
	/**
	 * emailaddress of acceptor
	 *
	 * @var string
	 */
	var $Email = null;
	
	/**
	 * Name of sender
	 *
	 * @var string 
	 */
	var $FromName = null;
	
	/**
	 * emailaddress of sender
	 *
	 * @var string
	 */
	var $FromEmail = null;
	
	/**
	 * reply emailaddress
	 *
	 * @var string
	 */
	var $ReplyTo = null;
	
	/**
	 * cc emailaddresses (max 5)
	 *
	 * @var array
	 */
	var $CC = null;
	
	/**
	 * bcc emailaddresses (max 5)
	 *
	 * @var array
	 */
	var $BCC = null;
	
	/**
	 * email priority
	 *
	 * @var integer
	 */
	var $Prio = null;
	
	/**
	 * email portal
	 *
	 * @var string
	 */
	var $Portal = null;
	
	/**
	 * attachment content
	 *
	 * @var string
	 */
	var $Attachment = null;
	
	/**
	 * what mailtype (instant, compendium)
	 *
	 * @var string
	 */
	var $MailType = null;
	
	/**
	 * contains all required params
	 *
	 * after params check it contains the results of checked params, if a error is found
	 *
	 * @var array
	 */
	var $CheckParams = array(
								'MemberTo_Id' 	=> '',
	  							'MemberFrom_Id' => '',
	  							'Category' 		=> '',
	  							'Type' 			=> '',
	 	 						'Subject' 		=> '',
	 	 						'Text' 			=> '',
	 							'Email' 		=> '',
	 							'Portal' 		=> '',
	 							'MailType' 		=> '',
							);
	
	/**
	 * ZEND Validator Object
	 *
	 * @var object
	 */
	var $validator;
	
	public function __construct(){
	}
	
	/**
	 * returns mailqueue model object
	 *
	 * @return unknown
	 */
	protected function _getMailQueueModel(){
		return new model_MailQueue();
	}
	
	/**
	 * @param insert mail in mailqueue $Params
	 * @return array
	 */
	public function sendMail($Params){
		$this->setEmailProperties($Params);
		if(count($this->CheckParams) == 0){
			$this->CheckParams['Insert'] = $this->_getMailQueueModel()->MailQueueInsert($this);
		}
		return $this->CheckParams;
	}
	
	/**
	 * accept email-properties and proof validation
	 *
	 * @param array $Params (
	 * 							'MemberTo_Id' 	-> required
	 * 							'MemberFrom_Id' -> required
	 * 							'Category' 		-> required
	 * 							'Type' 			-> required
	 *	 						'Subject' 		-> required
	 *	 						'Text' 			-> required
	 *	 						'Html'
	 *	 						'Email' 		-> required
	 *	 						'FromName'
	 *	 						'FromEmail'
	 *	 						'ReplyTo'
	 *	 						'CC'
	 *	 						'BCC'
	 *	 						'Prio'
	 *	 						'Portal' 		-> required
	 * 							'MailType' 		-> required
	 * 							'Attachment'
	 * 						)
	 */
	protected function setEmailProperties(&$Params){
		/**
		 * merge input params and required params 
		 */
		$Params = array_merge($this->CheckParams, $Params);
		/**
		 * clean checkparams array, to save possible errors
		 */
		$this->CheckParams = array();
		$this->validator = new Zend_Validate();
		foreach($Params as $param=>&$value){
			$checkfunction = "check".$param;				
			$this->$checkfunction($value);
			$this->validator->resetValidatorChain();
			$this->$param = $value;
		}
	}
	
	/**
	 * check attachment, no check
	 *
	 * @param array $value
	 */
	protected function checkAttachment(&$value){
		return;
	}
	
	/**
	 * check mailtype
	 *
	 * @param string $value
	 */
	protected function checkMailType(&$value){	
		$this->validator->addValidator(new Zend_Validate_NotEmpty(),true);
		if(!$this->validator->isValid($value)){
			$this->CheckParams['MailType'] = $this->validator->getMessages();
		}else{
			$value = strtolower($value);
			if($value != 'instant' && $value != 'compendium'){
				$this->CheckParams['MailType'] = "MailType is not 'instant' OR 'compendium'";
			}
		}
		return;
	}
	
	/**
	 * check portal
	 *
	 * @param string $value
	 */
	protected function checkPortal(&$value){	
		$this->validator->addValidator(new Zend_Validate_NotEmpty(),true);
		if(!$this->validator->isValid($value)){
			$this->CheckParams['Portal'] = $this->validator->getMessages();
		}
		$value = strtolower($value);
		return;
	}
	
	/**
	 * check prio
	 *
	 * @param integer $value
	 */
	protected function checkPrio(&$value){	
		$this->validator->addValidator(new Zend_Validate_Int(),true);
		if(!$this->validator->isValid($value)){
			$this->CheckParams['Prio'] = $this->validator->getMessages();
		}
		return;
	}
	
	/**
	 * check BCC
	 *
	 * @param array $value
	 */
	protected function checkBCC(&$value){	
		if(is_array($value) && count($value) > 0){
			$this->validator->addValidator(new Zend_Validate_EmailAddress(),true);
			$bccs = min(5,count($value));
			foreach($value as $email){
				if($bccs == 0){
					break;
				}
				if(!$this->validator->isValid($email)){
					$this->CheckParams['BCC'] = $this->validator->getMessages();
					return;
				}	
				$bccs--;
			}
		}
		return;
	}
	
	/**
	 * check CC
	 *
	 * @param array $value
	 */
	protected function checkCC(&$value){	
		if(is_array($value)){
			$this->validator->addValidator(new Zend_Validate_EmailAddress(),true);
			$ccs = min(5,count($value));
			foreach($value as $email){
				if($ccs == 0){
					break;
				}
				if(!$this->validator->isValid($email)){
					$this->CheckParams['CC'] = $this->validator->getMessages();
					return;
				}	
				$ccs--;
			}
		}
		return;
	}
	
	/**
	 * check ReplyTo
	 *
	 * @param string $value
	 */
	protected function checkReplyTo(&$value){	
		if(strlen($value) > 0){	
			$this->validator->addValidator(new Zend_Validate_EmailAddress(),true);
			if(!$this->validator->isValid($value)){
				$this->CheckParams['ReplyTo'] = $this->validator->getMessages();
			}
		}
		return;
	}
	
	/**
	 * check FromEmail
	 *
	 * @param string $value
	 */
	protected function checkFromEmail(&$value){	
		if(strlen($value) > 0){	
			$this->validator->addValidator(new Zend_Validate_EmailAddress(),true);
			if(!$this->validator->isValid($value)){
				$this->CheckParams['FromEmail'] = $this->validator->getMessages();
			}
		}
		return;
	}
	
	/**
	 * check FromName
	 *
	 * @param string $value
	 */
	protected function checkFromName(&$value){		
		return;
	}
	
	/**
	 * check Email
	 *
	 * @param string $value
	 */
	protected function checkEmail(&$value){		
		$this->validator->addValidator(new Zend_Validate_NotEmpty(),true);
		$this->validator->addValidator(new Zend_Validate_EmailAddress(),true);
		if(!$this->validator->isValid($value)){
			$this->CheckParams['Email'] = $this->validator->getMessages();
		}
		return;
	}
	
	/**
	 * check html
	 *
	 * @param string $value
	 */
	protected function checkHtml(&$value){		
		return;
	}
	
	/**
	 * check text
	 *
	 * @param string $value
	 */
	protected function checkText(&$value){		
		$this->validator->addValidator(new Zend_Validate_NotEmpty(),true);
		if(!$this->validator->isValid($value)){
			$this->CheckParams['Text'] = $this->validator->getMessages();
		}
		return;
	}
	
	/**
	 * check subject
	 *
	 * @param string $value
	 */
	protected function checkSubject(&$value){		
		$this->validator->addValidator(new Zend_Validate_NotEmpty(),true);
		if(!$this->validator->isValid($value)){
			$this->CheckParams['Subject'] = $this->validator->getMessages();
		}
		return;
	}
	
	/**
	 * check type
	 *
	 * @param string $value
	 */
	protected function checkType(&$value){		
		$this->validator->addValidator(new Zend_Validate_NotEmpty(),true);
		if(!$this->validator->isValid($value)){
			$this->CheckParams['Type'] = $this->validator->getMessages();
		}else{
			$value = strtoupper($value);
		}
		return;
	}
	
	/**
	 * check category
	 *
	 * @param string $value
	 */
	protected function checkCategory(&$value){		
		$this->validator->addValidator(new Zend_Validate_NotEmpty(),true);
		if(!$this->validator->isValid($value)){
			$this->CheckParams['Category'] = $this->validator->getMessages();
		}else{
			$value = strtoupper($value);
		}
		return;
	}
	
	/**
	 * check MemberTo_Id
	 *
	 * @param integer $value
	 */
	protected function checkMemberTo_Id(&$value){		
		$this->validator->addValidator(new Zend_Validate_NotEmpty(),true);
		$this->validator->addValidator(new Zend_Validate_Int(),true);
		if(!$this->validator->isValid($value)){
			$this->CheckParams['MemberTo_Id'] = $this->validator->getMessages();
		}
		return;
	}
	
	/**
	 * check MemberFrom_Id
	 *
	 * @param integer $value
	 */
	protected function checkMemberFrom_Id(&$value){	
		$this->validator->addValidator(new Zend_Validate_NotEmpty(),true);
		$this->validator->addValidator(new Zend_Validate_Int(),true);
		if(!$this->validator->isValid($value)){
			$this->CheckParams['MemberFrom_Id'] = $this->validator->getMessages();
		}
		return;
	}
	
	/**
	 * get file infos, which are important to create an attachment
	 *
	 * @param string $pathtofile
	 * @return array
	 */
	public function getAttachmentParamsFromFile($pathtofile){
		return array(
						'AttachmentContent' => file_get_contents($pathtofile),
						'AttachmentType' 	=> mime_content_type($pathtofile),
						'AttachmentFile' 	=> substr(strrchr($pathtofile,"/"),1),
					);
	}
	
	/**
	 * save an attachment
	 *
	 * @param array $Attachments
	 * @return boolean
	 */
	public function saveAttachments($Attachments){
		$return = array();
		return $this->_getMailQueueModel()->MailAttachmentInsert($Attachments);
	}
}


