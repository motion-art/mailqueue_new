<?php

class Unister_Classes_Class_MailStats {
	
	/**
	 * mail_archive id
	 *
	 * @var integer
	 */
	var $id = null;
	
	/**
	 * portal of mail_archive datas
	 *
	 * @var string
	 */
	var $portal = null;
	
	/**
	 * mail-type of mail_archive datas
	 *
	 * @var string
	 */
	var $mailtype = null;
	
	/**
	 * category of mail_archive datas
	 *
	 * @var string
	 */
	var $category = null;
	
	/**
	 * type of mail_archive datas
	 *
	 * @var string
	 */
	var $type = null;
	
	/**
	 * server of mail_archive datas
	 *
	 * @var string ip
	 */
	var $server = null;
	
	/**
	 * memberfrom_id of mail_archive datas
	 *
	 * @var string ip
	 */
	var $memberfrom_id = null;
	
	/**
	 * mod10 of mail_archive datas
	 *
	 * @var integer
	 */
	var $mod10;
	
	/**
	 * MemberFrom stats object
	 *
	 * @var object
	 */
	var $MailMemberFromStats = null;
	
	/**
	 * Mod stats object
	 *
	 * @var object
	 */
	var $MailModStats = null;
	
	/**
	 * Send stats object
	 *
	 * @var object
	 */
	var $MailSendStats = null;
	
	/**
	 * server stats object
	 *
	 * @var object
	 */
	var $MailServerStats = null;
	
	/**
	 * type stats object
	 *
	 * @var object
	 */
	var $MailTypeStats = null;
	
	public function __construct(){
	}
	
	protected function _getMailStatsModel(){
		return new model_MailStats();
	}
	
	
	public function makeMailStats(){
		/**
		 * fetch new archive datas into object
		 */
		$this->fetchNewObjectDatas();
		if((int) $this->id > 0){
			/**
			 * create new TypeStats
			 */
			$this->makeTypeStats();
			/**
			 * create new ServerStats
			 */
			$this->makeServerStats();
			/**
			 * create new SendStats
			 */
			$this->makeSendStats();
			/**
			 * create new ModStats
			 */
			$this->makeModStats();
			/**
			 * create new MemberFromStats
			 */
			$this->makeMemberFromStats();
			/**
			 * delete archive record
			 */
			$this->deleteArchiveRecord();
		}
	}
	
	public function fetchNewObjectDatas(){
		foreach ($this as $key=>$value){
			$this->$key = null;
		}
		$this->fetchMailArchive();
		return;
	}
	
	public function fetchMailArchive($id=0){
		$tmp = $this->_getMailStatsModel()->fetchArchiveDatas($id);
		if(is_array($tmp)){
			foreach ($tmp as $key=>$value){
				$this->$key=$value;
			}
		}		
		return;
	}
	
	protected function makeMemberFromStats(){
		$this->MailMemberFromStats = new class_MailStats_MailMemberFromStats();
		$this->MailMemberFromStats->addStats($this);
	}
	
	protected function makeModStats(){
		$this->MailModStats = new class_MailStats_MailModStats();
		$this->MailModStats->addStats($this);
	}
	
	protected function makeSendStats(){
		$this->MailSendStats = new class_MailStats_MailSendStats();
		$this->MailSendStats->addStats($this);
	}
	
	protected function makeServerStats(){
		$this->MailServerStats = new class_MailStats_MailServerStats();
		$this->MailServerStats->addStats($this);
	}
	
	protected function makeTypeStats(){
		$this->MailTypeStats = new class_MailStats_MailTypeStats();
		$this->MailTypeStats->addStats($this);
	}
	
	protected function deleteArchiveRecord(){
		return $this->_getMailStatsModel()->deleteArchiveRecord($this);
	}
}


