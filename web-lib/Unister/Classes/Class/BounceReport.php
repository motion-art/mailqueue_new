<?php
class Unister_Classes_Class_BounceReport
{
    protected $_portalReports = array(
        'schuelerprofile.de' => 'tobias.ehnert@unister.de'
    );

    public function sendReportsToPortals()
    {
        $mapper = new Unister_Classes_Model_MailDeleteBounce();
        foreach ($this->_portalReports as $portal => $mail) {
            $filtered = $mapper->fetchByPortal($portal, date('Y-m-d'));
            $text = 'Mails an folgende Adressen wurden verworfen:<br><br>' . $this->_getMailTable($filtered);
            //$query = 'INSERT INTO `mail_instant` (`Subject`, `Html`, `Email`, `Mod10`) VALUES (\'Statistik\', \'' . $text . '\', \'' . $mail . '\', 1)';
            $values = array(
                'Subject' => 'Gefilterte Bounces für ' . $portal . ' vom ' . date('Y-m-d'),
                'Html' => $text,
                'Email' => $mail,
                'Mod10' => 2
            );
            $mapper->getAdapter()->insert('mail_instant', $values);
        }
    }

    protected function _getMailTable($data, &$csv = null)
    {
        $output = '<table><tr><th>Mail</th><th>Anzahl</th></tr>';
        if (!is_null($csv)) {
            $csv = 'Mail;Anzahl' . PHP_EOL;
        }

        foreach ($data as $entry) {
            if (!is_null($csv)) {
                $csv .= $entry['email'] . ';' . $entry['counts'] . PHP_EOL;
            }
            $output .= '<tr><td>' . $entry['email'] . '</td>';
            $output .= '<td>' . $entry['counts'] . '</td></tr>';
        }
        $output .= '</table>';
        return $output;
    }
}