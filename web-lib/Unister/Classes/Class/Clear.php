<?php
class Unister_Classes_Class_Clear
{
    const HOLD_BACK_TIME = 30;   // Vorhaltezeit für die Attachments in Tagen...

    public function clearAttachments()
    {
        $mapper = new Unister_Classes_Model_Clear();
        $attachments = $mapper->fetchAttachments(5000);
        // Zehn Tage alte Mails filtern...
        $now = time() - (86400 * self::HOLD_BACK_TIME);
        $delete = array();

        $min = $now;
        $max = 0;
        $minId = 0;
        $maxId = 0;

        foreach ($attachments as $attachment) {
            $time = strtotime($attachment['creationdate']);
            if ($time < $now) {
                $delete[] = $attachment['attachmentid'];
                if ($min > $time) {
                    $min = $time;
                    $minId = $attachment['attachmentid'];
                }
                if ($max <= $time) {
                    $max = $time;
                    $maxId = $attachment['attachmentid'];
                }
            }
        }
        if (count($delete) == 0) {
            $msg = 'Cron delete Attachments: ' . count($delete) . ' (' . date('c', $min) . ' id:'
                . $minId . ' - ' . date('c', $max) . ' id:' . $maxId . ')';
            Zend_Registry::get('_LOGGER')
                ->log($msg, Zend_Log::INFO);
            return;
        }
        if ($mapper->deleteAttachments($delete)) {
            $msg = 'Cron delete Attachments: ' . count($delete) . ' (' . date('c', $min) . ' id:'
                . $minId . ' - ' . date('c', $max) . ' id:' . $maxId . ')';
            Zend_Registry::get('_LOGGER')
                ->log($msg, Zend_Log::INFO);
        } else {
            Zend_Registry::get('_LOGGER')
                ->log('Cron error: ' . $msg, Zend_Log::INFO);
        }
    }

    public function clearMailDeleteBounce()
    {
        $mapper = new Unister_Classes_Model_Clear();
        $mapper->clearMailDeleteBounceTable();
    }
}
