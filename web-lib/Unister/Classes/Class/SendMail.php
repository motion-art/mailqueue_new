<?php
class Unister_Classes_Class_SendMail
{
    /**
     * Bestimmt, ob die Mails allesamt in Dateien gespeichert werden sollen, statt diese über den SMTP zu versenden.
     *
     * Bei TRUE wird der File-Transporter genutzt, sonst der SMTP-Transporter.
     * Die Mails werden in den Ordner ./temp gespeichert...
     *
     * @var bool
     */
    const DEBUG_MODE_TRANSPORTER = false;

    protected $_portalsWithBounceBlacklistDelete = array(
        'schuelerprofile.de',
        'geld.de',
        'myimmo.de',
        'boersennews.de',
        'shopping.de',
        'epartnersuche.de',
        'fluege.de',
        'auto.de',
        'vuelo24.es',
        'volo24.it',
        'vol24.fr',
        'vlucht24.nl',
        'vermi',
        'ultindip',
        'ultindi',
        'travel24.com',
        'travel24.ch',
        'travel24.at',
        'svtc',
        'reisen.preisvergleich.de',
        'reisen.de',
        'preisvergleich.de',
        'partnersuche.de',
        'news_de',
        'hotelreservierung.de',
        'hotelreservation.com',
        'holidaytest.de',
        'billigfluege.de',
        'ab-in-den-urlaub.de',
        'itcontrolling.unister-gmbh.de',
        'carsb2b',
        'dsl.de',
        'holidayreporter.com',
    );

    protected $_invitationspammerSp = array(
        'robert_nguyenscb@qatar.io',
        'mcbagent2011@gmail.com',
        'jamilah.ashirinchambers@live.com.my',
    );

    /**
     * Ist die From-Adresse einer der folgenden Mails, so wird der envelope sender auf diese Adresse gesetzt.
     * Achtung: damit wird für diese Adresse das Bounce-Handling umgangen...
     * @var array
     */
    protected $_envelopeFrom = array(
        'noreply@travel24.com' => 'service@travel24.com',
        'noreply@travel24.nl' => 'service@travel24.nl',
        'noreply@travel24.co.uk' =>'service@travel24.co.uk',
        'noreply@travel24.ch' =>'service@travel24.ch',
        'noreply@travel24.at' =>'service@travel24.at',
        'noreply@ab-in-den-urlaub.de' => 'service@ab-in-den-urlaub.de',
        'KeineAntwortadresse@ab-in-den-urlaub.de' => 'service@ab-in-den-urlaub.de',
        'noreply@ab-in-den-urlaub.ch' => 'service@ab-in-den-urlaub.ch',
        'KeineAntwortadresse@ab-in-den-urlaub.ch' => 'service@ab-in-den-urlaub.ch',
        'noreply@reisen.de' => 'service@reisen.de',
        'KeineAntwortadresse@reisen.de' => 'service@reisen.de',
    );

    /**
     * Portale, welche einen envelope-sender mit der Form: [type]_[category]+[alias]=[domain]@bounces.[portal]
     *
     * @var array
     */
    protected $_bouncePortals = array(
        'auto.de',
        'geld.de',
        'schuelerprofile.de',
        'news.de',
        'news_de' => 'news.de',
        'boersennews.de',
        'ab-in-den-urlaub.ch',
        'ab-in-den-urlaub.de',
        'ab-in-den-urlaub.at',
        'epartnersuche.de',
        'hotelreservation.com',
        'myimmo.de',
        'partnersuche.de',
        'preisvergleich.de',
        'reisen.preisvergleich.de' => 'preisvergleich.de',
        'reisen.de',
        'shopping.de',
        'travel24.ch',
        'travel24.nl',
        'classic.unister.de',
        'fluege.de',
        'hotelreservierung.de',
        'fbApp.kostenlos.reisen.de',
        'unister.de',
        'unister-gmbh.de'
    );

    /**
     * E-Mail-Adressen, welche trotz Blacklisting nicht gefiltert werden
     *
     * @var array
     */
    protected $_bounceWhitelistAddresses = array(
        'service@ab-in-den-urlaub.de',
        'service@ab-in-den-urlaub.ch',
        'service@reisen.de',
        'reisenservice@reisen.de',
        'rheinhotel@netcologne.de',
        'outbound@portalservice.de',
        'reisen-dev@unister.de',
        'reisen@unister-travel.de'
    );

    /**
     * Ist die From-Adresse einer der folgenden Mails, so wird der Transport über den zugewiesenen
     * Mailserver festgesetzt
     *
     * @var array
     */
    protected $_routeBasedEnvelopeFrom = array(
        'KeineAntwortadresse@ab-in-den-urlaub.de' => 'srv4'
    );

    /**
     * Blacklist für die Sperrung der Domains
     * @var array
     */
    protected $_domainBlacklist = array('mmms.de');

    /**
     * mail object
     *
     * @var Zend_Mail
     */
    var $objMail;

    /**
     * contains portal values, like mail account, mailserver-ip, default reply and from
     *
     * @var array()
     */
    protected $_portalConfig = array();

    /**
     * contains all mailserver-ips
     *
     * @var array()
     */
    protected $_mailServer = array();

    /**
     * contains domains, which are blocked
     *
     * @var array
     */
    protected $_deniedPortal = array();

    /**
     * contains all mails, which are send
     *
     * @var array
     */
    protected $_mailDatas = array();

    /**
     * flag, to clean mailqueue tables
     *
     * @var boolean
     */
    protected $_cronSending = false;

    /**
     * return sendmail-model
     *
     */
    protected function _getSendMailModel()
    {
        return new model_SendMail();
    }

    /**
     * config function, which are using by sendmail - cron
     *
     * set the mailserver for every portal allowing for server-using weight
     *
     * @param array $portalvalues
     */
    protected function _setCronMailServer(&$portalvalues)
    {
        /**
         * get server configs
         */
        $this->_mailServer = Zend_Registry::get('_CONFIG_PORTALE')->mailservers->toArray();
        $weightSum = 0;
        foreach ($portalvalues['srv'] as $weight) {
            $weightSum = $weightSum + $weight;
        }
        if ($weightSum > 0) {
            while (1==1) {
                $usedWeight = rand(1, $weightSum);
                $weightTill = 0;
                foreach ($portalvalues['srv'] as $servername => $serverweight) {
                    $weightTill = $serverweight + $weightTill;
                    if ($weightTill >= $usedWeight) {
                        $portalvalues['srv'] = $this->_mailServer[$servername];
                        return;
                    }
                }
            }
        } else {
            $this->setDeniedPortal($portalvalues['name']);
        }
        return;
    }

    /**
     * config function, which are using bei sendmail - cron
     *
     * set portal config for sendmail cron
     *
     */
    public function _makeCronPortalConfig()
    {
        Zend_Registry::set( '_CONFIG_PORTALE', new Zend_Config_Ini( CONFIG_DIR . 'portale.ini', null, true ) );
        /**
         * get portal configs
         */
        $portale = Zend_Registry::get('_CONFIG_PORTALE')->portale->toArray();
        #print_r($portale);
        #exit;
        foreach ($portale as &$portalvalues) {
            $this->_setCronMailServer($portalvalues);

            $usesrv = null;
            if (array_key_exists('usesrv', $portalvalues)) {
                $usesrv = $portalvalues['usesrv'];
            }

            $this->_setPortalConfig(
                $portalvalues['name'],
                $portalvalues['username'],
                $portalvalues['password'],
                $portalvalues['from'],
                $portalvalues['reply'],
                $portalvalues['srv'],
                $usesrv
            );
        }
        unset($portale);
    }

    /**
     * mark a domain as blocked
     *
     * @param string $name
     */
    public function setDeniedPortal($name)
    {
        $this->_deniedPortal[] = $name;
        return;
    }

    /**
     * set connection datas
     *
     * @param string $portalname (name of portal)
     * @param string $username
     * @param string $password
     * @param string $from (general email-header value, can change, if is set in mail_array)
     * @param string $reply (general email-header value, can change, if is set in mail_array)
     * @param string $serverip
     * @param string $usesrv
     */
    public function _setPortalConfig(&$portalname,&$username,&$password,&$from,&$reply,&$serverip, $usesrv)
    {
        $this->_portalConfig[$portalname] = array(
            'username' => $username,
            'password' => $password,
            'from' => $from,
            'reply' => $reply,
            'srv' => $serverip,
            'usesrv' => $usesrv
        );

        return;
    }

    /**
     * set array with maildatas
     *
     * @param array $maildatas
     */
    public function _setMailDatas($maildatas)
    {
        $this->_mailDatas = $maildatas;
    }

    /**
     * function, which is called by the sendmail-cron script
     * send all mails from the mail-queue
     *
     * @param integer $Mod
     */
    public function runSendMailCron($mod)
    {
        /**
         * activates delete and update jobs in mailqueue db
         */
        $this->_cronSending = true;
        /**
         * how many calls of the send script till the cron is restared
         */
        for ($i = 0; $i < 100; $i++) {
            /**
             * fetch mails
             */
            $this->_setMailDatas($this->_getSendMailModel()->fetchMailsForCron($mod, $this->_deniedPortal));
            /**
             * start sending
             */
            $this->_runMail();
            sleep(1);
        }
    }

    /**
     * fetch the mails and create for every result a mail-object for send via smtp
     */
    protected function _runMail()
    {
        if (count($this->_mailDatas) >= 1) {
            //ermittelt Whitelist für den Filter
            if (Zend_Registry::get('_CONFIG')->filter->filterExternalRecipients) {
                $whitelist = $this->_getPortalNames();
                $whitelist[] = 'unister.de';
                $whitelist[] = 'unister-gmbh.de';
                $whitelist[] = 'unister-travel.de';
            }

            /**
             * array for all mailids, which are handled.
             * it's very important, that 'markToDelete' is the last key of this array
             */
            $resultJobs = array(
                'markToBlacklistInsert' => array(),
                'markToArchive' => array(),
                'markToTryUpdate' => array(),
                'markToDelete' => array(),
                'markToBounceDelete' => array(),
            );
            foreach ($this->_mailDatas as $value) {
                //filtert die Empfänger bei externen Mail-Adressen
                if (Zend_Registry::get('_CONFIG')->filter->filterExternalRecipients) {
                    $this->_filterRecipientsByDomain($value, 'cc', $whitelist);
                    $this->_filterRecipientsByDomain($value, 'bcc', $whitelist);
                    $this->_filterRecipientsByDomain($value, 'email', $whitelist);
                    if (empty($value['cc']) && empty($value['bcc']) && empty($value['email'])) {
                        $resultJobs['markToErrorCheck'][$value['mailtyp']][] = $value['mailid'];
                        continue;
                    }
                }

                //filtert die Empfänger anhand von gesperrten Domains
                if (Zend_Registry::get('_CONFIG')->filter->filterLockedRecipients) {
                    $this->_filterRecipientsByDomain($value, 'cc', null, $this->_domainBlacklist);
                    $this->_filterRecipientsByDomain($value, 'bcc', null, $this->_domainBlacklist);
                    $this->_filterRecipientsByDomain($value, 'email', null, $this->_domainBlacklist);
                    if (empty($value['cc']) && empty($value['bcc']) && empty($value['email'])) {
                        $resultJobs['markToErrorCheck'][$value['mailtyp']][] = $value['mailid'];
                        continue;
                    }
                }

                //filtert den Empfänger bei nicht existierendem Host-Name
                if (Zend_Registry::get('_CONFIG')->filter->filterHostName && !empty($value['email'])) {
                    $tmpMails = explode(';', $value['email']);
                    foreach ($tmpMails as $tmpMail) {
                        $mailParts = $this->_getMailParts($tmpMail);
                        if (!getmxrr($mailParts[1], $mxhosts)) {
                            $resultJobs['markToErrorCheck'][$value['mailtyp']][] = $value['mailid'];
                            $logMasg = 'E-Mail wurde nicht versendet. Grund: Hostname der E-Mail existiert nicht | E-Mail: '
                                . $value['email']  . ' | Subject: ' . $value['subject'] . ' | Portal: '
                                . $value['portal'] . ' | Datum: ' . $value['inserted'];
                            Zend_Registry::get('_LOGGER')->log($logMasg, Zend_Log::INFO);
                            continue;
                        }
                    }
                }

                $valid = true;
                if (in_array($value['portal'], $this->_portalsWithBounceBlacklistDelete)) {
                    $valid = $this->_checkAndFilterRecipients($value);
                }

                if (($value['portal'] == 'schuelerprofile.de' && $value['category'] == 'INVITATION'
                    && in_array($value['replyto'], $this->_invitationspammerSp))
//                    || $value['blackdomain'] == 0
                    || $valid == false
                ) {
                    /**
                     * if mail-address or domain is in blacklist
                     */
                    #echo "DEBUG: blacklisted: ".$value['email']."\n";
                    $resultJobs['markToBounceDelete'][$value['mailtyp']][] = $value['mailid'];
                    continue;
                }
                /**
                 * instance a mail-object, with the zend-mail class
                 */
                $this->objMail = new Zend_Mail($value['charset']);
                $this->objMail->addHeader('MIME-Version', '1.0');
                #echo "set right charset"."\n";
                /**
                 * if not utf-8 charset, decode
                 */
                if ($value['charset'] != 'utf-8') {
                    $value['html'] = utf8_decode($value['html']);
                    $value['text'] = utf8_decode($value['text']);
                    $value['fromname'] = utf8_decode($value['fromname']);
                }
                #echo "set html"."\n";
                /**
                 * set the mail-object attributes
                 * html-part
                 */
                if (strlen($value['html']) > 10) {
                    $this->objMail->setBodyHtml($value['html']);
                }
                #echo "set text"."\n";
                /**
                 * text-part
                 */
                $this->objMail->setBodyText($value['text']);
                #echo "set subject"."\n";
                /**
                 * subject
                 */
                #if(strlen($value['subject'])>200){
                #    $value['subject'] =  substr($value['subject'],0,195).'...';
                #}
                #$this->objMail->setSubject($value['subject']);

               //TODO: unbedingt refaktoieren, hier wurde sogar das ZF angepasst ....
               //am besten neu schreiben
               $regexp = '/.*\?Q\?(.*)\?=/';
               preg_match($regexp, $value['subject'], $results);

               //wenn subject schon rfc2047 schon kodiert in der DB liegen

               if (is_array($results) && count($results) > 1) {
                  $this->objMail->setSubject($value['subject']);
               } else {
                  $this->objMail->setSubject(mb_encode_mimeheader(utf8_decode($value['subject'])));
               }

                //$this->objMail->setSubjectNew(mb_encode_mimeheader(utf8_decode($value['subject'])));
                #echo "set acceptor"."\n";
                /**
                 * acceptor
                 */
                $tmp = explode(';', $value['email']);
                foreach ($tmp as $emailto) {
                    $this->objMail->addTo($emailto);
                }
                #echo "set sender"."\n";
                /**
                 * sender
                 */
                $fromname = '';
                if (strlen($value['fromname']) > 0) {
                    // macht bei codierten namen blödsinn draus -> weg
                    // =?utf-8?Q?Mar=C3=ADa=20Lorca=20Sola=20-=20vuelo24.es?=
                    //if (strlen($value['fromname']) > 40) {
                    //    $value['fromname'] =  substr($value['fromname'], 0, 40) . '...';
                    //}
                    $fromname = $value['fromname'];
                }

                if (strlen($value['fromemail']) > 0) {
                    $fromemail = $value['fromemail'];
                } else {
                    $fromemail = $this->_portalConfig[$value['portal']]['from'];
                }
                $this->objMail->setFrom($fromemail, $fromname);
                #echo "set reply"."\n";
                /**
                 * reply
                 */
                if (strlen($value['replyto'])>0) {
                    $replyto = $value['replyto'];
                } else {
                    $replyto = $this->_portalConfig[$value['portal']]['reply'];
                }
                $this->objMail->setReplyTo($replyto);
                //$this->objMail->addHeader('Reply-To', $replyto);

                if (count($tmp)==1) {
                    // envelope-from for bounce system
                    $portal = str_replace('_', '.', $value['portal']);
                    // Doppelte Anf�hrungszeichen entfernen...
                    // Achtung Beta: sollten jetzt wiederum andere Sonderzeichen im local-part
                    // der Mail erscheinen, könnte es erneut zu Problemen kommen
                    // Für Sonderzeichen siehe: http://tools.ietf.org/html/rfc2822
                    // hier sollte Punycode eingeführt werden....
                    $mailAddress = str_replace('"', '', $value['email']);
                    if ((in_array($portal, $this->_bouncePortals)
                        || array_key_exists($portal, $this->_bouncePortals))
                        && !array_key_exists($value['fromemail'], $this->_envelopeFrom)
                    ) {
                        // Type und category mit 'undef' ersetzen falls leerer string...
                        $_type = ($value['type'] == '') ? 'undef' : $value['type'];
                        $_category = ($value['category'] == '') ? 'undef' : $value['category'];

                        $_category = preg_replace('/[^0-9a-zA-Z]/', '', $_category);
                        $_type = preg_replace('/[^0-9a-zA-Z]/', '', $_type);

                        $bouncePortal = $portal;
                        if (isset($this->_bouncePortals[$portal])) {
                            $bouncePortal = $this->_bouncePortals[$portal];
                        }

                        $returnPath = $_type . '_' . $_category
                            . '+' . str_replace('@', '=', $mailAddress)
                            . '@bounces.' . $bouncePortal;
                        $this->objMail->setReturnPath($returnPath);
                    }
                    if (array_key_exists($value['fromemail'], $this->_envelopeFrom)) {
                        $this->objMail->setReturnPath($this->_envelopeFrom[$value['fromemail']]);
                    }

                }

                /**
                 * CC
                 */
                if (strlen($value['cc']) > 0) {
                    $tmp = explode(';', $value['cc']);
                    foreach ($tmp as $cc) {
                        $this->objMail->addCc($cc);
                    }
                }
                #echo "set bcc"."\n";
                /**
                 * BCC
                 */
                if (strlen($value['bcc']) > 0) {
                    $tmp = explode(';', $value['bcc']);
                    foreach ($tmp as $bcc) {
                        $this->objMail->addBcc($bcc);
                    }
                }
                #echo "set attachments"."\n";
                /**
                 * attachments
                 */
                if ($value['attachment'] == 1) {
                    $this->objMail->setType(Zend_Mime::MULTIPART_MIXED);
                    $attachments = $this->_getSendMailModel()
                        ->getMailAttachments($value['mailid'], $value['mailtyp']);
                    foreach ($attachments as $key => $attachment) {
                        //prüft ob inhalt base64 kodiert ist
                        if ((bool) preg_match('/^[a-zA-Z0-9\/\r\n+]*={0,2}$/', $attachment['attachmentcontent'])) {
                            $attachment['attachmentcontent'] = base64_decode($attachment['attachmentcontent']);
                        }

                        $at[$key] = $this->objMail->createAttachment($attachment['attachmentcontent']);
                        $at[$key]->type        = $attachment['attachmenttype'];
                        $at[$key]->filename    = $attachment['attachmentfile'];
                        // TEH(2011-05-02) Content-Transfer-Encoding fuer ICS Anhaenge bei Auto.de
                        if (($attachment['attachmenttype'] == 'text/calendar') && ($value['type'] == 'CALENDAR')) {
                            $at[$key]->encoding = Zend_Mime::ENCODING_8BIT;
                            $at[$key]->type     = $attachment['attachmenttype'];
                        }
                    }

                    // TEH(2011-05-02) Für ICS Anhänge bei Auto.de:
                    // Multipart/Mixed als content-type notwendig, sonst wird der Anhang nicht als Termin erkannt...
                    if ($value['type'] == 'CALENDAR' || $value['type'] == 'MIXED') {
                        $this->objMail->setType(Zend_Mime::MULTIPART_MIXED);
                    }
                }

                // Sonderfall ULT Buchungsbestätigungen...
                if (($value['type'] == 'AFFIRMATION' && $value['category'] == 'BOOKING')
                ) {
                    $this->objMail->setType(Zend_Mime::MULTIPART_RELATED);
                }

                //Workaround, weil in der Cloud das Senden sonst nicht funktioniert
                $smtpname = null;
                if (Zend_Registry::get('_CONFIG')->smtp->name > '') {
                    $smtpname = Zend_Registry::get('_CONFIG')->smtp->name;
                }

                /**
                 * instance a transport object of type smtp
                 * a high priority mail used the extra mailserver,
                 * which is named HighPrio in the config.ini. the other mails use Srv(n)
                 * the mail-servers were used by the mod10 value of the email
                 */
                if ($value['prio'] != 1) {
                    $useServer = $this->_portalConfig[$value['portal']]['srv'];

                    if (self::DEBUG_MODE_TRANSPORTER) {
                        // TEH 2012-04-04 achtung: nur zum debuggen...
                        $this->transport = new Zend_Mail_Transport_File(
                            array('path' => '/data/t.ehnert/Projects/mailqueue_new/temp')
                        );
                    } else {
                        if (RUN_MODE == 'staging') {
                            $host = $this->_mailServer['staging'];
                        } else if (array_key_exists($value['fromemail'], $this->_routeBasedEnvelopeFrom)) {
                            $host = $this->_mailServer[$this->_routeBasedEnvelopeFrom[$value['fromemail']]];
                        } else if (isset($this->_portalConfig[$value['portal']]['usesrv'])) {
                            $host = $this->_mailServer[$this->_portalConfig[$value['portal']]['usesrv']];
                        } else {
                            $host = $this->_portalConfig[$value['portal']]['srv'];
                        }
                        $this->transport = new Zend_Mail_Transport_Smtp(
                            $host,
                            array(
                                'auth' => false,
                                'username' => $this->_portalConfig[$value['portal']]['username'],
                                'password' => $this->_portalConfig[$value['portal']]['password'],
                                'name' => $smtpname
                            )
                        );
                    }
                } else {
                    $useServer = $this->_mailServer['highprio'];

                    if (self::DEBUG_MODE_TRANSPORTER) {
                        // TEH 2012-04-04 achtung: nur zum debuggen...
                        $this->transport = new Zend_Mail_Transport_File(
                            array('path' => '/data/t.ehnert/Projects/mailqueue_new/temp')
                        );
                    } else {
                        if (RUN_MODE == 'staging') {
                            $host = $this->_mailServer['staging'];
                        } else if (array_key_exists($value['fromemail'], $this->_routeBasedEnvelopeFrom)) {
                            $host = $this->_mailServer[$this->_routeBasedEnvelopeFrom[$value['fromemail']]];
                        } else if (isset($this->_portalConfig[$value['portal']]['usesrv'])) {
                            $host = $this->_mailServer[$this->_portalConfig[$value['portal']]['usesrv']];
                        } else {
                            $host = $this->_mailServer['highprio'];
                        }
                        $this->transport = new Zend_Mail_Transport_Smtp(
                            $host,
                            array(
                                'auth' => false,
                                'username' => $this->_portalConfig[$value['portal']]['username'],
                                'password' => $this->_portalConfig[$value['portal']]['password'],
                                'name' => $smtpname
                            )
                        );
                    }
                }

                /**
                 * try to send the email
                 * on successful sending save mailid for deleting and tracking
                 *
                 * on error
                 * proof email-address
                 * if valid, update try
                 * if invalid, save mailid for deleting and blacklisting
                 */
                try {
                    $this->objMail->send($this->transport);
                    $resultJobs['markToArchive'][$value['mailtyp']][$useServer][] = $value['mailid'];
                    $resultJobs['markToDelete'][$value['mailtyp']][] = $value['mailid'];
                    //echo PHP_EOL . 'sendmail done for:' . PHP_EOL;
                    //print_r($this->objMail);
                } catch (Exception $e) {
                    //echo PHP_EOL . 'sendmail failed with error: ' . $e->getMessage() . ' for:' . PHP_EOL;
                    //print_r($this->objMail);

                    if ($value['try'] >= 100) {
                        $resultJobs['markToErrorCheck'][$value['mailtyp']][] = $value['mailid'];
                        $logMsg = 'E-Mail wurde nicht versendet. Grund: ' . $e->getMessage() . ' | E-Mail: '
                            . $value['email']  . ' | Subject: ' . $value['subject'] . ' | Portal: '
                            . $value['portal'] . ' | Datum: ' . $value['inserted'];
                        Zend_Registry::get('_LOGGER')->log($logMsg, Zend_Log::INFO);
                    } else {
                        $resultJobs['markToTryUpdate'][$value['mailtyp']][] = $value['mailid'];
                        $resultJobs['markToErrorCheck'][$value['mailtyp']][] = $value['mailid'];
                    }
                }
                unset ($this->objMail);
            }
            /**
             * all mails were handled
             * now the results were considered
             */
            if ($this->_cronSending === true) {
                $this->_getSendMailModel()->makeAfterSendingJobs($resultJobs);
            }
        }
        unset($this->_mailDatas);
    }

    protected function _checkAndFilterRecipients(&$mail)
    {
        $cc = array();
        $bcc = array();
        $to = array();

        if (!is_null($mail['cc'])) {
            $cc = explode(';', $mail['cc']);
            $cc = array_flip($cc);
        }
        if (!is_null($mail['bcc'])) {
            $bcc = explode(';', $mail['bcc']);
            $bcc = array_flip($bcc);
        }
        if (!is_null($mail['email'])) {
            $to = explode(';', $mail['email']);
            $to = array_flip($to);
        }

        $address = array_merge($cc, $bcc, $to);
        $address = array_keys($address);

        //Bugfix - niemals die folgenden Adressen entfernen, selbst wenn sie auf der Blacklist stehen
        $address = array_diff($address, $this->_bounceWhitelistAddresses);

        $mapper = new Unister_Classes_Model_BounceBlacklist();
        $blacklisted = $mapper->fetchByAddress($address);
        if (count($blacklisted) != null) {
            foreach ($blacklisted as $entry) {
                if (isset($cc[$entry['email']])) {
                    unset($cc[$entry['email']]);
                }
                if (isset($bcc[$entry['email']])) {
                    unset($bcc[$entry['email']]);
                }
                if (isset($to[$entry['email']])) {
                    unset($to[$entry['email']]);
                }
            }
            $sql = 'INSERT INTO `tracker_try_bounce` (`date`, `portal`, `countMails`) VALUES (\''
                . date('Y-m-d') . '\', \'' . $mail['portal']
                . '\', 1) ON DUPLICATE KEY UPDATE `countMails`=`countMails`+1;';
            $mapper->getAdapter()->query($sql);
        }
        if (count($to) == 0) {
            // kein To-Empfänger mehr vorhanden, daher ist die Mail ungültig...
            return false;
        }
        if (!is_null($cc) && count($cc)) {
            $mail['cc'] = implode(';', array_keys($cc));
        }
        if (!is_null($bcc) && count($bcc)) {
            $mail['bcc'] = implode(';', array_keys($bcc));
        }
        if (!is_null($to) && count($to)) {
            $mail['email'] = implode(';', array_keys($to));
        }
        // Mail ist noch gültig, kann also versendet werden...
        return true;
    }


    /**
     * filtert die Empfängern, falls die Domain den Listen entsprechen
     * @param array $mail
     * @param string $recipientType
     * @param array $whitelist -> Domain muss darin enthalten sein
     * @param array $blacklist -> Domain darf darin nicht enthalten sein
     */
    protected function _filterRecipientsByDomain(&$mail, $recipientType, $whitelist = null, $blacklist = null)
    {
        $recipient = array();
        if (!is_null($mail[$recipientType])) {
            $recipient = explode(';', $mail[$recipientType]);
            $recipient = array_flip($recipient);
        }

        foreach ($recipient as $key => $entry) {
            $recipientParts = $this->_getMailParts($key);
            if (isset($whitelist)) {
                if (!in_array($recipientParts[1], $whitelist)) {
                    unset($recipient[$key]);
                }
            } else if (isset($blacklist)) {
                if (in_array($recipientParts[1], $blacklist)) {
                    unset($recipient[$key]);
                }
            }
        }

        $mail[$recipientType] = implode(';', array_keys($recipient));
    }

    /**
     * extrahiert aus einer E-Mail-Adresse die einzelnen Teile
     * @param string $email
     * @return array
     */
    protected function _getMailParts($email)
    {
        preg_match('/([^<>"\']+)@([^<>"\']+)/', $email, $matches);
        if (!empty($matches)) {
            array_shift($matches);
        }

        return $matches;
    }

    /**
     * liefert alle vorhandenen Portalnamen aus der Config
     * @return array
     */
    protected function _getPortalNames()
    {
        $portalConfig = Zend_Registry::get( '_CONFIG_PORTALE');

        $portalNames = array();
        foreach ($portalConfig->portale as $portal) {
            $portalNames[] = $portal->name;
        }
        array_unique($portalNames);

        return $portalNames;
    }
}
