<?php
/**
 * Filter für Punycode-Konvertierung
 *
 * @category   Mailqueue
 * @package    Mailqueue_Class
 * @subpackage Mailqueue_Class_Filter
 * @copyright  Copyright (c) 2012 Unister GmbH
 * @author     Unister GmbH <teamleitung-dev@unister-gmbh.de>
 * @author     Tobias Ehnert <tobias.ehnert@unister-gmbh.de>
 */
class Unister_Classes_Class_Filter_Idna implements Zend_Filter_Interface
{
    /**
     * Filter konvertiert $value mit Punycode.
     *
     * Zum Konvertieren wird die Net_IDNA2 Library genutzt.
     *
     * @see Zend_Filter_Interface::filter()
     * @return string
     */
    public function filter($value)
    {
        $idna = new Net_IDNA2();
        $encode = $idna->encode((string) $value);
        return $encode;
    }
}