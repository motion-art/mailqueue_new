<?php

class Unister_Classes_Class_MailStats_MailTypeStats {
	
	/**
	 * portal of mail_archive datas
	 *
	 * @var string
	 */
	var $portal = null;
	
	/**
	 * mailtype of mail_archive datas
	 *
	 * @var integer
	 */
	var $mailtype = null;
	
	public function __construct(){
	}
	
	protected function _getMailTypeStatsModel(){
		return new model_MailStats_MailTypeStats();
	}
	
	/**
	 * insert a new stats-record
	 *
	 * @param object $datas
	 */
	public function addStats(&$datas){
		foreach($this as $key=>$value){
			$this->$key = $datas->$key;
		}
		return $this->_getMailTypeStatsModel()->addStats($this);
	}
}


