<?php

class Unister_Classes_Class_MailStats_MailModStats {
	
	/**
	 * portal of mail_archive datas
	 *
	 * @var string
	 */
	var $portal = null;
	
	/**
	 * mod10 of mail_archive datas
	 *
	 * @var integer
	 */
	var $mod10 = null;
	
	public function __construct(){
	}
	
	protected function _getMailModStatsModelModel(){
		return new model_MailStats_MailModStats();
	}
	
	/**
	 * insert a new stats-record
	 *
	 * @param object $datas
	 */
	public function addStats(&$datas){
		foreach($this as $key=>$value){
			$this->$key = $datas->$key;
		}
		return $this->_getMailModStatsModelModel()->addStats($this);
	}
}


