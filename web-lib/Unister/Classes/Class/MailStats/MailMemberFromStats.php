<?php

class Unister_Classes_Class_MailStats_MailMemberFromStats {
	
	/**
	 * portal of mail_archive datas
	 *
	 * @var string
	 */
	var $portal = null;
	
	/**
	 * memberfrom_id of mail_archive datas
	 *
	 * @var string ip
	 */
	var $memberfrom_id = null;
	
	public function __construct(){
	}
	
	protected function _getMailMemberFromStatsModel(){
		return new model_MailStats_MailMemberFromStats();
	}
	
	/**
	 * insert a new stats-record
	 *
	 * @param object $datas
	 */
	public function addStats(&$datas){
		foreach($this as $key=>$value){
			$this->$key = $datas->$key;
		}
		return $this->_getMailMemberFromStatsModel()->addStats($this);
	}
}


