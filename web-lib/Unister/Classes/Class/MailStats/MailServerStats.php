<?php

class Unister_Classes_Class_MailStats_MailServerStats {
	
	/**
	 * portal of mail_archive datas
	 *
	 * @var string
	 */
	var $portal = null;
	
	/**
	 * server of mail_archive datas
	 *
	 * @var integer
	 */
	var $server = null;
	
	public function __construct(){
	}
	
	protected function _getMailServerStatsModel(){
		return new model_MailStats_MailServerStats();
	}
	
	/**
	 * insert a new stats-record
	 *
	 * @param object $datas
	 */
	public function addStats(&$datas){
		foreach($this as $key=>$value){
			$this->$key = $datas->$key;
		}
		return $this->_getMailServerStatsModel()->addStats($this);
	}
}


