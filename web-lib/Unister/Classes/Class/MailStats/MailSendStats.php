<?php

class Unister_Classes_Class_MailStats_MailSendStats {
	
	/**
	 * portal of mail_archive datas
	 *
	 * @var string
	 */
	var $portal = null;
	
	/**
	 * category of mail_archive datas
	 *
	 * @var string
	 */
	var $category = null;
	
	/**
	 * type of mail_archive datas
	 *
	 * @var string
	 */
	var $type = null;
	
	public function __construct(){
	}
	
	protected function _getMailSendStatsModelModel(){
		return new model_MailStats_MailSendStats();
	}
	
	/**
	 * insert a new stats-record
	 *
	 * @param object $datas
	 */
	public function addStats(&$datas){
		foreach($this as $key=>$value){
			$this->$key = $datas->$key;
		}
		return $this->_getMailSendStatsModelModel()->addStats($this);
	}
}


