<?php


class Unister_Classes_Model_MailQueue {
	/**
	 * DB object
	 *
	 * @var object
	 */
	protected $db;
	
	public function __construct(){
		/**
		 * instance DB Object
		 */
		$this->db = Zend_Registry::get('_DB');
	}
	
	/**
	 * create data array to insert a mail
	 *
	 * @param object $newMailQueueObject
	 * @return array
	 */
	protected function _getMailQueueInsertArray(&$newMailQueueObject){
		return array(
						'table'			=> 'mail_'.$newMailQueueObject->MailType,
						'fields'		=> array(
													'MemberTo_Id'		=> $newMailQueueObject->MemberTo_Id,
													'MemberFrom_Id'		=> $newMailQueueObject->MemberFrom_Id,
													'Category'			=> $newMailQueueObject->Category,
													'Type'				=> $newMailQueueObject->Type,
													'Subject'			=> $newMailQueueObject->Subject,
													'Text'				=> $newMailQueueObject->Text,
													'Html'				=> $newMailQueueObject->Html,
													'Email'				=> $newMailQueueObject->Email,
													'FromName'			=> $newMailQueueObject->FromName,
													'FromEmail'			=> $newMailQueueObject->FromEmail,
													'ReplyTo'			=> $newMailQueueObject->ReplyTo,
													'CC'				=> implode(";",$newMailQueueObject->CC),
													'BCC'				=> implode(";",$newMailQueueObject->BCC),
													'Prio'				=> $newMailQueueObject->Prio,
													'Portal'			=> $newMailQueueObject->Portal,
													'Mod10'				=> rand(0,9),
												),
					);
	}
	
	/**
	 * insert a mail in mailqueue
	 *
	 * @param object $newMailQueueObject
	 * @return boolean
	 */
	public function MailQueueInsert(&$newMailQueueObject){
		$mail_insert_datas = $this->_getMailQueueInsertArray($newMailQueueObject);
		/**
		 * mail contains attachment(s) 
		 */
		if(is_array($newMailQueueObject->Attachment) && count($newMailQueueObject->Attachment)>0){
			$mail_insert_datas['fields']['Attachment'] = 1;
		}
		try {
			$this->db->beginTransaction();
			$this->db->insert($mail_insert_datas['table'],$mail_insert_datas['fields']);
			/**
			 * insert mail attachment(s)
			 */
			if($mail_insert_datas['fields']['Attachment'] == 1){
				$mailid = $this->db->lastInsertId();
				foreach($newMailQueueObject->Attachment as $attachmentid){
					$mail_attachments_insert[] = "(".$attachmentid.",".$mailid.")";
				}
				$mail_attachments_insert = "INSERT INTO attachments_mail_".$newMailQueueObject->MailType." VALUES \n".implode(",",$mail_attachments_insert);
			}
			$this->db->query($mail_attachments_insert);
			$this->db->commit();
			return true;
		} catch( Exception $e  ) {
			$this->db->rollBack();
			Zend_Registry::get('_LOGGER')->log('File:'.$e->getFile().' Line:'.$e->getLine().' '.$e->getMessage().$sql, Zend_Log::CRIT);
		}
		return 'MailQueue Insert failed';
	}
	
	/**
	 * get data array to insert attachment
	 *
	 * @param array $Attachment
	 * @return array
	 */
	protected function _getMailAttachmentInsertArray(&$Attachment){
		return array(
						'table'			=> 'attachments',
						'fields'		=> array(
													'AttachmentContent'		=> $Attachment['AttachmentContent'],
													'AttachmentType'		=> $Attachment['AttachmentType'],
													'AttachmentFile'		=> $Attachment['AttachmentFile'],
												),
					);
	}
	
	/**
	 * insert a mail attachment
	 *
	 * @param array $Attachments
	 * @return array
	 */
	public function MailAttachmentInsert(&$Attachments){
		$return = array();
		$this->db->beginTransaction();
		foreach($Attachments as &$att){
			$insert_datas = $this->_getMailAttachmentInsertArray($att);
			try {
				$this->db->insert($insert_datas['table'],$insert_datas['fields']);
				$return[]=$this->db->lastInsertId();
			} catch( Exception $e  ) {
				$this->db->rollBack();
				Zend_Registry::get('_LOGGER')->log('File:'.$e->getFile().' Line:'.$e->getLine().' '.$e->getMessage().$sql, Zend_Log::CRIT);
				return false;
			}		
		}
		$this->db->commit();
		return $return;
	}
}


