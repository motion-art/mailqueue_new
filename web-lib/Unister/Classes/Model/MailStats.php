<?php

class Unister_Classes_Model_MailStats
{
	
	
	
	
	
	public function __construct(){
		/**
		 * instance DB Object
		 */
		$this->db = Zend_Registry::get('_DB');
	}
	
	
	public function fetchArchiveDatas($archiveId=0){
		$_where = "";
		if($archiveId > 0){
			$_where = " WHERE Id = $archiveId ";
		}
		$sql = 	"
					SELECT
						Id,
						Portal,
						MailType,
						Category,
						Type,
						Server,
						MemberFrom_Id,
						Mod10
					FROM
						tracker_creation_mail_datas
					$_where
					ORDER BY
						Id ASC
					LIMIT 1
				";
		try {
				return $this->db->fetchRow($sql);
			} catch( Exception $e  ) {
				Zend_Registry::get('_LOGGER')->log('File:'.$e->getFile().' Line:'.$e->getLine().' '.$e->getMessage().$sql, Zend_Log::CRIT);
			}
		return false;
	}
	
	public function deleteArchiveRecord(&$ArchivObject){
		try {
				$this->db->query("DELETE FROM tracker_creation_mail_datas WHERE Id = ".$ArchivObject->id);
			} catch( Exception $e  ) {
				Zend_Registry::get('_LOGGER')->log('File:'.$e->getFile().' Line:'.$e->getLine().' '.$e->getMessage().$sql, Zend_Log::CRIT);
			}
		return false;
	}
}
