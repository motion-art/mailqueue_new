<?php

class Unister_Classes_Model_BounceBlacklist
{
    /**
     * @var unknown_type
     */
    protected $_db = null;

    public function __construct()
    {
        $this->_db = Zend_Registry::get('_DB');
    }

    public function getAdapter()
    {
        return $this->_db;
    }

    public function fetchByAddress($address)
    {
        $select = new Zend_Db_Select($this->_db);
        $select->from(array('bb' => 'bounce_blacklist'))
               ->where('email IN (?)', $address)
               ->where('UNIX_TIMESTAMP(changed) >= ?', time() - 1209600) // Nur Adressen abfragen, die innerhalb der letzten zwei Wochen gebounct haben...
               ->where('inc >= ?', 10);
        try {
            return $this->_db->fetchAll($select);
        } catch( Exception $e  ) {
            Zend_Registry::get('_LOGGER')
                ->log('File:' . $e->getFile() . ' Line:' . $e->getLine() . ' ' . $e->getMessage(), Zend_Log::CRIT);
            return array();
        }
    }
}