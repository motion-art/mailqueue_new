<?php

class Unister_Classes_Model_MailStats_MailMemberFromStats
{
	public function __construct(){
		/**
		 * instance DB Object
		 */
		$this->db = Zend_Registry::get('_DB');
	}
	
	
	public function addStats(&$MailTypeObject){
		$sql = 	"
					INSERT INTO
						tracker_memberfrom_sending
						(Portal,MemberFrom_Id,Count,Date)
						VALUES
						('".$MailTypeObject->portal."','".$MailTypeObject->memberfrom_id."',1,NOW())
						ON DUPLICATE KEY UPDATE
						Count = Count+1
				";
		try {
				$this->db->query($sql);
				return true;
			} catch( Exception $e  ) {
				Zend_Registry::get('_LOGGER')->log('File:'.$e->getFile().' Line:'.$e->getLine().' '.$e->getMessage().$sql, Zend_Log::CRIT);
			}
		return false;
	}
}
