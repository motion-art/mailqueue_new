<?php

class Unister_Classes_Model_MailStats_MailSendStats
{
	
	public function __construct(){
		/**
		 * instance DB Object
		 */
		$this->db = Zend_Registry::get('_DB');
	}
	
	
	public function addStats(&$MailTypeObject){
		$sql = 	"
					INSERT INTO
						tracker_send_mail
						(Portal,Category,Type,Count,Date)
						VALUES
						('".$MailTypeObject->portal."','".$MailTypeObject->category."','".$MailTypeObject->type."',1,NOW())
						ON DUPLICATE KEY UPDATE
						Count = Count+1
				";
		try {
				$this->db->query($sql);
				return true;
			} catch( Exception $e  ) {
				Zend_Registry::get('_LOGGER')->log('File:'.$e->getFile().' Line:'.$e->getLine().' '.$e->getMessage().$sql, Zend_Log::CRIT);
			}
		return false;
	}
}
