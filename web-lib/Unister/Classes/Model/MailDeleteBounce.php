<?php

class Unister_Classes_Model_MailDeleteBounce
{
    /**
     * @var Zend_Db_Adapter_Pdo_Mysql
     */
    protected $_db = null;

    public function __construct()
    {
        $this->_db = Zend_Registry::get('_DB');
    }

    public function fetchByPortal($portal, $date)
    {
        $select = new Zend_Db_Select($this->_db);
        $select->from(array('mdb' => 'mail_delete_bounce'), array('Email' => 'email', 'insertTime', 'counts' => 'count(`Email`)'))
               ->where('DATE(`insertTime`) = ?', $date)
               ->where('Portal = ?', $portal)
               ->group('Email')
               ->order('counts DESC');
        try {
            return $this->_db->fetchAll($select);
        } catch( Exception $e  ) {
            Zend_Registry::get('_LOGGER')
                ->log('File:' . $e->getFile() . ' Line:' . $e->getLine() . ' ' . $e->getMessage(), Zend_Log::CRIT);
            return array();
        }
    }

    public function getAdapter()
    {
        return $this->_db;
    }

}
