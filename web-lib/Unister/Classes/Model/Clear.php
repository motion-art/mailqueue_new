<?php

class Unister_Classes_Model_Clear
{
    const TABLE_ATTACHMENTS = 'attachments';

    protected $_db = null;

    public function __construct()
    {
        $this->_db = Zend_Registry::get('_DB');
    }

    public function fetchAttachments($limit = 1000)
    {
        $query = 'SELECT `AttachmentId`, `CreationDate` FROM `'
            . self::TABLE_ATTACHMENTS . '` LIMIT ' . (string) $limit;
        try {
            return $this->_db->fetchAll($query);
        } catch( Exception $e  ) {
            Zend_Registry::get('_LOGGER')
                ->log('File:' . $e->getFile() . ' Line:' . $e->getLine() . ' ' . $e->getMessage(), Zend_Log::CRIT);
            return array();
        }
    }

    public function deleteAttachments($ids)
    {
        $query = 'DELETE FROM `attachments_mail_instant` WHERE `Attachment_Id` IN (' . implode(',', $ids) . ')';
        try {
            $this->_db->query($query);
        } catch( Exception $e  ) {
            Zend_Registry::get('_LOGGER')
                ->log('File:' . $e->getFile() . ' Line:' . $e->getLine() . ' ' . $e->getMessage(), Zend_Log::CRIT);
            return false;
        }
        $query = 'DELETE FROM `attachments_mail_compendium` WHERE `Attachment_Id` IN (' . implode(',', $ids) . ')';
        try {
            $this->_db->query($query);
        } catch( Exception $e  ) {
            Zend_Registry::get('_LOGGER')
                ->log('File:' . $e->getFile() . ' Line:' . $e->getLine() . ' ' . $e->getMessage(), Zend_Log::CRIT);
            return false;
        }
        $query = 'DELETE FROM `' . self::TABLE_ATTACHMENTS . '` WHERE `AttachmentId` IN (' . implode(',', $ids) . ')';
        try {
            $this->_db->query($query);
        } catch( Exception $e  ) {
            Zend_Registry::get('_LOGGER')
                ->log('File:' . $e->getFile() . ' Line:' . $e->getLine() . ' ' . $e->getMessage(), Zend_Log::CRIT);
            return false;
        }
        return true;
    }

    public function clearMailDeleteBounceTable()
    {
        try {
            $n = $this->_db->delete('mail_delete_bounce', 'UNIX_TIMESTAMP(`insertTime`) < ' . (time() - (86400 * 1)));
            Zend_Registry::get('_LOGGER')
                ->debug('clearMailDeleteBounceTable delete ' . $n . ' mails...');
        } catch( Exception $e  ) {
            Zend_Registry::get('_LOGGER')
                ->log('File:' . $e->getFile() . ' Line:' . $e->getLine() . ' ' . $e->getMessage(), Zend_Log::CRIT);
            return false;
        }
        return true;
    }
}
