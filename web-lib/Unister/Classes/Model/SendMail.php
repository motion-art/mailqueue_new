<?php

class Unister_Classes_Model_SendMail
{
	/**
	 * DB object
	 *
	 * @var object
	 */
	var $db;

	public function __construct(){
		/**
		 * instance DB Object
		 */
		$this->db = Zend_Registry::get('_DB');
	}

	/**
	 * fetch maildatas
	 *
	 * @param integer $mod
	 * @param array $deniedPortal
	 * @return array
	 */
	public function fetchMailsForCron(&$mod,&$deniedPortal){
		if(count($deniedPortal)>0){
			$_where_instant = "AND mail_instant.Portal NOT IN ('".implode("','",$deniedPortal)."')";
			$_where_compendium = "AND mail_compendium.Portal NOT IN ('".implode("','",$deniedPortal)."')";
		} else {
			$_where_instant = "";
			$_where_compendium = "";
		}
		try {
		        $select = "SELECT
										uni.MailId,
										uni.MemberTo_Id,
										uni.MemberFrom_Id,
										uni.Category,
										uni.Type,
										uni.Subject,
										uni.Text,
										uni.Html,
										uni.Email,
										uni.Try,
										uni.FromName,
										uni.FromEmail,
										uni.ReplyTo,
										uni.CC,
										uni.BCC,
										uni.mod10,
										uni.Attachment,
										uni.prio,
										uni.Portal,
										uni.mailtyp,
										uni.Charset,
										count(mail_blacklist_address.Id) AS BlackAddress,
										count(mail_blacklist_domains.Id) AS BlackDomain,
										uni.inserted
									FROM
										(
											SELECT
												`MailId`,
												`MemberTo_Id`,
												`MemberFrom_Id`,
												`Category`,
												`Type`,
												`Subject`,
												`Text`,
												`Html`,
												`Email`,
												`Try`,
												`FromName`,
												`FromEmail`,
												`ReplyTo`,
												`CC`,
												`BCC`,
												`Attachment`,
												`mod10`,
												`prio`,
												`Portal`,
												`Charset`,
												'instant' AS mailtyp,
												inserted
										FROM
											`mail_instant`
										WHERE
									        `mod10` = ".$mod." AND `try` < 100 AND `inProcess` = 0
											$_where_instant
										LIMIT 0,10
										 FOR UPDATE

										UNION ALL

										SELECT
											`MailId`,
											`MemberTo_Id`,
											`MemberFrom_Id`,
											`Category`,
											`Type`,
											`Subject`,
											`Text`,
											`Html`,
											`Email`,
											`Try`,
											`FromName`,
											`FromEmail`,
											`ReplyTo`,
											`CC`,
											`BCC`,
											`Attachment`,
											`mod10`,
											0 AS `prio`,
											`Portal`,
											`Charset`,
											'compendium' AS mailtyp,
											inserted
										FROM
											`mail_compendium`
										WHERE
									        `mod10` = ".$mod." AND `try` < 100 AND `inProcess` = 0
											$_where_compendium
										LIMIT 0,10
										 FOR UPDATE
									) 	AS `uni`
									LEFT JOIN
										mail_blacklist_address
									ON
										uni.Email = mail_blacklist_address.Email
										AND
										uni.Portal = mail_blacklist_address.Portal
									LEFT JOIN
										mail_blacklist_domains
									ON
										SUBSTRING(uni.Email,LOCATE('@',uni.Email)+1) = mail_blacklist_domains.Domain AND uni.Portal = mail_blacklist_domains.Portal
									GROUP BY
										uni.MailId
									LIMIT 0,10";

		            $this->db->beginTransaction();
		            try {
		                $mails = $this->db->fetchAll($select);
		                $mailInstantIds = array();
		                $mailCompendiumIds = array();
		                foreach ($mails as $mail) {
		                    if ($mail['mailtyp'] == 'instant') {
		                        $mailInstantIds[] = $mail['mailid'];
		                    } else if ($mail['mailtyp'] == 'compendium') {
		                        $mailCompendiumIds[] = $mail['mailid'];
		                    }
		                }
		                if (!empty($mailInstantIds)) {
		                    $where = $this->db->quoteInto('MailId IN (?)', $mailInstantIds);
		                    $this->db->update('mail_instant', array('inProcess' => 1), $where);
		                }

		                if (!empty($mailCompendiumIds)) {
		                    $where = $this->db->quoteInto('MailId IN (?)', $mailCompendiumIds);
		                    $this->db->update('mail_compendium', array('inProcess' => 1), $where);
		                }
		                $this->db->commit();
		            } catch (Exception $e) {
		                $this->db->rollBack();
		                throw $e;
		            }

					return $mails;
	        }
	        catch( Exception $e  ) {
	        	Zend_Registry::get('_LOGGER')->log('File:'.$e->getFile().' Line:'.$e->getLine().' '.$e->getMessage().$sql, Zend_Log::CRIT);
	        	return false;
	        }

	}

	public function getMailAttachments(&$mailid,&$mailtype){
		$table = "attachments_mail_".$mailtype;
		$select = $this->db->select()
	             ->from(array('m' => $table),
                    array())
             ->join(array('a' => 'attachments'),
                    'm.Attachment_Id = a.AttachmentId',
			array('AttachmentContent', 'AttachmentType', 'AttachmentFile'))
		->where('m.Mail_Id = ?', $mailid);

		try {
			return $this->db->fetchAll($select);
	        }
       	 catch( Exception $e  ) {
	        	Zend_Registry::get('_LOGGER')->log('File:'.$e->getFile().' Line:'.$e->getLine().' '.$e->getMessage().var_dump($select), Zend_Log::CRIT);
       	 }
	    return false;
	}

	/**
	 * fetch possible mail attachments
	 *
	 * @param integer $mailid
	 * @param string $mailtype
	 * @return array
	 */
	public function getMailAttachments_old(&$mailid,&$mailtype){
		$table = "attachments_mail_".$mailtype;
		$sql = 	"
					SELECT
						attachments.AttachmentContent,
						attachments.AttachmentType,
						attachments.AttachmentFile
					FROM
						$table
					INNER JOIN
						attachments
					ON
						$table.Attachment_Id = attachments.AttachmentId
					WHERE
						$table.Mail_Id = $mailid";
		try {
			return $this->db->fetchAll($sql);
        }
        catch( Exception $e  ) {
        	Zend_Registry::get('_LOGGER')->log('File:'.$e->getFile().' Line:'.$e->getLine().' '.$e->getMessage().$sql, Zend_Log::CRIT);
        }
	    return false;
	}

	/**
	 * do jobs after sending mails by cronjob
	 *
	 * @param array $result_jobs
	 */
	public function makeAfterSendingJobs(&$result_jobs){
		foreach($result_jobs as $job => $job_value){
			foreach ($job_value as $mail_type => $mail_ids){
				if(is_array($mail_ids) && count($mail_ids)>0){
					$this->$job($mail_type,$mail_ids);
				}
			}
		}
	}

	/**
	 * archive send mails
	 *
	 * @param string $mail_type (table extension mail_[instant] or [compendium])
	 * @param array $mail_ids (mailids)
	 */
	protected function markToArchive(&$mail_type,&$server){
		$mailtable = 'mail_'.$mail_type;
		foreach ($server as $ip=>$mail_ids){
			$sql = "
						INSERT INTO
							tracker_creation_mail_datas
						(Portal,MailType,Category,Type,Server,MemberFrom_Id,Mod10)
						(
							SELECT
								Portal,
								'$mail_type',
								Category,
								Type,
								'$ip',
								MemberFrom_Id,
								Mod10
							FROM
								$mailtable
							WHERE
								MailId IN (".implode(",",$mail_ids).")
						)
					";
			try {
				$this->db->query($sql);
			} catch( Exception $e  ) {
	        	Zend_Registry::get('_LOGGER')->log('File:'.$e->getFile().' Line:'.$e->getLine().' '.$e->getMessage().$sql, Zend_Log::CRIT);
	        }
    	}
        return true;
	}

	/**
	 * archive send mails
	 *
	 * @param string $mail_type (table extension mail_[instant] or [compendium])
	 * @param array $mail_ids (mailids)
	 */
	protected function markToArchiveOld(&$mail_type,&$mail_ids){
		$table = 'mail_'.$mail_type;
		$sql = 	"
					INSERT INTO
						tracker_send_mail
					(Portal,Category,Type,Count,Date)
					(
						SELECT
							Portal,Category,Type,1 AS Count,NOW()
						FROM
							$table
						WHERE
							MailId IN (".implode(",",$mail_ids).")
					)
					ON DUPLICATE KEY UPDATE
					Count = Count+1
				";
		try {
			$this->db->query($sql);
			return true;
        }
        catch( Exception $e  ) {
        	Zend_Registry::get('_LOGGER')->log('File:'.$e->getFile().' Line:'.$e->getLine().' '.$e->getMessage().$sql, Zend_Log::CRIT);
        }
        return false;
	}

	/**
	 * update the try-value of an mail, if it couldn't be send
	 *
	 * @param string $mail_type (table extension mail_[instant] or [compendium])
	 * @param array $mail_ids (mailids)
	 */
	protected function markToTryUpdate(&$mail_type,&$mail_ids){
		$table = 'mail_'.$mail_type;
		$sql = 	"
					UPDATE
						$table
					SET
						Try = Try+1, inProcess = 0
					WHERE
						MailId IN (".implode(",",$mail_ids).")
				";
		try {
			$this->db->query($sql);
			return true;
        }
        catch( Exception $e  ) {
        	Zend_Registry::get('_LOGGER')->log('File:'.$e->getFile().' Line:'.$e->getLine().' '.$e->getMessage().$sql, Zend_Log::CRIT);
        }
        return false;
	}

	/**
	 * copy a mail into error_check table
	 *
	 * @param string $mail_type (table extension mail_[instant] or [compendium])
	 * @param array $mail_ids (mailids)
	 */
	protected function markToErrorCheck(&$mail_type,&$mail_ids){
		$table = 'mail_'.$mail_type;
		/*
		$sql_copy = "
						INSERT INTO
							mail_error_correction
						(
							MailId,
							MemberTo_Id,
							MemberFrom_Id,
							Category,
							Type,
							Subject,
							Text,
							Html,
							Email,
							Try,
							FromName,
							FromEmail,
							ReplyTo,
							CC,
							BCC,
							Attachment,
							Mod10,
							Prio,
							Charset,
							Portal,
							MailType
						)
						(
							SELECT
								MailId,
								MemberTo_Id,
								MemberFrom_Id,
								Category,
								Type,
								Subject,
								Text,
								Html,
								Email,
								Try,
								FromName,
								FromEmail,
								ReplyTo,
								CC,
								BCC,
								Attachment,
								Mod10,
								Prio,
								Charset,
								Portal,
								'$mail_type' MailType
							FROM
								$table
							WHERE
								MailId IN (".implode(",",$mail_ids).")
						)
					";


		$sql_delete = 	"
							DELETE FROM
								$table
							WHERE
								MailId IN (".implode(",",$mail_ids).")
						";
		*/
		$sql_copy = "
			UPDATE $table SET mod10 = 66 WHERE MailId IN (".implode(",",$mail_ids).")
		";


		try {
			$this->db->beginTransaction();
			$this->db->query($sql_copy);
			#$this->db->query($sql_delete);
			$this->db->commit();
			return true;
        }
        catch( Exception $e  ) {
        	$this->db->rollBack();
        	print_r("<pre>");
        	 	print_r($e);
        	 	print_r("</pre>");
        	 	exit;
        	Zend_Registry::get('_LOGGER')->log('File:'.$e->getFile().' Line:'.$e->getLine().' '.$e->getMessage().$sql, Zend_Log::CRIT);
        }
        return false;
	}

    /**
     * insert an invalid mail in blacklist table
     *
     * @param string $mail_type (table extension mail_[instant] or [compendium])
     * @param array $mail_ids (mailids)
     */
    protected function markToBlacklistInsert(&$mail_type,&$mail_ids){
        $table = 'mail_'.$mail_type;
        $sql = "
            INSERT IGNORE INTO
                mail_blacklist_address
                (Email,Portal)
                (
                    SELECT
                        Email,Portal
                    FROM
                        $table
                    WHERE
                        MailId IN (".implode(",",$mail_ids).")
                )
        ";
        try {
           $this->db->query($sql);
           return true;
        }catch( Exception $e  ) {
            Zend_Registry::get('_LOGGER')->log('File:'.$e->getFile().' Line:'.$e->getLine().' '.$e->getMessage().$sql, Zend_Log::CRIT);
        }
        return false;
    }

    /**
     * delete mails out of mailqueue
     *
     * @param string $mail_type (table extension mail_[instant] or [compendium])
     * @param array $mail_ids (mailids)
     */
    protected function markToDelete(&$mail_type,&$mail_ids){
        $table = 'mail_' . $mail_type;
        $sql = 'DELETE FROM ' . $table . ' WHERE MailId IN (' . implode(',', $mail_ids) . ')';
        try {
            $this->db->query($sql);
            return true;
        }catch( Exception $e  ) {
            Zend_Registry::get('_LOGGER')->log('File:'.$e->getFile().' Line:'.$e->getLine().' '.$e->getMessage().$sql, Zend_Log::CRIT);
        }
        return false;
    }

    /**
     * delete mails out of mailqueue
     *
     * @param string $mail_type (table extension mail_[instant] or [compendium])
     * @param array $mail_ids (mailids)
     */
    protected function markToBounceDelete(&$mailType, &$mailIds)
    {
        $sql = "INSERT IGNORE INTO `mail_delete_bounce` SELECT 'MailId','MemberTo_Id','MemberFrom_Id','Category','Type','Subject','Text','Html','Email','Try','FromName','FromEmail','ReplyTo','CC','BCC','Attachment','Mod10','Prio','Charset','Portal', NOW() FROM mail_" . $mailType . ' WHERE MailId IN ('
            . implode(',', $mailIds) . ')';
        try {
            $this->db->query($sql);
            return $this->markToDelete($mailType, $mailIds);
        }catch( Exception $e  ) {
            Zend_Registry::get('_LOGGER')->log('File:'.$e->getFile().' Line:'.$e->getLine().' '.$e->getMessage().$sql, Zend_Log::CRIT);
        }
        return false;
	}
}
