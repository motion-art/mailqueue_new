<?php

class Unister_Classes_Model_MailErrorCorrection extends model_SendMail
{
    const DEBUG_REPAIR_N_DELETE = true;

    /**
     * fetch maildatas
     *
     * @param integer $mod
     * @param array $deniedPortal
     * @return array
     */
    public function fetchMailsForCron(&$mod, &$deniedPortal)
    {
        try {
             $tmp = $this->db->fetchAll('
                   SELECT
                    `MailId`,
                    `MemberTo_Id`,
                    `MemberFrom_Id`,
                    `Category`,
                    `Type`,
                    `Subject`,
                    `Text`,
                    `Html`,
                    `Email`,
                    `Try`,
                    `FromName`,
                    `FromEmail`,
                    `ReplyTo`,
                    `CC`,
                    `BCC`,
                    `Attachment`,
                    `mod10`,
                    `prio`,
                    `Portal`,
                    `Charset`,
                    \'instant\' AS MailType
                FROM
                    `mail_instant`
                WHERE
                    mod10 = 66
                LIMIT 0,100
                UNION ALL
                SELECT
                    `MailId`,
                    `MemberTo_Id`,
                    `MemberFrom_Id`,
                    `Category`,
                    `Type`,
                    `Subject`,
                    `Text`,
                    `Html`,
                    `Email`,
                    `Try`,
                    `FromName`,
                    `FromEmail`,
                    `ReplyTo`,
                    `CC`,
                    `BCC`,
                    `Attachment`,
                    `mod10`,
                    `prio`,
                    `Portal`,
                    `Charset`,
                    \'compendium\' AS MailType
                FROM
                    `mail_compendium`
                WHERE
                    mod10 = 66
                LIMIT 0,100'
            );
            return $tmp;
        } catch( Exception $e  ) {
            $msg = 'File:' . $e->getFile() . ' Line:' . $e->getLine() . ' ' . $e->getMessage() . $sql;
            Zend_Registry::get('_LOGGER')->log($msg, Zend_Log::CRIT);
        }
        return array();
    }


    public function doCorrectionJobs(&$correctionObject)
    {
        if (is_array($correctionObject->emailUpdates['markToDelete'])
            && count($correctionObject->emailUpdates['markToDelete']) > 0
        ) {
            foreach ($correctionObject->emailUpdates['markToDelete'] as $mailtype => $mailids) {
                if (is_array($mailids) && count($mailids) > 0
                    && ($mailtype == 'instant' || $mailtype == 'compendium')
                ) {
                    if (self::DEBUG_REPAIR_N_DELETE) {
                        $sqls[] = 'INSERT IGNORE INTO `mail_delete` SELECT *, NOW() FROM mail_' . $mailtype . ' WHERE MailId IN ('
                            . implode(',', $mailids) . ')';
                        $sqls[] = 'UPDATE `mail_delete` SET `Category`=\'(delete)\' WHERE MailId IN ('
                            . implode(',', $mailids) . ')';
                    }
                    $sqls[] = 'DELETE FROM mail_' . $mailtype . ' WHERE MailId IN (' . implode(',', $mailids) . ')';
                }
            }
        }
        if (is_array($correctionObject->emailUpdates['markToUpdate'])
            && count($correctionObject->emailUpdates['markToUpdate']) > 0
        ) {
            foreach ($correctionObject->emailUpdates['markToUpdate'] as $mailtype => $mailids) {
                if (is_array($mailids) && count($mailids) > 0
                    && ($mailtype == 'instant' || $mailtype == 'compendium')
                ) {
                    foreach ($mailids as $mailid => $updates) {
                        $foundkey = array_search($mailid, $correctionObject->emailUpdates['markToResend'][$mailtype]);
                        if ($foundkey !== false) {
                            $updates[] = 'mod10 = ' . rand(0, 9);
                            $updates[] = 'try = 0';
                            unset($correctionObject->emailUpdates['markToResend'][$mailtype][$foundkey]);
                        }
                        if (self::DEBUG_REPAIR_N_DELETE) {
                            $sqls[] = 'INSERT IGNORE INTO `mail_delete` SELECT *, NOW() FROM mail_'
                                . $mailtype . ' WHERE MailId = ' . $mailid;
                            $sqls[] = 'UPDATE `mail_delete` SET `Category` = \'(repair)\' WHERE MailId = ' . $mailid;
                        }
                        $sqls[] = 'UPDATE mail_' . $mailtype . ' SET ' . implode(',', $updates) . ' WHERE MailId = ' . $mailid;
                    }
                }
            }
        }
        if (is_array($correctionObject->emailUpdates['markToResend'])
            && count($correctionObject->emailUpdates['markToResend']) > 0
        ) {
            foreach ($correctionObject->emailUpdates['markToResend'] as $mailtype=>$mailids) {
                if (is_array($mailids)
                    && count($mailids) > 0
                    && ($mailtype == 'instant' || $mailtype == 'compendium')
                ) {
                    foreach ($mailids as $mailid) {
                        $sqls[] = 'UPDATE mail_' . $mailtype . ' SET mod10=' . rand(0, 9) . ' WHERE MailId=' . $mailid;
                    }
                }
            }
        }
        if (is_array($correctionObject->emailLogs) && count($correctionObject->emailLogs) > 0) {
            foreach ($correctionObject->emailLogs as $logdatas) {
                $sqls[] = '
                    INSERT INTO
                        tracker_error_mail
                    (Portal,Category,Type,Email,Server,Deleted,Date)
                    VALUES
                    (\''
                        . $logdatas['Portal'] . '\',\''
                        . $logdatas['Category'] . '\', \''
                        . $logdatas['Type'] . '\', '
                        . $logdatas['Email'] . ', '
                        . $logdatas['Server'] . ', '
                        . $logdatas['Deleted'] . ', NOW()
                    )
                    ON DUPLICATE KEY UPDATE
                    Email = Email + ' . $logdatas['Email'] . ',
                    Server = Server + ' . $logdatas['Server'] . ',
                    Deleted = Deleted + ' . $logdatas['Deleted'];
            }
        }
        try {
            $this->db->beginTransaction();
            foreach ($sqls as $sql) {
                $this->db->query($sql);
            }
            $this->db->commit();
            return true;
        } catch( Exception $e) {
            $this->db->rollBack();
            $msg = 'File:' . $e->getFile() . ' Line:' . $e->getLine() . ' ' . $e->getMessage() . $sql;
            Zend_Registry::get('_LOGGER')->log($msg, Zend_Log::CRIT);
        }
        return array();
    }
}
